from defs import *
from currents import *
# from mapping import save
load_data(elevation, fr"{basefile}\elevation.json")
load_data(wind, fr"{basefile}\wind.json")
load_data(wind_land, fr"{basefile}\wind_land.json")
load_data(pressure, basefile + r'\pressure.json')
# load_data(currents, basefile + r'\currents.json')
load_data(currentsTemp, basefile + r'\currents_temp.json')
load_data(influences, basefile + r'\influences.json')


def influences_define(season='Jul', dt=0):

    sea_level = seaLevelGet()
    ocean = oceanGet(seaLevel=sea_level)
    influences[season].clear()
    for h in hexdict:
        if not is_coastal(h, ocean):
            continue
        for n in all_neighbors[h]:
            if n not in ocean:
                continue
            if getTarget(n, wind[season][n]) in ocean:
                continue
            if currentsTemp[season][n] > currentSST(n, season) + dt:
                if (season == 'Jul' and latitude(h) < -23) or (season == 'Jan' and latitude(h) > 23):
                    influences[season][h] = 'hot'
                    break
            elif currentsTemp[season][n] < currentSST(n, season) - dt:
                influences[season][h] = 'cold'
                break
    for h in list(influences[season]):
        if not any(influences[season].get(n, None) == influences[season][h] for n in all_neighbors[h]):
            influences[season].pop(h, None)

    flowsto = {h: {n for n in all_neighbors[h] if abs(wind_land[season][h][1] - hexAngle(h, n)) % 360 >= 90 and n not in ocean} for h in wind_land[season] if h not in ocean}
    i = 1
    visited = set()
    while len(flowsto) > len(visited) != i:
        i = len(visited)
        for source in flowsto:
            if source not in influences[season]:
                continue
            if source in visited:
                continue
            if distToCoast(source, ocean) > 5:
                continue
            if all(target in visited for target in flowsto[source]):
                visited.add(source)
                continue
            for target in flowsto[source]:
                if target in visited:
                    continue
                influences[season][target] = influences[season][source]
            visited.add(source)
        print(len(flowsto), len(visited))

    for h in hexdict:
        if h in ocean:
            continue
        if h in influences[season]:
            continue
        if (season == 'Jul' and latitude(h) >= 0) or (season == 'Jan' and latitude(h) <= 0):
            if pressure[season][h] > -0.25:
                continue
        if (season == 'Jul' and latitude(h) <= 0) or (season == 'Jan' and latitude(h) >= 0):
            if pressure[season][h] < 0.25:
                continue
        d = distToCoast(h, ocean)
        if d >= 45:
            influences[season][h] = 'cont+'
        elif d >= 15:
            influences[season][h] = 'cont'

    save_data(influences, basefile + r'\influences.json')


def influences_png(season='Jul', dt=5, dh=5):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in influences[season] if hexdict[h][0][1] >= hexdict['207-518W'][0][1]], key=lambda h: wind[season].get(h, 0))
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    # max_c = max(current_sst(h, season) for h in t)
    col = lambda h: {'hot': 'ff0000', 'mild': 'ffffff', 'cold': '0000ff', 'cont': 'ffff00', 'cont+': 'cccc00'}[influences[season][h]]
    c = np.array([colors.hex2color(f"#{col(h)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Influences\{season}.png", dpi=150, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()