from defs import *

load_data(wind_land, fr"{basefile}\wind_land.json")
load_data(elevation, fr"{basefile}\elevation.json")
load_data(climate, fr"{basefile}\climate.json")
load_data(rain, fr"{basefile}\rain.json")


def rain_baseline(season, base=255/2, sea_level=None, ocean=None):

    if not sea_level:
        sea_level = seaLevelGet()
    if not ocean:
        ocean = oceanGet(seaLevel=sea_level)
    rain[season].clear()
    sigma = 19.75
    s = 15 if season == 'Jul' else -15
    rain[season] = {h: 0 for h in hexdict if is_coastal(h, ocean)}
    for h in rain[season]:
        x = latitude(h)
        rain[season][h] = int(1 * base * (
            math.exp(-(x - s) ** 2 / sigma ** 2) +
            0.5 * math.exp(-(x - 45 - s) ** 2 / sigma ** 2) +
            0.5 * math.exp(-(x + 45 + s) ** 2 / sigma ** 2)
        ) * (90 - x) * (90 + x) / 90 ** 2)

    save_data(rain, fr"{basefile}\rain.json")


def rain_blow(season, reset=True, verbose=True, r=0.946, k=2, base=255/2, blur=False, sea_level=None, ocean=None):

    rain_baseline(season=season, base=base)
    if not sea_level:
        sea_level = seaLevelGet()
    if not ocean:
        ocean = oceanGet(seaLevel=sea_level)
    available = {h: rain[season].get(h, 0) / 255 for h in wind_land[season]}
    flowsto = {h: {n for n in all_neighbors[h] if abs(wind_land[season][h][1] - hexAngle(h, n)) % 360 >= 90 and n not in ocean} for h in wind_land[season] if h not in ocean}
    i = 1
    visited = set()
    while len(flowsto) > len(visited) != i:
        i = len(visited)
        for source in flowsto:
            if rain[season].get(source, 0) == 0:
                continue
            if available.get(source, 0) < 0.005:
                continue
            if source in visited:
                continue
            if all(target in visited for target in flowsto[source]):
                for target in [n for n in all_neighbors[source] if n not in visited]:
                    dh = elevation[target]['altitude'] - elevation[source]['altitude']
                    j = abs(dh) / 25599 / 5 * r
                    a = max(0, available[source] - j * (k if dh > 0 else 1))
                    p = int(255 * (a + (k * j if dh < 0 else j))) * (a > 0)
                    available[target] = a if available.get(target, 0) == 0 else statistics.mean([
                        available.get(target, 0),
                        a, a
                    ])
                    rain[season][target] = int(p if rain[season].get(target, 0) == 0 else statistics.mean([rain[season].get(target, 0), p]))
                visited.add(source)
                continue
            for target in flowsto[source]:
                dh = elevation[target]['altitude'] - elevation[source]['altitude']
                j = abs(dh) / 25599 / 5 * r
                a = max(0, available[source] - j * (k if dh > 0 else 1))
                p = int(255 * (a + (k * j if dh < 0 else j))) * (a > 0)
                available[target] = a if available.get(target, 0) == 0 else statistics.mean([
                    available.get(target, 0),
                    a, a
                ])
                rain[season][target] = int(p if rain[season].get(target, 0) == 0 else statistics.mean([rain[season].get(target, 0), p]))
            visited.add(source)
        print(len(flowsto) - len(visited))

    if blur: box_blur(rain[season], g=3, r=2)

    save_data(rain, fr"{basefile}\rain.json")

    # year = range(1, 13)
    # for h in hexes:
    #     it = 4 if climate[h]['temperature']['Jan'] == min(climate[h]['temperature'][s] for s in climate[h]['temperature']) else 10
    #     ip = 4 if climate[h]['precipitation']['Jan'] == min(
    #         climate[h]['precipitation'][s] for s in climate[h]['precipitation']) else 10
    #     temps = [round((climate[h]['temperature'][s] - 32) * 5 / 9, 2) for s in climate[h]['temperature']]
    #     temps_year = [round(avg(temps) + (max(temps) - min(temps)) / 2 * sin(2 * math.pi/ 12 * (m - it)), 2) for m in year]
    #     rain = [climate[h]['precipitation'][s] for s in climate[h]['precipitation']]
    #     rain_year = [round(avg(rain) + (max(rain) - min(rain)) / 2 * sin(2 * math.pi/ 12 * (m - ip)), 2) if
    #                  temps_year[m - 1] > 0 else 0 for m in year]
    #     climate[h]['precipitation_annual'] = round(sum(rain_year), 2)


def rain_png(season='Jul'):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in rain[season] if hexdict[h][0][1] >= hexdict['207-518W'][0][1]],
               key=lambda h: rain[season][h])
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    max_c = max(rain[season][h] for h in t)
    min_c = min(rain[season][h] for h in t)
    c = np.array([colors.hex2color(f"#{colorTemp(rain[season][h], k=(min_c, max_c), col0='ffffff', col1='67a9cf', col2='0000ff')}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Rain\{season}.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()