import noise
import pynverse
import scipy.stats
import warnings

from defs import *

load_data(tectonics, basefile + r'\tectonics.json')
load_data(elevation, basefile + r'\elevation.json')
# load_data(all_neighbors, basefile + r'\all_neighbors.p')
load_data(plates, basefile + r'\plates.p')
load_data(hexPlates, basefile + r'\hexPlates.json')
load_data(plateTypes, basefile + r'\plateTypes.json')
load_data(faults, basefile + r'\faults.p')
load_data(plateInfluences, basefile + r'\plateInfluences.p')
load_data(climate, basefile + r'\climate.p')


def altitude_gen(h, less_noise=0):

    x = min(terrain[h]['fault'], key=lambda i: i[0])
    d = hexes[h]['dist_to_coast']
    s = (0.1, 0.95) if less_noise else (0.3, 0.85)
    c = (0.25, 0.25) if less_noise else (0.999, 0.001)
    # dist = (exp(-x / 500) * (1 + 0.1 * cos(2 * math.pi/ random.randint(100, 1000) * x)) + 0.1) / 1.2
    dist = 1.23 ** (-x[1] / 86) * (1 - x[0] / 2400)
    dist *= -1 if x == terrain[h]['fault'][2] else 1
    # dist *= c[0] * (1 - math.exp(-d / random.randint(5, 20))) + c[1]
    dist *= s[0] * noise.pnoise2(
        hexes[h]['c'][0], hexes[h]['c'][1], octaves=1 if less_noise else 6, repeatx=63000, repeaty=63000, base=0
    ) + s[1]
    return dist


def dhErosionAll(drainage=True, beta=0.5, verbose=False, seaLevel=0):

    dt = 7.889e12 # TODO redo drainage as we go
    elevationBackup = {h: {'altitude': elevation[h]['altitude']} for h in elevation}
    maxE = 0
    for h in sorted(elevation, key=lambda x: elevation[x]['drainage'], reverse=True):
        if not elevation[h]['drain_out']:
            continue
        if elevationBackup[h]['altitude'] <= seaLevel:
            continue
        s = elevationBackup[h]['altitude'] - elevationBackup[elevation[h]['drain_out'][0]]['altitude']
        A = elevation[h]['drainage']
        zE = dh_erosion(A, s)
        maxZH = max([elevationBackup[h]['altitude'] - (elevationBackup[n]['altitude'] + 1) for n in neighbors(h)])
        zE = clip(int(round(min(zE, maxZH))), 0, None)
        maxE = max(maxE, zE)
        Qs = beta * zE * 347 / dt * 2.788e7
        elevation[elevation[h]['drain_out'][0]].setdefault('sediment_load', dict())
        elevation[elevation[h]['drain_out'][0]]['sediment_load'].setdefault(h, 0)
        elevation[elevation[h]['drain_out'][0]]['sediment_load'][h] += round(Qs, 5)
        elevation[h]['altitude'] -= zE
        # elevation[h]['altitude'] = clip(elevation[h]['altitude'], sea_level + 1, None)
    if verbose:
        print(f"zE: {maxE}")


def dhUpliftAll(verbose=False, alpha=1.6, seaLevel=0):

    elevationBackup = {h: {'altitude': elevation[h]['altitude']} for h in elevation}
    maxH = max([elevation[h]['altitude'] for h in elevation])
    maxU = 0
    for h in elevation:
        u = tectonics[h]['uplift']
        zU = dhUplift(max(0, u) ** alpha)
        elevation[h]['altitude'] += clip(int(round(zU)), 0, None)
        maxU = max(maxU, zU)
        maxZ = int(25599 + seaLevel)
        elevation[h]['altitude'] = clip(elevation[h]['altitude'], None, maxZ)
        # dQ = sedimentDH(elevation[h]['altitude'] - elevationBackup[h]['altitude'])
        # try:
        #     elevation[h][elevation[h]['drain_out'][0]].setdefault('sediment_load', dict())
        #     elevation[h][elevation[h]['drain_out'][0]]['sediment_load'].setdefault(h, 0)
        #     elevation[h][elevation[h]['drain_out'][0]]['sediment_load'][h] += round(dQ, 5)
        # except:
        #     pass
    if verbose:
        print(f"z_u: {maxU}")


def dhSedimentAll(seaLevel=0):

    for h in elevation: # TODO probably too little capacity
        if not elevation[h]['drain_out'] or elevation[h]['altitude'] <= seaLevel:
            S = 0 #elevation[h]['altitude']
        else:
            S = (elevation[h]['altitude'] - elevation[elevation[h]['drain_out'][0]]['altitude'])
        elevation[h]['sediment_capacity'] = round(5e-8 * (elevation[h]['drainage'] * 347) ** 1.5 * S * 9.47e-6 * 4.665e3, 5)
        elevation[h].setdefault('sediment_load', dict())  # cu.ft/s
    return True


def dhDepositionAll(verbose=False, seaLevel=0):

    maxD = 0
    elevationBackup = {h: {'altitude': elevation[h]['altitude']} for h in elevation}
    for h in sorted(elevation, key=lambda x: elevation[x]['drainage'], reverse=False):
        if elevation[h]['altitude'] < seaLevel:
            continue
        for i in sorted(elevation[h]['drain_in'], key=lambda x: elevation[x]['drainage']):
            dQ = elevation[h]['sediment_load'].get(i, 0) - elevation[h]['sediment_capacity']
            if dQ < 0: # capacity is higher than load, no need to drop
                continue
            zD = dh_sediment(dQ) # the max amount of sediment
            maxZH = clip(max([j for j in [elevationBackup[i]['altitude'] - elevationBackup[n]['altitude'] for n in neighbors(i) if n != h] if j > 0] or [elevationBackup[i]['altitude'] - elevationBackup[h]['altitude']]), 0, None) # max height difference
            zD = min(zD, maxZH)
            maxD = max(zD, maxD)
            dQ = sedimentDH(zD)  # sediment based on height
            try:
                elevation[h]['sediment_load'][i] = math.fsum([elevation[h]['sediment_load'][i], -round(dQ, 5)])
            except:
                pass
            elevation[h]['altitude'] += int(round(zD))
    if verbose:
        print(f"zD: {maxD}")
    return True


def pushSediment(h, verbose=False):

    if elevation[h].get('drain_out'):
        s = 0
        for i in elevation[h].get('sediment_load', dict()):
            s = math.fsum([elevation[h]['sediment_load'][i], s])
        elevation[elevation[h]['drain_out'][0]].setdefault('sediment_load', dict())
        elevation[elevation[h]['drain_out'][0]]['sediment_load'].setdefault(h, 0)
        elevation[elevation[h]['drain_out'][0]]['sediment_load'][h] += round(s, 5)
        elevation[h]['sediment_load'].clear()
    return True


def pushSedimentAll(verbose=False, seaLevel=0):

    for h in sorted(elevation, key=lambda x: elevation[x]['altitude']):
        if elevation[h]['altitude'] <= seaLevel:
            continue
        pushSediment(h)
    return True


def dhCoastalAll(verbose=False, seaLevel=0):

    kC = 0.5
    elevationBackup = {h: {'altitude': elevation[h]['altitude']} for h in elevation}
    for h in sorted(elevation, key=lambda x: elevation[x]['altitude']):
        if elevation[h]['altitude'] <= seaLevel:
            continue
        i = sum(elevationBackup[n].get('altitude', 0) <= seaLevel for n in neighbors(h))
        if i:
            s = sum(elevation[h]['sediment_load'].values())
            if s:
                zC = clip(10 * i * kC, 0, None)
                elevation[h]['altitude'] = int(clip(elevation[h]['altitude'] - zC, seaLevel - 10, None))
    return True


def erosionAll(seaLevel=0, verbose=False, uplift=True, alpha=1.6, deposition=True, coastal=False, ocean=None):

    if uplift:
        dhUpliftAll(verbose=verbose, alpha=alpha, seaLevel=seaLevel)  # uplift
        drainageSet(verbose=False, seaLevel=seaLevel, ocean=ocean)

    for _ in range(1):
        dhErosionAll(verbose=verbose, seaLevel=seaLevel)  # erosion
        drainageSet(verbose=False, seaLevel=seaLevel, ocean=ocean)

    dhSedimentAll()  # sediment

    if deposition:
        dhDepositionAll(verbose=verbose, seaLevel=seaLevel)  # deposition
        pushSedimentAll(verbose=verbose, seaLevel=seaLevel)  # deposition
        drainageSet(verbose=False, seaLevel=seaLevel, ocean=ocean)

    if coastal: #coastal
        dhCoastalAll(verbose=verbose, seaLevel=seaLevel)
        drainageSet(verbose=False, seaLevel=seaLevel, ocean=ocean)

    return True


def river_remove_basins(update_map=False, hexes=hexes, target=0.18):
    timer(0)
    altitude_backup = {h: terrain[h]['altitude'] for h in hexes}
    # basins = sum(1 for h in hexes if not terrain[h]['drain_out'] and not hexes[h]['coastal'])
    basins = {h: get_basin(h) for h in hexes if is_endorheic(h)}
    print(basins, int(len(hexes) * target))
    n = int(basins) * 100
    if basins < len(hexes) * target:
        update_map = False
    while basins > len(hexes) * target and abs(n - basins) / basins > 0.001:
        n = int(basins)
        for i in sorted(hexes, key=lambda x: climate[x]['drainage'], reverse=True):
            if not terrain[i]['drain_out'] and not hexes[i]['coastal'] and random.random() > target:
                terrain[i]['altitude'] = min(altitude_backup[n] for n in hexes[i]['neighbors']) + 30
                continue
        with HiddenPrints():
            river_drainage()
        basins = sum(1 for h in hexes if not terrain[h]['drain_out'] and not hexes[h]['coastal'])
        print(basins, int(len(hexes) * target), '{:.4%}'.format(abs(n - basins) / basins))
    if update_map:
        save_new_elevation()


def river_remove_basins2(update_map=False, hexes=hexes, target=0.18):
    timer(0)
    # altitude_backup = {h: terrain[h]['altitude'] for h in hexes}
    # basins = sum(1 for h in hexes if not terrain[h]['drain_out'] and not hexes[h]['coastal'])
    basins = basins_total()
    print(basins[1], int(len(hexes) * target))
    n = int(basins[1]) * 100
    if basins[1] < len(hexes) * target:
        update_map = False
    while basins[1] > len(hexes) * target:  # and abs(n - basins[1]) / basins[1] > 0.001:
        n = int(basins[1])
        for b in sorted(basins[0], key=lambda x: len(get_basin(x)), reverse=True):
            terrain[b]['altitude'] = int(statistics.mean([terrain[m]['altitude'] for m in hexes[b]['neighbors']]) + 30)
        # if abs(n - basins[1]) / basins[1] >= 0.02:
        #     break
        coastal_mask()
        with HiddenPrints():
            river_drainage()
        basins = basins_total()
        print(basins[1], int(len(hexes) * target),
              '{:.4%} {:.4%}'.format(abs(n - basins[1]) / basins[1], len(hexes) * target / basins[1]))
    if update_map:
        save_new_elevation()

    print('{:} s'.format(timer(1)))


def river_remove_basins3(update_map=False, hexes=hexes, target=0.18):
    timer(0)
    # altitude_backup = {h: terrain[h]['altitude'] for h in hexes}
    # basins = sum(1 for h in hexes if not terrain[h]['drain_out'] and not hexes[h]['coastal'])
    basins = basins_total()
    print(basins[1], int(len(hexes) * target))
    n = int(basins[1]) * 100
    if basins[1] < len(hexes) * target:
        update_map = False
    while basins[1] > len(hexes) * target:  # and abs(n - basins[1]) / basins[1] > 0.001:
        n = int(basins[1])
        for b in sorted(basins[0], key=lambda x: hexes[x]['dist_to_coast'], reverse=True):
            terrain[b]['altitude'] = int(max([terrain[m]['altitude'] for m in hexes[b]['neighbors']]) + 30)
        # if abs(n - basins[1]) / basins[1] >= 0.02:
        #     break
        coastal_mask()
        with HiddenPrints():
            river_drainage()
        basins = basins_total()
        print(basins[1], int(len(hexes) * target),
              '{:.4%} {:.4%}'.format(abs(n - basins[1]) / basins[1], len(hexes) * target / basins[1]))
    if update_map:
        save_new_elevation()

    print('{:} s'.format(timer(1)))


def river_canyons(hexes=hexes, update_map=False, target=0.18):
    def a_star(start, end):

        open_list = list(hexes[start]['neighbors'])
        closed_list = [start]

        def travel(i, j):

            return 1 / abs(terrain[j]['altitude'] - terrain[i]['altitude']) if terrain[j]['altitude'] < terrain[i][
                'altitude'] else 1e3

        def g(current, closed_list=closed_list):

            z = zip(closed_list, closed_list[1:] + [current])
            return sum(travel(i, j) for i, j in z)

        def h(current):

            return hexDistance(current, end)

        def f(current, closed_list=closed_list):

            return g(current, closed_list=closed_list) + h(current)

        came_from = {sorted(open_list, key=lambda h: f(h))[0]: start}

        while open_list:
            s = sorted(open_list, key=lambda h: f(h))[0]
            open_list.remove(s)
            closed_list.append(s)
            if s == end:
                # print(closed_list)
                break
            for t in hexes[s]['neighbors']:
                if t not in closed_list:
                    if t not in open_list or g(t) < h(t):
                        open_list.append(t)
                    came_from.update({t: s})
            # print(f(s), closed_list)

        path = [end]
        while path[-1:][0] != start:
            path.append(came_from[path[-1:][0]])
        return path[::-1]

    basins = basins_total()
    print(basins[1], int(len(hexes) * target))
    while basins[1] > int(len(hexes) * target):
        n = basins[1]
        source = sorted(list(basins[0]), key=lambda h: len(basins[0][h]), reverse=True)[0]
        visited = set([source])
        current = set(hexes[source]['neighbors'])
        i = 0
        dest = ''
        while not dest:
            i += 1
            if not len(current):
                break
            for c in list(current):
                if terrain[c]['altitude'] < terrain[source]['altitude'] - 30 * (i + 1):
                    dest = c
                    break
                current.remove(c)
                visited.add(c)
                current |= set([h for h in hexes[c]['neighbors'] if h not in visited])
            # print(i, len(current))
        source = sorted(list(basins[0]), key=lambda h: len(basins[0][h]), reverse=True)[0]
        visited = set([source])
        current = set(hexes[source]['neighbors'])
        i = 0
        if not dest:
            print('Coastal')
        while not dest:  # find nearest coast instead
            for c in list(current):
                if hexes[c]['coastal']:
                    dest = c
                    break
                current.remove(c)
                visited.add(c)
                current |= set([h for h in hexes[c]['neighbors'] if h not in visited])
        for i, h in enumerate(a_star(source, dest)):
            # print(h, terrain[h]['altitude'], terrain[source]['altitude'] - 30 * i)
            terrain[h]['altitude'] = terrain[source]['altitude'] - 30 * i
        coastal_mask()
        with HiddenPrints():
            river_drainage()
        basins = basins_total()
        print(basins[1], int(len(hexes) * target), '{:.4%}'.format(abs(n - basins[1]) / basins[1]))


def erosion_v1(update_file=0, uplift=True):

    timer(0)

    print('Checking for drainage values...')

    for h in elevation:
        try:
            elevation[h]['drainage']
        except AttributeError:
            # river_drainage()
            print('Drainage calculation needed')
            break

    print('Tectonic uplift...')
    # altitude_backup = {h: elevation[h]['altitude'] for h in elevation}
    # max_C = max(terrain[h]['fault'][0] for h in hexes)
    # max_T = max(terrain[h]['fault'][1] for h in hexes)

    for h in sorted(elevation, key=lambda x: climate[x]['drainage'], reverse=True):
        ds = altitude_backup[h] - altitude_backup[terrain[h]['drain_out'][0]] if terrain[h]['drain_out'] else \
        altitude_backup[h]
        if hexes[h]['coastal']:
            ds = altitude_backup[h]
        if uplift:
            try:
                dist = altitude_gen(h, less_noise=1)
            except TypeError:
                print(h)
                raise Exception
        else:
            dist = 0
        change = dh(climate[h]['drainage'], ds, dist=dist)
        if change > 1000:
            print(h, climate[h]['drainage'], ds, altitude_backup[h])
        terrain[h]['altitude'] += change
        terrain[h]['altitude'] = min(25599, max(1, terrain[h]['altitude']))

    if update_file:
        save_new_elevation()

    print('{:} s'.format(timer(1)))

    if split:
        merge_elevation()


def elevation_svg(sea_level=None, highlight=None):

    # timer(0)  # 3379 s
    # lines = str(elevationmap)
    if not highlight:
        highlight = set()
    print('Saving elevation data...')
    string = ''
    if not sea_level:
        sea_level = seaLevelGet()
    for h in sorted(list(elevation) + list(highlight), key=lambda x: elevation[x]['altitude']):
        if elevation[h]['altitude'] <= sea_level:
            if highlight and h not in highlight:
                continue
            else:
                continue
        col = screen('000000', (elevation[h]['altitude'] - sea_level) / 100)
        if highlight and h in highlight:
            col = 'ff0000'
        string += '<path d="M {:} Z" id="{:}" class="hex" style="color:#{:}" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in hexdict[h]]),
            h,
            col
        )
    else:
        with open(basefile + r'\Elevation.svg', 'r') as f:
            file = bs(f, 'lxml')
        file.find('g', id='Elevation').contents = bs(string, 'lxml').find('body').contents
        save(file, extra_file=basefile + r'\Elevation.svg')


def elevation_labels(sea_level=0):
    """
    Deletes all existing elevation labels.
    Adds text to the label SVG for elevation values
    :return:
    """

    with open(basefile + r'\Labels.svg', 'r') as f:
        file = bs(f, 'lxml')

    # style = "font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:4.0000001px;" \
    #         "font-family:'Ubuntu Mono';-inkscape-font-specification:'Ubuntu Mono';text-align:end;letter-spacing:0px;" \
    #         "word-spacing:0px;text-anchor:end;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;" \
    #         "stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
    file.find('g', id='Altitude').clear()
    # for h in elevationmap.find('g', id = 'Elevation').find_all('path'):
    string = ''
    for h in elevation:
        if elevation[h]['altitude'] <= sea_level:
            continue
        # id = latlong(eval(h.get('d').split(' ')[1]), terr = True)
        string += f"<text id='{h}-altitude' x='{hexdict[h][0][0] + 37:.02f}' y='{hexdict[h][0][1] + 50 - 20:.02f}'>{elevation[h]['altitude'] - sea_level:}</text>"
        # z = terrain[h]['altitude']
        # t = labelmap.new_tag('text', id=h + '-altitude-label', x=hexes[h]['x'] + 41, y=hexes[h]['y'] + 24)
        # t.string = str(z)
        # labelmap.find('g', id='Altitude').append(t)
    else:
        file.find('g', id='Altitude').contents = bs(string, 'lxml').find('body').contents
        save(file)


def altitude_point_cloud():
    """
    Creates a xyz file for viewing with any 3D software. Just for fun. Isn't it all for fun?
    :return:
    """

    pointcloudfile = basefile + r'pointcloud.xyz'
    open(pointcloudfile, 'w').close()
    with open(hex_points_file, 'rb') as f:
        hexdict = pickle.load(f)
    with open(pointcloudfile, 'w') as f:
        for h in hexdict:
            if h not in hexes:
                f.write('{:} {:} 0\n'.format(hexdict[h][0][0] * 20, (hexdict[h][0][1] + 50) * 20))
            elif not terrain[h]['altitude']:
                f.write('{:} {:} 0\n'.format(hexdict[h][0][0] * 20, (hexdict[h][0][1] + 50) * 20))
            else:
                f.write(
                    '{:} {:} {:}\n'.format(hexdict[h][0][0] * 20, (hexdict[h][0][1] + 50) * 20, terrain[h]['altitude']))


def altitudeRescale(z=25599, seaLevel=0):

    b = max(elevation[h]['altitude'] for h in elevation)
    y = 1
    for h in elevation:
        c = elevation[h]['altitude']
        if c > seaLevel:
            elevation[h]['altitude'] = int(rescale(c, seaLevel, b, seaLevel, z + seaLevel))


def is_endorheic(h):

    return bool(
        all(elevation[n]['altitude'] > elevation[h]['altitude'] for n in all_neighbors[h]) and
        sum(min(all_neighbors[n], key=lambda i: elevation[i]['altitude']) == h for n in all_neighbors[h]) >= 3
        # not hexes[h]['coastal'] and
        # elevation[h]['drain_in'] and
        # not elevation[h]['drain_out'] and
        # elevation[h]['drainage'] > 3 if type(elevation[h]['drainage']) != list else sum(elevation[h]['drainage'])
    )


def is_exorheic(h, ocean, verbose=False):

    while True:
        if h in ocean:
            return True
        if is_endorheic(h) or not elevation[h]['drain_out']:
            return False
        h = elevation[h]['drain_out'][0]
        if verbose: print(h)


def a_star(start, end, verbose=False):

    open_list = list(hexes[start]['neighbors'])
    if not open_list:
        if verbose: print('No neighbors')
        return None
    closed_list = [start]
    came_from = {o: start for o in open_list}

    def travel(i, j):

        s = elevation[j]['altitude'] - elevation[i]['altitude']
        if s < 0:
            return s
        else:
            return 1 / abs(s)

    def g(current, s=None):

        path = [current]
        while path[-1] != start:
            try:
                path.append(came_from.get(path[-1], s))
            except KeyError:
                pass
        path = path[::-1]

        z = zip(path[:-1], path[1:])
        return round(sum(travel(i, j) for i, j in z), 3)

    def h(current):

        return hexDistance(current, end)

    def f(current):

        return g(current) + h(current)

    while open_list:
        s = sorted(open_list, key=lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        # if road_exists(s, end):
        #     path = [s]
        #     break
        if s == end:
            # print(closed_list)
            path = [end]
            break
        for t in hexes[s]['neighbors']:
            if travel(t, s) > 1e8:
                closed_list.append(t)
                continue
            if t not in closed_list:
                if t not in open_list or g(t, s=s) < h(t):
                    open_list.append(t)
                came_from.update({t: s})
        # print(f(s), closed_list)
        closed_list = list(set(closed_list))
        if verbose: print(f'{f(s):.0f} {g(s):.0f} {h(s):.0f} {len(closed_list)} {len(came_from)}')

    path = [end]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return None
    return path[::-1]


def a_star_star(start, ends, verbose=False):

    open_list = list(all_neighbors[start])
    if not open_list:
        if verbose: print('No neighbors')
        return None
    closed_list = [start]
    came_from = {o: start for o in open_list}

    def travel(i, j):

        s = elevation[j]['altitude'] - elevation[i]['altitude']
        if s < 0:
            return 1 / abs(s)
        else:
            return math.sqrt(s)

    def g(current, s=None):

        path = [current]
        while path[-1] != start:
            try:
                path.append(came_from.get(path[-1], s))
            except KeyError:
                pass
        path = path[::-1]

        z = zip(path[:-1], path[1:])
        return round(sum(travel(i, j) for i, j in z), 3)

    def h(current):

        return min(hexDistance(current, end) * elevation[end]['dist_to_coast'] for end in ends)

    def f(current):

        return g(current) + h(current) ** 2 # dynamic weighting

    while open_list:
        # while len(open_list) > 15: # beam search
        #     open_list = sorted(open_list, key=lambda h: f(h))[15:]
        s = sorted(open_list, key=lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        # if any(s in basins_get_v2(end, elevation) for end in ends):
        #     path = [s]
        #     break
        if s in ends:
            # print(closed_list)
            path = [s]
            break
        for t in all_neighbors[s]:
            if travel(t, s) > 1e8:
                closed_list.append(t)
                continue
            if t not in closed_list:
                if t not in open_list or g(t, s=s) < h(t):
                    open_list.append(t)
                came_from.update({t: s})
        # print(f(s), closed_list)
        closed_list = list(set(closed_list))
        if verbose: print(f'{f(s):.0f} {g(s):.0f} {h(s):.0f} {len(closed_list)} {len(open_list)} {len(came_from)}')

    path = [s]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return None
    return path[::-1]


def b_star_star(start, ends, verbose=False):

    def travel(i, j):

        s = elevation[j]['altitude'] - elevation[i]['altitude']
        if s < 0:
            return 1 / abs(s)
        else:
            return math.sqrt(s)

    def h(current):

        return min(hexDistance(current, end) for end in ends)


def riverProps(i, j):

    props = {'width': 0, 'depth': 0, 'velocity': 0, 'slope': 0}
    if j not in elevation[i]['drain_out']:
        return props
    A = elevation[i]['drainage'] * hexArea
    w = 0.044 * A ** 0.841 # ft
    d = 0.034 * A ** 0.585 # ft
    AR = math.pi * w * d / 2 # sqft
    e = math.sqrt(w ** 2 - (2 * d) ** 2) / w
    PW = sum(math.factorial(2 * i) ** 2 / math.factorial(2 ** i * i) ** 4 * (e ** (2 * i)) / (2 * i - 1) for i in range(1, int(2)))
    PW = w * math.pi * (1 - PW) # ft
    RH = AR / PW
    n = 0.05
    S = (elevation[i]['altitude'] - elevation[j]['altitude']) / 20 / 5280 * 10 # way too low due to low resolution
    v = 1 / n * RH ** (2/3) * S ** (1/2) # ft/s
    props.update({'width': round(w, 0), 'depth': round(d, 0), 'velocity': round(v, 3), 'slope': round(S, 5)})
    return props


def river_width(d, total_drain=1):

    return round(20 * math.sqrt(d / total_drain), 2)


def river(h1, h2, drain=10, s=1, straight=True, total_drain=0, hexes=hexes):

    """

    :param h1: starting hex
    :param h2: ending hex
    :param drain: amount of drainage from the starting hex
    :param s: smoothness - a function of the slope
    :return: append a new riverpath to the rivermap
    """

    if straight:
        river_path = [elevation[h1]['river_anchor'], elevation[h2]['river_anchor']]
        width = river_width(elevation[h1]['drainage'], total_drain=total_drain)
        if width < 0.5:
            return ''
        return '<path d="M {:}" id="{:} to {:}" style="stroke-width:{:.2f}" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in river_path]), h1, h2, width
        )

    # TODO add precipitation amounts to scale the river
    # TODO split each segment into 2 parts

    # s = 1.1
    # riverpath = [
    #     h1.river_anchor,
    #     h2.river_anchor
    # ]
    # f1 = fractalize(riverpath[0][0], riverpath[0][1], riverpath[1][0], riverpath[1][1], s)
    # riverpath.insert(1, f1)
    # f2 = fractalize(riverpath[0][0], riverpath[0][1], riverpath[1][0], riverpath[1][1], s)
    # riverpath.insert(1, f2)
    # f3 = fractalize(riverpath[2][0], riverpath[2][1], riverpath[3][0], riverpath[3][1], s)
    # riverpath.insert(3, f3)

    river_path = []
    L = distance(h1.river_anchor, h2.river_anchor)
    theta = atan2(
        h2.river_anchor[1] - elevation[h1]['river_anchor'][1],
        h2.river_anchor[0] - elevation[h1]['river_anchor'][0]
    )
    # w = radians(random.randint(1, 20) + random.randint(1, 20) + random.randint(1, 20) + random.randint(1, 20) +random.randint(1, 10) +random.randint(1, 10) +random.randint(1, 10) + 10)
    # w = radians(120 * math.exp(-s / 433))
    # w = radians(60)
    # w = 105 * math.exp(-5e-4 * (h1.altitude - h2.altitude))
    try:
        # w = radians(-12 * math.log(h1.altitude - h2.altitude + 0.1, 2) + 100)
        w = radians(100 * math.exp(-(elevation[h1]['altitude'] - elevation[h2]['altitude']) / 145))
    except ValueError:
        print(elevation[h1]['altitude'], elevation[h2]['altitude'])
    A = random.choice([-1, 1])
    n = 1

    def x(t, w=w):
        return scipy.integrate.quad(lambda r: cos(meander_phi(r, w, L)), 0, t)[0]

    def y(t, w=w):
        return scipy.integrate.quad(lambda r: sin(meander_phi(r, w, L)), 0, t)[0]

    def xx(t, w=w, theta=theta, A=A, L=L, n=n):
        return x(t) * L / x(L) * cos(theta) - L * A / 5 / x(L) * y(t) * sin(theta)

    def yy(t, w=w, theta=theta, A=A, L=L, n=n):
        return x(t) * L / x(L) * sin(theta) + L * A / 5 / x(L) * y(t) * cos(theta)

    for t in np.linspace(0, L, 15):
        river_path.append((
            xx(t) + h1.river_anchor[0],
            yy(t) + h1.river_anchor[1]
        ))
    river_path[0] = h1.river_anchor
    river_path[-1] = h2.river_anchor
    # TODO collision detection?

    river_path = 'M ' + '{:},{:}'.format(h1.river_anchor[0], h1.river_anchor[1]) + ' L' + ''.join([
        ' {:.2f},{:.2f}'.format(i[0], i[1]) for i in river_path[1:]
    ])

    # width = 99 * total_drain / (10 * total_drain - 30) * drain / total_drain + (total_drain - 300) / (
    #         10 * total_drain - 30)
    # width = (25 - 1) * math.log(drain) / math.log(total_drain) + 1
    width = - ((12 - 1) * math.log(drain) + math.log(total_drain) - 12 * math.log(3)) / (math.log(3) - math.log(total_drain))
    if width < 2:
        return ''
    return '<path d="{:}" id="{:} to {:} {:.0f}" style="stroke-width:{:.2f}" />'.format(
        river_path, h1.id, h2.id, math.degrees(w), width
    )
    # rivermap.find('g', id='Rivers').append(
    #     rivermap.new_tag('path', d=river_path, id='{:} to {:} {:.0f}'.format(h1.id, h2.id, math.degrees(w)),
    #                      **{'style': 'stroke-width:{:.2f}'.format(width)}))


def rivers_draw(straight=True, total_drain=0, sea_level=0):

    """
    Place rivers on the map based on the existing drainage amounts
    :return:
    """

    timer(0)  # 630 s
    print('Drawing rivers...')

    with open(riverfile, 'r') as f:
        rivermap = bs(f, 'lxml')

    rivermap.find('g', id='Rivers').clear()
    rivermap.find('g', id='NN').clear()
    rivermap.find('g', id='Lakes').clear()
    t = 0
    rivers = ''
    non_navigable = ''
    lakes = ''
    for h in sorted((h for h in elevation if elevation[h]['altitude']), key= lambda x: elevation[x]['drainage'], reverse=True):
        if elevation[h].get('lake', 0):
            lakes += '<circle cx="{:}" cy="{:}" r="{:.3f}" id="{:}-lake"/>'.format(
                elevation[h]['river_anchor'][0],
                elevation[h]['river_anchor'][1],
                2 * river_width(elevation[h]['drainage'], total_drain=total_drain),
                h,
            )
        if not elevation[h]['drain_out']:
            continue
        if elevation[h]['altitude'] < sea_level:
            continue
        # if len(hexes[h]['neighbors']) == 0:
        #     continue
        g = elevation[h]['drain_out'][0]
        if distance(h, g) > 200:
            continue
        # if '207-' in h and abs(parse_id(h)[1]) > 103 and '206S-' in g:
        #     continue
        # if '207-' in g and abs(parse_id(g)[1]) > 103 and '206S-' in h:
        #     continue
        if elevation[h]['drainage'] > 2:
            # s = 3 + 7 / (1 + math.exp(-0.005 * (terrain[h]['altitude'] - g.altitude) - 650))
            # TODO edit the drainage model
            s = abs(elevation[h]['altitude'] - elevation[g]['altitude'])
            if elevation[h]['altitude'] - elevation[g]['altitude'] > 600:
                non_navigable += river(h, g, drain=elevation[h]['drainage'], s=s, straight=straight, total_drain=total_drain)
            else:
                rivers += river(h, g, drain=elevation[h]['drainage'], s=s, straight=straight, total_drain=total_drain)
        t += 1
        # if not t % 1000:
        #     print('{:.0%}, {:} s'.format(t / len(hexes), timer(1)))
            # rivermap.find(id='Rivers').contents = bs(rivers, 'lxml').find('body').contents
            # rivermap.find(id='NN').contents = bs(non_navigable, 'lxml').find('body').contents
            # save(rivermap)
    for r in elevation:
        if elevation[r]['altitude'] < sea_level:
            continue
        if elevation[r]['drain_out']:
            continue
        if not elevation[r]['drain_in']:
            continue
        if elevation[r]['drainage'] < 2:
            continue
        targets = [n for n in all_neighbors[r] if elevation[n]['altitude'] < sea_level]
        if not targets:
            continue
        target = random.choice(targets)
        river_path = [elevation[r]['river_anchor'], (int(hexdict[target][0][0]) +random.randint(-10, 10), int(hexdict[target][0][1]) + 50 +random.randint(-10, 10))]
        if distance(*river_path) > 200:
            continue
        width = river_width(elevation[r]['drainage'], total_drain=total_drain)
        rivers += '<path d="M {:}" id="{:} to {:}" style="stroke-width:{:.2f}" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in river_path]), r, 'sea', width
        )
    else:
        if rivers:
            # rivers_clean = ''
            # for w in set([get_property(r, 'stroke-width') for r in bs(rivers, 'lxml').find_all('path')]):
            #     rivers_clean += '<path d="M {:}" style="stroke-width:{:}" />'.format(
            #         ' '.join([r['d'] for r in bs(rivers, 'lxml').find_all('path') if
            #                   get_property(r, 'stroke-width') == w]), w
            #     )
            rivermap.find(id='Rivers').contents = bs(rivers, 'lxml').find('body').contents
        if non_navigable: rivermap.find(id='NN').contents = bs(non_navigable, 'lxml').find('body').contents
        if lakes: rivermap.find(id='Lakes').contents = bs(lakes, 'lxml').find('body').contents
        save(rivermap)
        print('{:} s'.format(timer(1)))


def basins_total(sea_level=0, flatten=True, drainage=True, ocean=None):

    if not ocean:
        ocean = oceanGet(seaLevel=sea_level)
    with HiddenPrints():
        if drainage: drainageSet(flatten=flatten, ocean=ocean)
    # basins = {h for h in elevation if elevation[h]['altitude'] > sea_level and is_endorheic(h)}
    basins = {h for h in elevation if h not in ocean and is_endorheic(h) and all(elevation[n]['altitude'] > sea_level for n in all_neighbors[h])}
    # basins = {h: basins_get_v2(h) for h in basins}
    total_drainage = sum(elevation[b]['drainage'] for b in basins)
    return basins, total_drainage


def basins_carve_v1(target=0.18, flatten=False, sea_level=0, verbose=False):

    drainageSet()
    basins = basins_total(sea_level=sea_level)
    total_drainage = basins[1]
    basins = sorted([b for b in basins[0]], key=lambda x: elevation[x]['drainage'], reverse=True)
    # endorheics = sum(1 for h in hexes if is_endorheic(h))
    # target *= int(sum(1 for h in hexes if not terrain[h]['drain_in'] and terrain[h]['drain_out'] and climate[h]['drainage'] > 0) * 0.18)
    # target = int(target)
    target = int(sum(elevation[h]['altitude'] > sea_level for h in elevation) * target)
    if verbose: print('{:} basins found, target: {:}'.format(total_drainage, target))

    while total_drainage > target:
        for b in basins:
            if total_drainage < target:
                break
            # b = basins[0]
            if b not in basins:
                continue
            # if terrain[b]['altitude'] < 10:
            #     continue
            if verbose: print('{:} ({:}) to '.format(b, len(basins_get_v2(b, elevation))), end='', flush=True)
            sources = {h for h in basins_get_v2(b, elevation) if not elevation[h]['drain_in']}
            min_source = None
            for p in sorted(sources, key=lambda s: elevation[s]['drainage'], reverse=True):
                path_neighbors = [n for n in all_neighbors[p] if n not in basins_get_v2(b, elevation) and n != b]
                if path_neighbors:
                    min_source = p
                    break
            if not min_source:
                continue
            path = [min_source]
            while b not in path:
                try:
                    path.insert(0, elevation[path[0]]['drain_out'][0])
                except KeyError:
                    print(path)
                    raise KeyboardInterrupt
            min_neighbor = min(path_neighbors, key=lambda h: elevation[h]['altitude'])
            path.append(min_neighbor)
            while elevation[path[-1]]['altitude'] > elevation[b]['altitude'] - len(path) * 30:
                if path[-1] in basins:
                    if verbose: print('(e)', end='', flush=True)
                    break
                if elevation[path[-1]]['drain_out'] and elevation[path[-1]]['drain_out'][0] != path[-2] and elevation[path[-1]]['drain_out'][0] not in path:
                    path.append(elevation[path[-1]]['drain_out'][0])
                else:
                    break
            dh = max(1, (elevation[b]['altitude'] - elevation[path[-1]]['altitude']) // len(path))
            for p in path:
                elevation[p]['altitude'] = elevation[b]['altitude'] - dh
                dh += max(1, (elevation[b]['altitude'] - elevation[path[-1]]['altitude']) // len(path))
            for p, q in zip(path[:-1], path[1:]):
                if q in elevation[p]['drain_in']: elevation[p]['drain_in'].remove(q)
                elevation[p]['drain_out'] = [q]
                if q not in elevation[p]['drain_in']: elevation[q]['drain_in'].append(p)
            i = 1
            # while i != 0:
            #     i = 0
            #     if set(terrain[b]['drain_out']) & set(terrain[b]['drain_in']):
            #         terrain[h]['drain_in'].remove(terrain[h]['drain_out'][0])
            #         i += 1
            # coastal_mask()
            d = elevation[b]['drainage']
            if verbose: print('{:}, {:.0f} drainage, {:} long, {:.0f} remaining'.format(path[-1], d, len(path), total_drainage - target))
        basins = basins_total(sea_level=sea_level)
        total_drainage = basins[1]
        basins = sorted([h for h in basins[0]], key=lambda x: elevation[x]['drainage'], reverse=True)


def basins_carve_v2(target=0.5, sea_level=0, verbose=True):

    # drainage_set()
    sea_level = seaLevelGet()
    ocean = oceanGet(seaLevel=sea_level)
    all_basins = basins_total(sea_level=sea_level, flatten=True, drainage=True, ocean=ocean)
    current_drainage = all_basins[1]
    # basins = sorted(all_basins[0], key=lambda x: elevation[x]['dist_to_coast'], reverse=False)
    basins = sorted(all_basins[0], key=lambda x: elevation[x]['drainage'], reverse=True)
    # basins = sorted([b for b in all_basins[0]], key=lambda x: min(hex_distance(x, o) for o in ocean))
    target_drainage = int(sum(elevation[h]['altitude'] > sea_level for h in elevation) * target)
    sources = {h for h in elevation if is_exorheic(h, ocean)}
    visited = set()

    while current_drainage > target_drainage:

        if len(visited) == len(basins):
            visited.clear()
        basin = [b for b in basins if b not in visited][0]
        # basin = basins[0]

        if not is_endorheic(basin):
            all_basins = basins_total(sea_level=seaLevelGet(), flatten=True, drainage=True, ocean=ocean)
            basins = sorted(all_basins[0], key=lambda x: elevation[x]['drainage'], reverse=True)
            current_drainage = all_basins[1]
            print(f"{basin}: {1 - target_drainage / current_drainage:0.2%}")
            continue

        d = 0
        ends = []
        while len(ends) < 75:
            d += 5
            ends = [s for s in neighborsWithin(basin, d) if is_exorheic(s, ocean)]

        path = a_star_star(basin, ends, verbose=verbose)

        e = path[-1]
        if e not in ocean and e in sources:
            while e not in ocean:
                path.append(elevation[e]['drain_out'][0])
                e = path[-1]
        dh = max(int(((elevation[basin]['altitude'] - sea_level) / len(path))), 1)
        ds = 0
        for i, h in enumerate(path):
            dz = elevation[h]['altitude'] - clip(
                int(elevation[basin]['altitude'] + 1 - i * dh),
                sea_level + 1 + elevation[h]['dist_to_coast'],
                elevation[h]['altitude']
            )
            ds += sedimentDH(dh=dz)
            elevation[h]['altitude'] = clip(
                int(elevation[basin]['altitude'] + 1 - i * dh),
                sea_level + 1 + elevation[h]['dist_to_coast'],
                elevation[h]['altitude']
            )
        elevation[path[-1]]['sediment_load'].setdefault(path[-2], 0)
        elevation[path[-1]]['sediment_load'][path[-2]] += round(ds, 4)
        # for h in elevation:
        #     if elevation[h]['altitude'] > sea_level:
        #         elevation[h]['altitude'] = max(elevation[h]['altitude'], 1 * (sea_level + elevation[h]['dist_to_coast']))
        all_basins = basins_total(sea_level=sea_level, flatten=True, drainage=True, ocean=ocean)
        # basins = sorted(all_basins[0], key=lambda x: elevation[x]['dist_to_coast'], reverse=False)
        basins = sorted(all_basins[0], key=lambda x: elevation[x]['drainage'], reverse=True)
        current_drainage = all_basins[1]
        print(f"{basin}: {elevation[basin]['drainage']} {1 - target_drainage / current_drainage:0.2%}")
        visited.add(basin)

        # print(f"{basin}: {elevation[basin]['drainage']} {b / len(basins):0.2%}")
        # sources |= get_basin(basin)
        # drainage_set()
        # if target_drainage > current_drainage:
        #     break
    drainageSet(flatten=False, ocean=ocean)
    elevation_svg(sea_level=sea_level, highlight=ends + [basin])
    rivers_draw(straight=True, total_drain=max(hex.get('drainage') for hex in elevation.values()), sea_level=sea_level)


def coastal_mask(hexes=hexes):

    for h in hexes:
        terrain[h]['altitude'] = max(terrain[h]['altitude'], (hexes[h]['dist_to_coast'] + 1) * 30)


def flattenPlateaus(verbose=False):

    if verbose: print('Fixing plateaus...')
    i = 1
    while i != 0:
        i = 0
        for h in sorted(list(elevation), key=lambda h: elevation[h]['altitude'], reverse=True):
            for n in (n for n in neighbors(h)):
                if elevation[h]['altitude'] == elevation[n]['altitude']:
                    elevation[h]['altitude'] += 1
                    i += 1
        if verbose: print(str(i) + ' plateaus fixed')


def erode_basins(update_map=False, target=1, hexes=hexes):
    """
    Fills up all endorheic basins. Target is currently set to 0: instead, we should leave a certain number of basins.
    # TODO only erode those above a certain drainage?
    :return:
    """

    timer(0)
    # global hexes

    with open(elevationfile, 'r') as f:
        elevationmap = bs(f, 'lxml')

    endorheics = []
    target *= int(
        sum(1 for h in hexes if terrain[h]['drain_in'] and not terrain[h]['drain_out']) * 0.10)  # earthlike is 18%
    print('Target basins: {:}'.format(target))
    # i = 1
    # this makes it like an O(n3) process, insane
    print('Fixing plateaus...')
    i = 1
    while i != 0:
        i = 0
        for h in (h for h in elevation if elevation[h]['altitude'] != ''):
            for n in (n for n in all_neighbors[h]):
                if elevation[h]['altitude'] == elevation[n]['altitude']:
                    elevation[h]['altitude'] += 1
                    i += 1
        print(str(i) + ' plateaus fixed')
    print('Finding initial endorheic basins...')
    for h in hexes:
        if not terrain[h]['altitude']:
            continue
        i = 0
        for n in hexes[h]['neighbors']:
            if n not in hexes:
                continue
            if terrain[n]['altitude'] >= terrain[h]['altitude']:
                i += 30
        if i == 6:
            endorheics.append(h)
    print('Basins: {:}'.format(len(endorheics)))
    # if split:
    #     split_elevation()
    updates = set()
    while len(endorheics) > target:
        for e in sorted(endorheics, key=lambda x: terrain[x]['altitude'], reverse=True):
            # TODO use average or merely 10 or so above the next? that would preserve roughness?
            # TODO instead of changing this one, change the one thats next highest above it?
            # next_lowest = min((h for h in hexes[e]['neighbors']), key = lambda x: terrain[x]['altitude'])
            # i = 1
            # terrain[e]['altitude'] = terrain[next_lowest]['altitude'] + i
            avg_altitude = int(avg([terrain[n]['altitude'] for n in hexes[e]['neighbors']]))
            terrain[e]['altitude'] = max(1, min(25599, avg_altitude))
            # i += 1
            # while terrain[e]['altitude'] in [terrain[n]['altitude'] for n in hexes[e]['neighbors']]:
            #     terrain[e]['altitude'] = terrain[next_lowest]['altitude'] + i
            #     i += 1
            # m = max(terrain[h]['altitude'] for h in hexes[e]['neighbors'])
            # terrain[next_lowest]['altitude'] = max(0, terrain[e]['altitude'] - 2)
            # if terrain[next_lowest]['altitude'] == 0:
            #     print(next_lowest)
            # terrain[e]['altitude'] = n + 1 if n + 1 not in [terrain[h]['altitude'] for h in hexes[e]['neighbors']] else n + 2
            # terrain[e]['altitude'] = n if n not in [terrain[h]['altitude'] for h in hexes[e]['neighbors']] else m + 1
            # updates.add(e)
            # terrain[e]['altitude'] = min([terrain[h]['altitude'] for h in hexes[e]['neighbors']]) + 50
        endorheics = []
        while i != 0:
            i = 0
            for h in (h for h in hexes if terrain[h]['altitude'] != ''):
                for n in (n for n in hexes[h]['neighbors'] if n in hexes):
                    if terrain[h]['altitude'] == terrain[n]['altitude']:
                        terrain[h]['altitude'] += 30
                        i += 1
        for h in hexes:
            if not terrain[h]['altitude']:
                continue
            i = 0
            for n in hexes[h]['neighbors']:
                if n not in hexes:
                    continue
                if terrain[n]['altitude'] > terrain[h]['altitude']:
                    i += 30
            if i == 6:
                endorheics.append(h)
        print('{:} / {:}'.format(len(endorheics), target))
        # if timer(1) > 60 * 10:
        #     break
    # for h in updates:
    #     elevationmap.find('path', **{'id': h})['style'] = 'fill:#' + ('%x' % math.floor(terrain[h]['altitude'] / 100)).zfill(
    #         2) * 3
    # if split: merge_elevation()
    if update_map: save_new_elevation()
    # save(elevationmap)
    print('{:} s '.format(timer(1)))
    # merge_elevation()


def find_endorheics(elevation):
    """
    Assuming basin erosion
    :return:
    """

    lake_basins = []
    for h in elevation:
        if elevation[h]['drain_in'] and not elevation[h]['drain_out'] and not elevation[h]['coastal']:
            lake_basins.append(h)
    return lake_basins


def river_path(source):
    if not elevation[source]['drain_out']:
        return []
    target = elevation[source]['drain_out'][0]
    path = [target]
    while elevation[target]['drain_out']:
        target = elevation[target]['drain_out'][0]
        if target in path: return
        # print(target)
        path.append(target)
    return path


def lake_get(h, sea_level=None, ocean=None):

    if not sea_level:
        sea_level = seaLevelGet()
    if not ocean:
        ocean = oceanGet(seaLevel=sea_level)
    if not is_endorheic(h):
        return {'lake': set(), 'drain_out': dict(), 'drain_in': list(), 'level': None}
    lake = set()
    level = min(elevation[x]['altitude'] for x in all_neighbors[h] if elevation[x]['altitude'] > sea_level)
    g = {x for x in all_neighbors[h] if elevation[x]['altitude'] == level}
    lake |= {h} | g
    # print(lake)
    while all(min([n for n in all_neighbors[n]], key=lambda x: elevation[x]['altitude']) in lake for n in lake):
        level = min(elevation[x]['altitude'] for x in set(sum((all_neighbors[n] for n in lake), [])) if x not in lake and elevation[x]['altitude'] >= level)
        g = {x for x in set(sum((all_neighbors[n] for n in lake), [])) if sea_level < elevation[x]['altitude'] <= level}
        lake |= g
        # print(lake)
        lake |= elevation[h].get('lake', dict()).get('lake', set())
        for i in list(lake):
            lake |= elevation[i].get('lake', dict()).get('lake', set())
    drain_in = set()
    for i in lake:
        for n in all_neighbors[i]:
            if n in lake:
                drain_in.add(n)
            if i == min(all_neighbors[n], key=lambda x: elevation[x]['altitude']):
                drain_in.add(n)
    drain_out = min((x for x in set(sum((all_neighbors[n] for n in lake), [])) if x not in lake and elevation[x]['altitude'] <= level), key=lambda x: elevation[x]['altitude'])
    # drain_out = min([min(all_neighbors[n], key=lambda x: elevation[x]['altitude']) for n in all_neighbors[i] for i in lake], key=lambda x: elevation[x]['altitude'])
    drain_from = min([i for i in lake if drain_out in all_neighbors[i]], key=lambda x: elevation[x]['altitude'])
    return {'lake': lake, 'drain_out': {drain_from: drain_out}, 'drain_in': drain_in, 'level': level}


def drainageSet(verbose=False, ocean=None, seaLevel=0):
    """
    This function runs through all hexes and runs water down from the height to the sea.
    :return:
    """

    if not ocean:
        ocean = oceanGet(seaLevel=seaLevel)
    if verbose: print('Assigning raw drainage numbers...')
    sigma = 19.75
    # mean_rainfall = statistics.mean([climate[c]['precipitation_annual'] for c in climate])
    for h in elevation:
        elevation[h].update({'drainage': {h}, 'drain_in': list(), 'drain_out': []})
    # assign drain outlets if possible
    current = sorted(set(elevation) - ocean, key=lambda h: elevation[h]['altitude'], reverse=True)
    for h in current:
        g = [
            i for i in neighbors(h) if
            elevation[i]['altitude'] < elevation[h]['altitude'] and
            i not in elevation[h]['drain_in']
        ]
        g += [
            i for i in neighbors(h) if
            elevation[i]['altitude'] == elevation[h]['altitude'] and
            i not in elevation[h]['drain_in']
        ] if not g else []
        if g:
            g = min(g, key=lambda x: elevation[x]['altitude'])
        else:
            if verbose: print(f"{h} has no drain outlet")
            continue
        elevation[h]['drain_out'].append(g)
        elevation[g]['drain_in'].append(h)
    current = {h for h in set(elevation) - ocean if elevation[h]['drain_out'] and not elevation[h]['drain_in']}
    while current:
        for c in list(current):
            d = elevation[c]['drain_out']
            if not d:
                current.discard(c)
                continue
            d = d[0]
            if c in elevation[d]['drainage']:
                current.discard(c)
                continue
            elevation[d]['drainage'] |= elevation[c]['drainage']
            current.add(d)
            current.discard(c)
        if verbose: print(len(current))
    for h in elevation:
        elevation[h]['drainage'] = math.fsum([1 for i in elevation[h]['drainage']])
        # elevation[h]['drainage'] = math.fsum([round(climate.get(i, dict()).get('precipitation_annual', mean_rainfall) / mean_rainfall, 2) for i in elevation[h]['drainage']])
    return True


def flatten_islands():
    for h in hexes:
        if len(hexes[h]['neighbors']) == 0:  # only islands
            if terrain[h]['altitude'] > 12000:  # of a certain height
                terrain[h]['altitude'] = int(terrain[h]['altitude'] / 2)
        if len(hexes[h]['neighbors']) == 1 and len(hexes[hexes[h]['neighbors'][0]]['neighbors']) == 1:
            if hexes[hexes[h]['neighbors'][0]]['neighbors'][0] == h:
                if terrain[h]['altitude'] > 12000:  # of a certain height
                    terrain[h]['altitude'] = int(terrain[h]['altitude'] / 2)


def elevation_gamma(gamma=1):

    b = statistics.mean([terrain[h]['altitude'] for h in hexes])
    gamma = math.log(b / 25599) / math.log(2756 / 25599)

    for h in elevation:
        terrain[h]['altitude'] = int(terrain[h]['altitude'] ** gamma)

    altitudeRescale()
    save_new_elevation()


def elevation_noise(p=0.05, sea_level=0, keep_basins=False):

    for h in elevation:
        if elevation[h]['altitude'] <= sea_level:
            continue
        if keep_basins and elevation[h]['drainage'] > 6:
            continue
        d = 25599 - elevation[h]['altitude']
        elevation[h]['altitude'] = clip(int(random.gauss(elevation[h]['altitude'], d * p / 3)), elevation[h]['altitude'])
    # save_data(elevation, basefile + r'\elevation.p')


def average_slope():

    slopes = []
    for h in hexes:
        for n in hexes[h]['neighbors']:
            if n in hexes:
                slopes.append(abs(terrain[h]['altitude'] - terrain[n]['altitude']))

    return avg(slopes)


def dh_erosion(A, s):

    dt = 7.889e12  # s
    k = 1.778e-14  # 1/s
    A *= 9.674e9  # hexes -> ft^2
    s = min(s, 3e3)
    s *= 9.47e-6 # ft/hex -> ft/20 mi
    x = dt * (k * math.sqrt(A) * s)
    return x


def dhUplift(kU=1):

    dt = 7.889e12  # s
    u = 5.198e-11 * kU # ft/s
    x = dt * u
    return x


def dh_sediment(q=0):
    '''

    :param q: sediment amount
    :return: corresponding height change
    '''

    return q * 815.525 / 2


def sedimentDH(dh=0):
    '''

    :param dh: height amount
    :return: corresponding sediment amount
    '''

    return dh / 815.525 * 2


def hypsometry():

    alts = []
    for ht in range(255, 0, -1):
        alt = ht * 100
        alts.insert(0, sum(
            [1 for h in elevation if elevation[h]['altitude'] and alt < int(elevation[h]['altitude']) <= (alt + 100)]))
    fig, ax = plt.subplots()
    plt.cla()
    ax.barh(range(255), alts, align='center', color='grey')
    plt.show()


def applyHypsometry(seaLevel=0, ocean=None):

    def hyp(x, r=0.05, z=0.5, m=0.75):
        beta = (1 - r) / r
        return ((1 - x ** m) / (1 + beta * x ** m)) ** z

    maxZ = max(elevation, key=lambda h: elevation[h]['altitude'])
    maxA = elevation[maxZ]['altitude'] - seaLevel
    land = sorted(set(elevation) - ocean, key=lambda h: elevation[h]['altitude'], reverse=True)
    for i, h in enumerate(land):
        elevation[h]['altitude'] = min(elevation[h]['altitude'], round(maxA * hyp(i / len(land)) + seaLevel + 1))
    return True


def get_basin(h):

    all_sources = set(s for s in elevation[h]['drain_in'])
    sources = set(s for s in elevation[h]['drain_in'])
    visited = set()
    while sum((elevation[s]['drain_in'] for s in sources if s not in visited), []):
        visited |= sources
        sources = set(sum((elevation[s]['drain_in'] for s in sources), [])) | \
                  {s for s in sources if not elevation[s]['drain_in']}
        # for s in list(sources):
        #     sources |= elevation[s].get('lake', dict()).get('lake', set())
        # print(sources, visited)
        all_sources |= sources

    return all_sources


def basins_get_v2(h):

    visited = set([h])
    current = set(elevation[h]['drain_in'])
    while current:
        for c in list(current):
            current |= set(elevation[c]['drain_in'])
            current.remove(c)
            visited.add(c)
        # print(len(current))

    return visited


def upliftReset():

    for h in tectonics:
        tectonics[h]['uplift'] = float(tectonics[h]['uplift raw'] * (1.25 if plateTypes[hexPlates[h]] != 'oceanic' else 1))


def upliftExchange():

    """
    Randomly exchanges the uplift of a hex with one of its neighbors
    """

    done = set()
    for t in tectonics:
        if t in done:
            continue
        n = [h for h in neighbors(t) if t not in done]
        if not n:
            done.add(t)
            continue
        n = random.choice(n)
        tectonics[t]['uplift'], tectonics[n]['uplift'] = tectonics[n]['uplift'], tectonics[t]['uplift']
        done.add(t)
        done.add(n)


def upliftRipple(a=50, b=int(random.random() * 100), freq=1000, u0=0.1, umin=0, gamma=1, ridged=False, distort=False):

    def u_noise(u0, h, octaves=4, ridged=False, distort=False, freq=freq):

        def u_ridge(h, p_o=(0, 0), octaves=4, freq=freq):

            r = 1 - abs(noise.snoise2(
                (hexMap[h][0]) / freq,
                (hexMap[h][1]) / freq,
                octaves=octaves, repeatx=10000, repeaty=10000
            ))
            return r

        u = u0 * (0.20/2 * noise.snoise2(hexMap[h][0] / freq, hexMap[h][1] / freq, octaves=octaves, repeatx=10000, repeaty=10000) + 0.9)

        if ridged:
            return 3/8 * u_ridge(h, octaves=octaves, freq=freq * 2) + 5/8 * u
        else:
            return u

    upliftReset() #TODO split out the things that aren't actually ripple code
    for t in tectonics:
        tectonics[t]['uplift'] *= 1.25 if plateTypes[hexPlates[t]] == 'continental' else 1

    upliftBlur(g=2)
    for _ in range(2):
        upliftExchange()
    upliftHypso()

    for t in tectonics:
        tectonics[t]['uplift'] *= (0.75 + 0.25 * noise.snoise2(
            (hexMap[t][0]) / freq + tectonics[t]['fault']['C'] -
            tectonics[t]['fault']['D'] + tectonics[t]['fault']['T'] / 2,
            (hexMap[t][1]) / freq + tectonics[t]['fault']['C'] -
            tectonics[t]['fault']['D'] + tectonics[t]['fault']['T'] / 2, octaves=4, repeatx=50000, repeaty=100000))
        tectonics[t]['uplift'] += u_noise(0.25, t, ridged=ridged, distort=distort, freq=freq)
    upliftBlur(g=1)
    upliftMap(fileName='upliftRipple')

    save_data(tectonics, basefile + r'\tectonics.json')

    gc.collect()
    fig, ax = newMap(0)

    maxU = max(tectonics[h]['uplift'] for h in tectonics)
    minU = min(tectonics[h]['uplift'] for h in tectonics)
    t = sorted([h for h in tectonics if rescale(tectonics[h]['uplift'], minU, maxU, 0, 25599) > 0], key=lambda h: tectonics[h]['uplift'])
    t = list(tectonics)
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    maxU = max(tectonics[h]['uplift'] for h in t)
    minU = min(tectonics[h]['uplift'] for h in t)
    c = [colors.hex2color(f"#{screen('000000', rescale(tectonics[h]['uplift'], minU, maxU, 0, 255))}") for h in t]
    ax.scatter(x=x, y=y, marker='h', c=c, s=1, edgecolors=c, zorder=100, linewidths=0.1)

    fig.savefig(fr"{basefile}\Elevation\uplift.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def upliftRescale():

    b = max(tectonics[h]['uplift'] for h in tectonics)
    a = min(tectonics[h]['uplift'] for h in tectonics)
    for h in tectonics:
        c = tectonics[h]['uplift']
        tectonics[h]['uplift'] = round(rescale(c, 0, b, 0, 1), 5) if c >= 0 else round(rescale(c, a, 0, a / b, 0), 5)


def uplift_bins(b=5):

    for t in tectonics:
        u = tectonics[h]['uplift']
        tectonics[h]['uplift'] = round(roundBase(u, prec=5, base=1 / b), 5)
    save_data(tectonics, basefile + r'\tectonics.p')


def plate_type(faults, plates):

    pass


def assignPlateInfluences(sigma_p=20, verbose=False):

    print('Assigning tectonic distances...')
    f = {h: faultType(h) for h in list(filter(lambda h: isFault(h), hexMap))}
    c_faults = {h: f[h] for h in f if f[h]['type'] == 'C'}
    c_faults = poissonSamplev2(c_faults.keys(), r=int(sigma_p / 2), verbose=verbose)
    d_faults = {h: f[h] for h in f if f[h]['type'] == 'D'}
    d_faults = poissonSamplev2(d_faults.keys(), r=int(sigma_p / 2), verbose=verbose)
    t_faults = {h: f[h] for h in f if f[h]['type'] == 'T'}
    t_faults = poissonSamplev2(t_faults.keys(), r=int(sigma_p / 2), verbose=verbose)
    print(f"C: {len(c_faults)}, D: {len(d_faults)}, T: {len(t_faults)}")

    plateInfluences = {p: {i: set() for i in 'CDT'} for p in plates}
    for g, t in zip([c_faults, d_faults, t_faults], 'CDT'):
        for f in g:
            p = hexPlates[f]
            plateInfluences[p][t].add(f)
            for n in neighborsWithin(f, sigma_p):
                p = hexPlates[n]
                plateInfluences[p][t].add(f)

    save_data(plateInfluences, basefile + r'\plateInfluences.p')
    save_data({'C': c_faults, 'D': d_faults, 'T': t_faults}, basefile + r'\faultTypesReduced.p')


def assignUplift(a=[0.5, 0.4, 0.1, 0.25], n=2, r=None, flat=False):

    def gauss(x, mu, sigma):

        return np.exp(-(x - mu) ** 2 / sigma ** 2)

    def strength(h, fault, sigma):

        return f[fault]['strength'] * g(hexDistance(h, fault), 0, sigma)

    f = {h: faultType(h) for h in list(filter(lambda h: isFault(h), hexMap))}
    for p in plates:
        for h in plates[p]:
            idw_neighbors = [(f, hexDistance(h, f)) for f in plateInfluences[p]['C']]
            if r:
                idw_neighbors = [(f, d) for f, d in idw_neighbors if d <= r]
            if not idw_neighbors:
                c = 0
            else:
                if not flat:
                    c = sum(f[i]['strength'] / (x or 1) ** n for i, x in idw_neighbors) / sum(1 / (x or 1) ** n for i, x in idw_neighbors)
                else:
                    c = sum(1 / (x or 1) ** n for f, x in idw_neighbors)

            idw_neighbors = [(f, hexDistance(h, f)) for f in plateInfluences[p]['D']]
            if r:
                idw_neighbors = [(f, d) for f, d in idw_neighbors if d <= r]
            if not idw_neighbors:
                d = 0
            else:
                if not flat:
                    d = sum(f[i]['strength'] / (x or 1) ** n for i, x in idw_neighbors) / sum(1 / (x or 1) ** n for i, x in idw_neighbors)
                else:
                    d = sum(1 / (x or 1) ** n for f, x in idw_neighbors)

            idw_neighbors = [(f, hexDistance(h, f)) for f in plateInfluences[p]['T']]
            if r:
                idw_neighbors = [(f, d) for f, d in idw_neighbors if d <= r]
            if not idw_neighbors:
                t = 0
            else:
                if not flat:
                    t = sum(f[i]['strength'] / (x or 1) ** n for i, x in idw_neighbors) / sum(1 / (x or 1) ** n for i, x in idw_neighbors)
                else:
                    t = sum(1 / (x or 1) ** n for f, x in idw_neighbors)
            tectonics[h]['uplift raw'] = float(a[0] * c - a[1] * d + a[2] * t + a[3])
        print(p)

    save_data(tectonics, basefile + r'\tectonics.json')

    gc.collect()
    fig, ax = newMap(0)

    t = list(tectonics)
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    minU = min(tectonics[h]['uplift raw'] for h in tectonics)
    maxU = max(tectonics[h]['uplift raw'] for h in tectonics)
    c = [hexColor(f"{screen('000000', rescale(tectonics[h]['uplift raw'], minU, maxU, 0, 255))}") for h in t]
    ax.scatter(x=x, y=y, marker='*', c=c, s=5, edgecolors=c, zorder=100, linewidths=0.25)

    fig.savefig(fr"{basefile}\Elevation\uplift raw.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def hypso(u):

    if u >= 0:
        return -0.65 * 55 * math.exp(-u / 0.65) + 0.65 * 55
    else:
        return 55 * 0.15 * math.exp(u/0.15) + \
               95 / 1.5 / 2 * (1 + math.erf((u+4.6) / (1.5 * math.sqrt(2)))) - \
               55 * 0.15 * math.exp(0/0.15) - \
               95 / 1.5 / 2 * (1 + math.erf((0+4.6) / (1.5 * math.sqrt(2))))

def hypso_inv(u):

    i = pynverse.inversefunc(hypso, accuracy=5)
    return clip(float(i(u)), -11, 8)


def geologic_regime(u):

    if u <= 0.5: # basin
        return -0.01
    elif 0.5 < u <= 0.6: # platform
        return 0.05
    elif 0.6 < u < 0.7: # crust
        return 0.10
    elif 0.7 < u < 0.8: # shield
        return 0.25
    else: # orogen
        return 1


def uplift_regime(flat=True, f=0.1, mode=False):

    upliftReset()
    upliftRescale()
    b = random.randint(0, 100)
    for t in tectonics:
        u = tectonics[t]['uplift']
        u = geologic_regime(u)
        tectonics[t]['uplift'] = u
    if mode:
        uplift_mode(g=mode)
    if not flat:
        upliftRescale()
        for t in tectonics:
            u = tectonics[t]['uplift']
            n = f * noise.snoise2(
                    (hexdict[t][0][0]) / 500,
                    (hexdict[t][0][1]) / 500,
                    octaves=8, repeatx=63000, repeaty=63000, base=b
                ) / 2 + 0.5
            tectonics[t]['uplift'] = u + n
    upliftRescale(umin=-0.001)


def upliftBlur(g=1):

    for _ in range(g):
        old = {t: tectonics[t]['uplift'] for t in tectonics}
        for t in tectonics:
            u = old[t]
            n = statistics.mean([u] + [old[n] for n in neighbors(t)])
            tectonics[t]['uplift'] = n


def uplift_mode(g=3):

    for _ in range(g):
        old = {t: tectonics[t]['uplift'] for t in tectonics}
        for t in tectonics:
            if not all_neighbors[t]:
                continue
            try:
                tectonics[t]['uplift'] = statistics.mode([old[n] for n in all_neighbors[t]] + [old[t]])
            except statistics.StatisticsError:
                if old[t] in [old[n] for n in all_neighbors[t]]:
                    pass
                else:
                    tectonics[t]['uplift'] = old[all_neighbors[t][0]]


def upliftHypso():

    # hypso_curve = {i: hypso(i) for i in np.linspace(-11, 8, 10000)}
    # hypso_curve = {round(rescale(i, -11, 8, 0, 1), 5): round(rescale(hypso_curve[i], hypso(-11), hypso(8), 0, 1), 5) for i in hypso_curve}
    # hypso_curve = {v: k for k, v in hypso_curve.items()}
    warnings.filterwarnings("ignore")
    maxU = max(tectonics[h]['uplift'] for h in tectonics)
    minU = min(tectonics[h]['uplift'] for h in tectonics)
    sortedTectonics = sorted(tectonics, key=lambda h: tectonics[h]['uplift'])
    for i, h in enumerate(sortedTectonics):
        u = rescale(i, 0, len(tectonics), hypso(-11), hypso(8))
        u = hypso_inv(u)
        u = rescale(u, -11, 8, 0, 1)
        tectonics[h]['uplift'] = round(u, 5)
    # upliftRescale(umin=0)

    save_data(tectonics, basefile + r'\tectonics.json')


def uplift_set(a=[0.5, 0.4, 0.1, 0.25], gamma=1, n=3):

    maxs = {i: max(tectonics[h]['fault'][i] for h in tectonics) for i in 'CDT'}
    mins = {i: min(tectonics[h]['fault'][i] for h in tectonics) for i in 'CDT'}
    for h in tectonics:
        c, d, t = [rescale(pow(rescale(tectonics[h]['fault'][i], mins[i], maxs[i], 0.001, 1), gamma), 0.001 ** gamma, 1, 0.001, 1) for i in 'CDT']
        if n == 1:
            for i in (c, d, t):
                if i != max(c, d, t):
                    i = 0
        elif n == 2:
            for i in (c, d, t):
                if i == min(c, d, t):
                    i = 0
        val = (
            a[0] * (c) +
            a[1] * (t) -
            a[2] * (d) + a[3]
        )
        # terrain[h]['altitude'] = min(25599, max(1, int(25599 * val)))
        try:
            tectonics[h]['uplift raw'] = round(val, 5)
        except TypeError:
            print(h)
            return


def upliftMap(fileName='uplift'):

    fig, ax = newMap(0)

    t = sorted([h for h in tectonics], key=lambda h: tectonics[h]['uplift'])
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    maxU = max(tectonics[h]['uplift'] for h in t)
    minU = min(tectonics[h]['uplift'] for h in t)
    c = [hexColor(f"{screen('000000', rescale(tectonics[h]['uplift'], minU, maxU, 0, 255))}") for h in t]
    ax.scatter(x=x, y=y, marker='*', c=c, s=0.25, zorder=100)

    fig.savefig(fr"{basefile}\Elevation\{fileName}.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()
    del fig, ax
    gc.collect()


def assignFaults(sigma_p=10, sigma_s=50, k=1):

    timer(0)

    for h in hexMap.keys():
        tectonics.setdefault(h, dict())
        tectonics[h].setdefault('fault', {'C': 0, 'D': 0, 'T': 0})

    print('Finding tectonic influences...')


    print('Assigning tectonic distances...')
    f = {h: faultType(h) for h in list(filter(lambda h: isFault(h), hexMap))}
    c_faults = {h: f[h] for h in f if f[h]['type'] == 'C'}
    c_faults = poissonSamplev2(c_faults.keys(), r=int(sigma_p / 2), verbose=verbose)
    d_faults = {h: f[h] for h in f if f[h]['type'] == 'D'}
    d_faults = poissonSamplev2(d_faults.keys(), r=int(sigma_p / 2), verbose=verbose)
    t_faults = {h: f[h] for h in f if f[h]['type'] == 'T'}
    t_faults = poissonSamplev2(t_faults.keys(), r=int(sigma_p / 2), verbose=verbose)
    print(f"C: {len(c_faults)}, D: {len(d_faults)}, T: {len(t_faults)}")

    def g(x, mu, sigma):

        return np.exp(-(x - mu) ** 2 / sigma ** 2)

    def strength(h, fault, sigma):

        return f[fault]['strength'] * g(hexDistance(h, fault), 0, sigma)

    def closestFault(h, faultSet, sigma, k=1):

        return sorted(list(faultSet), key=lambda f: strength(h, f, sigma), reverse=True)[:k]

    # d_lim = min(d_lim, sigma * math.sqrt(-math.log(0.001)))

    n = 2
    i = 0
    for h in tectonics:
        i += 1
        tectonics[h]['fault raw dist'] = {
            'C': closestFault(h, c_faults.intersection(plates[hexPlates[h]]), sigma_s, k),
            'D': closestFault(h, d_faults.intersection(plates[hexPlates[h]]), sigma_s, k),
            'T': closestFault(h, t_faults.intersection(plates[hexPlates[h]]), sigma_s, k)
        }
        c = np.mean([strength(h, n, sigma_s) for n in tectonics[h]['fault raw dist']['C']] or [0])
        d = np.mean([strength(h, n, sigma_s) for n in tectonics[h]['fault raw dist']['D']] or [0])
        t = np.mean([strength(h, n, sigma_s) for n in tectonics[h]['fault raw dist']['T']] or [0])
        tectonics[h]['fault raw'] = {'C': int(c), 'D': int(d), 'T': int(t)}
        print(f'{i / len(tectonics):0.3%}')

    save_data(tectonics, basefile + r'\tectonics.p')

    maxF = {i: max(tectonics[h]['fault raw'][i] for h in tectonics) for i in 'CDT'}
    minF = {i: min(tectonics[h]['fault raw'][i] for h in tectonics) for i in 'CDT'}
    for h in tectonics:
        c, d, t = [rescale(tectonics[h]['fault raw'][i], minF[i], maxF[i], 0, 1) for i in 'CDT']
        tectonics[h]['fault'] = {i: int(round(x * maxF[i])) for i, x in zip('CDT', [c, d, t])}
        col = f'{int(255 * c):02x}{int(255 * t):02x}{int(255 * d):02x}'

    fig, ax = newMap(0)

    t = list(tectonics.keys())
    yh = {h: hexMap[h][0] for h in t}
    xh = {h: hexMap[h][1] for h in t}
    y = [yh[h] for h in yh.keys()]
    x = [xh[h] for h in xh.keys()]
    c = [colors.hex2color(f"#{int(255 * tectonics[h]['fault']['C'] / maxF['C']):02x}"
                          f"{int(255 * tectonics[h]['fault']['T'] / maxF['T']):02x}"
                          f"{int(255 * tectonics[h]['fault']['D'] / maxF['D']):02x}") for h in t]
    ax.scatter(x=x, y=y, marker='*', c=c, s=5, edgecolors=c, zorder=100, linewidths=0.25)

    fig.savefig(fr"{basefile}\Elevation\faults.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()

    return True


def tectonics_old():
    """
    Saves both the distance to the fault and the strength of collision/divergences for C/D/T
    :return:
    """

    timer(0)  # 9762 s -> ~3h
    global hexes
    print('Finding tectonic influences...', end='', flush=True)

    with open(basefile + r'\Tectonics.svg', 'r') as f:
        tectonicmap = bs(f, 'lxml')

    faults = dict()
    for f in tectonicmap.find(id='Gen-Faults').find_all('path'):
        cat = get_property(f, 'stroke')
        strength = int(get_property(f, 'stroke-width'))
        cat = 'C' if cat == '#ff0000' else ('D' if cat == '#0000ff' else 'T')
        p = parse_coordinate_list(clean_path(f['d']))
        p = (
            (p[0][0] + p[-1][0]) / 2,
            (p[0][1] + p[-1][1]) / 2
        )
        faults.update({
            latlong(p, id=True): (
                cat,
                strength
            )
        })
    print('{:} s'.format(timer(1)))
    print('Assigning tectonic distances...')
    c_faults = [f for f in faults if faults[f][0] == 'C']
    d_faults = [f for f in faults if faults[f][0] == 'D']
    t_faults = [f for f in faults if faults[f][0] == 'T']
    i = 0
    for h in hexes:
        # c, d, t = 1e10, 1e10, 1e10
        i += 1
        c = sorted(c_faults, key=lambda f: hexDistance(h, f))[0]
        d = sorted(d_faults, key=lambda f: hexDistance(h, f))[0]
        t = sorted(t_faults, key=lambda f: hexDistance(h, f))[0]
        c, c_s = hexDistance(h, c), faults[c][1]
        d, d_s = hexDistance(h, d), faults[d][1]
        t, t_s = hexDistance(h, t), faults[t][1]
        terrain[h]['fault'] = ((c, c_s), (t, t_s), (d, d_s))
        if not timer(1) % 300:
            print('{:} {:.3%}'.format(timer(1), i / len(hexes)))
    print('{:} s'.format(timer(1)))
    timer(0)
    print('Generating fault map...', end='', flush=True)
    string = ''
    for h in hexes:
        try:
            col = '{:02x}{:02x}{:02x}'.format(
                min(255, max(0, math.ceil(255 * 1.23 ** (-terrain[h]['fault'][0][0] / 22)))) * (
                            terrain[h]['fault'][0][0] == min(f[0] for f in terrain[h]['fault'])),
                min(255, max(0, math.ceil(255 * 1.23 ** (-terrain[h]['fault'][1][0] / 22)))) * (
                            terrain[h]['fault'][1][0] == min(f[0] for f in terrain[h]['fault'])),
                min(255, max(0, math.ceil(255 * 1.23 ** (-terrain[h]['fault'][2][0] / 22)))) * (
                            terrain[h]['fault'][2][0] == min(f[0] for f in terrain[h]['fault'])))
            string += (
                    '<path d="M ' +
                    ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]) +
                    ' Z" id="' + h +
                    '" style="fill:#' + col +
                    '" />\n'
            )
        except KeyError:
            print(terrain[h]['fault'])
            continue

    with open(basefile + r'\Faults.svg', 'r') as f:
        file = bs(f, 'lxml')

    file.find('g', id='Faults').contents = bs(string, 'lxml').find('body').contents
    save(file, extra_file=basefile + r'\Faults.svg')
    with open(hexpickle, 'wb') as f:
        pickle.dump(hexes, )

    string = ''
    for f in faults:
        col = 'f00' if faults[f][0] == 'C' else ('0f0' if faults[f][0] == 'T' else '00f')
        string += (
                '<path d="M ' +
                ' '.join([','.join([str(i) for i in p]) for p in hexdict[f]]) +
                ' Z" id="f-' + f +
                '" style="fill:#' + col +
                '" />\n'
        )
    file.find('g', id='Fault Hexes').contents = bs(string, 'lxml').find('body').contents
    save(file, extra_file=basefile + r'\Faults.svg')

    print('{:} s'.format(timer(1)))


def tectonics_get_faults():

    sector_rotate = {
        1: 60 * 2,
        2: 60 * 3,
        3: -60 * 2,
        4: 60,
        5: 0,
        6: -60,
        7: -60,
        8: 0,
        9: 60 * 1,
        10: -60 * 2,
        11: -60 * 3,
        12: 60 * 2
    }

    with open(basefile + r'\Tectonics.svg', 'r') as f:
        tectonicmap = bs(f, 'lxml')

    faults = []
    # plates = dict()
    for f in tectonicmap.find('g', id='New-Rough-Faults').find_all('path'):
        if 'stroke-dasharray:150, 150' in f['style'] or 'stroke-dasharray:150,150' in f['style']:
            vec = get_property(f, 'stroke')
            vec = int(int(vec[1:3], 16) * 1000 / 255), int(vec[3:5], 16) * 2
            vec = (
                int(vec[0] * cos(radians(vec[1]))),
                int(vec[0] * sin(radians(vec[1]))),
            )
            fault = parse_coordinate_list(f)
            fault_ang = -int(math.degrees(atan2(fault[-1][1] - fault[0][1], fault[-1][0] - fault[0][0])))
            sec = latlong((
                (fault[0][0] + fault[-1][0]) / 2,
                (fault[0][1] + fault[-1][1]) / 2
            ), terr=True, sector=True)
            # id = latlong((
            #     (fault[0][0] + fault[-1][0]) / 2,
            #     (fault[0][1] + fault[-1][1]) / 2
            # ), terr=True)
            # plates[get_property(f, 'stroke')] = plates.get(get_property(f, 'stroke'), set()) | set([id])
            # fault_ang *= -1
            fault_ang += sector_rotate[sec]
            fault_ang = fault_ang if -90 <= fault_ang <= 90 else (
                180 + fault_ang if -90 <= 180 + fault_ang <= 90 else fault_ang - 180)
            faults.append(
                (vec, int(math.hypot(fault[1][1] - fault[0][1], fault[-1][0] - fault[0][0])), fault_ang, fault[0],
                 fault[-1], sec, int(get_property(f, 'stroke')[5:7], 16)))

    fault_cols = {
        'C': 'ff0000',
        'D': '0000ff',
        'T': '00ff00',
    }

    fault_string = ''
    done = set()
    c, d, t = 0, 0, 0
    for fa in faults:
        if fa in done:
            continue
        for fb in faults:
            if fb in done:
                continue
            if fa != fb and fa[3] == fb[3] and fa[4] == fb[4] and fa[6] > fb[6]:
                phi = (fa[0][0] - fb[0][0], fa[0][1] - fb[0][1])
                phi = int(math.degrees(atan2(phi[1], phi[0])))  # resultant vector of top plate
                # ang_a = int(math.degrees(math.acos((
                #     vec[0] * (fa[4][0] - fa[3][0]) + vec[1] * (fa[4][1] - fa[3][1])
                # ) / (
                #     math.hypot(*vec) * math.hypot(fa[4][0] - fa[3][0]), (fa[4][1] - fa[3][1]))
                # )))) #- sector_rotate[fa[5]]
                # theta = fa[2] if -90 <= fa[2] <= 90 else 180 - fa[2]
                theta = fa[2]  # if -90 <= fa[2] <= 90 else (180 + fa[2] if -90 <= 180 + fa[2] <= 90 else fa[2] - 180)
                delta = phi - theta if phi - theta > -180 else phi - theta + 180
                fault_type = 'C' if delta < 0 else 'D'
                if -35 < delta < 35:
                    fault_type = 'T'
                f = (fa[0][0] - fb[0][0], fa[0][1] - fb[0][1])
                v = (fa[1] * cos(radians(fa[2])), fa[1] * sin(radians(fa[2])))
                fv = int(math.hypot(
                    f[0] - v[0] * (f[0] * v[0] + f[1] * v[1]) / (v[0] * v[0] + v[1] * v[1]),
                    f[1] - v[1] * (f[0] * v[0] + f[1] * v[1]) / (v[0] * v[0] + v[1] * v[1])
                ))
                projvf = int(math.hypot(
                    v[0] * (f[0] * v[0] + f[1] * v[1]) / (v[0] * v[0] + v[1] * v[1]),
                    v[1] * (f[0] * v[0] + f[1] * v[1]) / (v[0] * v[0] + v[1] * v[1])
                ))
                w = max(10, fv / 20) if fault_type in ['C', 'D'] else max(10, projvf / 20)
                # print(phi, theta, delta, fault_type, fa[3:5], fa[5])
                fault_string += '<path d="M {:}" style="stroke-linecap:round;stroke-width:{:0.3f};stroke:#{:}" />'.format(
                    ' '.join([','.join([str(i) for i in p]) for p in fa[3:5]]),
                    w,
                    fault_cols[fault_type]
                )
                done.add(fa)
                done.add(fb)
                if fault_type == 'C': c += 1
                if fault_type == 'D': d += 1
                if fault_type == 'T': t += 1
                print(fa)
                # print(fb)
                # raise Exception
    print('Convergent faults: {:}\nDivergent faults: {:}\nTransform faults: {:}'.format(c, d, t))
    tectonicmap.find('g', id='Gen-Faults').contents = bs(fault_string, 'lxml').find('body').contents
    # save(tectonicmap)
    with open(basefile + r'\Tectonics.svg', 'wb') as f:
        tectonicmap.find('body').replace_with_children()
        tectonicmap.find('html').replace_with_children()
        f.write(tectonicmap.prettify('utf-8'))


def tectonics_assign_raw():

    print('Finding tectonic influences...', end='', flush=True)

    with open(basefile + r'\Tectonics.svg', 'r') as f:
        tectonicmap = bs(f, 'lxml')

    faults = dict()
    for f in tectonicmap.find(id='Gen-Faults').find_all('path'):
        cat = get_property(f, 'stroke')
        strength = float(get_property(f, 'stroke-width'))
        cat = 'C' if cat == '#ff0000' else ('D' if cat == '#0000ff' else 'T')
        p = parse_coordinate_list(clean_path(f['d']))
        p = (
            (p[0][0] + p[-1][0]) / 2,
            (p[0][1] + p[-1][1]) / 2
        )
        faults.update({
            latlong(p, id=True): [
                cat,
                strength
            ]
        })

    # print('{:} s'.format(timer(1)))
    print('Assigning tectonic distances...')
    c_faults = {f: faults[f] for f in faults if faults[f][0] == 'C'}
    d_faults = {f: faults[f] for f in faults if faults[f][0] == 'D'}
    t_faults = {f: faults[f] for f in faults if faults[f][0] == 'T'}

    for h in hexdict.keys():
        tectonics.setdefault(h, dict())
        tectonics[h].setdefault('raw', dict())

    i = 0
    neighbor = {h: all_neighbors[h] for h in hexdict}
    for h in hexes:
        i += 1
        current = set([h])
        visited = set()
        d = 0
        c_flag, d_flag, t_flag = 0, 0, 0
        while current and not c_flag * d_flag * t_flag:
            for c in list(current):
                if c in c_faults and not c_flag:
                    tectonics[h]['raw']['C'] = (c_faults[c][1], (d + 1))
                    c_flag = 1
                elif c in d_faults and not d_flag:
                    tectonics[h]['raw']['D'] = (d_faults[c][1], (d + 1))
                    d_flag = 1
                elif c in t_faults and not t_flag:
                    tectonics[h]['raw']['T'] = (t_faults[c][1], (d + 1))
                    t_flag = 1
                current.discard(c)
                visited.add(c)
                current |= set([n for n in neighbor[c] if n not in visited])
            # print(len(current), len(visited))
            d += 1
        print(f'{i / len(tectonics):0.3%}')

    save_data(tectonics, basefile + r'tectonics.p')
    return True


def unflatten_plateaus(hexes=hexes):
    # TODO instead, run back up the rivers and make sure each one is 30' higher
    i = 1
    while i != 0:
        i = 0
        coastal_mask()
        for h in sorted(hexes, key=lambda x: climate[x]['drainage'], reverse=True):

            # for h in (h for h in hexes if terrain[h]['altitude'] != ''):
            for n in hexes[h]['neighbors']:
                if terrain[h]['altitude'] == terrain[n]['altitude']:
                    terrain[h]['altitude'] += 30
                    # hexes[min([n, h], key=lambda x: climate[x]['drainage'])]['altitude'] += 30
                    i += 1
        print(i)
    with HiddenPrints():
        river_drainage()
    # mouths = sorted([h for h in hexes if hexes[h]['coastal'] and terrain[h]['drain_in']], key=lambda x: climate[x]['drainage'], reverse=True)
    # for h in mouths:
    #     current = set(terrain[m]['drain_in'])
    #     visited = set([m])
    #     while len(current):
    #         for c in list(current):
    #             terrain[c]['altitude'] = max(terrain[c]['altitude'], hexes[terrain[c]['drain_out'][0]]['altitude'] + 30)
    #             current |= set(terrain[c]['drain_in'])
    #             visited.add(c)
    #         current -= visited


def elevation_png(elevation, year=0, seaLevel=0, map_file=basefile + r'\Elevation\gif', s=0.25):

    gc.collect()
    fig, ax = newMap(0)

    t = sorted([h for h in elevation if elevation[h]['altitude'] > seaLevel], key=lambda h: elevation.get(h, dict()).get('altitude', 0))
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    maxA = max(elevation[h]['altitude'] for h in elevation)
    c = [colors.hex2color(f"#{screen('000000', rescale(elevation.get(h, dict()).get('altitude', 0), seaLevel, 25599 + seaLevel, 0, 255))}") for h in t]
    ax.scatter(x=x, y=y, marker='h', c=c, s=s, edgecolors=c, zorder=100, linewidths=1/8)

    fig.savefig(fr"{map_file}\{year}.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()
    gc.collect()


def assignPlates():

    for p in plates:
        for h in plates[p]:
            hexPlates.update({h: p})
    save_data(hexPlates, fr"{basefile}\hexPlates.json")
    return True


def assignPlateTypes():

    for p in plates:
        plateTypes.update({p: 'oceanic' if np.random.rand() < 0.71 else 'continental'})
    save_data(plateTypes, fr"{basefile}\plateTypes.json")
    return True


def assignEulerRotation():

    for p in plates:
        plateRotations.update({
            p: {
                'pole': random.choice(list(hexMap)),
                'rotation': np.random.normal(loc=0.65, scale=0.05)}})
    return True


def linearVelocity(h):

    omega = plateRotations[hexPlates[h]]['rotation']
    r = hexDistance(h, plateRotations[hexPlates[h]]['pole']) * hexMiles
    v = omega * 10e-6 * r * 1600
    return round(v, 5)


def faultVector(h):

    g = plateRotations[hexPlates[h]]['pole']
    if 'S' in h and 'S' not in g:
        h, g = g, h
    p1, p2 = hexMap[h], hexMap[g]
    if 'S' not in h and 'S' in g:
        p2 = hexTransform(g)
    slope = (p2[0] - p1[0], p2[1] - p1[1])
    slope = (-slope[1], slope[0])
    v = linearVelocity(h)
    w = np.hypot(*slope)
    return slope[0] / w * v, slope[1] / w * v


def faultAngle(h):

    if not isFault(h):
        return None
    pts = [hexMap[n] for n in filter(lambda i: isFault(i) and hexPlates[h] == hexPlates[i], neighbors(h))]
    pts += [hexMap[h]]
    res = scipy.stats.linregress([p[0] for p in pts], [p[1] for p in pts])
    theta = math.degrees(math.atan2(res.slope, 1))
    return theta


def faultType(h):

    theta = faultAngle(h)
    ptsAlpha = [faultVector(n) for n in filter(lambda i: isFault(i) and hexPlates[h] == hexPlates[i], neighbors(h))] + [faultVector(h)]
    ptsAlpha = avg([p[0] for p in ptsAlpha]), avg([p[1] for p in ptsAlpha])
    ptsBeta = [faultVector(n) for n in filter(lambda i: isFault(i) and hexPlates[h] != hexPlates[i], neighbors(h))]
    ptsBeta = avg([p[0] for p in ptsBeta]), avg([p[1] for p in ptsBeta])
    alpha = math.degrees(math.atan2(*ptsAlpha[::-1]))
    beta = math.degrees(math.atan2(*ptsBeta[::-1]))
    d = ptsAlpha[0] + ptsBeta[0], ptsAlpha[1] + ptsBeta[1]
    phi = math.degrees(math.atan2(*d[::-1]))
    delta = phi - theta if phi - theta > -180 else phi - theta + 180
    faultStrength = np.hypot(*ptsAlpha) + np.hypot(*ptsBeta)
    g = [n for n in filter(lambda i: isFault(i) and hexPlates[h] != hexPlates[i], neighbors(h))][0]
    if getRawTarget(g, beta) in plates[hexPlates[h]] and getRawTarget(h, alpha) in plates[hexPlates[g]]:
        faultType = 'C'
    elif getRawTarget(g, beta) in plates[hexPlates[g]] and getRawTarget(h, alpha) in plates[hexPlates[h]]:
        faultType = 'D' if abs(delta) > 30 else 'T'
    else:
        faultType = 'C'
        faultStrength = np.hypot(*d)
    return {'type': faultType, 'strength': faultStrength}


def isFault(h):

    return any(hexPlates[n] != hexPlates[h] for n in neighbors(h))


def plateVoronoi():

    for h in hexMap:
        p = min(plates, key=lambda i: hexDistance(h, i))
        plates[p].add(h)
    save_data(plates, basefile + r'\plates.p')
    return True


def platesGrow():

    current = {p: set([p]) for p in plates}
    for p in plates:
        plates[p].clear()
    # platesTemp = {p: set([p]) for p in plates}
    done = set(plates)
    while any(current[p] for p in plates):
        for p in plates:
            if not current[p]:
                continue
            for c in list(current[p]):
                possibles = [n for n in neighbors(c) if n not in done]
                if not possibles:
                    current[p].remove(c)
                    continue
                for possible in np.random.choice(possibles, size=random.randrange(1, max((len(possibles) + 1) // 2, 2))):
                    done.add(possible)
                    plates[p].add(possible)
                    current[p].add(possible)
                # if all(n in done for n in neighbors(c)):
                current[p].remove(c)
        print(len(plates[p]))

    hexPlates.clear()
    for p in plates:
        for h in plates[p]:
            hexPlates.update({h: p})

    current = set([h for h in hexMap if h not in hexPlates])
    current -= set([c for c in current if all(n not in hexPlates for n in neighbors(c))])
    while current:
        for c in list(current):
            n = [n for n in neighbors(c) if n in hexPlates]
            if not n:
                continue
            else:
                n = n[0]
                # print(c, n)
            p = hexPlates[n]
            hexPlates.update({c: p})
            plates[p].add(c)
            current.discard(c)
            for n in neighbors(c):
                if n not in hexPlates:
                    current.add(n)
        print(len(current), len(hexPlates))

    hexPlates.clear()
    for p in plates:
        for h in plates[p]:
            hexPlates.update({h: p})

    save_data(hexPlates, fr"{basefile}\hexPlates.json")
    assignPlateTypes()

    plateColors = {p: randcolor() for p in plates}

    gc.collect()
    fig, ax = newMap(0)

    t = list(hexPlates)
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    c = [hexColor(plateColors[hexPlates[h]]) for h in t]
    ax.scatter(x=x, y=y, marker='h', c=c, s=0.25, zorder=100, linewidths=0.25)

    # t = list(plates)
    # yh = {h: hexMap[h][0] for h in t}
    # xh = {h: hexMap[h][1] for h in t}
    # y = [yh[h] for h in yh.keys()]
    # x = [xh[h] for h in xh.keys()]
    # ax.scatter(x=x, y=y, marker='*', color=colors.hex2color('#ff0000'), s=5, zorder=100)

    fig.savefig(fr"{basefile}\Elevation\platesGen4.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def generateTectonics():

    platesGrow()
    assignPlates()
    assignPlateTypes()
    assignEulerRotation()

    plateColors = {p: randcolor() for p in plates}

    gc.collect()
    fig, ax = newMap(0)

    t = list(hexPlates)
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    c = [hexColor(f"{screen(plateColors[hexPlates[h]], 0 if plateTypes[hexPlates[h]] != 'oceanic' else 0.75 * 255)}") for h in t]
    ax.scatter(x=x, y=y, marker='h', c=c, s=0.25, edgecolors=c, zorder=100, linewidths=0.25)

    t = list(filter(lambda h: isFault(h), hexMap))
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    f = {h: faultType(h) for h in t}
    minF = min(f[h]['strength'] for h in t)
    maxF = max(f[h]['strength'] for h in t)
    c = [hexColor({'C': 'ff0000', 'D': '0000ff', 'T': '00ff00'}[f[h]['type']]) for h in t]
    s = [(f[h]['strength'] - minF) / (maxF - minF) * 10 for h in t]
    ax.scatter(x=x, y=y, marker='.', c=c, s=s, zorder=100, linewidths=0.25)

    t = list(plates)
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    ax.scatter(x=x, y=y, marker='*', color=hexColor('ff0000'), s=5, zorder=100)

    fig.savefig(fr"{basefile}\Elevation\platesGen5.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def terrain_randomize(h, y, freq=1000, z=5):

    n = noise.pnoise3(hexdict[h][0][0] / freq, hexdict[h][0][1] / freq, y / freq, octaves=2)
    return round(z * n)


def applySeaLevel(seaLevel=0):

    for h in elevation:
        if elevation[h]['altitude'] > seaLevel:
            elevation[h]['altitude'] -= seaLevel
        else:
            elevation[h]['altitude'] = -1


def terrainGenerate_v1(start=0, steps=100, randomize=0, _basins=False, deposition=True, coastal=False,
                regime=False, fresh=True, verbose=False, uplift=True, keep_basins=False, _noise=False):

    seaLevel = seaLevelGet(verbose=verbose)
    ocean = oceanGet(seaLevel=seaLevel)
    if fresh:
        upliftRescale()
        elevation.clear()
        # elevation.update({h: {'altitude': int(25599 * max(0, tectonics[h]['uplift'])) + random.randint(-10, 10)} for h in tectonics.keys()})
        elevation.update({h: {'altitude': random.randint(0, 1000)} for h in tectonics.keys()})
        seaLevel = seaLevelGet(verbose=verbose)
        ocean = oceanGet(seaLevel=seaLevel)
        applyHypsometry(seaLevel=seaLevel, ocean=ocean)
        # altitudeRescale(z=25599, seaLevel=seaLevel)
        # weiZouDong(seaLevel=seaLevel, verbose=verbose)
        elevation_png(elevation=elevation, year=0, seaLevel=seaLevel)
    flattenPlateaus(verbose=verbose)
    drainageSet(seaLevel=seaLevel, verbose=verbose, ocean=ocean)
    maxA = max(elevation[h]['altitude'] for h in elevation)
    y = start
    # while max(elevation[h]['altitude'] for h in elevation) < 25599:
    while y < steps + 1 and maxA - seaLevel <= 25599:
        try:
            # maxZ = max(elevation, key=lambda h: elevation[h]['altitude'])
            # minZ = min(elevation, key=lambda h: elevation[h]['altitude'])
            # s = elevation[maxZ]['altitude'] - elevation[elevation[maxZ]['drain_out'][0]]['altitude']
            # print(f"{y}, {seaLevel}, {minZ}, {elevation[minZ]['altitude']}, {maxZ}, {elevation[maxZ]['altitude']:0.0f}, {elevation[maxZ]['altitude'] - seaLevel:0.0f}, {s:0.0f}")
            # maxA = elevation[maxZ]['altitude']
            applyHypsometry(seaLevel=seaLevel, ocean=ocean)
            erosionAll(verbose=verbose, deposition=deposition, coastal=coastal, seaLevel=seaLevel, ocean=ocean, alpha=1)
            # seaLevel = seaLevelGet(verbose=verbose)
            # applySeaLevel(seaLevel=seaLevel)
            ocean = oceanGet(seaLevel)
            maxA = max(elevation[h]['altitude'] for h in elevation)
            # if _noise and y and y % 10 and not y % 5:
            #     elevation_noise(sea_level=seaLevel, keep_basins=keep_basins, p=0.025)
            if not y % 10:
                seaLevel = seaLevelGet(verbose=verbose)
                # altitudeRescale(z=maxA + seaLevel, seaLevel=seaLevel)
                elevation_png(elevation=elevation, year=y, seaLevel=seaLevel)
            if y > 10 and not (y - 1) % 25:
                weiZouDong(seaLevel=seaLevel, verbose=verbose)
            maxA = max(elevation[h]['altitude'] for h in elevation)
            print(y, maxA - seaLevel)
            y += 1
        except KeyboardInterrupt:
            elevation_png(elevation=elevation, year=y, seaLevel=seaLevel)
            save_data(elevation, basefile + r'\elevation.json')
            raise KeyboardInterrupt
    seaLevel = seaLevelGet(verbose=verbose)
    applyHypsometry(seaLevel=seaLevel, ocean=ocean)
    weiZouDong(seaLevel=seaLevel, verbose=verbose)
    erosionAll(verbose=verbose, deposition=deposition, coastal=coastal, seaLevel=seaLevel, ocean=ocean, alpha=1)
    elevation_png(elevation=elevation, year=y + 1, seaLevel=seaLevel)
    save_data(elevation, basefile + r'\elevation.p')


def terrainGenerate_v2(y=0, steps=100, randomize=0, uplift=True, deposition=True, coastal=False, fresh=True, verbose=False):

    upliftRescale()
    elevation.clear()
    elevation.update({h: {'altitude': int(25599 * max(0, tectonics[h]['uplift'])) + random.randint(-10, 10)} for h in tectonics.keys()})
    seaLevel = seaLevelGet(verbose=verbose)
    ocean = oceanGet(seaLevel=seaLevel)
    maxA = max(elevation[h]['altitude'] for h in elevation)
    elevation_png(elevation=elevation, year=y, seaLevel=seaLevel)
    altitudeRescale(z=25599, seaLevel=seaLevel)
    elevation_png(elevation=elevation, year=y + 1, seaLevel=seaLevel)
    applyHypsometry(seaLevel=seaLevel, ocean=ocean)
    flattenPlateaus(verbose=verbose)
    elevation_png(elevation=elevation, year=y + 2, seaLevel=seaLevel)
    drainageSet(seaLevel=seaLevel, verbose=verbose, ocean=ocean)
    while y < steps + 1:
        erosionAll(verbose=verbose, uplift=not y % 5, deposition=deposition, coastal=coastal, seaLevel=seaLevel, ocean=ocean, alpha=1)
        altitudeRescale(z=25599, seaLevel=seaLevel)
        seaLevel = seaLevelGet(verbose=False)
        ocean = oceanGet(seaLevel=seaLevel)
        applyHypsometry(seaLevel=seaLevel, ocean=ocean)
        seaLevel = seaLevelGet(verbose=False)
        ocean = oceanGet(seaLevel=seaLevel)
        if not y % 10:
            elevation_png(elevation=elevation, year=y, seaLevel=seaLevel)
        print(y, seaLevel)
        y += 1

    seaLevel = seaLevelGet(verbose=False)
    applyHypsometry(seaLevel=seaLevel, ocean=ocean)
    weiZouDong(seaLevel=seaLevel, verbose=True)
    erosionAll(verbose=False, uplift=False, deposition=False, coastal=False, seaLevel=seaLevel, ocean=ocean, alpha=1)
    elevation_png(elevation=elevation, year=y, seaLevel=seaLevel)
    drainageSet(seaLevel=seaLevel, verbose=False, ocean=ocean)
    save_data(elevation, basefile + r'\elevation.json')


def tectonics_get_plates(sigma_p=10):

    with open(basefile + r'\Tectonics.svg', 'r') as f:
        tectonicmap = bs(f, 'lxml')

    faults = dict()
    for f in tectonicmap.find(id='Gen-Faults').find_all('path'):
        cat = get_property(f, 'stroke')
        strength = float(get_property(f, 'stroke-width'))
        cat = 'C' if cat == '#ff0000' else ('D' if cat == '#0000ff' else 'T')
        p = parse_coordinate_list(clean_path(f['d']))
        p = (
            (p[0][0] + p[-1][0]) / 2,
            (p[0][1] + p[-1][1]) / 2
        )
        # fix_id = lambda h: h if not h.startswith('208') else '206' + ('S' if 'N' in h else 'S') + h[4:]
        faults.update({
            # (fix_id(latlong(p[0], id=True)), fix_id(latlong(p[-1], id=True))): [
            latlong(p, id=True): [
                cat,
                strength
            ]
        })

    print('Assigning tectonic distances...')
    c_faults = {f: faults[f] for f in faults if faults[f][0] == 'C'}
    c_faults = poissonSample(c_faults.keys(), r=int(sigma_p / 2))
    d_faults = {f: faults[f] for f in faults if faults[f][0] == 'D'}
    d_faults = poissonSample(d_faults.keys(), r=int(sigma_p / 2))
    t_faults = {f: faults[f] for f in faults if faults[f][0] == 'T'}
    t_faults = poissonSample(t_faults.keys(), r=int(sigma_p / 2))
    print(f"C: {len(c_faults)}, D: {len(d_faults)}, T: {len(t_faults)}")

    def near_fault(h, sigma_p, faults=c_faults | d_faults | t_faults):

        if not h:
            return False
        current = set([h])
        visited = set()
        d = 0
        while d < sigma_p:
            for c in list(current):
                if c in visited:
                    continue
                if c in faults:
                    return True
                current |= {n for n in all_neighbors[c] if n not in visited}
                visited.add(h)
                current.discard(h)
            d += 1
            # print(d, len(current), len(visited))
        else:
            return False

    near_faults = [h for h in hexdict if near_fault(h, sigma_p)]

    h = np.random.choice(list(hexdict))
    done = set()
    while h in faults or h in near_faults or h in done:
        h = np.random.choice(list(hexdict))
        print(h, h in faults, near_fault(h, sigma_p), h in done)
    p = 1
    dj = len(done) + 1
    while len(done) != dj:
        for i in hexdict:
            if i in done:
                continue
            if i in near_faults:
                continue
            h = i
            print(h)
            break
            # try:
            #     h = np.random.choice(list(hexdict) if p < 25 else [i for i in hexdict if i not in done and i not in near_faults])
            #     print(h, h in faults, h in near_faults, h in done)
            # except:
            #     break
        visited = set()
        current = {n for n in all_neighbors[h] if n not in visited}
        while current:
            for c in list(current):
                for n in all_neighbors[c]:
                    if n in visited:
                        continue
                    if n in done:
                        continue
                    if n in current:
                        continue
                    if near_fault(n, sigma_p):
                        continue
                    current.add(n)
                visited.add(c)
                current.discard(c)
            print(p, len(current), len(visited))
        else:
            for v in visited:
                elevation[v]['plate'] = p
            p += 1
            dj = len(done)
            done |= visited

    plates = {i: {e for e in elevation if elevation[e].get('plate') == i} for i in range(1, p)}

        # plate cleanup
        # for h in list(plates[0]):
        #     if 'plate' not in elevation[h]:
        #         continue
        #     for n in all_neighbors[h]:
        #         if 'plate' not in elevation[n]:
        #             continue
        #         elif elevation[n]['plate'] != 0:
        #             elevation[h]['plate'] = elevation[n]['plate']
        #             plates[0].discard(h)
        #             plates[elevation[n]['plate']].add(h)
        #             break

    while any('plate' not in elevation[n] for n in elevation):
        for c in elevation:
            if 'plate' in elevation[c]:
                continue
            else:
                for n in all_neighbors[c]:
                    if 'plate' in elevation[n]:
                        elevation[c]['plate'] = elevation[n]['plate']
                        plates[elevation[n]['plate']].add(c)
                # for c in list(current):
                #     if c in visited:
                #         continue
                #     for n in all_neighbors[c]:
                #         if n in visited:
                #             continue
                #         if elevation[n].get('plate') == p:
                #             visited.add(n)
                #             current.discard(n)
                #         if not elevation[n].get('plate'):
                #             visited.add(n)
                #             current.discard(n)
                #             elevation[n]['plate'] = p
                #             plates[p].add(n)
                #     visited.add(c)
                #     current.discard(c)
        print(sum(len(plates[p]) for p in plates))

    save_data(plates, basefile + r'\plates.p')

    def get_neighbors_within(h, r=sigma_p):

        current = {h}
        visited = set()
        while current:
            for c in list(current):
                if c in visited:
                    current.discard(c)
                    continue
                if hexDistance(h, c) > r:
                    current.discard(c)
                    # visited.add(c)
                    continue
                else:
                    current |= set(all_neighbors[c])
                    current.discard(c)
                    visited.add(c)
            # print(current, visited)
        return visited


    plate_influences = {p: {i: set() for i in 'CDT'} for p in plates}
    for f in c_faults:
        p = tectonics[f]['plate']
        plate_influences[p]['C'].add(f)
        for n in get_neighbors_within(f, sigma_p):
            p = tectonics[n]['plate']
            plate_influences[p]['C'].add(f)
    for f in d_faults:
        p = tectonics[f]['plate']
        plate_influences[p]['D'].add(f)
        for n in get_neighbors_within(f, sigma_p):
            p = tectonics[n]['plate']
            plate_influences[p]['D'].add(f)
    for f in t_faults:
        p = tectonics[f]['plate']
        plate_influences[p]['T'].add(f)
        for n in get_neighbors_within(f, sigma_p):
            p = tectonics[n]['plate']
            plate_influences[p]['T'].add(f)

    save_data(plates, basefile + r'\plates.p')
    save_data(plate_influences, basefile + r'\plate_influences.p')

    elevation_map(elevation={t: {'altitude': 100 * (2 * len(plate_influences[elevation[t]['plate']]['C']) - len(plate_influences[elevation[t]['plate']]['D']) + len(plate_influences[elevation[t]['plate']]['C']))} for t in tectonics}, year=1000, sea_level=-100, s=1/2)


def weiZouDong(e=1, seaLevel=0, ocean=None, verbose=False):

    if not seaLevel:
        seaLevel = seaLevelGet()
        ocean = oceanGet(seaLevel=seaLevel)

    current = sorted([h for h in set(elevation) - ocean if any(n in ocean for n in neighbors(h))], key=lambda h: elevation[h]['altitude'], reverse=False)
    W = {h: 25599 + seaLevel for h in set(elevation) - ocean if elevation[h]['altitude'] > seaLevel}
    P, Q = set(), set()
    for h in current:
        W[h] = elevation[h]['altitude']
        P.add(h)
    while P or Q:
        if P:
            c = max(P, key=lambda h: elevation[h]['altitude'])
            P.remove(c)
        else:
            c = max(Q, key=lambda h: elevation[h]['altitude'])
            Q.remove(c)
            if elevation[c]['altitude'] >= W[c]:
                continue
        for n in neighbors(c):
            if n in ocean:
                continue
            if elevation[n]['altitude'] >= W[n]:
                continue
            if elevation[n]['altitude'] >= (W[c] + e):
                W[n] = elevation[n]['altitude']
                P.add(n)
            elif W[n] > (W[c] + e):
                W[n] = W[c] + e
                Q.add(n)
        if verbose: print(c, elevation[c]['altitude'], len(P), len(Q))
        for c in list(Q):
            if elevation[c]['altitude'] >= W[c]:
                Q.remove(c)
    for h in elevation:
        if h in W:
            elevation[h]['altitude'] = W[h]

