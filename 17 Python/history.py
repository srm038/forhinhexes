from currents import *
from demographics import *
from mapping import assign_terrain

load_data(all_neighbors, all_neighbors_file)
load_data(cities, cities_file)
load_data(hegemonies, hegemonies_file)
load_data(seaways, seaways_file)
load_data(destroyed, destroyed_file)
load_data(wars, wars_file)
load_data(citysizes, city_size_file)
load_data(hegemonysizes, hegemony_size_file)
load_data(battles, battles_file)
load_data(asabiya, asabiya_file)
load_data(sociology, sociology_file)
load_data(terrain, terrain_file)
load_data(climate, climate_file)

# TODO check max name length, seems a bit too long for some languages

from desirability import *
from infrastructure import *


def writelog(historytext, file=basefile + r'\history.txt'):

    # print(string)
    with open(file, 'a', encoding='utf-8') as f:
        for i in historytext:
            f.write(f'{i[0]}, {i[1]}\n')
    historytext.clear()


def cities_clear():

    for h, hex in sociology.items():
        hex['city'] = None
        hex.get('ruins', dict()).clear()
        hex.get('castles', dict()).clear()
        if 'population' in hex:
            hex['population'] = 0
        if 'race' in hex:
            del hex['race']
    cities.clear()
    destroyed.clear()
    hegemonies.clear()
    wars.clear()
    battles.clear()
    citysizes.clear()
    hegemonysizes.clear()
    asabiya.clear()

    with open(cities_file, 'wb') as f:
        pickle.dump(cities, f)
    with open(hegemonies_file, 'wb') as f:
        pickle.dump(hegemonies, f)
    with open(destroyed_file, 'wb') as f:
        pickle.dump(destroyed, f)
    with open(wars_file, 'wb') as f:
        pickle.dump(wars, f)
    with open(battles_file, 'wb') as f:
        pickle.dump(battles, f)
    with open(city_size_file, 'wb') as f:
        pickle.dump(citysizes, f)
    with open(hegemony_size_file, 'wb') as f:
        pickle.dump(hegemonysizes, f)


def cities_draw(year, w=250, k=0.15, s=1/2):

    m = max(cities.get(c, dict()).get('population', 0) + sociology[c].get('population', 0) for c in hexes)
    hinterlands = [h for h in hexes if 'hinterland' in sociology[h] and h not in cities]
    fig, ax = plt.subplots()
    # fig = plt.figure()
    # ax = plt.Axes(fig, [0., 0., 1., 1.])
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b])-50, max([i[1] for i in b])+50
    ylim = min([i[0] for i in b])-50, max([i[0] for i in b])+50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], colors.hex2color('#bad9ff'))

    yh = {h: hexdict[h][0][0] for h in hexes.keys() if hexdict[h][0][1] >= hexdict['207-518W'][0][1]}
    xh = {h: hexdict[h][0][1] + 50 for h in hexes.keys() if hexdict[h][0][1] >= hexdict['207-518W'][0][1]}
    y = [yh[h] for h in yh.keys() if h in hexes]
    x = [xh[h] for h in xh.keys() if h in hexes]
    ax.scatter(x=x, y=y, marker='h', c='w', edgecolors='w', s=1/4, zorder=100)

    t = sorted([h for h in cities.keys()] + [h for h in hinterlands], key=lambda h: sociology[h].get('population', 0) + cities.get(h, dict()).get('population', 0))
    y = [hexdict[h][0][0] for h in t]
    x = [hexdict[h][0][1] + 50 for h in t]
    c = {h: hegemonies.get(cities[h].get('hegemony', dict()) if h in cities else sociology[h].get('hinterland', dict()),
                        dict()).get('color', 'ffffff') for h in t}
    c = [colors.hex2color(f"#{screen(c[h], w - w * ((sociology.get(h, dict()).get('population', 0) + cities.get(h, dict()).get('population', 0)) / m) ** k)}") for h in c]
    # c += [colors.hex2color(f"#{screen(hegemonies.get(sociology[h]['hinterland'], dict()).get('color', 'ffffff'), w - w * ((sociology[h].get('population', 0)) / m) ** k)}") for h in hinterlands]
    ax.scatter(x=x, y=y, marker='h', c=c, s=s, edgecolors=c, zorder=100, linewidths=s/2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    # fig.set_size_inches(6, 4)
    ax.annotate(f"{year: 5}", (0.0, 0.0), xycoords='axes fraction', size=12, fontfamily='monospace')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(basefile + fr'\Hegemony\gif\{year:05}.png', dpi=300, bbox_inches='tight', pad_inches = 0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def population(t, K, r=0.49 / 100, p0=150):

    return round(
        K / (1 + (K - p0) / p0 * math.exp(-r * t))
    )


# def founded(pop, now):
#
#     r = (random.randint(-10, 0) + random.randint(0, 10) + 49) / 10000
#     # r = 0.49 / 100
#     p0 = 30
#     return round(now - math.log(pop / p0) / r)


def cities_save():

    save_data(cities, cities_file)
    save_data(hegemonies, hegemonies_file)
    with open(destroyed_file, 'wb') as f:
        pickle.dump(destroyed, f)
    with open(wars_file, 'wb') as f:
        pickle.dump(wars, f)
    with open(asabiya_file, 'wb') as f:
        pickle.dump(asabiya, f)

    with open(battles_file, 'rb') as f:
        battles_f = pickle.load(f)
    for battle in battles:
        battles_f.setdefault(battle, dict()).update(battles[battle])
    with open(battles_file, 'wb') as f:
        pickle.dump(battles_f, f)
    battles.clear()

    with open(city_size_file, 'rb') as f:
        citysizes_f = pickle.load(f)
    for city in citysizes:
        citysizes_f.setdefault(city, dict()).update(citysizes[city])
    with open(city_size_file, 'wb') as f:
        pickle.dump(citysizes_f, f)
    citysizes.clear()

    with open(hegemony_size_file, 'rb') as f:
        hegemonysizes_f = pickle.load(f)
    for city in hegemonysizes:
        hegemonysizes_f.setdefault(city, dict()).update(hegemonysizes[city])
    with open(hegemony_size_file, 'wb') as f:
        pickle.dump(hegemonysizes_f, f)
    hegemonysizes.clear()


def cities_svg():

    with open(basefile + r'\Cities.svg', 'r') as f:
        file = bs(f, 'lxml')

    string = ''
    for c, city in cities.items():
        if city['hegemony'] not in hegemonies:
            continue
        if c == hegemonies[city['hegemony']]['capital']:
            continue
        size = round(rescale(math.sqrt(city['population']), math.sqrt(150), math.sqrt(1e5), 2, 15))
        if 'castles' in sociology[c]:
            string += f'<rect x="{hexdict[c][0][0] - size / 2:0.2f}" y="{hexdict[c][0][1] + 50 - size / 2:0.2f}" ' \
                f'width="{size:0.2f}" height="{size:0.2f}"" id="city-{c}" class="city" ' \
                f'style="color:#{recipe[city["race"]]["color"]}"/>'
        else:
            string += '<circle cx="{:}" cy="{:}" r="{:0.2f}" id="city-{:}" class="city" style="color:#{:}"/>'.format(
                hexdict[c][0][0],
                hexdict[c][0][1] + 50,
                # clip(round((cities[c]['population'] - 150) * (20 - 3) / (1e5 - 150) + 3, 2), 0, 20),
                size,
                c,
                recipe[city['race']]['color']
            )
    for c, hex in sociology.items():
        if c in cities:
            continue
        if 'ruins' in hex and hex['ruins']:
            size = round(rescale(150, math.sqrt(150), math.sqrt(1e5), 4, 15) / 2)
            string += f'<rect x="{hexdict[c][0][0] - size / 2:0.2f}" y="{hexdict[c][0][1] + 50 - size / 2:0.2f}" ' \
                f'width="{size:0.2f}" height="{size:0.2f}"" id="city-{c}" class="city" ' \
                f'style="color:black" transform="rotate(45 {hexdict[c][0][0] - size / 2:0.2f} {hexdict[c][0][1] + 50 - size / 2:0.2f})"/>'

    if not string:
        file.find('g', id='cities').clear()
    else:
        file.find('g', id='cities').contents = bs(string, 'lxml').find('body').contents

    string = ''
    for c, city in cities.items():
        if city['hegemony'] not in hegemonies:
            continue
        if c != hegemonies[city['hegemony']]['capital']:
            continue
        size = round(rescale(math.sqrt(city['population']), math.sqrt(150), math.sqrt(1e5), 4, 15))
        if 'castles' in sociology[c]:
            string += f'<rect x="{hexdict[c][0][0] - size / 2:0.2f}" y="{hexdict[c][0][1] + 50 - size / 2:0.2f}" ' \
                f'width="{size:0.2f}" height="{size:0.2f}"" id="city-{c}" class="city" ' \
                f'style="color:#{recipe[city["race"]]["color"]}"/>'
        else:
            string += '<circle cx="{:}" cy="{:}" r="{:0.2f}" id="city-{:}" class="city" style="color:#{:}"/>'.format(
                hexdict[c][0][0],
                hexdict[c][0][1] + 50,
                # clip(round((cities[c]['population'] - 150) * (20 - 3) / (1e5 - 150) + 3, 2), 0, 20),
                size,
                c,
                recipe[city['race']]['color']
            )

    if not string:
        file.find('g', id='capitals').clear()
    else:
        file.find('g', id='capitals').contents = bs(string, 'lxml').find('body').contents

    save(file, extra_file=basefile + r'\Cities.svg')


def cities_png():

    fig, ax = newMap(0)
    s = 1 / 2
    t = sorted([h for h in cities if hexdict[h][0][1] >= hexdict['207-518W'][0][1]],
               key=lambda h: cities[h]['population'])
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    m = max(city['population'] for city in cities.values())
    # max_c = max(current_sst(h, season) for h in t)
    col = lambda h: screen(recipe[cities[h]['race']]['color'], 255 - 255 * cities[h]['population'] / m)
    c = np.array([colors.hex2color(f"#{col(h)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)
    fig.savefig(basefile + fr'\Population\cities.png', dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def cities_filter(x, race):

    temp = [h for h in terrain if h not in cities and sociology[h]['desirability'][race]]


def city_k(x, race, a=1.25, b=10, verbose=False, ignore=None):

    # K0 = 700 * math.exp(10 * (sociology[x]['desirability_raw'][race] - 1 / 2)) * min(1, recipe[race]['modR'])
    ignore = set(ignore) if ignore else set()
    K0 = 1e5 * sociology[x]['desirability_raw'][race] * min(1, recipe[race]['modR'])
    if K0 < 100:
        return 0
    K0 /= 5 if race == 'orc' else 1
    if race == 'orc':
        return clip(int(K0), 200, 1.5e5)
    level = lambda x: round(math.log(x, b))
    k_level = level(K0)
    # if verbose: print(i, 2 ** (i + 1), '-', level, f'{K0:.0f}')
    while k_level:
        # print(level)
        limit = round(a ** (level(K0) + 1))
        try:
            closest = 0
            current = {n for n in all_neighbors[x] if n in terrain}
            visited = {x}
            while current:
                for c in list(current):
                    if c in cities and level(cities[c]['K']) == k_level and c not in ignore:
                        current = set()
                        break
                    if closest >= limit:
                        current = set()
                        break
                    if c in visited:
                        current.remove(c)
                        continue
                    current |= {n for n in all_neighbors[c] if n in terrain}
                    visited.add(c)
                    current.remove(c)
                closest += 1
        except ValueError:
            k_level -= 1
            continue
        except KeyboardInterrupt:
            print(level(K0), limit, closest, K0, x, race)
            raise KeyboardInterrupt
        if limit > closest:
            K0 /= b
        if verbose: print(level(K0), limit, closest, f'{K0:.0f}')
        if K0 < 150:
            return 0
        k_level -= 1
    kmax = 1.5e5 if elevation[x]['drainage'] >= 3 else 1000 + random.randint(0, 1000)
    return round(clip(int(K0), 200, kmax))


def distance_to_civilization(h, race, limit=7):

    if 'hinterland' in hexes[h] and cities.get(hegemonies[sociology[h]['hinterland']]['capital'], dict()).get('race') == race:
        return 0

    current = {h}
    visited = set()
    d = 0

    while current and d < limit:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            if c in hexes and 'hinterland' in hexes[c] and cities.get(hegemonies[sociology[c]['hinterland']]['capital'], dict()).get('race') == race:
                return d
            if c in cities and cities[c]['race'] == race:
                return d
            if d >= limit:
                return limit + 1
            for n in all_neighbors[c]:
                current.add(n)
                # if n in hexes:
                #     if n in cities:
                #         if cities[hegemonies[cities[n]['hegemony']]['capital']]['race'] == race:
                #             current.add(n)
                #     if 'hinterland' in hexes[n] and sociology[n]['hinterland'] in hegemonies:
                #         if cities.get(hegemonies[sociology[n]['hinterland']]['capital'], dict()).get('race') == race:
                #             current.add(n)
            visited.add(c)
            current.remove(c)
        # print(current)
        d += 1
    return limit + 1


def cities_place(year=0, reset=False, formation_chance=0.5, limit=7, threshold=0.15, targets=None):

    if reset:
        cities_clear()
    cities0 = len(cities)
    string = ''
    migration_targets = targets
    if not migration_targets:
        migration_targets = {race: great_migration(race, limit=limit, threshold=threshold) for race in races}
    for race in races:
        i = 0
        # n = random.choice(list(range(1, ceil(recipe[race]['modR']) + 1)))
        if random.random() >= new_cities_chance(race):
            continue
        n = npr.choice([0, 1, 2], p=[0.875, 0.1, 0.025]) if len([c for c in cities if cities[c]['race'] == race]) else 1
        if year == 0:
            n = 1
        # md = max(sociology[h]['desirability'][race] for h in hexes if h not in cities) # probably slowing things down
        targets = list(migration_targets[race])
        if not targets:
            targets = [h for h in hexes if sociology[h]['desirability_raw'][race] >= threshold]
        targets.sort(key=lambda x: sociology[x]['desirability_raw'][race], reverse=True)
        while targets and n and i < n:
            for h in targets:
                if i >= n:
                    break
                if h in hegemonies:
                    continue
                if random.random() < max(1e-3, formation_chance + math.log10(recipe[race]['modR'])):
                    new = city_new(h, year, race)
                    if not new:
                        continue
                    # print('X')
                    i += 1
                    # if cities[h]['K'] < 150:
                    #     del cities[h]
                    #     continue
                    hexes[h].update({'city': True})
                    historytext.append((year, f'{cities[h]["name"]} ({race}) was founded (K = {cities[h]["K"]:.0f}) and the {hegemonies[cities[h]["hegemony"]]["name"]} hegemony formed'))

    # print('Year {:}: {:} cities placed.'.format(year, len(cities) - cities0))
    # cities_save()


def city_resize_zipf(h, A, cs, HNs, s):

    if not A:
        A = max([cities[c]['population'] for c in cities])
    if not cs:
        cs = sorted(list(set([cities[c]['population'] for c in cities])), reverse=True)
    k = cs.index(cities[h]['population']) + 1
    if not s:
        s = 1
    if not HNs:
        HNs = sum(1 / i ** s for i in range(1, len(cities) + 1))
    return A / k ** s / HNs


def cities_resize(verbose=False):

    cs = sorted([c for c, city in cities.items()], key=lambda x: cities[x]['t0'])
    for i, c in enumerate(cs):
        race = cities[c]['race']
        k = cities[c]['K']
        k_new = city_k(c, race, ignore=cs[i:])
        if 0 < k_new < k:
            if verbose: print(f'{cities[c]["t0"]} {cities[c]["name"]} has been downsized from {k} to {k_new}')
            cities[c]['K'] = clip(k_new, 1, 1.5e5)
        if k < k_new:
            if verbose: print(f'{cities[c]["t0"]} {cities[c]["name"]} has been enlarged from {k} to {k_new}')
            cities[c]['K'] = clip(k_new, 1, 1.5e5)


def city_new(h, year, race, p0=150, mother=None, hegemony=None, new_name=True):

    name = markov_word(names[race], word_length=random.randint(4, 6)).title()
    if new_name:
        if name in citysizes.keys() or name in [hegemonies[heg]['name'] for heg in hegemonies]:
            name = f"New {name.title()}"
    else:
        while name in citysizes.keys() or name in [hegemonies[heg]['name'] for heg in hegemonies]:
            name = markov_word(names[race], word_length=random.randint(4, 6)).capitalize()
    k = city_k(h, race)
    if not k:
        return False
    cities.update({
        h: {
            'population': p0,
            'K': k,
            'r': clip(round(recipe[race]['modR'] * random.random() * 0.02 *
                            max(sociology[h]['desirability'][race], sociology[h]['desirability_raw'][race])
                            , 4), 0.5 / 100,
                      2 / 100),
            't0': year,
            'race': race,
            'name': name,
            'mother': mother if mother else h,
            'hegemony': hegemony if hegemony else h,
            'S': 0,
            'W': 0
        }
    })
    if not hegemony:
        sociology[h]['population'] = 150
        hegemony_new(h, year)
    # road_limit[h] = npr.choice(range(1, 4), p=[0.1, 0.45, 0.45])
    add_toponym(h)
    return True


def city_grow(c):

    new = births(cities[c]['population'], cities[c]['K'], cities[c]['r'])
    cities[c]['population'] += new
    return new


def cities_grow():

    for c in cities:
        # cities[c].update({'population': population(P=cities[c]['population'], K=cities[c]['K'], r=cities[c]['r'])})
        # new = births(cities[c]['population'], cities[c]['K'], cities[c]['r'])
        city_grow(c)
        # if new > 0:
        #     writelog('{:} gained {:} through birth'.format(cities[c]['name'], new))
        # elif new < 0:
        #     writelog('{:} lost {:} through death'.format(cities[c]['name'], abs(new)))
        # cities[c]['population'] += new
        cities[c]['K'] = int(clip(cities[c]['K'] + random.gauss(mu=0, sigma=500), 1, 200000))
        cities[c]['r'] = round(clip(cities[c]['r'] + random.gauss(mu=0, sigma=0.0005 / 6), - 2.5 / 100, 1.5 / 100), 4)
        # TODO make the maximum growth rate depend on history


def cities_migrate():
    for c in sorted(cities, key=lambda x: sociology[x]['desirability'][cities[x]['race']]):
        try:
            d = max([n for n in all_neighbors[c] if n in cities and
                     cities[c]['race'] == cities[n]['race'] and
                     cities[n]['population'] < cities[n]['K'] and
                     road_exists(c, n)
                     ], key=lambda x: sociology[x]['desirability'][cities[x]['race']])
        except:
            continue
        if not sociology[c]['desirability'][cities[c]['race']] and not sociology[d]['desirability'][cities[c]['race']]:
            continue
        migrate = (sociology[c]['desirability'][cities[c]['race']] - sociology[d]['desirability'][
            cities[c]['race']]) / 10 / max(sociology[c]['desirability'][cities[c]['race']],
                                           sociology[d]['desirability'][cities[c]['race']]) * cities[c]['population']
        migrate = math.floor(migrate)
        if not migrate:
            continue
        cities[c]['population'] += migrate
        cities[d]['population'] -= migrate
        if migrate < 0:
            c, d = d, c
        # writelog('{:} people moved from {:} to {:}'.format(abs(migrate), cities[c]['name'], cities[d]['name']))


def cities_disasters():
    for c in cities:
        if random.random() > 0.005:
            continue
        new = math.floor(cities[c]['population'] * clip(random.gauss(mu=0.8, sigma=0.3 / 6), None, 0.99))
        # writelog('{:} lost {:} people to disasters'.format(cities[c]['name'], cities[c]['population'] - new))
        cities[c]['population'] = new


def cities_destroy(year):

    for c in list(cities):
        if cities[c]['population'] <= 50:
            historytext.append((year, f'{cities[c]["name"]} was abandoned'))
            sociology[c]['ruins'] = hexes[c].get('ruins', dict()).update({cities[c]['name']: year})
            del cities[c]


def cities_flood_fill(start):
    current = {start}
    visited = set()

    while current:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            current |= set([n for n in all_neighbors[c] if
                            n in cities and cities[n]['race'] == cities[start]['race']])
            visited.add(c)
            current.remove(c)
        # print(len(visited), len(current))

    return visited


def city_expand(city, year):

    if city not in cities:
        return
    if cities[city]['hegemony'] not in hegemonies:
        return
    too_many = lambda h: len(hegemonies[h]['cities']) / (len(hegemonies[h]['cities']) + len(hegemonies[h].get('hinterland', set())))
    if random.random() < too_many(cities[city]['hegemony']):
        return
    # if not (300 < k_limit(city) or k_limit(city) < cities[city]['population']):
    #     return
    elif cities[city]['population'] < 300:
        return
    race = cities[city]['race']
    if year - cities[city].get('expanded', 0) < random.randint(10, 20) - recipe[race]['modR']:
        return
    race = cities[city]['race']
    if race == 'orc':
        return
    if random.random() >= new_cities_chance(race):
        return
    # if random.random() > 10/100 + math.log10(cities[city]['population'])/5:
    #     return
    # targets = [t for t in sociology[city]['neighbors'] if t not in cities and sociology[t]['desirability'][race]]
    targets = None
    # if not targets:
        # if random.random() < 0.5:
        #     return
    if 'hinterland' in hegemonies[cities[city]['hegemony']]:
        targets = [
            t for t in hegemonies[cities[city]['hegemony']]['hinterland'] if
            sociology[t]['desirability_raw'][race] and
            hexDistance(city, t) < 4
            # infrastructure_impact(city, t) > 0.25
        ]
        # else:
        #     targets = sum([[t for t in all_neighbors[n] if t not in cities and sociology[t]['desirability'][race]] for n in
        #                hegemonies[cities[city]['hegemony']]['cities']], [])
    if not targets:
        return
    if random.random() > 0.85: # support city or industrial center?
        targets = sorted(
            targets,
            key=lambda h: sociology[h]['desirability_raw'][race] / (hexDistance(city, h) + 1), reverse=True
        )
    else:
        targets = sorted(targets, key=lambda h: sociology[h]['desirability_raw'][race], reverse=True)
    for target in targets:
        p = random.randint(150, max(150, min(sociology[target].get('infrastructure', 0) * 346 // 10, cities[city]['population'] - 150)))
        # if infrastructure_impact(hegemonies[cities[city]['hegemony']]['capital'], target, p) < rebellion_threshold:
        #     continue

        new = city_new(target, year, race, p0=p, mother=cities[city]['mother'], hegemony=cities[city]['hegemony'])
        if not new:
            continue
            # if cities[target]['K'] < 200:
            #     del cities[target]
            #     return
        cities[city]['population'] -= p
        cities[city]['expanded'] = year
        cities[target]['defeated'] = year
        hegemonies[cities[city]['hegemony']]['cities'].add(target)
        if 'hinterland' in hexes[target]:
            if sociology[target]['hinterland'] not in hegemonies:
                del sociology[target]['hinterland']
                continue
            hegemonies[sociology[target]['hinterland']].get('hinterland', set()).discard(target)
            del sociology[target]['hinterland']
        historytext.append((year, f'Settlers from {cities[city]["name"]} ({cities[city]["population"]}) have founded {cities[target]["name"]} (K = {cities[target]["K"]:.0f})'))
        break


def city_migrate(city, year):

    if city not in cities:
        return
    heg = cities[city]['hegemony']
    if heg not in hegemonies:
        return
    N = cities[city]['population']
    k = min([k_limit(city), 150000, cities[city]['K']])
    if N < k:
        return
    dN = N - k
    targets = [c for c in hegemonies[heg]['cities'] if cities[c]['population'] < min([k_limit(c), 150000, cities[c]['K']])]
    targets.sort(key=lambda c: cities[c]['population'] - min([k_limit(c), 150000, cities[c]['K']]), reverse=True)
    for target in targets:
        if dN <= 0:
            break
        e = min([k_limit(target), 150000, cities[target]['K']]) - cities[target]['population']
        if e < 0:
            continue
        e = clip(random.randint(0, e), None, dN)
        cities[target]['population'] += e
        cities[target].setdefault('migrants', 0)
        cities[target]['migrants'] += e
        cities[city]['population'] -= e
        cities[city].setdefault('migrants', 0)
        cities[city]['migrants'] -= e
        # print(target, e, dN)
        if e > 1000:
            historytext.append((year, f"{e} people moved from {cities[city]['name']} to {cities[target]['name']}"))


# def city_road(city, year):
#
#     if sociology[city].get('infrastructure', 0) < 50:
#         return
#     if len(sociology[city]['road_to']) >= road_limit.get(city, 2):
#         return
#     targets = [h for h in cities if
#                sociology[h].get('infrastructure', 0) >= 50 and len(hexes[h]['road_to']) < road_limit.get(city, 2) and h != city]
#     if not targets:
#         return
#     else:
#         target = max(targets, key=lambda h: cities[h]['population'] / hex_distance(city, h))
#         road = None
#         with stopit.ThreadingTimeout(1):
#             road = road_path(city, target)
#         if not road:
#             return
#         for r in road:
#             if len(hexes[r]['road_to']) >= road_limit.get(r, 2):
#                 return
#         for r, s in zip(road[:-1], road[1:]):
#             hexes[r]['road_to'] |= {s}
#             hexes[s]['road_to'] |= {r}


# def city_seaway(city, year):
#
#     if not sociology[city]['coastal']:
#         return
#     if sociology[city].get('infrastructure', 0) < 75:
#         return
#     if all(cities[n]['population'] < 5000 for n in road_flood_fill(city) if n in cities):
#         return
#     if all(n in temp_ice for n in all_neighbors[city] if n not in hexes):
#         return
#     targets = [h for h in cities if hexes[h]['coastal'] and
#                sociology[h].get('infrastructure', 0) >= 75 and h != city and hex_distance(city, h) <= 150 and
#                cities[h]['population'] >= 5000 and not all(n in temp_ice for n in all_neighbors[h] if n not in hexes)]
#     if not targets:
#         return
#     a = sociology[city].get('harbor', random.choice([n for n in all_neighbors[city] if n not in hexes]))
#     sociology[city]['harbor'] = a
#     for target in sorted(targets, key=lambda x: sociology[x].get('infrastructure', 0), reverse=True):
#         if target in cities[city].get('seaways', set()):
#             continue
#         b = hexes[target].get('harbor', random.choice([n for n in all_neighbors[target] if n not in hexes]))
#         hexes[target]['harbor'] = b
#         seaway0 = None
#         with stopit.ThreadingTimeout(10):
#             seaway0 = sea_route(a, b)
#         if not seaway0 or not seaway0[0]:
#             continue
#         else:
#             # check if something is closer
#             r = [None, None]
#             for stop in seaways_flood_fill(target):
#                 if 'harbor' not in cities[stop]:
#                     continue
#                 r = sea_route(a, cities[stop]['harbor'])
#                 if not r[1]:
#                     continue
#                 if r[1] < seaway0[1]:
#                     seaway0 = r
#             seaway_stops(seaway0)
#             seaway1 = None
#             with stopit.ThreadingTimeout(10):
#                 seaway1 = sea_route(b, a, limit=None)
#             if not seaway1:
#                 continue
#             if seaway1[0]:
#                 r = [None, None]
#                 for stop in seaways_flood_fill(city):
#                     if 'harbor' not in cities[stop]:
#                         continue
#                     r = sea_route(a, cities[stop]['harbor'])
#                     if not r[1]:
#                         continue
#                     if r[1] < seaway0[1]:
#                         seaway1 = r
#                 seaway_stops(seaway1)
#             return


def city_colonize(city, year, verbose=False):

    if cities[city]['population'] < 300:
        return
    if not sociology[city]['coastal']:
        return
    if not [n for n in all_neighbors[city] if n not in hexes]:
        return
    if sociology[city].get('colony', False):
        return
    race = cities[city]['race']
    if random.random() >= new_cities_chance(race):
        return

    a = sociology[city].setdefault('harbor', random.choice([n for n in all_neighbors[city] if n not in hexes]))
    # cities[city]['harbor'] = a
    visited = {a}
    seaway = []
    # seaway = [[city], 0]
    b = a
    while b not in hexes:
        if b not in hexdict:
            return
        b = check_target(b, roundBase(currents[b], base=60))
        visited.add(b)
        if b in seaway:
            return
        if verbose: print(b)
        seaway.append(b)
    target = b
    if len(seaway) < 7:
        if verbose: print('Too close')
        return
    # c = seaway[-2]
    # cities[target]['harbor'] = c
    # seaway = [None]
    # with stopit.ThreadingTimeout(10):
    #     seaway = sea_route(a, c, limit=None)
    # if not seaway[0]:
    #     if verbose: print('No route found')
    #     return
    # if verbose: print(seaway)
    # seaway_stops(seaway)
    if len(seaway) < 5:
        return
    if target not in cities:
        p = 150 #random.randint(1000, max(1000, sociology[target].get('infrastructure', 0) * 346 // 10))
        race = cities[city]['race']
        new = None
        if race != 'orc':
            new = city_new(target, year, race, p0=p, mother=city, hegemony=cities[city]['hegemony'])
        if not new:
            return
        cities[city]['population'] -= p
        cities[target]['harbor'] = seaway[-2]
        cities[city]['colony'] = True
        historytext.append((
            year, f'Colonists from {cities[city]["name"]} ({cities[city]["population"]}) have founded {cities[target]["name"]} (K = {cities[target]["K"]:.0f})'
        ))
    # seaway1 = sea_route(c, a, limit=None)
    # if seaway1[0]:
    #     if verbose: print(seaway1)
    #     seaway_stops(seaway1)
    # seaways.append(seaway)
    # cities[city]['seaways'] = cities[city].get('seaways', []) + [target]
    # cities[target]['seaways'] = cities[target].get('seaways', []) + [city]


def city_loyalty(city, year):

    loyalty = cities[city].get('loyalty', 0)
    heg = cities[city]['hegemony']
    if heg not in hegemonies:
        hegemony_new(city, year)
        cities[city]['loyalty'] = 0
        return
    cap = hegemonies[heg]['capital']
    if cap not in cities:
        cap = city
    loyalty += int(round((hegemonies[heg].setdefault('asabiya', 0.5) - 0.5) * 10))
    loyalty -= int(hegemonies[heg].setdefault('internal warfare', 0))
    if city == cap:
        loyalty += 2
    else:
    # loyalty += 1
        loyalty += random.randint(-2, 0) if cities[cap]['race'] != cities[city]['race'] else random.randint(-1, 1)
        # loyalty += random.randint(-4, 0) if hegemony_control(city) <= rebellion_threshold else 0
        loyalty += random.randint(-2, 1) if cities[cap]['mother'] != cities[city]['mother'] else random.randint(-1, 2)
        if city not in hegemony_flood_fill(cap):
            loyalty -= 2
        else:
            for n in hegemonies[heg].get('conflict', dict()):
                    loyalty += 0.25 if hegemonies[heg]['conflict'][n] >= 3 else 0
        loyalty += random.randint(0, 1) if sum(cities.get(n, dict()).get('hegemony') == heg or hexes[n].get('hinterland') == heg for n in sociology[city]['neighbors']) > 3 else random.randint(-1, 0)

    cities[city]['loyalty'] = clip(int(loyalty), -100, 100)


def city_grow_v1(city, year):

    h = cities[city]['hegemony']
    if h not in hegemonies:
        return
    cities[city]['migrants'] = 0
    cities[city]['casualties'] = 0
    N = cities[city]['population']
    k = min([cities[city]['K'], k_limit(city), 150000])
    if k < cities[city]['K']:
        # if k < N > 5000 and cities[city]['race'] != 'orc':
        #     historytext.append((
        #         year, f"{cities[city]['name']} ({city}, {cities[city]['population']}) is starving"
        #     ))
        cities[city].setdefault('loyalty', 0)
        cities[city]['loyalty'] -= 1
        # hegemonies[cities[city]['hegemony']]['morale'] *= 0.8
        # excess = births(cities[city]['population'], cities[city]['K'], cities[city]['r'])
        # if excess >= 150:
        #     race = cities[city]['race']
        #     targets = None
        #     if 'hinterland' in hegemonies[cities[city]['hegemony']]:
        #         targets = [
        #             t for t in hegemonies[cities[city]['hegemony']]['hinterland'] if
        #             sociology[t]['desirability_raw'][race] and city_k(t, race)
        #         ]
        #     if targets:
        #         if random.random() > 0.5:  # support city or industrial center?
        #             targets = sorted(
        #                 targets,
        #                 key=lambda h: sociology[h]['desirability_raw'][race] / (hex_distance(city, h) + 1), reverse=True
        #             )
        #         else:
        #             targets = sorted(targets, key=lambda h: city_k(h, race), reverse=True)
        #         for target in targets:
        #             if race != 'orc':
        #                 new = city_new(target, year, race, p0=excess, mother=cities[city]['mother'],
        #                                hegemony=cities[city]['hegemony'])
        #                 if not new:
        #                     continue
        #             cities[city]['expanded'] = year
        #             hegemonies[cities[city]['hegemony']]['cities'].add(target)
        #             if 'hinterland' in hexes[target]:
        #                 if sociology[target]['hinterland'] not in hegemonies:
        #                     del sociology[target]['hinterland']
        #                     continue
        #                 hegemonies[sociology[target]['hinterland']].get('hinterland', set()).discard(target)
        #                 del sociology[target]['hinterland']
        #             if cities[city]['race'] != 'orc':
        #                 historytext.append((year, f'Settlers from {cities[city]["name"]} ({cities[city]["population"]}) have founded {cities[target]["name"]} (K = {cities[target]["K"]:.0f})'))
        #             break
        #     else:
        #         targets = [c for c in hegemonies[cities[city]['hegemony']]['cities'] if cities[c]['population'] < cities[c]['K'] < k_limit(c)]
        #         if targets:
        #             target = sorted(targets, key=lambda h: cities[h]['population'], reverse=True)[0]
        #             cities[target]['population'] += excess
    if not cities[city]['population']:
        return
    N = math.log10(N)
    if not k or k < 0:
        k = 10
    k = clip(math.log10(k), 0.01, None)
    W = hegemonies[h].setdefault('internal warfare', 0)
    S = hegemonies[h].setdefault('state resources', 0)
    if 'population' not in hegemonies[h] or not hegemonies[h]['population'][0]:
        return
    pN = cities[city]['population'] / hegemonies[h]['population'][0]
    S *= pN
    W = clip(math.log10(pN * 10 ** W), 0, None) if W else 0
    c = 0.5
    delta = 0.005 / hegemonies[h].get('asabiya', 0.5)
    # if random.random() < 1 - math.exp(-0.8 * len(hegemonies[cities[city]['hegemony']]['conflict'])):
    #     delta = 0
    #     W = 0
    kW = clip(k - c * W, math.log10(20), math.log10(cities[city]['K']))
    dN = cities[city]['r'] * N * (1 - N / k) #- delta * N * W
    cities[city]['dN'] = round(dN, 3)
    # if N > kW and dN > 0:
    #     print(city, year, N, dN, W, k, c, delta, kW)
    #     raise KeyboardInterrupt
    cities[city]['population'] = clip(math.floor(10 ** (N + dN)), 0, None)
    cities[city]['S'] = clip(round(S, 3), 0, None)
    cities[city]['W'] = clip(round(W, 3), 0, None)
    cities[city]['Kf'] = round(10 ** kW)
    r_bump = sociology[city].get('infrastructure', 0) / 50 / 2260 * (1 - 2 * (cities[city]['W'] != 0))
    cities[city]['r'] = round(clip((1 + r_bump) * cities[city]['r'] + random.gauss(mu=0, sigma=0.0005 / 6), 0.5 / 100, 1.5 / 100), 4)
    # cities[city]['r'] = round(
    #     clip(cities[city]['r'] + random.gauss(mu=recipe[cities[city]['race']]['modR'] / 10000, sigma=0.0005 / 6), - 2.5 / 100,
    #          0.02 * (1 - math.exp(-hegemony_tech_level(cities[city]['hegemony']) / 3.5))), 4)


def city_disaster(city, year):

    new = math.floor(cities[city]['population'] * clip(random.gauss(mu=0.8, sigma=0.3 / 6), None, 0.99))
    # writelog('{:} lost {:} people to disasters'.format(cities[city]['name'], cities[city]['population'] - new))
    # historytext.append((year, f'Disaster strikes {cities[city]["name"]} ({cities[city]["population"] - new:.0f} casualties)'))
    cities[city]['population'] = new
    # hegemony_morale(cities[city]['hegemony'], 1 - morale_disaster)


def city_disease(city, year, verbose=False):

    if 'plague' not in cities[city] or not cities[city]['plague']:
        if random.random() > sociology[city].get('infrastructure', 0) / 1e4:
            return
        t = npr.choice(
            ['Bubonic plague', 'Typhoid fever', 'Smallpox', 'Cholera', 'Typhus', 'Yellow fever', None],
            p=[0.001875, 0.001875, 0.001875, 0.001875, 0.001875, 0.001875, 0.98875]
        )
        if not t:
            return
    else:
        t = cities[city].get('plague', dict()).get('t')
    I = cities[city].get('plague', dict()).get('I', 1)
    R = cities[city].get('plague', dict()).get('R', 0)
    S = cities[city]['population'] - I - R
    N = S + I + R
    plagues = {
        'Bubonic plague': {'gamma': 1 / 26, 'mu': 1 / 3},
        'Typhoid fever': {'gamma': 1 / 10, 'mu': 4 / 5},
        'Smallpox': {'gamma': 1 / 17, 'mu': 7 / 10},
        'Cholera': {'gamma': 1 / 4.4, 'mu': 1 / 2},
        'Typhus': {'gamma': 1 / 14, 'mu': 3 / 5},
        'Yellow fever': {'gamma': 1 / 6, 'mu': 1 / 4},
    }
    gamma = plagues[t]['gamma']
    mu = plagues[t]['mu']
    beta = 1 / 2
    d = 0
    while round(I) > 0 and round(N) > 0 and d < 366:
        N = S + I + R
        dS = -beta * S * I / N
        dI = beta * S * I / N - gamma * I
        dR = gamma * mu * I
        S += dS
        I += dI
        R += dR
        if verbose:
            print(f"days: {d}, N: {N:.0f}, S: {S:.0f}, I: {I:.0f}, R:{R:.0f}")
        d += 1
    deaths = cities[city]['population'] - N
    cities[city]['plague'] = {} if d < 365 else {'I': I, 'R': R, 't': t}
    cities[city]['population'] = round(N)
    historytext.append((year, f"{t} has killed {deaths:.0f} in {cities[city]['name']} ({city})"))
    current = {city}
    visited = set()
    infected = set()
    d = 0
    while current and len(infected) < 3 and d < 1 / gamma:
        d += 1
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            if c in cities and c != city:
                current.remove(c)
                infected.add(c)
                if verbose: print(c)
                continue
            current |= set([n for n in all_neighbors[c] if 'population' in hexes[n]])
            visited.add(c)
            current.remove(c)
        if verbose: print(len(visited), len(current))
    if infected:
        for i in npr.choice(list(infected), min(2, len(infected)), replace=False):
            if not cities[i].get('plague', dict()) and random.random() < 1/3:
                cities[i]['plague'] = {'I': 1, 't': t}
                historytext.append((year, f"{t} has spread from {cities[city]['name']} ({city}) to {cities[i]['name']} ({i})"))
    # for n in sorted(hegemonies[cities[city]['hegemony']]['cities'], key=lambda x: cities[city]['t0']):
    #     if n not in cities:
    #         continue
    #     k = city_k(n, cities[n]['race'])
    #     cities[n]['K'] = max(k, cities[n]['K'])
    # hegemonies[cities[city]['hegemony']]['morale'] *= 1 - mu


def city_destroy(city, year):

    historytext.append((year, f'{cities[city]["name"]} ({city}) was abandoned'))
    destroyed.setdefault(city, dict())
    destroyed[city].update({'name': cities[city]["name"]})
    destroyed[city].update({'year': year})
    if cities[city]['hegemony'] in hegemonies:
        hegemonies[cities[city]['hegemony']]['cities'].discard(city)
    for w in wars:
        if hegemonies[cities[city]['hegemony']]['name'] in w:
            if not wars[w][-1][1]:
                wars[w][-1][1] = year
    hegemony_remove_city(city, cities[city]['hegemony'])
    # hegemony_manage(cities[city]['hegemony'], year)
    # if city in hegemonies and len(hegemonies[city]['cities']) > 1:
    #     capital = max(hegemonies[city]['cities'], key=lambda x: sociology[x].get('infrastructure', 0) * cities.get(x, dict()).get('population', 0))
    #     hegemonies[capital] = {k: v for k, v in hegemonies[city].items()}
    #     for c in hegemonies[capital]['cities']:
    #         if c not in cities:
    #             continue
    #         try:
    #             cities[c]['hegemony'] = capital
    #             # hegemonies[city]['cities'].discard(c)
    #         except KeyError:
    #             print(c, city, capital)
    #             raise KeyError
    sociology[city].setdefault('ruins', dict())
    sociology[city]['ruins'].update({cities[city]['name']: year})
    city_history_update(city, year)
    del cities[city]
    for s in list(seaways):
        if s[0][0] == city or s[0][-1] == city:
            del s
    #TODO what if it is a hegemon?


fought = set()
morale_totalwar = 0.25
morale_raid = 0.1
morale_repel = 0.11
morale_disaster = 0.075
morale_win = 0.2


def hegemony_history_update(h, year):

    heg = hegemonies[h]
    hegemonysizes.setdefault(h, dict())
    hegemonysizes[h].setdefault(year, dict())
    hegemonysizes[h][year].update({'n_cities': len(heg['cities'])})
    hegemonysizes[h][year].update({'n_hinter': len(heg.get('hinterland', set()))})
    hegemonysizes[h][year].update({'pop': hegemonies[h]['population']})
    hegemonysizes[h][year].update({'tech': hegemonies[h]['tech level']})
    hegemonysizes[h][year].update({'asabiya': hegemony_asabiya_average(h)})
    hegemonysizes[h][year].update({'power': hegemonies[h].get('power', 0)})
    hegemonysizes[h][year].update({'state resources': hegemonies[h].get('state resources', 0)})
    hegemonysizes[h][year].update({'internal warfare': hegemonies[h].get('internal warfare', 0)})
    hegemonysizes[h][year].update({'Pt': hegemonies[h].get('Pt', 0)})
    hegemonysizes[h][year].update({'Pr': hegemonies[h].get('Pr', 0)})
    hegemonysizes[h][year].update({'army': hegemonies[h].get('army', 0)})
    hegemonysizes[h][year].update({'alpha': hegemonies[h].get('alpha/delta', 0)})


def city_history_update(city, year):

    citysizes.setdefault(city, dict())
    citysizes[city].setdefault(year, dict())
    citysizes[city][year].update({'N': cities[city]['population']})
    citysizes[city][year].update({'S': cities[city]['S']})
    citysizes[city][year].update({'W': cities[city]['W']})
    citysizes[city][year].update({'asabiya': asabiya.get(city, 0)})
    citysizes[city][year].update({'loyalty': cities[city].get('loyalty', 0)})
    citysizes[city][year].update({'r': cities[city]['r']})
    citysizes[city][year].update({'hegemony': cities[city]['hegemony']})
    citysizes[city][year].update({'K': cities[city]['K']})
    citysizes[city][year].update({'Kf': cities[city].get('Kf', cities[city]['K'])})
    citysizes[city][year].update({'migrants': cities[city].get('migrants', 0)})
    citysizes[city][year].update({'dN': cities[city].get('dN', 0)})
    citysizes[city][year].update({'casualties': cities[city].get('casualties', 0)})


def city_war(city, year, raid=None):

    if city not in cities:
        return
    # if cities[city]['population'] < 1000:
    #     return
    h = cities[city]['hegemony']
    if h not in hegemonies:
        return
    heg = hegemonies[h]
    if heg.get('army', 0) == 0:
        return
    # if heg.setdefault('battles', 0) >= max(1, math.log10(len(heg['cities']))):
    #     return
    targets = [
        n for n in all_neighbors[city] if
        n in sociology and n in cities and
        cities[n]['hegemony'] != h and
        heg['conflict'].get(cities[n]['hegemony'], 0) >= 3 and
        f'{city}-{n}-{year}' not in fought# and
        # sociology[n]['desirability_raw'][cities[n]['race']] >=
        #     statistics.mean([sociology[c]['desirability_raw'][cities[c]['race']] for c in heg['cities'] if c in cities] or [0.01])
    ] + [n for n in all_neighbors[city] if
        n in sociology and n in cities and
        cities[n]['hegemony'] != h and
        f'{city}-{n}-{year}' not in fought and
        cities[n]['race'] == 'orc'
    ]
    if not targets:
        home_areas = heg['cities']
        home_areas |= heg['hinterland'] if 'hinterland' in heg else set()
        all_areas = home_areas.copy()
        for m in heg['conflict']:
            if m in hegemonies and heg['conflict'][m] > 0:
                all_areas = hegemonies[m]['cities']
                all_areas |= hegemonies[m]['hinterland'] if 'hinterland' in hegemonies[m] else set()
        current = {n for n in all_neighbors[city] if n in all_areas}
        visited = {city}
        # flood fill
        d = 0
        while current:
            if any(c in cities and c not in heg['cities'] for c in visited):
                targets = [c for c in visited if c in cities and c not in heg['cities']]
                break
            for c in list(current):
                if c in visited:
                    current.remove(c)
                    continue
                current |= {n for n in all_neighbors[c] if n in home_areas}
                current |= {n for n in all_neighbors[c] if n in all_areas} if d <= 3 else set()
                visited.add(c)
                current.remove(c)
            d += 1
            # print(d, visited, current)
    if not targets:
        return
    for target in sorted(targets, key=lambda i: sociology[i]['desirability_raw'][cities[city]['race']]):
        if f'{city}-{target}-{year}' in fought:
            continue
        else:
            break
    else:
        return
    heg_target = hegemonies.get(cities[target]['hegemony'])
    if not heg_target:
        return
    # if cities[target].get('loyalty', 0) < 0 or hegemonies[cities[target]['hegemony']].get('morale', 0) <= morale_break:
    #     historytext.append((year, f"{cities[target]['name']} ({target}) has surrendered to the {heg['name']} hegemony ({city})"))
    #     heg_target['cities'].discard(target)
    #     heg['cities'].add(target)
    #     cities[target]['loyalty'] = 0
    #     cities[target]['hegemony'] = cities[city]['hegemony']
    #     asabiya[target] = round(statistics.mean([asabiya[target], hegemony_asabiya_average(cities[city]['hegemony'])]), 3)
    #     return
    conflict = battle(city, target)
    if not conflict:
        return
    fought.add(f'{city}-{target}-{year}')
    # reduce all troops
    conflict['a'] = city
    conflict['d'] = target
    heg['army'] -= conflict['cA']
    heg_target['army'] -= conflict['cD']
    heg.setdefault('battles', 0)
    heg['battles'] += 1
    # for c in conflict['Ap']:
    #     if not conflict['cA']:
    #         continue
    #     x = clip(round(conflict['Ap'][c] * conflict['cA'] / (conflict['nA'] + conflict['pA'])), 0, None)
    #     cities[c]['population'] -= x
    #     cities[c].setdefault('casualties', 0)
    #     cities[c]['casualties'] += x
    # for c in conflict['Dp']:
    #     if not conflict['cD']:
    #         continue
    #     x = clip(round(conflict['Dp'][c] * conflict['cD'] / (conflict['nD'] + conflict['pD'])), 0, None)
    #     cities[c]['population'] -= x
    #     cities[c].setdefault('casualties', 0)
    #     cities[c]['casualties'] += x
    # declare winner
    total_war = random.random() < (0.02 if cities[city]['race'] != 'orc' or cities[target]['race'] == 'orc' else 0.8) * (5 * (conflict['nA'] - conflict['cA']) > (conflict['nD'] - conflict['cD'] + cities[target]['population']))
    if raid is None:
        raid = (not total_war) * random.random() < (0.25 if cities[city]['race'] != 'orc' else 0.8)
    if conflict['w'] == city and total_war:
        historytext.append((
            year, f'The {heg["name"]} hegemony ({city}) '
            f'has destroyed the {"castle" if bool(sociology[target].get("castles", False)) else "city"}'
            f' of {cities[target]["name"]} '
            f'({target}, {hegemonies[cities[target]["hegemony"]]["name"]} hegemony), '
            f'{conflict["nA"]} vs {conflict["nD"]}, {conflict["cA"]} to '
            f'{conflict["cD"] + cities[target]["population"]} losses, '
            f'{conflict["alpha"]:.2f} to {conflict["delta"]:.2f} rate'
        ))
        battles.update({f'{city}-{target}-{year}': conflict})
        battles[f'{city}-{target}-{year}']['type'] = 'total war'
        battles[f'{city}-{target}-{year}']['cD'] = cities[target]["population"] + conflict['nD']
        city_destroy(target, year)
        # hegemony_morale(heg, 1 + morale_totalwar)
        # hegemony_morale(heg_target, 1 - morale_totalwar)
    else:
        pass
    if conflict['w'] == city and raid and not total_war:
        historytext.append((
            year, f'The {heg["name"]} hegemony ({city}) '
            f'has raided the {"castle" if bool(sociology[target].get("castles", False)) else "city"}'
            f' of {cities[target]["name"]} '
            f'({target}, {hegemonies[cities[target]["hegemony"]]["name"]} hegemony), '
            f'{conflict["nA"]} vs {conflict["nD"]}, {conflict["cA"]} to '
            f'{conflict["cD"]} losses, '
            f'{conflict["alpha"]:.2f} to {conflict["delta"]:.2f} rate'
        ))
        # cities[target]['r'] *= 0.8
        battles.update({f'{city}-{target}-{year}': conflict})
        battles[f'{city}-{target}-{year}']['type'] = 'raid'
        # hegemony_morale(heg, 1 + morale_raid)
        # hegemony_morale(heg_target, 1 - morale_raid)
    else:
        pass
    if conflict['w'] == city and not total_war and not raid:
        historytext.append((
            year, f'The {heg["name"]} hegemony ({city}) '
            f'has defeated the {"castle" if bool(sociology[target].get("castles", False)) else "city"}'
            f' of {cities[target]["name"]} '
            f'({target}, {hegemonies[cities[target]["hegemony"]]["name"]} hegemony), '
            f'{conflict["nA"]} vs {conflict["nD"]}, {conflict["cA"]} to {conflict["cD"]} losses, '
            f'{conflict["alpha"]:.2f} to {conflict["delta"]:.2f} rate'
        ))
        cities[target]['loyalty'] = 0
        cities[target]['defeated'] = year
        battles.update({f'{city}-{target}-{year}': conflict})
        battles[f'{city}-{target}-{year}']['type'] = ''
        if cities[target]['race'] != 'orc' and cities[city]['race'] != 'orc':
            heg['cities'].add(target)
        # heg['conflict'].pop(target, None)
            hegemonies[cities[target]['hegemony']]['cities'].discard(target)
            if hegemonies[cities[target]['hegemony']]['capital'] == target:
                if [c for c in hegemonies[cities[target]['hegemony']]['cities'] if c in cities]:
                    new_capital = max([c for c in hegemonies[cities[target]['hegemony']]['cities'] if c in cities], key=lambda c: sociology[c].get('infrastructure', 0) * cities[c].get('population'))
                    hegemonies[cities[target]['hegemony']]['capital'] = new_capital
            # hegemony_morale(heg, 1 + morale_win)
            # hegemony_morale(heg_target, 1 - morale_win)
            # if hegemonies[cities[target]['hegemony']]['morale'] <= morale_break:
            #     historytext.append((year, f'The {hegemonies[cities[target]["hegemony"]]["name"]} hegemony ({cities[target]["hegemony"]}) morale has fallen to critical levels'))
                # hegemony_surrender(cities[target]['hegemony'], cities[city]['hegemony'], year)
            cities[target]['hegemony'] = cities[city]['hegemony']
            asabiya[target] = round(statistics.mean([asabiya.get(target, 0.5), hegemony_asabiya_average(cities[city]['hegemony'])]), 3)
    elif not total_war and not raid:
        historytext.append((
            year, f'The {"castle" if bool(sociology[target].get("castles", False)) else "city"}'
            f' of {cities[target]["name"]} ({target}) '
            f'has repelled the {heg["name"]} hegemony ({city}), '
            f'{conflict["nD"]} vs {conflict["nA"]}, {conflict["cD"]} to {conflict["cA"]} losses, '
            f'{conflict["delta"]:.2f} to {conflict["alpha"]:.2f} rate'
        ))
        battles.update({f'{city}-{target}-{year}': conflict})
        battles[f'{city}-{target}-{year}']['type'] = ''
        # hegemony_morale(heg, 1 - morale_raid)
        # hegemony_morale(heg_target, 1 + morale_raid)
        cities[city].setdefault('loyalty', 0)
        cities[city]['loyalty'] += 2
    return conflict


def city_borderfort(city, year):

    if cities[city]['race'] == 'orc':
        return
    if cities[city]['population'] < 10000:
        return
    heg = cities[city]['hegemony']
    if heg not in hegemonies:
        return
    if not hegemonies[heg]['conflict']:
        return
    race = cities[city]['race']
    opponents = [c for c in hegemonies[heg]['conflict'] if hegemonies[heg]['conflict'][c] > 2]
    for opp in opponents:
        if opp not in hegemonies:
            hegemonies[heg]['conflict'].pop(opp)
            continue
        if not hegemonies[opp]['cities']:
            hegemony_delete(opp, year)
            hegemonies[heg]['conflict'].pop(opp)
            continue
        try:
            targets = [t for t in hegemony_border_hexes(city, hegemonies[opp]['capital']) if t not in cities and hegemony_control(t) > rebellion_threshold]
        except TypeError:
            return
        if not targets:
            return
        target = max(targets, key=lambda t: sociology[t]['desirability_raw'][race])
        p = random.randint(1500, 2500)
        new = city_new(target, year, cities[city]['race'], p0=p, mother=cities[city]['mother'], hegemony=heg)
        if not new:
            return
        cities[city]['population'] -= p
        cities[city]['expanded'] = year
        hegemonies[heg]['cities'].add(target)
        hegemonies[heg]['hinterland'].discard(target)
        del sociology[target]['hinterland']
        hexes[target].setdefault('castles', dict()).update({heg: year})
        historytext.append(
            (year, f'{cities[city]["name"]} ({cities[city]["population"]}) has established a border fort in {cities[target]["name"]} (K = {cities[target]["K"]:.0f})'))


def city_castle(city, year):

    if cities[city]['population'] < 1500:
        return
    if any(i in terrain[city]['terrain'] for i in 'bfmw'):
        return
    if sociology[city].get('castles', dict()).keys():
        return
    try:
        heg = hegemonies[cities[city]['hegemony']]
    except KeyError:
        print(f'Hegemony not found for {city}')
        return
    if statistics.mean(sociology[h].get('infrastructure', 0) for h in heg['cities']) < 20:
        return
    current = {n for n in sociology[city]['neighbors'] if n in list(heg.get('hinterland', [])) +
               list(heg['cities']) +
               sum([list(hegemonies[m].get('hinterland', [])) for m in heg['conflict'] if
                    heg['conflict'].get(m, 0) > 0 and m in hegemonies], []) +
               sum([list(hegemonies[m]['cities']) for m in heg['conflict'] if
                    heg['conflict'].get(m, 0) > 0 and m in hegemonies], [])}
    visited = {city}
    # flood fill
    d = 0
    while current:
        # if any(c in cities and c not in heg['cities'] and c not in heg.get('hinterland', []) for c in visited):
        #     targets = [c for c in visited if c in cities and c not in heg['cities'] and c not in heg.get('hinterland', [])]
        #     break
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            current |= {n for n in all_neighbors[c] if n in list(heg.get('hinterland', [])) +
                        list(heg['cities'])}
            current |= {n for n in all_neighbors[c] if n in sum(
                [list(hegemonies[m].get('hinterland', [])) for m in heg['conflict'] if
                 heg['conflict'].get(m, 0) > 0 and m in hegemonies], []) +
                        sum([list(hegemonies[m]['cities']) for m in heg['conflict'] if
                             heg['conflict'].get(m, 0) > 0 and m in hegemonies], [])} if d <= 3 else set()
            visited.add(c)
            current.remove(c)
        d += 1
    targets = [c for c in visited if c in cities and c not in heg['cities'] and c not in heg.get('hinterland', [])]
    if targets:
        sociology[city].setdefault('castles', dict())
        sociology[city]['castles'].update({cities[city]['hegemony']: year})
        historytext.append((year, f'A castle has been built in {cities[city]["name"]}'))


def manage(city, year):
    if city not in cities:
        return
    # migration takes the most time
    funcs = [
        # (city_grow, 1),
        (city_loyalty, 1),
        # (city_disaster, 0.0005),
        (city_expand, 0.5 + math.log10(recipe[cities.get(city, dict()).get('race', 'orc')]['modR'])),
        # (city_road, 0.5 + math.log10(recipe[cities[city]['race']]['modR'])),
        # (city_migrate, 1),
        # (city_seaway, 0.6),
        (city_colonize, 0.5 + math.log10(recipe[cities[city]['race']]['modR'])),
        (city_war, 1),
        (city_castle, 0.25),
        (city_secession, 0.5),
        (city_borderfort, 0.3),
        (city_disease, 1),
    ]
    random.shuffle(funcs)
    funcs.insert(0, (city_grow, 1))
    for f in funcs:
        # timer(0)
        # with stopit.ThreadingTimeout(1):
        try:
            if random.random() > f[1]:
                continue
            f[0](city, year)

            if city not in cities:
                return
            if cities[city]['population'] < 25:
                city_destroy(city, year)
                return

            if cities[city]['hegemony'] not in hegemonies:
                hegemony_delete(cities[city]['hegemony'], year)
                hegemony_new(city, year)
                cities[city]['hegemony'] = city
        except:
            print(f, city, year)
            cities_save()
            writelog(historytext)
            raise KeyboardInterrupt
        # print(f, timer(1))
    if city in cities and cities[city]['population'] < 25:
        city_destroy(city, year)

historytext = list()


def history(start=0, end=6000, formation_chance=2 / 100, verbose=True, draw=True, migrate=14, draw_terrain=False, threshold=0.025, clear_draw=True):

    if not start:
        historytext.clear()
        with open(basefile + r'\population_growth.csv', 'w') as f:
            f.write(
                'year\tcities\thuman cities\tdwarven cities\telven cities\torcish cities\thalfing cities\ttotal pop\tlargest\tmax inf\n')
        with open(basefile + r'\first_city.csv', 'w') as f:
            f.write('year\tpopulation\tK\n')
        open(basefile + r'\history.txt', 'w').close()
        cities_clear()
        with HiddenPrints():
            roads_clear()
        for h in hexes:
            hexes[h].get('ruins', dict()).clear()
        road_limit.clear()
        if clear_draw:
            with open(basefile + r'\Roads.svg', 'r') as f:
                file = bs(f, 'lxml')
            file.find('g', id='searoutes').clear()
            save(file, extra_file=basefile + r'\Roads.svg')
        resources_assign_base()
        cities_save()
    migrate_targets = {race: great_migration(race, limit=migrate, threshold=threshold) for race in races}
    while len(cities) == 0:
        cities_place(year=0, formation_chance=formation_chance, limit=migrate, targets=migrate_targets)
    for year in range(start, end + 1):
        print(year)
        fought.clear()
        new_cities(year)
        hegemony_assign(year)
        hegemony_agriculture()
        # for h in list(hegemonies.keys()):
        #     hegemony_manage(h, year)
        for city in random.sample(list(cities.keys()), len(cities)):
            manage(city, year)
        # for city in list(cities.keys()):
        #     # if city in road_limit and cities[city]['population'] > 25000:
        #     #     road_limit[city] = min(road_limit[city] + 1, 4)
        #     if cities[city]['population'] < 25:
        #         city_destroy(city, year)
        if year:
            cities_place(year, formation_chance=formation_chance, limit=migrate, targets=migrate_targets)
            infrastructure(update_map=bool(not year % 100 and draw))
            rural_grow()
        for h in list(hegemonies.keys()):
            hegemony_manage(h, year)
        if not year % 5:
            if draw: cities_draw(year)
        if not year % 10:
            # if year > 0: population_map(k=0.4, races=races)
            # mother_map()
            writelog(historytext)
            battles_export()
            cities_save()
            migrate_targets = {race: great_migration(race, limit=migrate, threshold=threshold) for race in races}
        # with HiddenPrints():
        if not year % 50 or year == end:
            if year: infrastructure(update_map=draw)
            if draw:
                cities_map()
                cities_names_map()
                hegemony_map(limit=migrate, threshold=threshold)
                battles_map(year)
                population_map()
            cities_resize()
        if not year % 100 or year == end:
            desirability(races=races, recipe=recipe, update_map=bool(not year % 500 and draw), verbose=False)
            if draw_terrain: assign_terrain(update_map=draw)
            cities_save()
            # infrastructure(update_map=True)
            # roads_map(hexes=hexes)
            # with open(basefile + r'\road_limit.p', 'wb') as f:
            #     pickle.dump(road_limit, f)
        if not verbose:
            continue
        # if not start and year == start:
        #     first = random.choice(list(cities))
        # if first not in cities:
        #     first = sorted(cities, key=lambda x: cities[x]['t0'])[0]
        #     with open(basefile + r'\first_city.csv', 'a') as f:
        #         f.write('x\n')
        with open(basefile + r'\population_growth.csv', 'a') as f:
            # f.write('year\thuman cities\tdwarven cities\telven cities\torcish cities\thalfing cities\tlargest\tmax inf\tseaways\n')
            # pass
            f.write('{:}\t{:}\t{:}\t{:}\t{:}\t{:}\t{:}\t{:}\t{:}\t{:}\t{:}\t{:}\n'.format(
                year, len(cities),
                len([c for c in cities if cities[c]['race'] == 'human']),
                len([c for c in cities if cities[c]['race'] == 'dwarf']),
                len([c for c in cities if cities[c]['race'] == 'elf']),
                len([c for c in cities if cities[c]['race'] == 'orc']),
                len([c for c in cities if cities[c]['race'] == 'halfling']),
                sum(sociology[c].get('population', 0) + cities[c]['population'] for c in cities),
                max(cities[c]['population'] for c in cities),
                max(cities, key=lambda c: cities[c]['population']),
                cities[max(cities, key=lambda c: cities[c]['population'])]['name'],
                len(hegemonies),
                # len(seaways),
                # sum(sociology[h].get('infrastructure', 0) for h in hexes) * 346
            ))
        for c, city in cities.items():
            city_history_update(c, year)
        for h, heg in hegemonies.items():
            hegemony_history_update(h, year)
        # with open(basefile + r'\first_city.csv', 'a') as f:
        #     f.write('{:}\t{:}\t{:}\n'.format(year, cities[first]['population'], cities[first]['K']))
    # desirability_map()
    # roads_generate_v3(update_map=True)
    # seaways_generate(update_map=True)


def hegemony_flood_fill(start, verbose=False):

    current = {start}
    if start not in cities:
        return {}
    visited = set()
    heg = hegemonies[cities[start]['hegemony']] if cities[start]['hegemony'] in hegemonies else None
    if not heg:
        return current
    all_cities = heg['cities']
    all_cities |= heg['hinterland'] if 'hinterland' in heg else set()
    # all_cities = heg.get('hinterland', set()) | heg.get('cities', set())

    while current:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            # if c in cities:
            #     current |= set([n for n in all_neighbors[c] if
            #                     n in cities and
            #                     cities[n]['hegemony'] == cities[start]['hegemony']])
            # current |= {n for n in all_neighbors[c] if n in all_cities}
            for n in all_neighbors[c]:
                if n in all_cities:
                    current.add(n)
            visited.add(c)
            current.remove(c)
        if verbose: print(len(visited), len(current))

    return visited


def mother_flood_fill(start):
    current = {start}
    visited = set()

    while current:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            current |= set([n for n in all_neighbors[c] if
                            n in cities and cities[n]['race'] == cities[start]['race'] and cities[n]['mother'] ==
                            cities[start]['mother']])
            visited.add(c)
            current.remove(c)
        # print(len(visited), len(current))

    return visited

relations_done = set()


def hegemony_delete(h, year, alliance=False):

    for w in wars:
        if h in w:
            wars[w][-1][1] = year
    if h not in hegemonies:
        return
    hegemony_history_update(h, year)
    if not alliance:
        for city in hegemonies[h]['cities']:
            if city in cities:
                if city in hegemonies:
                    hegemony_new(city, year, alt=cities[city]['hegemony'])
                if city not in hegemonies:
                    hegemony_new(city, year)
                    cities[city]['hegemony'] = city
    for n in list(hegemonies[h]['conflict']):
        if n in hegemonies:
            hegemonies[n]['conflict'].pop(h, None)
    destroyed.setdefault('hegemonies', dict())
    destroyed['hegemonies'].setdefault(h, dict())
    destroyed['hegemonies'][h].update({'t0': hegemonies[h]['t0']})
    destroyed['hegemonies'][h].update({'color': hegemonies[h]['color']})
    destroyed['hegemonies'][h].update({'name': hegemonies[h]['name']})
    destroyed['hegemonies'][h].update({'capital': hegemonies[h]['capital']})
    hegemonies.pop(h, None)


morale_break = 0.1


def hegemony_surrender(h, n, year):

    historytext.append((
        year,
        f'The {hegemonies[h]["name"]} hegemony ({h}) has sworn fealty to the {hegemonies[n]["name"]} hegemony ({n})'
    ))
    for city in set(hegemonies[h]['cities']):
        cities[city]['hegemony'] = n
        hegemonies[n]['cities'].add(city)
        hegemonies[h]['cities'].discard(city)
        cities[city]['loyalty'] = 0
    hegemony_delete(h, year)


def hegemony_ad(h):

    return round(clip((0.1179 * hegemonies[h]['tech level'] ** 2 - 1.3667 * hegemonies[h]['tech level'] + 4.3846) + statistics.mean(
            len(hex_resources.get(i, [])) for i in hegemonies[h]['cities'] | hegemonies[h].get('hinterland', set())
        ) / 10, 0.001, None), 4)


def hegemony_assign(year):

    relations_done.clear()

    for h, hegemony in list(hegemonies.items()):
        hegemony['battles'] = 0
        for city in list(hegemony['cities']):
            if city not in cities:
                hegemony['cities'].discard(city)
            elif cities[city]['hegemony'] != h:
                hegemony['cities'].discard(city)
        if not hegemony['cities']:
            if year - hegemony['t0'] > 5:
                historytext.append((year, f'The {hegemony["name"]} hegemony has fallen (founded {hegemony["t0"]})'))
            hegemony_delete(h, year) # need to save this historically
        elif not any(cities[city]['hegemony'] == h for city in hegemony['cities']):
            hegemony_delete(h, year)
            continue

        if h not in hegemonies:
            continue
        for n in list(hegemonies[h].get('conflict', dict())):
            if n not in hegemonies or n not in hegemonies[h]['conflict']:
                continue
            if hegemonies[h]['conflict'][n] == 0:
                if random.random() < 0.01:
                    if len(hegemonies[h]['cities']) > len(hegemonies[n]['cities']):
                        for c in hegemonies[n]['cities']:
                            if c not in cities:
                                continue
                            cities[c]['hegemony'] = h
                        hegemonies[h]['cities'] |= hegemonies[n]['cities']
                        historytext.append((
                            year, f'{hegemony["name"]} and {hegemonies[n]["name"]} have formed an alliance.'
                        ))
                        hegemony_delete(n, year, alliance=True)
        hegemonies[h]['tech level'] = hegemony_tech_level(h)
        hegemony_asabiya(h)
        hegemony_nsw(h, year)
        hegemonies[h]['power'] = hegemony_power(h)
        hegemonies[h]['alpha/delta'] = hegemony_ad(h)

        # hegemony_new_cities(h, year)

    for c, city in cities.items():
        try:
            hegemonies[city['hegemony']]['cities'].add(c)
        except KeyError:
            # print(f'{c}: {city} has no hegemony assigned')
            hegemony_delete(city['hegemony'], year)
            hegemony_new(city['hegemony'], year)
            continue
    for c, city in cities.items():
        h = city['hegemony']
        if h not in hegemonies:
            hegemony_delete(h, year)
            hegemony_new(c, year)
            continue
        continue
        if hegemonies[h]['capital'] not in hegemony_flood_fill(c):
            seceding = hegemony_flood_fill(c)
            capital = max(seceding, key=lambda x: sociology[x].get('infrastructure', 0) * cities[x].get('population'))
            if len(seceding) > 1:
                historytext.append((year, f'{cities[capital]["name"]} has seceded from '
                    f'{hegemonies[h]["name"]}; '
                    f'they are joined by {", ".join([cities[s]["name"] for s in seceding if s != capital])}'))
            else:
                historytext.append((year, f'{cities[capital]["name"]} has seceded from '
                    f'{hegemonies[h]["name"]}'))
            hegemony_new(capital, year)
            hegemonies[h]['conflict'].update({capital: 1})
            hegemonies[h]['cities'] -= seceding
            hegemonies[capital]['conflict'].update({h: 1})
            hegemonies[capital]['cities'] = seceding
            for s in seceding:
                cities[s]['hegemony'] = capital

    for h, hegemony in list(hegemonies.items()):
        hegemony_army(h)

    # for h in {city['hegemony'] for c, city in cities.items()}:
    #     controls = {c for c, city in cities.items() if city['hegemony'] == h}
    #     if h not in cities:
    #         continue
    #     if h in hegemonies:
    #         hegemonies[h].update({'cities': controls})
    #         for n in hegemony_neighbors(h):
    #             if n not in hegemonies[h]['conflict']:
    #                 hegemonies[h]['conflict'].update({n: 0})
    #                 # TODO ensure that two will always match - how?
    #     else:
    #         hegemonies[h] = {'cities': controls, 'name': cities[h]['name'], 't0': cities[h]['t0']}
    #         hegemonies[h].update({'conflict': {n: 0 for n in hegemony_neighbors(h)}})
    #     hegemonies[h]['urbanization'] = urbanization(statistics.mean(sociology[city].get('infrastructure', 0) for city in hegemonies[h]['cities']))
    #     hegemonies[h].setdefault('military', round(random.gauss(mu=4.5, sigma=0.5) / 100, 4))
    #     hegemonies[h].setdefault('color', randcolor())


def urbanization(i, a=0.96, tau=0.00039166969103811296138):

    return round(a * (1 - math.exp(-tau * i)) + (1 - a), 4)


transition_matrix = [
    [0.8000, 0.1500, 0.0500, 0.0000, 0.0000, 0.0000],
    [0.1000, 0.9000, 0.1000, 0.0000, 0.0000, 0.0000],
    [0.0000, 0.1000, 0.9000, 0.1000, 0.0000, 0.0000],
    [0.0000, 0.0000, 0.1000, 0.9000, 0.1000, 0.0000],
    [0.0000, 0.0000, 0.0000, 0.1000, 0.9000, 0.1000],
    [0.0010, 0.0000, 0.0000, 0.0000, 0.0075, 0.9915]
]

for _ in range(10):
    for i, j in enumerate(transition_matrix):
        for k, m in enumerate(j):
            transition_matrix[i][k] = m / sum(j)


def war(t, A, B, alpha, beta, n=1.25):

    return round(A / 2 * math.exp(-math.sqrt(alpha * beta) * A ** (1 - n / 2) * B ** (1 - n / 2) * t) * (
            math.exp(2 * math.sqrt(alpha * beta) * A ** (1 - n / 2) * B ** (1 - n / 2) * t) + 1
    ) - (math.sqrt(beta) * B * A ** (1 - n / 2) * B ** (n / 2 - 1) * math.exp(
        -math.sqrt(alpha * beta) * A ** (1 - n / 2) * B ** (1 - n / 2) * t) * (
            math.exp(2 * math.sqrt(alpha * beta) * A ** (1 - n / 2) * B ** (1 - n / 2) * t) - 1
    )) / (2 * math.sqrt(alpha))), round(B / 2 * math.exp(-math.sqrt(alpha * beta) * A ** (1 - n / 2) * B ** (1 - n / 2) * t) * (
            math.exp(2 * math.sqrt(alpha * beta) * A ** (1 - n / 2) * B ** (1 - n / 2) * t) + 1
    ) - (math.sqrt(alpha) * A * B ** (1 - n / 2) * A ** (n / 2 - 1) * math.exp(
        -math.sqrt(alpha * beta) * A ** (1 - n / 2) * B ** (1 - n / 2) * t) * (
            math.exp(2 * math.sqrt(alpha * beta) * A ** (1 - n / 2) * B ** (1 - n / 2) * t) - 1
    )) / (2 * math.sqrt(beta)))


def L(d, s=2.82):

    return 1 / (1 + 1 / s) ** d


def battle(a, d, n=1.25, m=0.5):

    hA = cities[a]['hegemony']
    hD = cities[d]['hegemony']
    # reinforcements = random.random() < (0.8 if sociology[a].get('infrastructure', 0) > sociology[d].get('infrastructure', 0) else 1)
    # cA = hegemony_flood_fill(a)
    # cD = hegemony_flood_fill(d)
    if hD not in hegemonies or hA not in hegemonies:
        return None
    try:
        # bA = round(hegemony_border(hegemonies[hA]['capital'], hegemonies[hD]['capital']) / sum(
        #         [hegemony_border(hegemonies[hA]['capital'], hegemonies[c]['capital']) for c
        #          in hegemonies[hA]['conflict'] if c in hegemonies and hegemonies[hA]['conflict'].get(c, 0) > 0]
        #     ), 4)
        bA = sum(hegemonies[hA]['conflict'][c] > 3 for c in hegemonies[hA]['conflict'] if c in hegemonies)
        if not bA:
            return
        bA = 1 / bA
    except ZeroDivisionError:
        # print(f'No conflicts for {hA}: {a} ({hD}: {d})?')
        return None
    except KeyError:
        return None
        # raise ZeroDivisionError
    try:
        # bD = round(hegemony_border(hegemonies[hD]['capital'], hegemonies[hA]['capital']) / sum(
        #         [hegemony_border(hegemonies[hD]['capital'], hegemonies[c]['capital']) for c
        #          in hegemonies[hD]['conflict'] if c in hegemonies and hegemonies[hD]['conflict'].get(c, 0) > 0]
        #     ), 4)
        bD = sum(hegemonies[hD]['conflict'][c] > 3 for c in hegemonies[hD]['conflict'] if c in hegemonies)
        if not bD:
            return
        bD = 1 / bD
    except ZeroDivisionError:
        # print(f'No conflicts for {hD}: {d} ({hA}: {a})?')
        return None
    except KeyError:
        return None
        # raise ZeroDivisionError
    try:
        # A_len = hegemony_border_length(hA)
        # Ap = {
        #     c: math.floor(
        #         (sociology[c].get('population', 0) + cities[c]['population']) *
        #         bA *
        #         hegemonies[hA]['military'] # / (hex_distance(c, hegemonies[hA]['capital']) + 1) ** m
        #     ) for c in cA if c in cities
        # }
        A = int(L(hexDistance(d, hegemonies[hA]['capital'])) * bA * hegemonies[hA].get('army', 0))
        # Ap.update({a: math.floor((population_rural(a) + cities[a]['population']) * hegemonies[hA]['military'])})
        # D_len = hegemony_border_length(hD)
        # Dp = {
        #     c: math.floor(
        #         (sociology[c].get('population', 0) + cities[c]['population']) *
        #         bD *
        #         hegemonies[hD]['military'] # / (hex_distance(c, hegemonies[hD]['capital']) + 1) ** m
        #     ) for c in cD if c in cities and c != d
        # } if reinforcements else dict()
        # Dp.update({d: math.floor((sociology[d].get('population', 0) + cities[d]['population']) * hegemonies[hD]['military'])})
        D = int(L(hexDistance(d, hegemonies[hD]['capital'])) * bD * hegemonies[hD].get('army', 0))
    except KeyError:
        print(a, d)
    # pressganged from countryside
    # A_press = sum({
    #         c: math.floor(
    #             sociology[c].get('population', 0) *
    #             bA *
    #             hegemonies[hA]['military']# / (hex_distance(c, a) + 1) ** m
    #         ) for c in hegemonies[hA].get('hinterland', set()) if c in cA
    #     }.values() or [0])
    # D_press = sum({
    #         c: math.floor(
    #             sociology[c].get('population', 0) *
    #             bD  *
    #             hegemonies[hD]['military']# / (hex_distance(c, d) + 1) ** m
    #         ) for c in hegemonies[hD].get('hinterland', set()) if c in cD
    #     }.values() or [0])
    estimate = 3.5 * math.exp(-hegemonies[hA]['tech level'] / 4)
    if A < (D * random.uniform(1-estimate, 1+estimate)):
        return None
    if not A or A < 10:
        return None
    if not D:
        if A > D:
        # return None
            return {
                'w': a, 'cA': 0, 'cD': D, 'nA': A, 'nD': D,
                'alpha': 1, 'delta': 0, 'bA': bA, 'bD': bD,
            }
        else:
            return {
                'w': d, 'cA': A, 'cD': 0, 'nA': A, 'nD': D,
                'alpha': 0, 'delta': 1, 'bA': bA, 'bD': bD,
            }
        # raise ZeroDivisionError
    # Ap = {c: Ap[c] / A for c in Ap}
    # Dp = {c: Dp[c] / D for c in Dp}
    try:
        # alpha = statistics.mean([sociology[c]['desirability'][cities[c]['race']] for c in hegemonies[hA]['cities'] if c in cities] or [0.01])
        # alpha *= math.log10(max(1.01, statistics.mean([sociology[c].get('infrastructure', 0) for c in hegemonies[hA]['cities']])))
        # tA = hegemonies[hA]['tech level']
        # tD = hegemonies[hD]['tech level']
        # alpha = clip((0.1179 * tA ** 2 - 1.3667 * tA + 4.3846) + statistics.mean(
        #     len(hex_resources.get(h, 0)) for h in hegemonies[hA]['cities'] | hegemonies[hA].get('hinterland', set())
        # ) / 10, 0.001, None)
        alpha = hegemonies[hA].get('alpha/delta', 0.001)
        alpha /= 2 if bool(sociology[d].get('castles', False)) and hegemonies[hA]['tech level'] < 11 else 1
        # delta = statistics.mean([sociology[c]['desirability'][cities[c]['race']] for c in hegemonies[hD]['cities'] if c in cities] or [0.01])
        # delta *= math.log10(max(1.01, statistics.mean([sociology[c].get('infrastructure', 0) for c in hegemonies[hD]['cities']])))
        delta = hegemonies[hD].get('alpha/delta', 0.001)
        # delta = clip((0.1179 * tD ** 2 - 1.3667 * tD + 4.3846) + statistics.mean(
        #     len(hex_resources.get(h, 0)) for h in hegemonies[hD]['cities'] | hegemonies[hD].get('hinterland', set())
        # ) / 10, 0.001, None)
        alpha = round(alpha * (1.5 if cities[a]['race'] == 'orc' else 1), 4)
        delta = round(delta * (1.5 if cities[d]['race'] == 'orc' else 1), 4)
    except ValueError:
        print(a, d)
        raise ValueError
    try:
        t = 0
        w = None
        rout_A = clip(random.gauss(0.7, 0.2), 0.25, 0.75)
        rout_D = clip(random.gauss(0.5, 0.2), 0.25, 0.75) if cities[d]['race'] != 'orc' else 0
        t_step = 10 ** math.floor(-1.5 * round(math.log10(max(alpha, delta))) - 2.5 - 2)
        # for i in range(1, int(1e6)):
        while True:
            t += t_step
            if war(t, A, D, alpha, delta)[0] < max(1, rout_A * A):
                w = d
                break
            if war(t, A, D, alpha, delta)[1] < max(1, rout_D * D):
                w = a
                break
            # print(t)
        t = round(t, -round(math.log10(t_step)))
    except ZeroDivisionError:
        print(a, d)
        raise ZeroDivisionError
    except TypeError:
        print(a, d)
        raise TypeError
    except KeyboardInterrupt:
        print(a, d)
        raise KeyboardInterrupt
    if not w:
        print(a, d, t)
        # raise UnboundLocalError
        return None
    casualties = war(t, A, D, alpha, delta)
    if any(i < 0 for i in casualties):
        casualties = war(t - t_step, A, D, alpha, delta)
        # print('error in casualties', a, d, A, D, A_press, D_press, alpha, delta, rout_A, rout_D, t)
        # raise ValueError
        # return None
    return {
        'w': w, 'cA': A - casualties[0], 'cD': D - casualties[1], 'nA': A,
        'nD': D, 'alpha': alpha, 'delta': delta, 'bA': bA, 'bD': bD,
    }


def battles_export():

    with open(battles_file, 'rb') as f:
        battles_f = pickle.load(f)
    for battle in battles:
        battles_f.setdefault(battle, dict()).update(battles[battle])
    with open(basefile + r'\battles.csv', 'w') as f:
        f.write('year,a,d,wA,nA,nD,cA,cD,alpha,delta,bA,bD,type\n')
        for b in sorted(battles_f, key=lambda x: int(x.split('-')[-1])):
            a = '-'.join(b.split("-")[0:2])
            d = '-'.join(b.split("-")[2:4])
            f.write(f'{int(b.split("-")[-1])},{a},{d},'
                    f'{1 if a == battles_f[b]["w"] else 0},{battles_f[b]["nA"]},'
                    f'{battles_f[b]["nD"]},{battles_f[b]["cA"]},{battles_f[b]["cD"]},{battles_f[b]["alpha"]},'
                    f'{battles_f[b]["delta"]},'
                    f'{battles_f[b].get("bA", 0):.2f},'
                    f'{battles_f[b].get("bD", 0):.2f},'
                    f'{battles_f[b]["type"]}\n')


def battles_map(year):

    """
    Places an X on the location of all battles
    :return:
    """

    # timer(0)  # 232 s
    # print('Fixing hex labels...')
    with open(basefile + r'\Battles.svg', 'r', encoding='utf8') as f:
        file = bs(f, 'lxml')
    with open(battles_file, 'rb') as f:
        battles_full = pickle.load(f)
    for battle in battles:
        battles_full.setdefault(battle, dict()).update(battles[battle])
    done = set()
    style = ""
    # clear the existing hex label
    file.find('g', id='battles').clear()
    string = ''
    y0, yf = 0, 0
    if battles_full:
        y0 = 0
        yf = year
    if y0 == yf:
        return
    s = lambda t: 11 * math.exp(-(yf - t) / 500) + 1
    done = set()
    for b in sorted(battles_full, key=lambda x: int(x.split('-')[-1]), reverse=True):
        a = '-'.join(b.split('-')[0:2])
        d = '-'.join(b.split('-')[2:4])
        t = int(b.split('-')[-1])
        if (a, d) in done:
            continue
        done.add((a, d))
        done.add((d, a))
        (x, y) = None, None
        for p1, p2 in zip(hexes[d]['pts'], hexes[d]['pts'][1:] + hexes[d]['pts'][:1]):
            R = seg_intersect(np.array(p1), np.array(p2), np.array(hexes[a]['c']), np.array(hexes[d]['c']))
            if R:
                (x, y) = R
                break
        if not x or not y:
            continue
        if (x, y) in done:
            continue
        done.add((x, y))
        string += f'<text id="{a}-{d}" ' \
            f'x="{x:.02f}" y="{y + s(t) / 2:.02f}" ' \
            f'class="battle-label" style="font-size:{s(t):.0f}pt">X</text>\n'
    else:
        if string:
            file.find('g', id='battles').contents = bs(string, 'lxml').find('body').contents
        else:
            file.find('g', id='battles').clear()
        save(file)
    # print('{:} s'.format(timer(1)))


def hegemony_new(h, year, alt=None):

    if h in hegemonies and not alt:
        print(f"{h} already exists!")
        raise KeyboardInterrupt
    heg = h if not alt else alt
    name = cities[h]['name']
    if name.startswith('New '):
        name = name[4:]
    if cities[h]['mother'] in toponyms:
        for topo in toponyms[cities[h]['mother']]:
            if topo[1] == 'pre' and name.startswith(topo[0]):
                name = name[len(topo[0]):].title()
            if topo[1] == 'post' and cities[h]['name'].endswith(topo[0]):
                name = name[:len(name)-len(topo[0])].title()
    if name in hegemonies:
        name = 'New ' + name
    hegemonies[heg] = {'cities': {h}, 'name': name, 't0': cities[h]['t0'], 'capital': h}
    cities[h]['hegemony'] = heg
    for n in hegemony_neighbors(heg):
        if n not in hegemonies:
            continue
        if n not in hegemonies[heg].setdefault('conflict', dict()):
            hegemonies[heg]['conflict'].update({n: 0})
            hegemonies[n].setdefault('conflict', dict()).update({heg: 0})
    else:
        hegemonies[heg].update({'conflict': {n: 0 for n in hegemony_neighbors(heg)}})
    hegemonies[heg]['tech level'] = hegemony_tech_level(heg)
    hegemonies[heg]['urbanization'] = urbanization(
        statistics.mean(sociology[city].get('infrastructure', 0) for city in hegemonies[heg]['cities'])
    )
    hegemonies[heg].setdefault('military', round(random.gauss(mu=4.5, sigma=0.5) / 100, 4))
    if cities[hegemonies[h]['capital']]['race'] == 'orc':
        hegemonies[heg]['military'] = round(random.gauss(mu=55, sigma=0.5) / 100, 4)
    hegemonies[heg].setdefault('color', randcolor())
    # hegemonies[h]['morale'] = 1
    Pr = sum(sociology[h].get('population', 0) for h in hegemony_flood_fill(hegemonies[heg]['capital'])) * 0.9
    Pt = sum(cities.get(h, dict()).get('population', 0) for h in hegemony_flood_fill(hegemonies[heg]['capital']))
    hegemonies[heg]['Pr'] = Pr
    hegemonies[heg]['Pt'] = Pt
    hegemonies[heg]['population'] = hegemony_population(heg)
    hegemony_army(heg)


def hegemony_population(h):

    urban = sum(cities.get(city, dict()).get('population', 0) for city in hegemonies[h]['cities'])
    rural = sum(sociology[city].get('population', 0) for city in hegemonies[h]['cities'])
    hinterland = sum(sociology[city].get('population', 0) for city in hegemonies[h].get('hinterland', set()))
    return urban, rural, hinterland, hegemonies[h].get('army')


def hegemony_tech_level(h):

    if h not in hegemonies:
        return 0
    if not hegemonies[h]['cities']:
        return 0
    m = avg([sociology[c].get('infrastructure', 0) for c in hegemonies[h]['cities'] | hegemonies[h].get('hinterland', set())], weights=[cities.get(c, dict()).get('population', 0) + sociology[c].get('population', 0) for c in hegemonies[h]['cities'] | hegemonies[h].get('hinterland', set())])
    for t in tech_infra:
        if tech_infra[t] > m:
            return t - 1
    return 0


def hegemony_capital(h, n=3):

    for i in hegemonies[h]['cities']:
        print(i, sum(cities[i]['population'] / (hexDistance(i, j) + 1) ** n for j in hegemonies[h]['cities']))


def hegemony_check(year):

    for h in hegemonies:
        for c in set(hegemonies[h]['cities']):
            if c not in cities:
                hegemonies[h]['cities'].discard(c)
                print(h, c)


def hegemony_remove_city(city, h):

    if city not in hegemonies[h]['cities']:
        return
    W = hegemonies[h].get('internal warfare', 0)
    S = hegemonies[h].get('state resources', 0)
    p = cities[city]['population'] / hegemonies[h]['population'][0]
    if W and p < 1:
        W = 10 ** W
        W *= (1 - p)
        W = round(math.log10(W), 3)
        hegemonies[h]['internal warfare'] = W
    if S and p < 1:
        S *= (1 - p)
        S = round(S, 3)
        hegemonies[h]['state resources'] = S


def hegemony_recapital(h, year):

    capital = hegemonies[h]['capital']
    top5 = sorted(hegemonies[h]['cities'],
                  key=lambda c: sociology[c].get('infrastructure', 0) * (c in cities) * cities.get(c, dict()).get(
                      'population', 0), reverse=True)[:5]
    top5 = [t for t in top5 if t in cities]
    if not top5:
        hegemony_delete(h, year)
        return
    if capital not in top5 or capital not in cities:
        hegemonies[h]['capital'] = random.choice(top5)
        if capital in cities:
            historytext.append((
                year,
                f"The capital of {hegemonies[h]['name']} ({h}) has moved from {cities[capital]['name']} ({capital}) to {cities[hegemonies[h]['capital']]['name']} ({hegemonies[h]['capital']})"
            ))
        else:
            historytext.append((
                year,
                f"The capital of {hegemonies[h]['name']} ({h}) has moved to {cities[hegemonies[h]['capital']]['name']} ({hegemonies[h]['capital']})"
            ))
        capital = hegemonies[h]['capital']
    return capital


def hegemony_manage(h, year):

    if h not in hegemonies:
        hegemony_delete(h, year)
        return
    # hegemonies[h]['morale'] += 0.0001 * math.sqrt(len(hegemonies[h]['cities']))
    # hegemony_morale(h, 1)

    hegemony_recapital(h, year)

    hegemonies[h]['population'] = hegemony_population(h)

    neighbors = hegemony_neighbors(hegemonies[h]['capital'])

    for n in list(hegemonies[h]['conflict']):
        if n not in hegemonies:
            hegemony_delete(n, year)
            hegemonies[h]['conflict'].pop(n, None)
            continue
        if n not in hegemonies or n not in neighbors:
            hegemonies[h]['conflict'].pop(n, None)
            continue
        if (h, n) in relations_done or (n, h) in relations_done:
            continue
        if hegemonies[h].get('treaties', dict()).get(n, 0) > year:
            continue
        i = hegemonies[h]['conflict'][n]
        j = npr.choice(list(range(6)), p=transition_matrix[i])
        # if they've attacked in the last 50 years, potential for escalation
        for a in hegemonies[h]['cities']:
            for b in hegemonies[n]['cities']:
                for y in range(year - 50, year + 1):
                    if f'{y}-{a}-{b}' in battles or f'{y}-{b}-{a}' in battles:
                        j = clip(j + 1, 0, 5)
                        break
        # if there is a power differential, potential for escalation
        if hegemonies[h].get('power', 0) - hegemonies[n].get('power', 0) > 0.1:
            j = clip(j + 1, 0, 5)
        if cities[capital]['race'] == 'orc':
            j = 5
        if i == 1 and j == 0: # peace begins
            hegemony_treaty(h, n, year, w=False)
        if i in range(4) and j in range(4, 6): # war begins
            wars.setdefault(tuple(sorted([hegemonies[h]['name'], hegemonies[n]['name']])), list())
            wars[tuple(sorted([hegemonies[h]['name'], hegemonies[n]['name']]))].append([year, None])
            # print(f'wars: {len([w for w in wars if w[-1][1] is None])}')
            historytext.append((year, f'{hegemonies[h]["name"]} and {hegemonies[n]["name"]} have gone to war'))
        if i in range(4, 6) and j in range(4): # war ends
            try:
                # wars.setdefault(tuple(sorted([hegemonies[h]['name'], hegemonies[n]['name']])), list())
                wars[tuple(sorted([hegemonies[h]['name'], hegemonies[n]['name']]))][-1][1] = year
                # if math.floor(log(sum(hegemony_population(h)), 10) - 1) == math.floor(log(sum(hegemony_population(n)), 10) - 1):
                # else:
                #     historytext.append((year, f'{hegemonies[h]["name"]} and {hegemonies[n]["name"]} have agreed to peace'))
            except KeyError:
                # print(h, n, i, j)
                pass
            hegemony_treaty(h, n, year, w=True)
            j = 0
        hegemonies[h]['conflict'].update({n: j})
        hegemonies[n]['conflict'].update({h: j})
        relations_done.add((h, n))
        relations_done.add((n, h))
    # hegemonies[h]['urbanization'] = urbanization(
    #     statistics.mean(sociology[city].get('infrastructure', 0) for city in hegemonies[h]['cities'])
    # )
    if not [c for c in hegemonies[h]['cities'] if c in cities]:
        return
    for n in neighbors:
        if n in hegemonies[h]['conflict']:
            continue
        elif n not in hegemonies:
            continue
        else:
            hegemonies[h]['conflict'].update({n: 0})
            hegemonies[n]['conflict'].update({h: 0})
            relations_done.add((h, n))
            relations_done.add((n, h))


def hegemony_morale(h, n):

    if type(h) == str and h in hegemonies:
        h = hegemonies[h]

    h['morale'] = round(clip(n * h['morale'], 0, 1), 4)


def hegemony_treaty(h, n, year, w=False):

    # if a war is not ending, less chance for a treaty
    return
    if not w:
        if random.random() < 0.99:
            return
        elif random.random() < 0.5:
            return
    hegemonies[h].setdefault('treaties', dict())
    hegemonies[n].setdefault('treaties', dict())
    length = round(clip(random.gauss(5, 5), 1, None))
    hegemonies[h]['treaties'].update({n: year + length})
    hegemonies[n]['treaties'].update({h: year + length})
    hegemonies[h]['conflict'][n] = 0
    hegemonies[n]['conflict'][h] = 0
    historytext.append((
        year, f'{hegemonies[h]["name"]} and '
        f'{hegemonies[n]["name"]} have agreed to peace ({length} year treaty until {year + length})'
    ))


def hegemony_linearization_promotion(h, L=6):

    current = {hegemonies[h]['capital']}
    visited = set()
    while current:
        for c in list(current):
            if c in visited:
                continue
            pool = {i for i in hegemonies[h]['cities'] if i not in visited}
            while len(cities[c].setdefault('control', set())) < L and pool:
                for d in sorted(pool, key=lambda i: sociology[i].get('infrastructure', 0) / (hexDistance(c, i) + 1), reverse=True):
                    if d in cities[c]['control'] or d == c or d in visited or c in cities[d].get('control', set()):
                        continue
                    else:
                        cities[c]['control'].add(d)
                        break
            current |= cities[c]['control']
            current.remove(c)
            visited.add(c)
        print(len(current), len(visited), len(pool))


def hegemony_history(h):

    open(fr"{basefile}\{h} history.csv", 'w').close()
    records = hegemonysizes[h]
    years = list(range(max((records.keys()))))
    with open(fr"{basefile}\{h} history.csv", 'w') as f:
        f.write(f"year,# cities,# hint.,urban,rural,hint.,tech,city dens.,hint. dens.,alpha\n")
        for year in years:
            if year not in records:
                f.write(f"{year}\n")
                continue
            record = records[year]
            f.write(f"{year},{record['n_cities']},{record['n_hinter']},"
                    f"{record['pop'][0]},{record['pop'][1]},{record['pop'][2]},{record['tech']},"
                    f"{sum(record['pop'][:2])/record['n_cities']/346.5:.1f},"
                    f"{record['pop'][2]/record['n_hinter']/346.5 if record['n_hinter'] else 0:.1f},"
                    f"{record['alpha']:0.4f}\n")


def population_rural_v1(c):

    heg = cities[c]['hegemony']
    pu = sum(cities[city]['population'] for city in hegemonies[heg]['cities'] if cities.get(city))
    it = sum(sociology[city].get('infrastructure', 0) for city in hegemonies[heg]['cities'] if cities.get(city))
    p = cities[c]['population']
    if not it:
        return 0
    i = sociology[c].get('infrastructure', 0)
    u = hegemonies[heg]['urbanization']
    pt = round(pu / u - p)
    pr = round(i / it * pt)
    return pr


def population_rural(c, I=None, race=None):

    k = 0.002
    m = 300 # 580
    L = 60 * 2 * 346.5
    if I is None:
        I = sociology[c].get('infrastructure', 0)
    h = sociology[c].get('hinterland', cities.get(c, dict()).get('hegemony', None))
    I0 = 2268 / 2
    if not race:
        try:
            race = sociology[c].get('race', cities.get(hegemonies.get(sociology[c].get('hinterland', cities.get(c, dict()).get('hegemony')), dict()).get('capital'), dict()).get('race'))
        except:
            # print(f"Hinterland error: {c}")
            return 0
    if not race:
        return 0
        # raise KeyError
    # d = sociology[c]['desirability'][race] * (hegemony_tech_level(h) if h else 0) * (1 - math.exp(-I / m))
    d = (hegemonies[h]['tech level'] if h in hegemonies else 0) * (1 - math.exp(-I / m)) * (1 + sociology[c]['desirability'][race])
    return round(L * d / (1 + math.exp(-k * (I - I0))) + (sociology[c]['desirability'][race] * 37 - 5.25) * 347)


def rural_grow():

    for h, hex in sociology.items():
        try:
            if 'population' in hex and sociology[h]['population']:
                race = hex.get('race', cities.get(hegemonies.get(hex.get('hinterland', cities.get(h, dict()).get('hegemony')), dict()).get('capital'), dict()).get('race'))
                sociology[h]['race'] = race
                N = hex.get('population')
                K = population_rural(h)
                r = clip(round(recipe[race]['modR'] * random.random() * 0.02 * max(hex['desirability'][race], hex['desirability_raw'][race]), 4), - 0.5 / 100, 1.5 / 100)
                sociology[h]['population'] = clip(N + int(r * N * (1 - N / max(K, 1))), 0, None)
            elif 'hinterland' in hex or h in cities:
                race = hex.get('race', cities.get(hegemonies.get(hex.get('hinterland', cities.get(h, dict()).get('hegemony')), dict()).get('capital'), dict()).get('race'))
                sociology[h]['race'] = race
                N = 150
                K = population_rural(h, race=race)
                r = clip(round(recipe[race]['modR'] * random.random() * 0.02 * max(hex['desirability'][race], hex['desirability_raw'][race]), 4), - 0.5 / 100, 1.5 / 100)
                sociology[h]['population'] = N + int(r * N * (1 - N / max(K, 1)))
        except TypeError:
            print(h)
            raise TypeError
        except KeyError:
            print(h)
            raise KeyError


def k_limit(city, farmer_ratio=2, ag_ratio=0.9):

    if cities[city]['hegemony'] not in hegemonies:
        return cities[city]['K']
    if hegemonies[cities[city]['hegemony']]['tech level'] <= 6:
        return cities[city]['K']
    P = cities[city]['population']
    if city in hegemony_flood_fill(hegemonies[cities[city]['hegemony']]['capital']):
        Pr = hegemonies[cities[city]['hegemony']]['Pr']
        Pt = hegemonies[cities[city]['hegemony']]['Pt']
    else:
        Pr = sum(sociology[h].get('population', 0) * (1/16 if hexes[h]['coastal'] or climate[h]['drainage'] > 10 else 1) for h in hegemony_flood_fill(city)) * ag_ratio
        Pt = sum(cities.get(h, dict()).get('population', 0) for h in hegemony_flood_fill(city))
    if Pt == 0:
        return 0
    # n = farmer_ratio * P
    Paf = P / Pt * Pr
    return math.floor(Paf / farmer_ratio)


def city_power(city, h=2, s_avg=None):

    heg = cities[city]['hegemony'] if city in cities else sociology[city].get('hinterland', None)
    if heg not in hegemonies:
        return 0

    A = hegemonies[heg]['cities'] | hegemonies[heg].get('hinterland', set())
    cap = hegemonies[heg]['capital']
    if not s_avg:
        s_avg = hegemony_asabiya_average(h)
    d = hexDistance(city, cap)

    return round(len(A) * s_avg * math.exp(-d / h), 5)


def hegemony_power(h, hh=2):

    if h not in hegemonies:
        return 0

    A = hegemonies[h]['cities'] | hegemonies[h].get('hinterland', set())
    if not A:
        return 0
    s_avg = hegemony_asabiya_average(h)

    return round(statistics.mean([city_power(city, h=hh, s_avg=s_avg) for city in A]), 5)


def hegemony_asabiya(h, r0=0.2, d=0.1, hh=2, b=1, intensity=1):

    if h not in hegemonies:
        return

    A = hegemonies[h]['cities'] | hegemonies[h].get('hinterland', set())

    for i in A:
        asabiya.setdefault(i, random.gauss(mu=0.125, sigma=0.01))
        S = asabiya[i]
        frontier = 0
        for n in all_neighbors[i]:
            if n not in A:
                frontier = 1
                if 'hinterland' in hexes[n] or n in cities:
                    intensity = 1
                else:
                    intensity = 0.25
        if frontier:
            if 'mountain' in terrain[i]['terrain']:
                S = S - 1.5 * d * S
            if climate[i]['drainage'] >= 10:
                S = S - d / 2 * S
            else:
                S = S - d * S
        else:
            if climate[i]['drainage'] >= 10:
                S = S + 2 * r0 * S * (intensity - S) # * (1 - len(A) / (2 * b))
            else:
                S = S + r0 * S * (intensity - S) # * (1 - len(A) / (2 * b))
        asabiya[i] = clip(round(S, 3), 0, 1)


def hegemony_nsw(h, year):

    c = random.gauss(mu=0.5, sigma=0.25 / 6)
    t = hegemonies[h]['tech level']
    rho0 = 1
    b = hegemonies[h].get('asabiya', 0.5)
    alpha = t / 10
    a = alpha / 10
    beta = t / 20
    delta = b / 10
    N = hegemonies[h]['population'][0] if 'population' in hegemonies[h] else sum(cities[city]['population'] for city in hegemonies[h]['cities'])
    if not N:
        return
    N = math.log10(N)
    k = sum(min([cities[c]['K'], k_limit(c), 150000]) for c in set(hegemonies[h]['cities']) if c in cities)
    if not k:
        k = 20
    k = math.log10(k)
    S = hegemonies[h].setdefault('state resources', 0)
    W = hegemonies[h].setdefault('internal warfare', 0)
    k = clip(k - c * W, math.log10(20), math.log10(150000) * len(hegemonies[h]['cities']))
    A = hegemonies[h]['army']
    A = math.log10(A) if A else 0
    dS = rho0 * N * (1 - N / k) - beta * N - beta * 1.5 * A
    dW = a * N ** 2 - b * W - alpha * S - (sum(hegemonies[h]['conflict'][w] > 3 for w in hegemonies[h]['conflict']) / len(hegemonies[h]['conflict']) if len(hegemonies[h]['conflict']) else 0)
    dW *= (year - hegemonies[h]['t0']) > 40
    hegemonies[h]['state resources'] = round(clip(S + dS, 0, None), 3)
    hegemonies[h]['internal warfare'] = round(clip(W + dW, 0, 5), 3)


def hegemony_asabiya_average(h):

    A = hegemonies[h]['cities'] | hegemonies[h].get('hinterland', set())
    s = round(statistics.mean([asabiya.get(i, 0.5) for i in A] or [0]), 3)
    hegemonies[h]['asabiya'] = s

    return s


def new_cities_chance(race):

    n = sum(city['race'] == race for city in cities.values())
    p = math.exp(-n / 430) / n if n else 1
    return p


def new_cities(year):

    A = {h for h in hexes if 'population' in hexes[h] and not hexes[h].get('hinterland') and not h in cities and h not in hegemonies}
    for h in A:
        if sociology[h].get('population', 0) > 150 * 4 and random.random() > 0.025:
            if random.random() >= new_cities_chance(sociology[h]['race']):
                continue
            new = city_new(h, year, sociology[h]['race'])
            if new:
                hexes[h].update({'city': True})
                historytext.append((year, f'{cities[h]["name"]} ({hexes[h]["race"]}) was founded (K = {cities[h]["K"]:.0f}) and the {hegemonies[cities[h]["hegemony"]]["name"]} hegemony formed'))
                sociology[h]['population'] -= 150


def hegemony_new_cities(h, year):

    race = cities[hegemonies[h]['capital']]['race']
    if race == 'orc':
        return
    A = set(hegemonies[h].get('hinterland', set()))
    if random.random() >= new_cities_chance(race):
        return
    too_many = lambda h: len(hegemonies[h]['cities']) / (len(hegemonies[h]['cities']) + len(hegemonies[h].get('hinterland', set())))
    if random.random() < too_many(cities[city]['hegemony']):
        return

    for a in A:
        p = sociology[a].get('infrastructure', 0) / 2268 / 4
        p *= 2 if 'ruins' in hexes[a] else 1
        if random.random() > p:
            continue
        new = city_new(a, year, race, mother=hegemonies[h]['capital'], hegemony=h)
    for a in A:
        if a not in cities:
            p = hexes[a].setdefault('population', 150)
            p = (p >= 150) * p * 0.05
            if sociology[a].get('population', 0) < 150 * 4 and random.random() > 0.025:
                continue
            new = city_new(a, year, race, mother=hegemonies[h]['capital'], hegemony=h)
            if not new:
                continue
            sociology[a]['population'] -= 150
        hexes[a].pop('population', None)
        hegemonies[h]['hinterland'].discard(a)
        hegemonies[h]['cities'].add(a)
        historytext.append((year, f'{cities[a]["name"]} ({a}; K = {cities[a]["K"]}) has been founded in the {hegemonies[h]["name"]} hegemony'))


def hegemony_agriculture(ag_ratio=0.9):

    for heg in hegemonies.values():
        Pr = sum(sociology[h].get('population', 0) * (1 + 1/16 if hexes[h]['coastal'] or climate[h]['drainage'] > 10 else 1) for h in hegemony_flood_fill(heg['capital'])) * ag_ratio
        Pt = sum(cities.get(h, dict()).get('population', 0) for h in hegemony_flood_fill(heg['capital']))
        heg['Pr'] = math.floor(Pr)
        heg['Pt'] = math.floor(Pt)


def hegemony_army(h):

    if not hegemonies[h].get('army', 0):
        army = 0
        for city in hegemonies[h]['cities']:
            if city not in cities:
                continue
            race = cities[city]['race']
            a = int(cities[city]['population'] * sum(p_age[race][0:int(50 * xm[race] / xm['human'])-int(15 * xm[race] / xm['human'])+1]) * (0.5 - 0.05 - random.randint(1, 4) / 100) * 0.5 * 0.5)
            army += a
            cities[city]['population'] -= a
        for city in hegemonies[h].get('hinterland', set()) | hegemonies[h]['cities']:
            race = sociology[city].setdefault('race', cities.get(hegemonies.get(sociology[city].get('hinterland', cities.get(city, dict()).get('hegemony')), dict()).get('capital'), dict()).get('race', 'human'))
            a = int(sociology[city].get('population', 0) * sum(p_age[race][0:int(50 * xm[race] / xm['human'])-int(15 * xm[race] / xm['human'])+1]) * (0.5 - 0.05 - random.randint(1, 4) / 100) * 0.5 * 0.5)
            army += a
            if sociology[city].get('population', 0):
                sociology[city]['population'] -= a
    else:
        recruits = 0
        army = hegemonies[h].get('army', 0) or 0
        retirees = army * p_age[cities[hegemonies[h]['capital']]['race']][int(50 * xm[cities[hegemonies[h]['capital']]['race']] / xm['human'])]
        pop = sum(hegemonies[h]['population'][:3]) if 'population' in hegemonies[h] else 0
        for city in hegemonies[h]['cities']:
            if city not in cities:
                continue
            race = cities[city]['race']
            i = int(cities[city]['population'] * p_age[race][0] * (0.5 - 0.05 - random.randint(1, 4) / 100) * 0.5 * 0.5)
            # j = int(cities[city]['population'] * p_age[race][int(50 * xm[race] / xm['human'])-int(15 * xm[race] / xm['human'])+1] * (0.5 - 0.05 - random.randint(1, 4) / 100) * 0.5 * 0.5) if army else 0
            recruits += i
            j = int(retirees * cities[city]['population'] / pop) if pop else 0
            cities[city]['population'] -= i
            cities[city]['population'] += j
            army -= j
        for city in hegemonies[h].get('hinterland', set()) | hegemonies[h]['cities']:
            if 'population' not in sociology[city]:
                continue
            race = sociology[city].setdefault('race', cities.get(hegemonies.get(sociology[city].get('hinterland', cities.get(city, dict()).get('hegemony')), dict()).get('capital'), dict()).get('race', 'human'))
            i = int(sociology[city].get('population', 0) * p_age[race][0] * (0.5 - 0.05 - random.randint(1, 4) / 100) * 0.5 * 0.5)
            # j = int(sociology[city].get('population', 0) * p_age[race][int(50 * xm[race] / xm['human'])-int(15 * xm[race] / xm['human'])+1] * (0.5 - 0.05 - random.randint(1, 4) / 100) * 0.5 * 0.5) if army else 0
            recruits += i
            j = int(retirees * sociology[city]['population'] / pop) if pop else 0
            if sociology[city].get('population', 0):
                sociology[city]['population'] -= i
                sociology[city]['population'] += j
                army -= j
        # army -= retirees
        army += recruits
    hegemonies[h]['army'] = max(0, army)
    hegemonies[h]['population'] = hegemony_population(h)
    return hegemonies[h]['army']


def hegemony_neighbors(a):

    if a not in cities:
        # print(f'{a} not in cities')
        return {}
    a = cities[a]['hegemony']
    if a not in hegemonies:
        print(f'{a} not in hegemonies')
        return {}
    cities_a = hegemonies[a]['cities'] | hegemonies[a].get('hinterland', set())
    n = set()

    for i in cities_a:
        for j in [n for n in all_neighbors[i] if n in terrain]:
            if j not in cities and 'hinterland' not in sociology[j]:
                continue
            if j in cities and cities[j]['hegemony'] != a:
                n.add(cities[j]['hegemony'])
            elif 'hinterland' in sociology[j] and sociology[j]['hinterland'] != a:
                n.add(sociology[j]['hinterland'])

    return {i for i in n if i != a}


def hegemony_border_length(h):

    if h not in hegemonies:
        return None

    b = 0
    h_cities = hegemonies[h]['cities']
    for city in h_cities:
        if any(n not in h_cities for n in sociology[city]['neighbors']):
            b += 1

    return b


def hegemony_border(a, b):

    if a not in cities or b not in cities:
        return 0

    a = cities[a]['hegemony']
    b = cities[b]['hegemony']

    if a not in hegemonies or b not in hegemonies:
        return 0

    if a == b:
        return 0

    cities_a = hegemonies[a]['cities'] | hegemonies[a].get('hinterland', set())
    cities_b = hegemonies[b]['cities'] | hegemonies[b].get('hinterland', set())

    # how many hexes from A touch B?
    a_touching_b = 0
    for city in cities_a:
        a_touching_b += any(n in cities_b for n in sociology[city]['neighbors'])
    # how many hexes from B touch A?
    b_touching_a = 0
    for city in cities_b:
        b_touching_a += any(n in cities_a for n in sociology[city]['neighbors'])

    return statistics.mean([a_touching_b, b_touching_a])


def hegemony_border_hexes(a, b):

    if a not in cities and 'hinterland' not in hexes[a]:
        return None
    if b not in cities and 'hinterland' not in hexes[b]:
        return None

    a = cities[a]['hegemony'] if a in cities else sociology[a]['hinterland']
    b = cities[b]['hegemony'] if b in cities else sociology[b]['hinterland']

    if a == b:
        return 0

    cities_a = hegemonies[a]['cities'] | hegemonies[a].get('hinterland', set())
    cities_b = hegemonies[b]['cities'] | hegemonies[b].get('hinterland', set())

    # how many hexes from A touch B?
    a_touching_b = set()
    for city in cities_a:
        if any(n in cities_b for n in sociology[city]['neighbors']):
            a_touching_b.add(city)

    return a_touching_b


def great_migration(race, limit=7, threshold=0.15):

    # current = {c for c in cities if cities[c]['race'] == race}
    # for h, heg in hegemonies.items():
    #     if cities.get(heg['capital'], dict()).get('race') == race:
    #         current |= heg.get('hinterland', set())
    current = set()
    for h, hex in hexes.items():
        if 'population' in hex and sociology[h]['population'] and sociology[h]['race'] == race:
            current.add(h)
    for c in list(current):
        if len(all_neighbors[c]) == 6:
            if all(n in current for n in all_neighbors[c]):
                current.remove(c)
    visited = set()
    d = 0
    # floodfill out to limit
    while current and d <= limit:
        for c in list(current):
            # if c == b:
            #     return True
            if c in visited:
                current.remove(c)
                continue
            for n in all_neighbors[c]:
                if n in hexes:
                    if 'hinterland' in hexes[n] or n in cities:
                        continue
                    else:
                        current.add(n)
                else:
                    current.add(n)
            # current |= {n for n in all_neighbors[c] if sociology[n].get('infrastructure', 0)}
            visited.add(c)
            current.remove(c)
        # print(d, len(current), len(visited))
        d += 1
    migration = {c for c in visited if c in hexes}
    migration = {m for m in migration if sociology[m]['desirability_raw'][race] >= threshold}
    migration = {m for m in migration if m not in cities}
    migration = {m for m in migration if 'hinterland' not in hexes[m]}
    migration = {m for m in migration if 'population' not in hexes[m] or not sociology[m]['population']}

    return migration


def hegemony_png(w=153, k=1):

    s = 1 / 2
    fig, ax = newMap(0)

    t = sum([list(hegemonies[h]['cities']) for h in hegemonies], [])
    t += sum([list(hegemonies[h].get('hinterland', [])) for h in hegemonies], [])
    t = set(t)
    t = sorted([h for h in t], key=lambda h: sociology[h]['infrastructure'], reverse=True)
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    m = max(sociology[h]['infrastructure'] for h in t)
    # max_c = max(current_sst(h, season) for h in t)
    col = lambda h: screen(hegemonies.get(hegemony_get(h), {'color': 'ff0000'})['color'], w - (w * sociology[h]['infrastructure'] / m) ** k)
    c = np.array([colors.hex2color(f"#{col(h)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    fig.savefig(fr"{basefile}\Hegemony\hegemony.png", dpi=150, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def hegemony_svg(limit=7, threshold=0.15, k=0.5, w=153):

    # hegemony_colors = {h: randcolor() for h, hegemony in hegemonies.items()}
    string = ''
    m = max(city['infrastructure'] for city in sociology.values())

    for h, city in cities.items():
        # col = screen(mother_colors[cities[h]['mother']], 255 - 255 * (cities[h]['population'] / m) ** k)
        # col = hegemony_colors[cities[h]['hegemony']]
        if h not in cities:
            continue
        if city['hegemony'] not in hegemonies:
            print(h, city['hegemony'])
            continue
        col = screen(hegemonies[city['hegemony']]['color'], w - w * (sociology[h]['infrastructure'] / m) ** k)
        string += '<path d="M {:} Z" class="hex" id="{:}" style="color:#{:}" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in hexdict[h]]),
            h,
            col
        )

    for h, heg in hegemonies.items():
        # col = screen(mother_colors[cities[h]['mother']], 255 - 255 * (cities[h]['population'] / m) ** k)
        # col = hegemony_colors[cities[h]['hegemony']]
        # col = heg['color']
        for c in heg.get('hinterland', set()):
            col = screen(heg['color'], w - w * (sociology[c]['infrastructure'] / m) ** k)
            string += '<path d="M {:} Z" class="hex" id="{:}" style="color:#{:}" />'.format(
                ' '.join([','.join([str(i) for i in p]) for p in hexdict[c]]),
                c,
                col
            )

    # for h, hex in sociology.items():
    #     # col = screen(mother_colors[cities[h]['mother']], 255 - 255 * (cities[h]['population'] / m) ** k)
    #     # col = hegemony_colors[cities[h]['hegemony']]
    #     # col = heg['color']
    #     if 'hinterland' in hex or h in cities:
    #         continue
    #     if 'race' in hex and 'population' in hex:
    #         # col = screen(recipe[race]['color'], w - w * (13 / m) ** k)
    #         col = screen(recipe[sociology[h]['race']]['color'], w - w * (sociology[h]['population'] / m) ** k)
    #         string += '<path d="M {:} Z" class="hex" id="{:}" style="color:#{:}" />'.format(
    #             ' '.join([','.join([str(i) for i in p]) for p in hexdict[h]]),
    #             h,
    #             col
    #         )

    # for race in races:
    #     targets = great_migration(race, limit=limit, threshold=threshold)
    #     for h in targets:
    #         col = screen(recipe[race]['color'], w - w * (13 / m) ** k)
    #         # col = hegemony_colors[cities[h]['hegemony']]
    #         # col = heg['color']
    #         string += f"<path d='M {' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']])} Z' class='hex' id='{h}' style='color:#{col}' />"

    with open(basefile + r'\Hegemony.svg', 'r') as f:
        file = bs(f, 'lxml')
    if string:
        file.find('g', id='hegemony').contents = bs(string, 'lxml').find('body').contents
    else:
        file.find('g', id='hegemony').clear()
    save(file)


def mother_map(k=0.5):

    mothers = {city['mother'] for c, city in cities.items()}
    randcolor = lambda: ''.join(
        ['{:0x}'.format(random.randint(128, 204)), '{:0x}'.format(random.randint(128, 204)), '{:0x}'.format(random.randint(128, 204))])
    mother_colors = {mother: randcolor() for mother in mothers}
    m = max(city['population'] for c, city in cities.items())
    string = ''
    for h in cities:
        # col = screen(mother_colors[cities[h]['mother']], 255 - 255 * (cities[h]['population'] / m) ** k)
        col = mother_colors[cities[h]['mother']]
        string += '<path d="M {:} Z" class="hex" id="{:}" style="color:#{:}" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]),
            h,
            col
        )

    with open(basefile + r'\Mothers.svg', 'r') as f:
        file = bs(f, 'lxml')
    if string:
        file.find('g', id='mothers').contents = bs(string, 'lxml').find('body').contents
    else:
        file.find('g', id='mothers').clear()
    save(file, extra_file=basefile + r'\Mothers.svg')



toponyms = dict()


def add_toponym(city):

    mother = cities[city]['mother']
    toponym = toponyms.setdefault(mother, [])
    name = None
    new_prefix = False
    if 'New ' in cities[city]['name']:
        new_prefix = True
        cities[city]['name'] = cities[city]['name'][4:]
    while not name or name in {c['name'] for c in cities.values() if city != c} | {heg['name'] for heg in hegemonies.values()}:
        if not toponym:
            toponyms[mother].append((markov_word(names[cities[city]['race']], word_length=random.randint(3,4)), random.choice(['pre', 'post'])))
        roll = random.random()
        if roll < 0.45: # choose from mothers list
            topo = random.choice(toponyms[mother])
            if topo[1] == '>':
                name = topo[0].capitalize() + cities[city]['name'].lower()
                # print('{:}, population {:}, founded {:}'.format(topo[0].capitalize() + cities[city]['name'].lower(), cities[city]['population'], cities[city]['t0']))
            else:
                name = cities[city]['name'] + topo[0]
                # print('{:}, population {:}, founded {:}'.format(cities[city]['name'] + topo[0], cities[city]['population'], cities[city]['t0']))
        elif False: #0.2 <= roll < 0.45: # get similar to mother's list
            orig_topo = random.choice(toponyms[mother])
            new_topo = orig_topo
            # with stopit.ThreadingTimeout(1):
            new_topo = get_similar(names[cities[city]['race']], orig_topo[0], word_length=random.randint(3,4))
            # toponyms[mother].append((new_topo, orig_topo[1]))
            if orig_topo[1] == '>':
                name = new_topo[0].capitalize() + cities[city]['name'].lower()
                # print('{:}, population {:}, founded {:}'.format(new_topo[0].capitalize() + cities[city]['name'].lower(), cities[city]['population'], cities[city]['t0']))
            else:
                name = cities[city]['name'] + new_topo[0]
                # print('{:}, population {:}, founded {:}'.format(cities[city]['name'] + new_topo[0], cities[city]['population'], cities[city]['t0']))
        elif 0.45 <= roll < 0.5: # make a new one
            new_topo = markov_word(names[cities[city]['race']], word_length=random.randint(3,4))
            toponyms[mother].append((new_topo, random.choice(['>', '<'])))
            if toponyms[mother][-1][1] == '>':
                name = new_topo.capitalize() + cities[city]['name'].lower()
                # print('{:}, population {:}, founded {:}'.format(new_topo.capitalize() + cities[city]['name'].lower(), cities[city]['population'], cities[city]['t0']))
            else:
                name = cities[city]['name'] + new_topo
                # print('{:}, population {:}, founded {:}'.format(cities[city]['name'] + new_topo, cities[city]['population'], cities[city]['t0']))
        else:
            pass
    cities[city]['name'] = f"{'New ' if new_prefix else ''}{cluster_reduction(name.lower()).title()}"


def hegemony_control(city, n=2):

    if city in cities:
        if cities[city]['hegemony'] not in hegemonies:
            return 0
        capital = hegemonies[cities[city]['hegemony']]['capital']
    else:
        if 'hinterland' not in sociology[city]:
            return 0
        if sociology[city]['hinterland'] not in hegemonies:
            del sociology[city]['hinterland']
            return 0
        capital = hegemonies[sociology[city]['hinterland']]['capital']
    if city == capital:
        return 1
    if capital not in cities:
        return 0
    return infrastructure_impact(capital, city)


rebellion_threshold = 1 / 100


def rebellion_risks():

    with open(basefile + r'\rebellion.csv', 'w', encoding='utf8') as f:
        f.write(f'city,I0,I,C\n')
        for h, hex in hexes.items():
            if h in cities and cities[h]['population'] >= 1000:
                f.write(f'{cities.get(h, dict()).get("name", h)},{cities[h]["population"] * 5 / 346.5:.0f},{hex["infrastructure"]:},{hegemony_control(h):}\n')
            if 'hinterland' in hex:
                f.write(f'{cities.get(h, dict()).get("name", h)},,{hex["infrastructure"]:},{hegemony_control(h):}\n')


def fellow_rebels(city):

    current = {city}
    visited = set()
    while current:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            current |= {n for n in all_neighbors[c] if sociology[n].get('infrastructure', 0) and
                        cities.get(c, dict()).get('hegemony', hexes[c].get('hinterland')) == cities.get(n, dict()).get('hegemony', hexes[n].get('hinterland')) and
                        cities.get(n, dict()).get('loyalty', 0) <= cities.get(c, dict()).get("loyalty", 0) and
                        cities.get(n, dict()).get('name', '') != hegemonies.get(cities.get(c, dict()).get('hegemony'), dict()).get('name')}
            visited.add(c)
            current.remove(c)
        # print(current, visited)
    # seceding |= {h for h in hegemonies[cities[city]['hegemony']]['cities'] if h in cities and
    #              cities[h].get('loyalty', 0) <= cities[city].get("loyalty", 0) and
    #              infrastructure_impact(city, h) > rebellion_threshold * 10 and
    #              cities[h]['hegemony'] != h and
    #              cities[h]['name'] != hegemonies[cities[city]['hegemony']]['name']}
    return {c for c in visited if c in cities and c != cities[c]['hegemony']}


def city_secession(city, year):

    if cities[city]['hegemony'] == city:
        return
    if cities[city]['hegemony'] not in hegemonies:
        hegemony_manage(city, year)
        hegemony_new(city, year)
        cities[city]['loyalty'] = 0
        return
    if len(hegemonies[cities[city]['hegemony']]['cities']) == 1:
        return
    if cities[city]['name'] == hegemonies[cities[city]['hegemony']]['name']:
        return
    if hegemonies[cities[city]['hegemony']]['capital'] == city:
        return
    if 'defeated' in cities[city] and cities[city]['defeated'] + 30 < year:
        return
    if cities[city].get('loyalty', 0) > 100 * math.exp(-hegemonies[cities[city]['hegemony']].get('asabiya', 1) / 0.25):
        # if random.random() > hegemonies[cities[city]['hegemony']].get('internal warfare', 0) / 10:
        return
    if hegemonies[cities[city]['hegemony']].get('internal warfare', 1) == 0:
        return
    # if cities[city].get('loyalty', 0) >= 12 and hegemonies[cities[city]['hegemony']].get('asabiya', 1) > 0.03:
    #     # if random.random() > hegemonies[cities[city]['hegemony']].get('internal warfare', 0) / 10:
    #     return
    if (hegemonies[cities[city]['hegemony']].get('asabiya', 1) > 0.02 and
        hegemonies[cities[city]['hegemony']].get('internal warfare', 1) == 0 and
        city in hegemony_flood_fill(hegemonies[cities[city]['hegemony']]['capital'])):
        return
    if len(hegemony_flood_fill(city)) == 1:
        n = [i for i in [cities.get(n, dict()).get('hegemony') for n in sociology[city]['neighbors']] + [hexes[n].get('hinterland') for n in sociology[city]['neighbors']] if i and i in hegemonies]
        if n:
            n = random.choice(n)
            hegemony_remove_city(city, cities[city]['hegemony'])
            hegemonies[cities[city]['hegemony']]['cities'].discard(city)
            hegemonies[n]['cities'].add(city)
            cities[city]['hegemony'] = n
            historytext.append((year, f"{cities[city]['name']} ({city}) has defected to the {hegemonies[n]['name']} hegemony ({n})"))
            cities[city]['loyalty'] = 50
            cities[city]['defeated'] = year
            return
    # if hegemonies[cities[city]['hegemony']]['morale'] < morale_break:
    #     if (cities[city]['population'] * 5 / 346.5) < (50 + cities[city].get('loyalty', 0)):
    #         return
    # else:
    #     if (cities[city]['population'] * 5 / 346.5) < (100 + cities[city].get('loyalty', 0)):
    #         return
        # if hegemony_control(city) > rebellion_threshold:
        #     return
    if cities[city]['name'] in [hegemonies[h]['name'] for h in hegemonies]: # if they're rejoining a rebellion
        rejoin = [h for h in hegemonies if hegemonies[h]['name'] == cities[city]['name']][0]
        old = cities[city]['hegemony']
        hegemonies[rejoin]['cities'].add(city)
        hegemony_remove_city(city, cities[city]['hegemony'])
        hegemonies[cities[city]['hegemony']]['cities'].discard(city)
        cities[city]['hegemony'] = rejoin
        cities[city]['loyalty'] = 50
        W = 10 ** hegemonies[old].get('internal warfare', 0)
        S = hegemonies[old].get('state resources', 0)
        p = cities[city]['population'] / (cities[city]['population'] + hegemonies[old]['population'][0])
        hegemonies[rejoin]['internal warfare'] += round(math.log10(p * W), 3)
        hegemonies[old]['internal warfare'] = round(math.log10((1 - p) * W), 3)
        hegemonies[rejoin]['state resources'] += round(p * S, 3)
        hegemonies[old]['state resources'] = round((1 - p) * S, 3)
        hegemonies[rejoin]['army'] += int(round(p * hegemonies[old]['army']))
        hegemonies[old]['army'] -= int(round(p * hegemonies[old]['army']))
        historytext.append((
            year, f'{cities[city]["name"]} (loyalty: {cities[city].get("loyalty", 0)}) has seceded from '
            f'{hegemonies[cities[city]["hegemony"]]["name"]} and joined '
            f'{hegemonies[rejoin]["name"]}'))
        cities[city]['defeated'] = year
        return
    seceding = fellow_rebels(city)
    seceding = {s for s in seceding if cities[s].get('defeated', 0) + 30 < year}
    if len(seceding) < 2:
        return
    if all(s in hegemonies for s in seceding):
        return
    capital = max({s for s in seceding if s not in hegemonies }, key=lambda x: sociology[x].get('infrastructure', 0) * cities[x].get('population'))
    if rebel_power(capital, seceding) < hegemonies[cities[city]['hegemony']].get('asabiya', 1):
        return
    if len(seceding) >= 2:
        historytext.append((year, f'{cities[capital]["name"]} (loyalty: {cities[city].get("loyalty", 0)}) has seceded from '
        f'{hegemonies[cities[capital]["hegemony"]]["name"]}; '
        f'they are joined by {", ".join([cities[s]["name"] for s in seceding if s != capital])}'))
    else:
        historytext.append((year, f'{cities[capital]["name"]} (loyalty: {cities[city].get("loyalty", 0)}) has seceded from '
        f'{hegemonies[cities[capital]["hegemony"]]["name"]}'))
    hegemony_new(capital, year) # need to pick a name that doesn't exist already
    cities[city]['loyalty'] = 50
    for s in seceding:
        hegemony_remove_city(s, cities[s]['hegemony'])
        cities[s]['loyalty'] = 50
        cities[s]['defeated'] = year
    old = cities[capital]['hegemony']
    hegemonies[old]['conflict'].update({capital: 5})
    hegemonies[old]['cities'] -= seceding
    hegemonies[capital]['conflict'].update({old: 5})
    hegemonies[capital]['cities'] = seceding
    if capital in destroyed.get('hegemonies', dict()):
        hegemonies[capital]['t0'] = destroyed['hegemonies'][capital]['t0']
        hegemonies[capital]['name'] = destroyed['hegemonies'][capital]['name']
        hegemonies[capital]['color'] = destroyed['hegemonies'][capital]['color']
    for s in seceding:
        cities[s]['hegemony'] = capital
    W = 10 ** hegemonies[old].get('internal warfare', 0)
    S = hegemonies[old].get('state resources', 0)
    p = hegemonies[capital]['population'][0] / (hegemonies[capital]['population'][0] + hegemonies[old]['population'][0])
    hegemonies[capital]['internal warfare'] = clip(round(math.log10(p * W), 3), 0, None)
    hegemonies[old]['internal warfare'] = clip(round(math.log10((1 - p) * W), 3), 0, None)
    hegemonies[capital]['state resources'] = clip(round(p * S, 3), 0, None)
    hegemonies[old]['state resources'] = clip(round((1 - p) * S, 3), 0, None)
    hegemonies[capital]['army'] += int(round(p * hegemonies[old]['army']))
    hegemonies[old]['army'] -= int(round(p * hegemonies[old]['army']))
    historytext[-1] = (
        historytext[-1][0],
        historytext[-1][1] + f" (army: {hegemonies[capital]['army']})",
    )


def rebel_power(cap, others=None):

    if not others:
        others = {cap}
    s = round(statistics.mean([asabiya.get(i, 0.5) for i in others] or [0]), 3)
    S = s * len(others)
    return round(statistics.mean([S * math.exp(-hexDistance(cap, i) / 2) for i in others]), 5)


def cities_names_map():

    """
    Deletes all existing hex labels.
    Adds text to the label SVG for coordinate values
    :return:
    """

    # timer(0)  # 232 s
    # print('Fixing hex labels...')
    with open(basefile + r'\Labels.svg', 'r', encoding='utf8') as f:
        file = bs(f, 'lxml')
    # TODO only fix the ones that are wrong...should be quicker than this since I/O is heavy?
    style = ""
    # clear the existing hex label
    file.find('g', id='cities').clear()
    string = ''
    for c, city in cities.items():
        if city['hegemony'] in hegemonies and hegemonies[city['hegemony']]['capital'] == c:
            continue
            # string += f'<text id="{c}" x="{hexes[c]["x"]:.02f}" y="{hexes[c][ "y"] - 15:.02f}" class="city-label capital">{cities[c]["name"]}</text>\n'
        else:
            string += f'<text id="{c}" x="{hexes[c]["x"]:.02f}" y="{hexes[c]["y"] - 15:.02f}" class="city-label">{cities[c]["name"]}</text>\n'
    else:
        if string:
            file.find('g', id='cities').contents = bs(string, 'lxml').find('body').contents
        else:
            file.find('g', id='cities').clear()

    file.find('g', id='capitals').clear()
    string = ''
    for c, city in cities.items():
        if city['hegemony'] in hegemonies and hegemonies[city['hegemony']]['capital'] == c:
            string += f'<text id="{c}" x="{hexes[c]["x"]:.02f}" y="{hexes[c][ "y"] - 15:.02f}" class="city-label capital">{cities[c]["name"]}</text>\n'
        else:
            continue
            # string += f'<text id="{c}" x="{hexes[c]["x"]:.02f}" y="{hexes[c]["y"] - 15:.02f}" class="city-label">{cities[c]["name"]}</text>\n'
    else:
        if string:
            file.find('g', id='capitals').contents = bs(string, 'lxml').find('body').contents
        else:
            file.find('g', id='capitals').clear()

    save(file)
    # print('{:} s'.format(timer(1)))


def population_map(k=0.5, w=240):

    with open(basefile + r'\Population.svg', 'r') as f:
        file = bs(f, 'lxml')
    string = ''
    if not cities:
        return
    m = max(cities.get(c, dict()).get('population', 0) + sociology[c].get('population', 0) for c in hexes)
    for c, hex in hexes.items():
        p = cities.get(c, dict()).get('population', 0) + sociology[c].get('population', 0)
        if not p:
            continue
        r = hexes[c].get('race', cities.get(hegemonies.get(hexes[c].get('hinterland', cities.get(c, dict()).get('hegemony')), dict()).get('capital'), dict()).get('race', 'human'))
        col = screen(recipe[r]['color'], w - w * (p / m) ** k)
        if col == 'ffffff':
            continue
        string += f'<path d="M { " ".join([",".join([str(i) for i in p]) for p in hexes[c]["pts"]])} Z" class="hex" id="{c}" style="color:#{col}" />'

    # for h, heg in hegemonies.items():
    #     r = cities[heg['capital']]['race']
    #     for c in heg.get('hinterland', set()):
    #         p = sociology[c].get('population', 0)
    #
    #         col = screen(recipe[r]['color'], w - w * (p / m) ** k)
    #         if col == 'ffffff':
    #             continue
    #         string += f'<path d="M {" ".join([",".join([str(i) for i in p]) for p in hexes[c]["pts"]])} Z" class="hex" id="{c}" style="color:#{col}" />'

    if not string:
        file.find('g', id='pop').clear()
    else:
        file.find('g', id='pop').contents = bs(string, 'lxml').find('body').contents

    save(file, extra_file=basefile + r'\Population.svg')


def roads_generate_v3(clear=True, update_map=False, verbose=False, limit=3):

    if clear:
        roads_clear()
    done = set()
    # bordering capitals
    if verbose: print('Capitals...')
    capitals = sorted([
        hegemonies[h]['capital'] for h in hegemonies
    ], key=lambda x: cities[x]['t0'])
    for city in capitals:
        if roads_total(city) >= limit:
            continue
        if cities[city]['race'] == 'orc':
            continue
        heg = cities[city]['hegemony']
        targets = [hegemonies[h]['capital'] for h in hegemony_neighbors(heg) if h in hegemonies]
        for target in targets:
            if road_exists(city, target):
                continue
            if sociology[target].get('infrastructure', 0) < 10:
                continue
            road = None
            with stopit.ThreadingTimeout(5):
                road = a_star(city, target, travel=road_travel, limit=limit, major=False)
            if not road:
                continue
            if verbose: print(city, target)
            for r, s in zip(road[:-1], road[1:]):
                hexes[r].setdefault('roads', set())
                hexes[r]['roads'] |= {s}
                hexes[s].setdefault('roads', set())
                hexes[s]['roads'] |= {r}
            if len(sociology[city].get('roads', set())) < limit:
                continue
        done.add(city)
        if verbose: print(f'{len(done) / len(capitals):.3%}')
    for heg in hegemonies:
        # big cities
        cities_t = sorted([
            city for city in hegemonies[heg]['cities'] if sociology[city].get('infrastructure', 0) >= 50 and city not in capitals
        ], key=lambda x: cities[x]['t0'])
        for city in cities_t:
            if road_exists(city, hegemonies[heg]['capital']):
                continue
            road = None
            with stopit.ThreadingTimeout(5):
                road = a_star(hegemonies[heg]['capital'], city, travel=road_travel, limit=limit, major=True)
            if not road:
                continue
            if verbose: print(city, hegemonies[heg]['capital'])
            for r, s in zip(road[:-1], road[1:]):
                hexes[r].setdefault('roads', set())
                hexes[r]['roads'] |= {s}
                hexes[s].setdefault('roads', set())
                hexes[s]['roads'] |= {r}
        # tracks
        cities_t = sorted([
            city for city in hegemonies[heg]['cities'] if sociology[city].get('infrastructure', 0) < 50 and city not in capitals and not roads_total(city)
        ], key=lambda x: cities[x]['t0'])
        for city in cities_t:
            if road_exists(city, hegemonies[heg]['capital']):
                continue
            if roads_total(city) >= limit:
                continue
            targets = [h for h in hegemony_flood_fill(city) if 1 <= roads_total(h) < limit and h != city]
            if not targets:
                continue
            roads = None
            with stopit.ThreadingTimeout(5):
                road = a_star_star(city, targets, travel=road_travel, limit=limit, major=True)
            if verbose: print(city, road[-1])
            for r, s in zip(road[:-1], road[1:]):
                if s in hexes[r].get('roads', set()):
                    continue
                hexes[r].setdefault('tracks', set())
                hexes[r]['tracks'] |= {s}
                hexes[s].setdefault('tracks', set())
                hexes[s]['tracks'] |= {r}
        cities_t = sorted([
            city for city in hegemonies[heg].get('hinterland', set()) if not roads_total(city)
        ], key=lambda x: sociology[x].get('infrastructure', 0))
        for city in cities_t:
            if road_exists(city, hegemonies[heg]['capital']):
                continue
            if roads_total(city) >= limit:
                continue
            targets = [h for h in hegemonies[heg]['cities'] | hegemonies[heg].get('hinterland', set()) if 1 <= roads_total(h) < limit and h != city]
            if not targets:
                continue
            roads = []
            with stopit.ThreadingTimeout(5):
                road = a_star_star(city, targets, travel=road_travel, limit=limit, major=True)
            if verbose: print(city, road[-1])
            for r, s in zip(road[:-1], road[1:]):
                if s in hexes[r].get('roads', set()):
                    continue
                hexes[r].setdefault('tracks', set())
                hexes[r]['tracks'] |= {s}
                hexes[s].setdefault('tracks', set())
                hexes[s]['tracks'] |= {r}

    if update_map:
        roads_map()


def nohistory():
    year = 0

    # available = lambda r: [h for h in terrain if h not in cities and h not in ignore and sociology[h]['desirability_raw'][r] > 0.15]
    available = {h: max(sociology[h]['desirability_raw'], key=sociology[h]['desirability_raw'].get) for h in terrain if h not in cities}
    available = {h: available[h] for h in available if sociology[h]['desirability_raw'][available[h]] >= 0.15}
    print(len(available))
    cities_clear()
    for h in sorted(available, key=lambda h: sociology[h]['desirability_raw'][available[h]], reverse=True):
        # h = max([h for h in terrain if h not in cities and h not in ignore], key=lambda h: sociology[h]['desirability_raw']['human'])
        city_new(h, 0, available[h], new_name=False)
        if h in cities:
            k = cities[h]['K']
            cities[h]['population'] = k
            print(len(cities), cities[h]['name'], k)
        else:
            print(f'skipped {h}')
    for heg in hegemonies:
        hegemonies[heg].pop('army', None)
        hegemonies[heg]['tech level'] = hegemony_tech_level(heg)
    for h in sociology:
        if 'hinterland' in sociology[h] or h in cities:
            sociology[h]['population'] = population_rural(h)
    for heg in hegemonies:
        hegemonies[heg]['population'] = hegemony_population(heg)
        hegemonies[heg]['army'] = hegemony_army(heg)
        hegemonies[heg]['conflict'].clear()
        hegemonies[heg]['conflict'].update({n: 4 for n in hegemony_neighbors(heg)})
    cities_save()
    while len(hegemonies) > 1000:
        hegemony_check(year)
        for c in random.sample(list(cities), k=len(cities)):
            if c not in cities:
                continue
            city_grow(c)
            conflict = city_war(c, year, raid=False)
            if conflict:
                print(conflict)
                for heg in list(hegemonies):
                    if not hegemonies[heg]['cities']:
                        hegemony_delete(heg, year)
        writelog(historytext)
        for heg in list(hegemonies):
            hegemony_recapital(heg, year)
            if heg not in hegemonies:
                continue
            hegemonies[heg]['population'] = hegemony_population(heg)
            hegemonies[heg]['army'] = hegemony_army(heg)
            hegemonies[heg]['conflict'].clear()
            hegemonies[heg]['conflict'].update({n: 4 for n in hegemony_neighbors(hegemonies[heg]['capital'])})
            hegemonies[heg]['alpha/delta'] = hegemony_ad(heg)
            hegemonies[heg]['power'] = hegemony_power(heg)
        year += 1
        print(f"hegemonies: {len(hegemonies)}")
        print(f"cities: {len(cities)}")
        infrastructure(update_map=False)
    cities_save()

    hegemony_svg(k=1, w=104)
    hegemony_png(k=2, w=104)
    cities_png()
    cities_svg()
    infrastructure_png()
    # infrastructure_svg()
