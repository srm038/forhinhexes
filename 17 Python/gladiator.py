from math import floor
import random
import shortuuid


def roll(i):
    random.seed()
    returnrandom.randint(1, i)


def ability():
    x = [roll(6) for i in range(0, 4)]

    return sum(x) - min(x)


xp = [
    0,
    200,
    450,
    700,
    1100,
    1800,
    2300,
    2900,
    3900,
    5000,
    5900,
    7200,
    8400,
    10000,
    11500,
    13000,
    15000,
    18000,
    20000,
    22000,
    25000
]

levels = [
    0,
    300,
    900,
    2700,
    6500,
    14000,
    23000,
    34000,
    48000,
    64000,
    85000,
    100000,
    120000,
    140000,
    165000,
    195000,
    225000,
    265000,
    305000,
    355000
]


class Fighter:
    def __init__(self, id):

        self.id = id
        self.strength = ability()
        self.constitution = ability()
        self.dexterity = ability()
        self.stats = [self.strength, self.dexterity, self.constitution]
        self.strmod = floor((self.strength - 10) / 2)
        self.conmod = floor((self.constitution - 10) / 2)
        self.dexmod = floor((self.dexterity - 10) / 2)
        self.level = 1
        self.prof = 2
        self.hp = 10 + self.conmod
        # if they're strong enough they'll wear chain
        if self.strength < 13:
            self.ac = 12 + self.dexmod + 2
        else:
            self.ac = 16 + 2
        # just padded
        self.ac = 12 + self.dexmod + 2
        # just chain
        self.ac = 16 + 2
        self.age = 18
        self.fights = 0
        self.xp = 0
        self.n_attacks = 1

    def levelup(self):

        self.level += 1
        self.hp += self.conmod + roll(10)
        if self.level in [5, 9, 13, 17]: self.prof += 1
        # Extra Attacks
        if self.level in [5, 11, 20]: self.n_attacks += 1
        # Ability Score Increase
        if self.level in [4, 6, 8, 12, 14, 16, 19]:
            a = roll(3)
            b = roll(3)
            self.stats[a] = min(self.stats[a] + 1, 20)
            self.stats[b] = min(self.stats[b] + 1, 20)

    def growup(self):

        self.age += 1
        if self.age > 60:
            self.strength -= int(roll(8) > 6)
            self.constitution -= int(roll(8) > 6)
            self.dexterity -= int(roll(8) > 6)
            self.strmod = floor((self.strength - 10) / 2)
            self.conmod = floor((self.constitution - 10) / 2)
            self.dexmod = floor((self.dexterity - 10) / 2)

    def attack(self):

        atk = roll(20)
        if atk == 20:
            dmg = 8 + max(roll(8) + self.strmod, 0)
        else:
            dmg = max(roll(8) + self.strmod, 0)

        # shortsword
        # return (
        # roll20() + self.strmod + self.prof,
        # max(roll6() + self.strmod, 0)
        # )
        # longsword
        return (
            atk + self.strmod + self.prof + 2,
            dmg
        )

    def print_stats(self):

        with open(filename, 'a') as f:
            f.write(','.join([str(self.level),
                              str(self.id),
                              str(self.age),
                              str(self.fights),
                              str(self.strength),
                              str(self.dexterity),
                              str(self.constitution),
                              str(self.hp),
                              str(self.xp) + '\n']))


global numbers
numbers = 400

global pool
pool = [Fighter(shortuuid.uuid()) for i in range(0, numbers)]

fights_per_year = 4


def fight(years, alt = 0):
    global pool

    y = 0
    while y < years:
        random.shuffle(pool)
        for t in range(fights_per_year):
            for i in range(0, len(pool), 2):
                # x = 1
                # try:
                #     while abs(fi.level - pool[i + x].level) > 1:
                #         x += 1
                #     else:
                #         fj = pool[i + x]
                # except:
                #     pass
                try:
                    fi = pool[i]
                    fj = pool[i + 1]
                    tothedeath(fi, fj, alt)
                except:
                    pass
        for f in pool: f.growup()
        while len(pool) < numbers:
            pool.append(Fighter(shortuuid.uuid()))
        y += 1


alt_xp = 0

filename = r'D:\Dropbox\Predictor\PY\gladiator_career' + ('_alt.csv' if alt_xp else '.csv')

def tothedeath(f1, f2, alt = 0):
    global pool

    hp1 = f1.hp
    hp2 = f2.hp

    while f1.hp > 0 and f2.hp > 0:
        for atk in range(0, f1.n_attacks):
            hit = f1.attack()
            if hit[0] >= f2.ac:
                f2.hp -= hit[1]
                if alt:
                    f2.xp += 50 * hit[1]
                    f1.xp += hit[1]
        for atk in range(0, f2.n_attacks):
            hit = f2.attack()
            if hit[0] >= f1.ac:
                f1.hp -= hit[1]
                if alt:
                    f1.xp += 25 * hit[1]
                    f2.xp += hit[1]

    if f1.hp >= 0:
        f1.hp = hp1
        f2.hp = hp2
        f1.fights += 1
        if not alt: f1.xp += xp[f2.level]
        while f1.xp > levels[f1.level]: f1.levelup()
        if f2.level: log_career(f1, f2, alt)
        pool.remove(f2)
    else:
        f2.hp = hp2
        f1.hp = hp1
        f2.fights += 1
        if not alt: f2.xp += xp[f1.level]
        while f2.xp > levels[f2.level]: f2.levelup()
        if f1.level: log_career(f2, f1, alt)
        pool.remove(f1)


def show_results():
    for f in pool:
        print(','.join([
            str(f.level),
            str(f.id),
            str(f.age),
            str(f.fights),
            str(f.strength),
            str(f.dexterity),
            str(f.constitution),
            str(f.hp),
            str(f.xp)
        ]))


def log_career(x, y, alt = 0):
    filename = r'D:\Dropbox\Predictor\PY\gladiator_career' + ('_alt.csv' if alt else '.csv')
    with open(filename, 'a') as f:
        f.write(','.join([
            str(x.level),
            str(x.id),
            str(x.age),
            str(x.fights),
            str(x.strength),
            str(x.dexterity),
            str(x.constitution),
            str(x.hp),
            str(x.xp),

            str(y.level),
            str(y.id),
            str(y.age),
            str(y.fights),
            str(y.strength),
            str(y.dexterity),
            str(y.constitution),
            str(y.xp) + '\n'
        ]))


def start_over(alt = 0):
    filename = r'D:\Dropbox\Predictor\PY\gladiator_career' + ('_alt.csv' if alt else '.csv')
    open(filename, 'w').close()
    with open(filename, 'a') as f:
        f.write('Level,ID,Age,Fights,STR,DEX,CON,HP,XP,Level,ID,Age,Fights,STR,DEX,CON,XP\n')


start_over(0)
start_over(1)
