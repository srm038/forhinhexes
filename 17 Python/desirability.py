from defs import *
from resources import *

load_data(elevation, fr"{basefile}\elevation.json")
load_data(climate, fr"{basefile}\climate.json")
# load_data(sociology, sociology_file)
load_data(sociology, fr"{basefile}\sociology.json")
load_data(terrain, fr"{basefile}\terrain.json")
load_data(topography, fr"{basefile}\topography.json")
load_data(hex_resources, tradefilepath + r'\resources.p')

desire_file = r'G:\forhinhexes\12 Species\desirability.json'

recipe = dict()
load_data(recipe, desire_file)

races = list(recipe)
races.remove('orc')

# with open(cities_file, 'rb') as f:
#     cities = pickle.load(f)

# cities = {sociology[h]['city']: {'location': h, 'pop': 0} for h in hexes if sociology[h]['city']}


def desirability_clear(races=races):
    """
    Clear the svg desirability heatmap
    :param races:
    :return:
    """

    with open(basefile + r'\Desirability.svg', 'r') as f:
        file = bs(f, 'lxml')
    for race in races:
        file.find('g', id=race).clear()
    save(file, extra_file=basefile + r'\Desirability.svg')


def desirability(races=races, recipe=recipe, verbose=True, gold_boost=2, ocean=None):

    """
    Assign desirability to hexes based on recipe conditions
    :param update_map:
    :param races:
    :param recipe:
    :param verbose:
    :return:
    """

    if not ocean:
        ocean = oceanGet(seaLevel=seaLevelGet())

    if verbose: print('Finding good places to live...')
    max_drain = max(elevation[h]['drainage'] for h in elevation if h not in ocean)
    for race in races:
        coastal = recipe[race]['coastal']
        river = recipe[race]['river']
        harbor = recipe[race]['naturalHarbor']
        infra = recipe[race]['infrastructure']
        fB, aB, nB = recipe[race]['favoredBiomes'], recipe[race]['avoidedBiomes'], recipe[race]['noBiomes']
        fT, aT, nT = recipe[race]['favoredTopo'], recipe[race]['avoidedTopo'], recipe[race].get('noTopo', set())
        fTr, aTr, nTr = recipe[race]['favoredTerrain'], recipe[race]['avoidedTerrain'], recipe[race]['noTerrain']
        for h in elevation.keys():
            if h in ocean:
                continue
            d = len(hex_resources.get(h, set())) + gold_boost * ('gold' in hex_resources.get(h, set()))
            # d = 0
            d2c = elevation[h]['dist_to_coast']
            lat = latitude(h)
            d += coastal * (d2c <= 6) * (1 - d2c / 6) * 3 * math.sqrt(1 - abs(lat) / 90)
            d += coastal * (d2c == 1) * (elevation[h]['altitude'] < 120) * math.sqrt(1 - abs(lat) / 90)
            d += -1 * coastal * (d2c == 1) * (elevation[h]['altitude'] > 500)
            d += harbor * natural_harbor(h, ocean) * 4 * math.sqrt(1 - abs(lat) / 90)
            for c in climate[h]['koppen']:
                if c in fB:
                    d += 1
                if c in aB:
                    d -= 3
                if c in nB:
                    d *= 0
            for c in topography[h]:
                if c in fT:
                    d += 1
                if c in aT:
                    d -= 3
                if c in nT:
                    d *= 0
            for c in terrain[h]:
                if c in fTr:
                    d += 1
                if c in aTr:
                    d -= 3
                if c in nTr:
                    d *= 0
            # d += river * terrain[h]['lake'] * math.sqrt(abs(lat) / 90) * ('EF' not in climate[h]['koppen'])
            d += river * elevation[h]['drainage'] / max_drain * 15 * math.sqrt(1 - abs(lat) / 90) * ('EF' not in climate[h]['koppen'])
            d += river * (len([n for n in elevation[h]['drain_in'] if elevation[n]['drainage'] >= 5]) - 1) / 2 * math.sqrt(1 - abs(lat) / 90) * ('EF' not in climate[h]['koppen'])
            # d -= len(sociology[h].get('ruins', dict())) / 2
            # TODO the raw desire should actually penalize infra?
            sociology.setdefault(h, dict())
            sociology[h].setdefault('desirability_raw', dict())
            d = clip(d, 0, None)
            sociology[h]['desirability_raw'][race] = d
        min_d = min(sociology[h]['desirability_raw'][race] for h in terrain.keys())
        max_d = max(sociology[h]['desirability_raw'][race] for h in terrain.keys())
        for h in sociology.values():
            h['desirability_raw'][race] = rescale(h['desirability_raw'][race], min_d, max_d, 0, 1)
        n = 1
        for n in range(10, 100):
            if round(statistics.mean([sociology[h]['desirability_raw'][race] ** (10 / n) for h in terrain.keys() if sociology[h]['desirability_raw'][race]]), 1) == 0.3:
                break
        for h in sociology.keys():
            sociology[h]['desirability_raw'][race] = round(sociology[h]['desirability_raw'][race] ** (10 / n), 4)
            if not sociology[h].get('infrastructure', 0):
                sociology[h].setdefault('desirability', dict())
                sociology[h]['desirability'][race] = sociology[h]['desirability_raw'][race]
                continue
                # if any(hexes[n].get('infrastructure', 0) for n in hex['neighbors']):
                #     hex['desirability_raw'][race] *= 1.9
            h_inf = sociology[h].get('infrastructure', 0)
            sociology[h]['desirability'][race] = clip(
                round(sociology[h]['desirability_raw'][race] + (
                    infra * h_inf * 0.375 * (1 - math.exp(-h_inf / 580)) +
                    infra * any(sociology[c].get('city', False) for c in all_neighbors[h] if c not in ocean) * 0.125 +
                    infra * len(sociology[h].get('roads', set())) * 0.1), 4)
                , 0, 1)
        if verbose: print(f"{race}: {100 * sum(sociology[h]['desirability'][race] >= 0.5 for h in sociology) / len(sociology):0.1f} | {sum(sociology[h]['desirability_raw'][race] > 0.5 for h in sociology) / len(sociology):0.1%}")


def desirability_svg(races=races, raw=False, threshold=0.15):

    """
    Write a heatmap of desirability to file
    :param races:
    :return:
    """

    with open(basefile + r'\Desirability.svg', 'r') as f:
        file = bs(f, 'lxml')
    for g in file.find_all('g'):
        g.clear()
    for race in races:
        string = ''
        m = max(sociology[h]['desirability_raw'][race] for h in hexes if h not in cities)
        for h in hexes:
            if h in cities:
                continue
            if sociology[h]['desirability_raw'][race] < threshold:
                continue
            col = screen(recipe[race]['color'], 255 - 255 * sociology[h]['desirability_raw'][race] / m)
            if col == 'ffffff':
                continue
            string += '<path d="M {:} Z" class="hex" id="{:}" style="color:#{:}" />'.format(
                ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]),
                h,
                col
            )

        if string:
            file.find('g', id=race).contents = bs(string, 'lxml').find('body').contents
        else:
            file.find('g', id=race).clear()
    save(file, extra_file=basefile + r'\Desirability.svg')


def desirability_png(race, threshold=0.25):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59],
         [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in sociology if hexdict[h][0][1] >= hexdict['207-518W'][0][1]],
               key=lambda h: sociology.get(h, dict()).get('desirability_raw', dict()).get(race, 0))
    t = [i for i in t if sociology.get(i, dict()).get('desirability_raw', dict()).get(race, 0) >= threshold]
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    m = max(sociology[h]['desirability_raw'][race] for h in sociology if h not in cities)
    # max_c = max(current_sst(h, season) for h in t)
    col = lambda h: screen(recipe[race]['color'], 255 - 255 * sociology[h]['desirability_raw'][race] / m)
    c = np.array([colors.hex2color(f"#{col(h)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Desirability\{race}.png", dpi=150, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()



def desirability_all_png(threshold=0.14):

    fig, ax = newMap(0)

    available = {h: min(sociology[h]['desirability_raw'], key=sociology[h]['desirability_raw'].get) for h in terrain}
    available = {h: available[h] for h in available if sociology[h]['desirability_raw'][available[h]] >= threshold}

    s = 1 / 2
    t = sorted([h for h in available], key=lambda h: sociology.get(h, dict()).get('desirability_raw', dict()).get(available[h], 0))
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    m = max(sociology[h]['desirability_raw'][available[h]] for h in available)
    # max_c = max(current_sst(h, season) for h in t)
    col = lambda h: screen(recipe[available[h]]['color'], 255 - 255 * sociology[h]['desirability_raw'][available[h]] / m)
    c = np.array([colors.hex2color(f"#{col(h)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    fig.savefig(fr"{basefile}\Desirability\all.png", dpi=150, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def natural_harbor(h, ocean):

    return bool(
        elevation[h]['dist_to_coast'] * len([n for n in all_neighbors[h] if n not in ocean]) == 5
    )


def set_harbors():

    for h, hex in hexes.items():
        hex['naturalharbor'] = natural_harbor(h)
    save_data(hexes, hexpickle)


def nearest_city(h):

    try:
        i = min([cities[c]['location'] for c in cities if c != h], key=lambda x: hexDistance(cities[x]['location'], h))
        j = hexDistance(cities[i]['location'], h)
        return i, j
    except:
        return 0, 1

