from defs import *

# import matplotlib.pyplot as plt
# import forceatlas2 as fa2
import networkx as nx

from tradenetwork import *
from resources import *
from history import *

load_data(sociology, sociology_file)

# G = nx.MultiDiGraph()

# TODO rework for the new system

rarity = 0.02

# layover = 1

# filepath = r'C:\Users\Shelby\Google Drive\D&D\Behind the Veil\Maps\'
filepath = r'G:\Drive\D&D\Behind the Veil\15 Locations\Trade'

# routesfile = filepath + r'\traderoutes.csv'
# referencesfile = filepath + r'\assignment.csv'

martialweapons = filepath + r'\martial.csv'
rangedweapons = filepath + r'\ranged.csv'
servicesfile = filepath + r'\services.csv'
# armorfile
commodityfile = filepath + r'\commodities.csv'
industryfile = filepath + r'\industry.csv'
logfile = filepath + r'\log.txt'
gemsfile = filepath + r'\gems.csv'
travelmodsfile = filepath + r'\travel.csv'
foodstuffsfile = filepath + r'\foodstuff.csv'

# def log(text):
#     """
#
#     :param text:
#     :return:
#     """
#
#     with open(logfile, 'a') as f:
#         f.write(str(text) + '\n')

#
# cities = {
#     'Demoland': {
#         'Andox',
#         'Malis',
#         'Gerlin',
#         'Derl',
#         'Ffith',
#         'Cadewin',
#         'Betryn',
#         'Llen',
#         'Norys',
#         'Kenor',
#     },
# }
#
# season = {
#     'Demoland': {'dry'}
# }
#
# major_trade = {'Andox'}
# major_trade_switch = 0

# travelmods = {}
# with open(travelmodsfile, 'r') as f:
#     reader = csv.reader(f, delimiter = ',')
#     next(reader, None)
#     for row in reader:
#         try:
#             travelmods[row[0]] = {
#             'mod': float(row[1])
#         }
#         except:
#             pass

# foodstuffs = {}


def nation(loc):
    for c in cities:
        if loc in cities[c]:
            return c
    else:
        return None

#
# diplomacy = {
#     'Tjavan': {
#         'hostile': {},
#         'friendly': {'Lone Hope', 'Ehedvrath'},
#         'uneasy': {'Marca'}
#     },
#     'Lone Hope': {
#         'hostile': {},
#         'friendly': {'Tjavan', 'Ehedvrath', 'Marca'},
#         'uneasy': {''}
#     },
# }
#
# monopoly = {
#     'Tjavan': {'glass'},
#     'Lone Hope': {'sugarcane', 'sugar'}
# }


# unknown = {
#     'Tjavan': {'glaive'}
# }




# routes = []


# create graph from edgelist
def graph_from_network():

    G = nx.DiGraph()
    maxd = max([list(r)[3] for r in routes])
    for r in routes:
        G.add_edge(
            list(r)[1],
            list(r)[2],
            key = list(r)[0],
            weight = round(list(r)[3] / maxd, 4),
            distance = list(r)[3],
            # rise = r[6],
            # route = r[3]
        )

    return G


# with open(logfile, 'w') as f:
#     pass

# determine base reference relationships

# commodity = {'gold': {'unit/ref': 1500, 'unit': 'oz'}}
# commodity['silver'] = {'unit/ref': gold_ref * 10, 'unit': 'lb'}
# commodity['copper'] = {'unit/ref': gold_ref * 100, 'unit': 'lb'}

# party_gold = 10000 / gold_ref * 0
# party_loc = 'Lone Hope'

# with open(commodityfile, 'r') as f:
#     reader = csv.reader(f, delimiter = ',')
#     next(reader, None)
#     for row in reader:
#         commodity[row[0]] = {
#             'unit cost': float(row[1]),
#             'unit/ref': round(100 * gold_ref / float(row[1])),
#             # 'unit/ref': float(row[1]) / 100
#             'unit': row[2],
#             'stage': int(row[3]) if row[3] not in ['p', 'x'] else row[3],
#             'tech level': int(row[4]),
#             'weight': float(row[5]) if row[3] not in ['p', 'x'] else None,
#             'occurrence': float(row[6])/100 if row[6] else None,
#             'terrain': list(row[7]),
#             'ht': eval(row[8]) if row[8] != '' else None,
#             'lat': eval(row[9]) if row[9] != '' else None,
#             'koppen': row[10] if row[10] != '' else None,
#             'coincident': row[11].split(','),
#             'topo': row[12].split(','),
#             # '/ref': float(row[13]) if row[13] != '' else None,
#             '/acre': float(row[14]) if row[14] != '' else None,
#             'acre/pop': float(row[15]) if row[15] != '' else None,
#             'roll': row[16],
#             'recipe': eval(row[17]),
#             'type': row[18],
#         }


# for loc in G:
#     G.node[loc]['references'] = {}
#     G.node[loc]['racial references'] = {}
#     for c in commodity:
#         G.node[loc]['references'][c] = 0
#         G.node[loc][c] = 0


# races = {
#     'human',
#     'elf',
#     'dwarf'
# }


def check_tech(loc, pop = 0):
    if loc:
        for lvl in tech_levels:
            if G.node[loc]['population'] in tech_levels[lvl]:
                return (lvl)
    elif pop:
        for lvl in tech_levels:
            if pop in tech_levels[lvl]:
                return (lvl)
    return 18


def references_set(G, reset=True):

    if reset:
        hex_references.clear()
        for city in G:
            heg_tech = hegemony_tech_level(cities[city]['hegemony'])
            available = [r for r in hex_resources[city] if heg_tech >= commodity[r]['tech level']]
            if not available:
                continue
            for a in available:
                # pop = floor(min(0.25, 1 / len(available)) * 0.67 * (sociology[city]['infrastructure'] * 5 + cities[city]['population']))
                pop = population_rural(city) + cities.get(city, dict()).get('population', 0) * int(commodity[a]['type'] != 'A')
                pop = floor(min(0.25, 1 / len(available)) * pop)
                if not commodity[a]['ref/1000pop']:
                    if not commodity[a]['ref/acre']:
                        continue
                    n = round(pop * commodity[a]['ref/acre'] * commodity[a]['acre/pop'], 4)
                else:
                    n = round(pop * commodity[a]['ref/1000pop'] / 1000, 4)
                if commodity[a]['type'] == 'A':
                    n = round(max(0, n / (2268 - 1) * (2268 - sociology[city]['infrastructure'])), 4)
                hex_references.setdefault(city, dict())
                hex_references[city].update({a: n})
            for i in [c for c in commodity if commodity[c]['stage'] == 'p']:
                if city not in cities:
                    continue
                if cities[city]['population'] < commodity[i]['recipe']['SV']:
                    continue
                if sociology[city]['infrastructure'] < tech_infra[commodity[i]['tech level']]:
                    continue
                p = (cities[city]['population'] - commodity[i]['recipe']['SV']) / (cities[city]['population'] - commodity[i]['recipe']['SV'] / 2)
                n = 0
                for j in range(floor(cities[city]['population'] / commodity[i]['recipe']['SV'])):
                    if random.random() < p:
                        n += 1
                if n:
                    hex_references.setdefault(city, dict())
                    hex_references[city].update({i: n})
    for city in G:
        G.node[city]['references'] = dict()
        for a in hex_references.get(city, dict()):
            G.node[city]['references'][a] = hex_references[city][a]
        # G.node[city].setdefault('racial references', dict()).update({cities[city]['race']: cities[city]['population']})
        if city in cities:
            G.node[city].setdefault('racial references', dict())
            G.node[city]['racial references'].update({cities[city]['race']: population_rural(city) + cities[city]['population']})
        else:
            G.node[city].setdefault('racial references', dict())
    graph_save(G)


def set_references_old(G):
    """

    :param G:
    :return:
    """

    # referencesfile = r'D:\Dropbox\Python Projects\references.csv'
    # referencesfile = r'D:\Dropbox\Python Projects\tjavan hexes.csv'
    # referencesfile = r'D:\Drive\D&D\Behind the Veil\Maps\tjavan hexes.csv'
    # referencesfile = r'C:\Users\Shelby\Google Drive\D&D\Behind the Veil\Maps\tjavan hexes.csv'

    def parse_reference(ref, type = '', loc = '', pop = 1e9):

        if ref == '':
            val = 0
        elif 'x' in ref:
            try:
                if commodity[type]['stage'] == 'p':
                    val = pop > commodity[type]['recipe']['SV']
                    # log(loc + ': ' + type + ', ' + str(ref))
                else:
                    val = 0
            except KeyError:
                val = 0
        else:
            try:
        #         # check = check_tech(False, pop = pop) >= commodity[type]['tech level']
                val = float(ref) #* check
        #         # if not check:
        #         #     log('!! ' + loc + ': ' + type + ', ' + str(ref))
            except KeyError:
                print(ref, type)
                val = float(ref)
        return val

        # log(loc + ': ' + type + ', ' + str(val))

    with open(referencesfile, 'r') as f:
        reader = csv.reader(f, delimiter = ',')
        for row in reader:
            if 'q' in str(row[0]):
                commodities = [str(x) for x in row[6:]]
                log(', '.join(commodities))
            elif str(row[0]) == 'cost':
                pass
            elif str(row[0]) == 'units':
                pass
            elif '' in row[:6]:
                pass
            else:
                if int(row[5]) >= 10:
                    try:
                        G.node[str(row[2])]['population'] += int(row[5])
                    except KeyError:
                        if str(row[2]) not in G.nodes():
                            print(f'{row[2]} must not be connected to the network')
                        else:
                            G.node[str(row[2])]['population'] = int(row[5])
                    except ValueError:
                        print(row[0], row[1])
                for i, c in enumerate(row[6:]):
                    if commodities[i] in races and str(row[2]) in G.nodes:
                        city = G.node[str(row[2])]['racial references'].setdefault(commodities[i], 0)
                        city += int(c or 0) * G.node[str(row[2])]['population']
                        continue
                    try:
                        G.node[str(row[2])]['references'][commodities[i]] += parse_reference(c, commodities[i],
                                                                                             str(row[2]), int(row[3]))
                    except KeyError:
                        if str(row[2]) not in G.nodes():
                            # TODO handle this by adding it as a unique node
                            print(f'{row[2]} must not be connected to the network')
                        else:
                            G.node[str(row[2])]['references'][commodities[i]] = parse_reference(c, commodities[i],
                                                                                            str(row[2]), int(row[3]))
                    except KeyError:
                        print('error', row[0], row[1], i)
                    except ValueError:
                        print('error', row[0], row[1], commodities[i])


# TODO if there is no market somewhere, give the resources to the closest market town


def closest_market(G, here, market_towns=dict()):

    closest = {}
    if any(n in market_towns and n in nx.algorithms.descendants(G, here) for n in hexes[here]['neighbors']):
        try:
            return min([n for n in hexes[here]['neighbors'] if n in market_towns and n in nx.algorithms.descendants(G, here)], key=lambda n: trade_distances[here][n])
        except KeyError:
            return None
    for loc in nx.algorithms.descendants(G, here) & set(market_towns):
        # if hex_distance(loc, here) > 21:
        #     continue
        closest.update({loc: trade_distances[here][loc]})
    if not closest:
        return None
    return min(closest, key = closest.get)


def trade_distance(G, a, b):

    return round(nx.astar_path_length(
        G,
        source = a,
        target = b,
        weight = 'distance'
    ), 2)


def route(a, b):

    return nx.astar_path(
        G,
        source = a,
        target = b,
        weight = 'distance'
    )


def embargo(loc):
    G.remove_edges_from([x for x in G.edges() if loc in x])


def trade_distances_generate(G, clear=True):

    i = 0
    if clear: trade_distances.clear()
    for source in sorted(G, key=lambda x: len(nx.algorithms.descendants(G, x))):
        i += 1
        trade_distances.setdefault(source, dict())
        print(f'{source}: {len(nx.algorithms.descendants(G, source))}; {(i - 1) / len(G.nodes()):.2%}')
        for target in nx.algorithms.descendants(G, source):
            trade_distances.setdefault(target, dict())
            if target in trade_distances[source]:
                pass
            else:
                try:
                    trade_distances[source].update({target: trade_distance(G, source, target)})
                except nx.exception.NetworkXNoPath:
                    pass
            if source in trade_distances[target]:
                pass
            else:
                try:
                    trade_distances[target].update({source: trade_distance(G, target, source)})
                except nx.exception.NetworkXNoPath:
                    pass
    with open(tradefilepath + r'\tradedistances.p', 'wb') as f:
        pickle.dump(trade_distances, f)


def get_currency(G, c):

    if G.node[c]['references'].get('gold'):
        return 'gold'
    if any(G.node[loc]['references'].get('gold') for loc in nx.algorithms.descendants(G, c)):
        return 'gold'
    if G.node[c]['references'].get('silver'):
        return 'silver'
    if any(G.node[loc]['references'].get('silver') for loc in nx.algorithms.descendants(G, c)):
        return 'silver'
    if G.node[c]['references'].get('chalcopyrite'):
        return 'chalcopyrite'
    if any(G.node[loc]['references'].get('chalcopyrite') for loc in nx.algorithms.descendants(G, c)):
        return 'chalcopyrite'
    return None


def calculate_trade(G, verbose=False):

    # clear out existing values
    print('Setting up...')
    for loc in G:
        for c in list(G.node[loc]):
            if c not in ['references', 'population', 'racial references']:
                del G.node[loc][c]
        G.node[loc]['pricing references'] = dict()
        G.node[loc]['demographics'] = {r: G.node[loc]['racial references'].get(r, 0) for r in races}
    print(f"{len(G.nodes())} locations")
        # G.node[loc]['hegemony demographics'] = {r: G.node[loc]['racial references'].get(r, 0) for r in races}

    mantissa = lambda i: (100 * i / len(G.nodes()) - int(100 * i / len(G.nodes())))

    print('Calculating demographics...')
    i = 0
    for source in G:
        for target in nx.algorithms.descendants(G, source):
            if source == target:
                continue
            # if relationship(source, target) > 3:
            #     continue
            for r in races:
                d = trade_distances.get(source, dict()).get(target, None)
                if not d:
                    continue
                G.node[source]['demographics'][r] += G.node[target]['racial references'].get(r, 0) / d
        G.node[source]['demographics'] = {
            r: round(G.node[source]['demographics'][r] / sum(G.node[source]['demographics'][x] for x in races), 3) for r in races
        }
        for r in races:
            G.node[source]['racial demographics'] = {r: round(G.node[source]['demographics'].get(r, 0) * sum(G.node[source]['racial references'].values())) for r in races}
        i += 1
        if verbose and mantissa(i) < 0.01: print(f'{i / len(G.nodes()):.0%}')

    # redistribute resources to closest market towns
    print('Redistribute resources to markets...')
    i = 0
    market_towns = get_markets(G)
    for loc in G:
        if loc not in market_towns or not hexes[loc].get('roads', set()):
            m = closest_market(G, loc, market_towns=market_towns)
            if not m:
                continue
            # log(loc + ' sends its goods to ' + m)
            for c in commodity:
                if G.node[loc]['references'].get(c, 0):
                    G.node[m]['references'].setdefault(c, 0)
                    G.node[m]['references'][c] += G.node[loc]['references'].get(c, 0)
                    del G.node[loc]['references'][c]
        i += 1
        if verbose and mantissa(i) < 0.01: print(f'{i / len(G.nodes()):.0%}')
            # G.node[m]['hegemony population'] = G.node[m].get('hegemony population', 0) + G.node[loc]['population']
            # G.node[m]['hegemony demographics'] = G.node[m].get('hegemony population', 0) + G.node[loc]['population']

    # TODO require a certain resource for certain technologies
    # for loc in G:
    #     for c in labor:
    #         try:
    #             if loc not in market_towns:
    #                 m = closest_market(loc)
    #                 G.node[m][c] += G.node[loc].get(c, 0)
    #         except KeyError:
    #             pass

    # calculate new values
    print('Calculate currencies...')
    i = 0
    per_oz = {'gold': gp_per_oz, 'silver': sp_per_oz, 'chalcopyrite': cp_per_oz / 2.8}
    to_cp = {'gold': 100, 'silver': 10, 'chalcopyrite': 1}
    for loc_here in G:
        currency = get_currency(G, loc_here)
        if not currency:
            continue
        # log(loc_here + ' fau')
        aau = G.node[loc_here]['references'].get(currency, 0) * currency_ratio
        aur = G.node[loc_here]['references'].get(currency, 0)
        bau = G.node[loc_here]['references'].get(currency, 0) * currency_ratio
        e = rarity
        for loc in nx.algorithms.descendants(G, loc_here):
            if loc_here != loc:
                if relationship(loc, loc_here) > 3:
                    continue
                try:
                    d = trade_distances.get(loc, dict()).get(loc_here, None)
                    if not d:
                        continue
                    aau += G.node[loc]['references'].get(currency, 0) * currency_ratio / d
                    aur += G.node[loc]['references'].get(currency, 0) / d
                    bau += G.node[loc]['references'].get(currency, 0) * currency_ratio
                except KeyError:
                    # log('::err ' + loc + ' ' + c)
                    pass
                except nx.exception.NetworkXNoPath:
                    # log('::err ' + loc + ' ' + c)
                    pass

        try:
            fau = to_cp[currency] * per_oz[currency] * (1 + e) * bau / aau
            G.node[loc_here]['fau'] = round(fau, 4) # gold cp/unit local price
            G.node[loc_here]['pricing references'][currency] = round(aur, 4)
            G.node[loc_here]['standard'] = currency
        except ZeroDivisionError:
            pass
        i += 1
        if verbose and mantissa(i) < 0.01: print(f'{i / len(G.nodes()):.0%}')

    print('Calculate raw reference prices...')
    i = 0
    for loc_here in G:
        # log(loc_here + ' stage 0')
        if not G.node[loc_here].get('fau', 0):
            continue
        currency == G.node[loc_here]['standard']
        for c in stage[0]:
            if c == currency:
                continue
            a = G.node[loc_here]['references'].get(c, 0)
            b = G.node[loc_here]['references'].get(c, 0)
            j = commodity[c]['unit/ref']
            if not j:
                continue
            e = rarity
            for loc in nx.algorithms.descendants(G, loc_here):
                if loc_here != loc:
                    try:
                        d = trade_distances.get(loc, dict()).get(loc_here, None)
                        if not d:
                            continue
                        a += (G.node[loc]['references'].get(c, 0) / d) * bool(G.node[loc]['references'].get(c, 0) / d >= 0.02)
                        b += (G.node[loc]['references'].get(c, 0)) * bool(G.node[loc]['references'].get(c, 0) / d >= 0.02)
                    except KeyError:
                        print(f'{loc} has no {c}')
                    except nx.exception.NetworkXNoPath:
                        pass

            try:
                G.node[loc_here][c] = to_cp[currency] * per_oz[currency] * (1 + e) * G.node[loc_here].get('fau', 0) / j
                travel_mod = a / b / 0.05
                # if round(travel_mod) > 2:
                #     log('        travel mod ' + c + ': ' + str(round(travel_mod)))
                G.node[loc_here][c] /= travel_mod
                G.node[loc_here][c] = round(G.node[loc_here][c], 4)
                G.node[loc_here]['pricing references'][c] = round(a, 4)
            except KeyError:
                # log('::err ' + loc_here + ' ' + c)
                pass
            except ZeroDivisionError:
                # log('::err ' + loc_here + ' ' + c)
                pass
        i += 1
        if verbose and mantissa(i) < 0.01: print(f'{i / len(G.nodes()):.0%}')

    print('Calculating industry...')
    i = 0
    for loc_here in G:
        if not G.node[loc_here].get('fau', 0):
            continue
        # log(loc_here + ' labor')
        for c in labor:
            a = G.node[loc_here]['references'].get(c, 0)
            b = G.node[loc_here]['references'].get(c, 0)
            j = commodity[c]['unit/ref']
            if not j:
                continue
            e = rarity
            for loc in nx.algorithms.descendants(G, loc_here):
                if loc_here != loc:
                    # if relationship(loc, loc_here) > 3:
                    #     continue
                    try:
                        d = trade_distances.get(loc, dict()).get(loc_here, None)
                        if not d:
                            continue
                        # if commodity[c]['type'] == 'food':
                        #     if d < 8:
                        #         a += G.node[loc]['references'][c] / d
                        #         b += G.node[loc]['references'][c]
                        #     else:
                        #         pass
                        # else:
                        a += (G.node[loc]['references'].get(c, 0) / d) * bool(
                            G.node[loc]['references'].get(c, 0) / d >= 0.02)
                        b += (G.node[loc]['references'].get(c, 0)) * bool(
                            G.node[loc]['references'].get(c, 0) / d >= 0.02)
                        # a *= bool(a < 0.02)
                        # b *= bool(a < 0.02)
                    except KeyError:
                        # log('::err ' + loc_here + ' ' + c)
                        pass
                    except nx.exception.NetworkXNoPath:
                        # log('::err ' + loc_here + ' ' + c)
                        pass

            try:
                G.node[loc_here][c] = to_cp[currency] * per_oz[currency] * (1 + e) * G.node[loc_here].get('fau', 0) / j
                travel_mod = a / b / 0.05
                # if round(travel_mod) > 2:
                #     log('        travel mod ' + c + ': ' + str(round(travel_mod)))
                G.node[loc_here][c] /= travel_mod
                G.node[loc_here][c] = round(G.node[loc_here][c], 4)
                G.node[loc_here]['pricing references'][c] = round(a, 4)
            except KeyError:
                pass
            except ZeroDivisionError:
                pass
        i += 1
        if verbose and mantissa(i) < 0.01: print(f'{i / len(G.nodes()):.0%}')

    # set_labor_default(G)

    graph_save(G)


def reference_total(G, loc_here, c):

    if c not in stage[0] and c not in labor:
        return
    b = G.node[loc_here]['references'].get(c, 0)
    if not j:
        return
    for loc in nx.algorithms.descendants(G, loc_here):
        if loc_here != loc:
            d = trade_distances.get(loc, dict()).get(loc_here, None)
            if not d:
                continue
            b += (G.node[loc]['references'].get(c, 0)) * bool(
                G.node[loc]['references'].get(c, 0) / d >= 0.02)
    return b


def average_prices():
    for c in sum(stage, []):
        average_price[c] = hmean(
            [G.node[j][c] for j in G.node.keys()]
        )


def gdp():
    loss_rate = 0.01
    # TODO calculate per city?
    totalgold = int(total.get('gold', 0) * commodity['gold']['unit/ref'] * gp_per_oz / loss_rate)
    totalgold += int(total.get('silver', 0) * commodity['gold']['unit/ref'] * gp_per_oz / loss_rate / 10)
    totalgold += int(total.get('copper', 0) * commodity['gold']['unit/ref'] * gp_per_oz / loss_rate / 100)
    totalgold *= 100
    gdp = 0
    for loc in G:
        for c in commodity:
            try:
                gdp += G.node[loc]['pricing references'][c] * gold_ref
            except:
                pass
    return round((gdp + totalgold) // 100)


# def avg(L):
#     return sum(L) / len(L)


def trade_flow(origin, dest, good):

    try:
        return G.node[origin]['references'][good] / trade_distance(origin, dest)
    except ZeroDivisionError:
        return 0


def sourcepercent(loc1, loc2, resource):

    d = trade_distance(loc2, loc1)
    if not d:
        d = 1
    return round(
        (G.node[loc2]['references'][resource] / d) / G.node[loc1]['pricing references'][resource],
        2
    )


# TODO winter trade routes
# TODO winter resource modification


def fromwhere(loc1, resource):

    for loc2 in G:
        p = sourcepercent(loc1, loc2, resource)
        if p > 0:
            print(loc2, '\t', p)



def display(resource = 'fau'):
    H = nx.Graph()

    for u in G:
        for v in G[u]:
            j = 0
            j = statistics.mean([G[u][v][k]['distance'] for k in G[u][v]] + [G[v][u][k]['distance'] for k in G[v][u]])
            H.add_edge(
                u,
                v,
                # weight = w,
                distance = j
            )

    max_size = max([
                       G.node[loc]['fau'] for loc in G
                   ] if resource == 'fau' else [
        G.node[loc]['references'][resource] for loc in G
    ])

    if not max_size:
        max_size = max([
            G.node[loc][resource] for loc in G
        ])

    edges = [sum([sum([trade_flow(loc, v, c) for c in stage[0]]) for loc in G]) for u, v in H.edges()]
    edges = [e / max(edges) * 15 for e in edges]

    nx.draw(
        H,
        fa2.forceatlas2_networkx_layout(H, niter = 1000),
        weight = 'distance',
        width = edges,
        node_color = 'yellow',
        node_size = [
            500 * max_size / G.node[loc]['fau'] for loc in G
        ] if resource == 'fau' else ([
            500 * G.node[loc]['references'][resource] / max_size for loc in G
        ] if resource in stage[0] else [
            500 * max_size / G.node[loc][resource] for loc in G
        ]),
        alpha = 0.25,
        arrows = False,
        font_family = 'JasmineUPC',
        font_size = 18,
        iterations = 1,
        with_labels = True
    )
    plt.show()


def price(loc, good, amount):

    try:
        return round(G.node[loc][good] * amount)
    except KeyError:
        return 0


def set_labor_default(G):

    global labor_default
    labor_default = round(statistics.mean(
        sum([[
            G.node[j].get(i) for i in commodity if i in G.node[j] and not isnan(G.node[j][i]) and commodity[i]['stage'] == 'p'
        ] for j in G], []
        )
    ), 4)


def industry(G, loc, good, labor = True):

    p = 0
    w = 0

    pref = 0

    if commodity[good]['stage'] == 0:
        return (G.node[loc].get(good, 0), G.node[loc]['pricing references'].get(good, 0))

    # make recursive

    for i in commodity[good]['recipe']:
        if 'min' in i:
            p += min([
                industry(G, loc, j)[0] *
                commodity[good]['recipe'][i][j] for j in commodity[good]['recipe'][i] if industry(G, loc, j)[0]
            ])
            prefmins = []
            for j in commodity[good]['recipe'][i]:
                prefmins.append(
                    commodity[good]['recipe'][i][j] * industry(G, loc, j)[1]
                )
            pref += min(prefmins, key=lambda x: x > 0)
        elif i == 'avg':
            p += hmean([
                industry(G, loc, j)[0] *
                commodity[good]['recipe'][i][j] for j in commodity[good]['recipe'][i] if industry(G, loc, j)[0]
            ])
            prefavgs = []
            for j in commodity[good]['recipe'][i]:
                prefavgs.append(
                    commodity[good]['recipe'][i][j] * industry(G, loc, j)[1]
                )
            pref += hmean(prefavgs, key=lambda x: x > 0)

        elif i == 'labor':
            labor = G.node[loc]['pricing references'][commodity[good]['recipe'][i]]
            pref *= (1 / labor + 1)

        elif i == 'w':
            w = commodity[good]['recipe'][i]

        else:
            p += industry(G, loc, i)[0] * commodity[good]['recipe'][i]
            pref += industry(G, loc, i)[1] * commodity[good]['recipe'][i]

    pref = round(pref, 4)
    p *= (1 / labor + 1)
    p *= 2 ** w
    return (round(p, 4), round(pref, 4))


def costs(loc):
    if not (G.node[loc].get('market', 0) + G.node[loc].get('outpost', 0)):
        print('No trading posts here!')
        return

    print(loc)
    print('Goods')
    for c in sorted(commodity):
        if price(loc, c) != 0:
            print(f'{c},,{commodity[c]["unit"]},{ceil(price(loc, c))}')
            # print('{: <30}{: >11} \ {:>}'.format('Item', 'cp', 'unit'))
            # print('-' * len('{: <30}{: >11} \ {:>}'.format('Item', 'cp', 'unit')))
            # for c, val2 in sorted(G.node[loc].items()):
            #     if (c != 'references' and
            #                 c in stage[0] + stage[1] + stage[2] + stage[3] and
            #                 G.node[loc][c] != 0 and
            #                 c not in ['gold']):
            #         print('{: <30}{: >8.0f} {:0>2.0f} \ {:}'.format(
            #             c,
            #             G.node[loc][c] // 100,
            #             G.node[loc][c] % 100,
            #             commodity[c]['unit']
            #         ))


def graph_save(G):

    with open(tradefilepath + r'\G.p', 'wb') as f:
        pickle.dump(G, f)


def save(savefile = filepath + r'\allcosts.txt'):

    with open(savefile, 'w') as f:
        pass
    with open(savefile, 'a') as f:
        for loc, val in sorted(G.node.items()):
            f.write('# ' + loc + '\n\n')
            f.write('Commodity | Cost (cp) | Unit\n')
            f.write(':-|-:|:-\n')
            for c, val2 in sorted(G.node[loc].items()):
                if (c != 'references' and
                            c in commodity and
                            G.node[loc][c] != 0 and
                            # G.node[loc][c] < average_spending_coef(G.node[loc]['population']) and
                            c != 'gold'):
                    f.write(f'{c} | {G.node[loc][c] // 100:.0f} {G.node[loc][c] % 100:0>2.0f} | {commodity[c]["unit"]}\n')
            f.write('\n')

# TODO create a simpler network with 5 or 6 cities to demonstrate

def wavg(L):
    """

    :param L:
    :return:
    """

    L2 = [i for i in L if i != 0]
    L3 = sorted(L, reverse = True)
    L4 = list(range(len(L) + 1))[1:]
    L5 = []
    for i, j in zip(L3, L4):
        L5 += [i] * j * 2
    try:
        return statistics.mean(L5)
    except ZeroDivisionError:
        print(L)
        print(L2)
        print(L3)
        print(L4)
        print(L5)


def rural_population(city1, distance1, city2 = None, distance2 = None):
    """

    :param city1:
    :param distance1:
    :param city2:
    :param distance2:
    :return:
    """

    extra =random.randint(-10, 10)

    if not city2 and not distance2:
        if distance1 == 0:
            return {
                'population': city1,
                'density': int(city1 / 458)
            }
        pop = city1 / (2 ** (distance1 + 1))
        return {
            'population': ceil(pop + extra) * (pop > 0),
            'density': ceil((pop + extra) / 458) * (pop > 0)
        }
    else:
        # pop = int(avg([city1 / (2 ** distance1), city2 / (2 ** distance2)]) + extra)
        PA = (0, city1)
        PB = (distance1 + distance2, city2)
        PC = (PB[0] * PA[1] / (PA[1] + PB[1]), PB[1] / 2)
        A = (((PB[1] - PA[1]) * (PA[0] - PC[0]) + (PC[1] - PA[1]) * (PB[0] - PA[0])) /
             ((PA[0] - PC[0]) * (PB[0] ** 2 - PA[0] ** 2) + (PB[0] - PA[0]) * (PC[0] ** 2 - PA[0] ** 2)))
        B = (((PB[1] - PA[1]) - A * (PB[0] ** 2 - PA[0] ** 2)) /
             (PB[0] - PA[0]))
        C = PA[1] - A * PA[0] ** 2 - B * PA[0]
        pop = A * distance1 ** 2 + B * distance1 + C
        return {
            'population': ceil(pop + extra),
            'density': ceil((pop + extra) / 458)
        }


# def infrastructure(city1, route1, city2 = None, route2 = None):
#     mods = {
#         'p': 3,
#         'f': 4,
#         'm': 5
#     }
#
#     inf1 = city1
#     for r in route1:
#         inf1 /= mods[r]
#     inf2 = city2
#     for r in route2:
#         inf2 /= mods[r]
#     return inf1 + inf2


def pop_list(x):
    for i in x:
        print(rural_population(i[0], i[1], i[2], i[3])['population'])


def snow_line(lat):
    lat = float(lat)

    return int(
        0.65 * lat ** 3 -
        49.44 * lat ** 2 +
        1058.41 * lat +
        14443.93
    )


def hmean(list):
    list = [x for x in list if x != 0]

    return (
        len(list) /
        sum([1 / x for x in list])
    )


def income(loc, good):
    n = len(G[loc])
    total = 0
    # exports = [c for c in G.node[loc]['references'] if G.node[loc]['references'][c] > 0]

    # for c in exports:
    for dest in G[loc]:
        total += (
                     commodity[good]['unit/ref'] *
                     G.node[loc]['references'][good] *
                     (price(dest, good) - price(loc, good)) *
                     bool(price(dest, good) - price(loc, good))
                 ) / n

    return round(total)


def total_income(loc):
    n = len(G[loc])
    total = 0
    exports = [c for c in G.node[loc]['references'] if G.node[loc]['references'][c] > 0]

    for c in exports:
        for dest in G[loc]:
            total += (
                         commodity[c]['unit/ref'] *
                         G.node[loc]['references'][c] *
                         (price(dest, c) - price(loc, c)) *
                         bool(price(dest, c) > price(loc, c))
                     ) / n

    return round(total)


def total_expenses(loc):
    n = len(G[loc])
    total = 0

    for dest in G[loc]:
        exports = [c for c in G.node[dest]['references'] if G.node[dest]['references'][c] > 0]
        for c in exports:
            total += (
                         commodity[c]['unit/ref'] *
                         G.node[dest]['references'][c] *
                         (price(loc, c) - price(dest, c)) *
                         bool(price(loc, c) > price(dest, c))
                     ) / n

    return round(total)


def best_investment(loc, limit = None, pc_mod = 0.75, everything = False, self_shipped = False, csv = False):
    profit = 0
    if limit:
        limit *= 100
    for dest in G:
        if loc == dest:
            continue
        r = route(loc, dest)
        total_route = []
        total_distance = 0
        shipping_cost = 0
        for i, j in enumerate(r):
            try:
                best_route = min(G[r[i]][r[i + 1]], key = lambda x: G[r[i]][r[i + 1]][x]['distance'])
                total_route += G[r[i]][r[i + 1]][best_route]['route']
                total_distance += G[r[i]][r[i + 1]][best_route]['distance']
            except:
                pass
        mod = total_distance / len(total_route)
        for k in total_route:
            if 'river' in k or 'ocean' in k:
                shipping_cost += G.node[loc]['shipping (water)']
            else:
                shipping_cost += G.node[loc]['shipping (land)']
        if self_shipped:
            # shipping_cost *=
            pass
        else:
            shipping_cost *= mod * 20
        for c in sum(stage, []):
            wt = 1
            # if c not in currency:
            if commodity[c]['unit'] == 'lb':
                wt = 1
            elif commodity[c]['unit'] == 'oz':
                wt = 1 / 16
            elif commodity[c]['unit'] == 'ct':
                wt = 1 / 141
            else:
                continue
            income = price(dest, c) * pc_mod
            expense = shipping_cost * wt + price(loc, c)
            if expense == 0:
                continue
            if price(loc, c) == 0:
                continue
            profit = 0 if everything else profit
            if income - expense > profit:
                profit = income - expense
                profit_margin = 100 * profit / expense
                if limit:
                    amount = math.floor(limit / (shipping_cost * wt + price(loc, c)))
                    income *= amount
                    expense *= amount
                    if limit >= expense:
                        if not csv:
                            print(f'Trade {amount:.0f} {commodity[c]["unit"]} of {c} to {dest} '
                                  f'for a profit of {profit_margin:.0f}%. '
                                  f'The overhead is {expense:.0f}gp. '
                                  f'The trade takes {trade_distance(loc, dest):.0f} days, one way.')
                        else:
                            print(f'{amount:.0f},{commodity[c]["unit"]},{c},{dest},{profit_margin:.0f}%,'
                                  f'{expense // 100:.0f},{income // 100:.0f},{trade_distance(loc, dest):.0f}')
                    else:
                        pass
                else:
                    if not csv:
                        print(f'Trade {c} to {dest} for a profit of {profit_margin:.0f}%. '
                              f'The trade takes {trade_distance(loc, dest):.0f} days, one way.')
                    else:
                        print(f'{c},{dest},{profit_margin:.0f}%,{trade_distance(loc, dest):.0f}')


def spending_money(loc):
    pass


def average_spending_coef(pop):
    def power_law(pop, lvl):

        k = 3.3
        p0 = pop / sum([3.3 ** -(x - 1) for x in range(1, 11)])
        return math.floor(p0 * k ** (1 - lvl))

    n = 5
    # just commodities for now, no currency
    castes = []
    for lvl in range(1, 11):
        p = power_law(pop, lvl)
        if p != 0:
            castes.append(p)
    gini = []
    for lvl in reversed(castes):
        if lvl != 0:
            gini.append(lvl / pop)
    gdpp = gdp() / total['population']
    w = avg([pop * gdpp * g / 2 for g in gini])
    return (w)


def print_all():
    print('loc,pop,' + ','.join([commodity[c]['unit'] for c in sorted(commodity)]))
    print(',,' + ','.join([c for c in sorted(commodity)]))
    for loc in G:
        print(
            str(loc) + ',' + str(G.node[loc]['population']) + ',' +
            ','.join(['{:.0f}'.format(G.node[loc][c]) for c in sorted(commodity)])
        )


def stuff():
    for c in commodity:
        if total[c] != 0:
            print(f'{c}\t{commodity[c]["weight"]}\t{commodity[c]["unit"]}')


# def calories(loc):
#     """
#
#     :param loc:
#     :return:
#     """
#
#     cal_per_year_per_head = 1e6
#     cal_total_production = 0
#     pop = G.node[loc]['population']
#
#     cal_per_year = pop * cal_per_year_per_head
#
#     for f in foodstuffs:
#         try:
#             cal_total_production += G.node[loc]['pricing references'][f] * foodstuffs[f]['cal/ref']
#         except KeyError:
#             pass
#
#     surplus = cal_total_production - cal_per_year
#     support = int(cal_total_production / cal_per_year_per_head)
#     if surplus <= 0:
#         log(loc + ' is below caloric replacement, can only support ' + str(support) + ' people (' + str(
#             pop - support) + ' extra)')
#         print(loc + ' is below caloric replacement, can only support ' + str(support) + ' people (' + str(
#             pop - support) + ' extra)')
#         G.node[loc]['food surplus'] = 0
#     else:
#         G.node[loc]['food surplus'] = int(surplus - pop)


def cheapest_place():
    print('Good                     Market')
    print('-------------------------------')
    for c in sorted(commodity):
        cheapest = (1e9, '')
        try:
            for loc in G:
                if 0 < G.node[loc][c] < cheapest[0] and G.node[loc]['outposts']:
                    cheapest = (G.node[loc][c], loc)
        except:
            pass
        print(f'{c: <25}{cheapest[1]:}')


def trade_analysis_v1():

    # takes a really long time to run O(n^2 m)
    routes = []
    for loc_here in G:
        for c in stage[0]:
            for loc in G:
                if loc_here != loc:
                    try:
                        d = trade_distance(loc, loc_here)
                        if G.node[loc]['references'][c] / d >= 0.02:
                            routes += route(loc, loc_here)[1:-1]
                    except KeyError:
                        pass
                    except ZeroDivisionError:
                        pass
    return dict((r, routes.count(r)) for r in routes)



def dot_write(G):

    try:
        import pydot
        from networkx.drawing.nx_pydot import write_dot
        print("using package pydot")
    except ImportError:
        print()
        print("Both pygraphviz and pydot were not found ")
        print("see  https://networkx.github.io/documentation/latest/reference/drawing.html")
        print()
        raise
    write_dot(G, tradefilepath + r"\grid.dot")


def trade_analysis():

    # only O(n^2)!
    routes = []
    for loc_here in G:
        for loc in G:
            if loc_here != loc:
                try:
                    routes += route(loc, loc_here)[1:-1]
                except KeyError:
                    pass
                except ZeroDivisionError:
                    pass
    return dict((r, routes.count(r)) for r in routes)

load_commodities()

gp_per_oz = 1 / 0.48
sp_per_oz = 1 / 0.153
cp_per_oz = 1 /  0.044
currency_ratio = 0.8
gold_ref = commodity['gold']['unit/ref'] * gp_per_oz

for c in list(commodity):
    if commodity[c].get('stage') == 'x':
        commodity.pop(c)

verify_commodities()

# currency = ['gold', 'silver', 'copper']
currency = ['gold']
stage = [[] for i in range(0, 6)]
labor = []
for c in commodity:
    if commodity[c]['stage'] != 'x':
        if commodity[c]['stage'] == 'p':
            labor.append(c)
        else:
            try:
                stage[commodity[c]['stage']].append(c)
            except IndexError:
                print(c, commodity[c]['stage'])

with open(tradefilepath + r'\G.p', 'rb') as f:
    G = pickle.load(f)

hex_references = dict()
#
# references_set(G)

def get_markets(G):

    return {loc for loc in G if G.node[loc].get('references', dict()).get('market')}


# total = {
#     k: v for (k, v) in zip(
#     [c for c in commodity],
#     [round(sum([G.node[loc].get('references', dict()).get(c, 0) for loc in G]), 4) for c in commodity if commodity[c]['stage'] == 0]
# )
# }
# # try:
# total['population'] = sum(G.node[loc].get('population', 0) for loc in G)
# # except KeyError:
#     # log('Population error: ' + loc)
#
# totalcurrency = total.get('gold', 0) + total.get('silver' , 0) / 10 #+ total.get('copper', 0) / 100
# races = set(cities[city]['race'] for city in cities)

trade_distances = dict()
load_data(trade_distances, tradefilepath + r'\tradedistances.p')

# references_set(G)