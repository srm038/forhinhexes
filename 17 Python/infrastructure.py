from desirability import *
from topography import *

load_data(climate, climate_file)
load_data(sociology, sociology_file)
load_data(terrain, terrain_file)

# with open(cities_file, 'rb') as f:
#     cities = pickle.load(f)

def infrastructure_impact(a, b, k = 5, p = 0):

    if a not in cities:
        return 0
    d = {'r': 3, 'd': 4, 'i': 5, "b": 6, "f": 6, "m": 6, "w": 6}
    infra = {a: {a: math.floor((cities[a]['population'] - p) / 346.4 * k)}}
    current = set(n for n in hexes[a]['neighbors'] if n not in cities or cities[n]['hegemony'] == cities[a]['hegemony'])
    visited = set([a])
    i = True
    while i:
        i = False
        for c in list(current):
            if c in visited:
                continue
            infra.setdefault(c, dict())
            try:
                source = max((
                    n for n in hexes[c]['neighbors'] if
                    n in infra and
                    a in infra[n]
                    # any((n, c) in hegemony['cities'] for heg, hegemony in hegemonies.items())
                ), key=lambda x: infra.get(x, dict()).get(a, 0))
            except ValueError:
                # print('error')
                continue
            if not source:
                current.remove(c)
                visited.add(c)
            r = d.get(terrain[c]['terrain'], 2)
            r += clip(
                (terrain[c]['altitude'] -
                 terrain[source]['altitude']) / 400, 0, None)
            # r += 'mountain' in terrain[c]['topography']
            # r += climate[c]['drainage'] // 50
            r -= 2 * (source in terrain[c]['drain_in'])
            r += 5000 if cities[a]['race'] == 'orc' else 0
            r = clip(r, 2, None)
            infra[c].update({a: math.floor(infra[source][a] / r)})
            if c == b:
                break
            if infra[c][a] > 0:
                i = True
            current |= set(
                n for n in hexes[c]['neighbors'] if n not in cities or cities[n]['hegemony'] == cities[a]['hegemony'])
            # print(current)
            current.remove(c)
            visited.add(c)
    if b not in infra:
        return 0
    if a not in infra[b]:
        return 0
    if not infra[a][a]:
        return 0
    return round(infra[b][a] / infra[a][a], 4)


def infrastructure(update_map=True, k=5, reset=True):

    # TODO separate function for A's impact on B; use for secession

    if reset:
        for h, hex in sociology.items():
            sociology[h]['infrastructure'] = 0
            if 'hinterland' in hex:
                del hex['hinterland']

    for c, city in cities.items():
        sociology[c]['infrastructure'] = math.floor(city['population'] / 346.4 * k)

    for h, heg in hegemonies.items():
        heg.get('hinterland', set()).clear()

    # d = {'r': 3, 'd': 4, 'i': 5, "b": 6, "f": 6, "m": 6, "w": 6}
    d = {k: 3 for k in ['d', 'i', 'b', 'f', 'm', 'w']}

    infra = {c: {c: sociology[c]['infrastructure']} for c, city in cities.items()}
    for t, city in cities.items():
        S = hegemonies[h].get('asabiya', 0.5)
        current = set(n for n in all_neighbors[t] if n in terrain and (n not in cities or cities[n]['hegemony'] == city['hegemony']))
        visited = set([t])
        i = True
        while i:
            i = False
            for c in list(current):
                if c in visited:
                    continue
                infra.setdefault(c, dict())
                try:
                    source = max((
                        n for n in all_neighbors[c] if
                        n in infra and
                        t in infra[n]
                        # any((n, c) in hegemony['cities'] for heg, hegemony in hegemonies.items())
                    ), key=lambda x: infra.get(x, dict()).get(t, 0))
                except ValueError:
                    continue
                if not source:
                    current.remove(c)
                    visited.add(c)
                r = 2
                # r = d.get(terrain[c]['terrain'], 2)
                r += clip(
                    (elevation[c]['altitude'] -
                     elevation[source]['altitude']) / 400, 0, None)
                # r += 'mountain' in terrain[c]['topography']
                # r += climate[c]['drainage'] // 50
                # r -= 2 * (source in terrain[c]['drain_in'])
                r += 5000 if cities[t]['race'] == 'orc' else 0
                # r /= S
                r = clip(r, 2, None)
                infra[c].update({t: math.floor(infra[source][t] / r)})
                if infra[c][t] > 0:
                    i = True
                current |= set(n for n in all_neighbors[c] if n in terrain and (n not in cities or cities[n]['hegemony'] == city['hegemony']))
                # print(current)
                current.remove(c)
                visited.add(c)
            # print(city, c)
    for h in infra:
        if not infra[h]:
            sociology[h]['infrastructure'] = 0
            continue
        rulers = [i for i in infra[h] if i in cities and cities[i]['hegemony'] in hegemonies]
        if not rulers:
            sociology[h]['infrastructure'] = 0
            continue
        ruler = cities[max(rulers, key=lambda x: infra[h][x])]['hegemony']
        if ruler not in hegemonies:
            sociology[h]['infrastructure'] = 0
            continue
        # ruler = cities[ruler]['hegemony']
        sociology[h]['infrastructure'] = sum(infra[h].get(c, 0) for c, city in cities.items() if city['hegemony'] == ruler)
        if h not in cities and sociology[h]['infrastructure'] and hegemonies[ruler]['capital'] in cities and cities[hegemonies[ruler]['capital']]['race'] != 'orc':
            hegemonies[ruler].setdefault('hinterland', set()).add(h)
            sociology[h]['hinterland'] = ruler
    if update_map:
        infrastructure_map()


def infrastructure_png():

    fig, ax = newMap(0)
    s = 1 / 2
    t = sorted([h for h in sociology if hexdict[h][0][1] >= hexdict['207-518W'][0][1]],
               key=lambda h: sociology[h]['infrastructure'])
    t = [i for i in t if sociology[i]['infrastructure']]
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    m = max(sociology[city]['infrastructure'] for city in t)
    # max_c = max(current_sst(h, season) for h in t)
    col = lambda h: screen(recipe.get(hegemony_get(h), dict()).get('color', 'ff0000'), 204 - 204 * sociology[h]['infrastructure'] / m)
    c = np.array([colors.hex2color(f"#{col(h)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)
    fig.savefig(basefile + fr'\Infrastructure\cities.png', dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def infrastructure_svg():

    with open(basefile + r'\Infrastructure.svg', 'r') as f:
        file = bs(f, 'lxml')
    string = ''
    m = max([sociology[h]['infrastructure'] for h in sociology])
    if not m:
        return
    for h in sociology:
        if not sociology[h]['infrastructure']:
            continue
        col = screen(recipe.get(hegemony_get(h), dict()).get('color', 'ff0000'), 204 - 204 * sociology[h]['infrastructure'] / m)
        if col == 'ffffff':
            continue
        string += '<path d="M {:} Z" class="hex" id="{:}" style="color:#{:}" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in hexdict[h]]),
            h,
            col
        )

    if string:
        file.find('g', id='infra').contents = bs(string, 'lxml').find('body').contents
    else:
        file.find('g', id='infra').clear()
    string = ''
    for h, hex in sociology.items():
        ii = hex.get('infrastructure', 0)
        # if 'hinterland' in hex:
        #     i = infrastructure_impact(hex['hinterland'], h)
        # elif h in cities:
        #     i = infrastructure_impact(hegemonies[cities[h]['hegemony']]['capital'], h)
        # else:
        #     continue
        if ii: string += f'<text id="{h}" x="{hexdict[h][0][0]:.02f}" y="{hexdict[h][0][1] + 20:.02f}" class="infra-label">{ii}</text>\n'
        # if i >= 0.01: string += f'<text id="{h}" x="{hexes[h]["x"]:.02f}" y="{hexes[h]["y"] + 30:.02f}" class="infra-label">{i:.2f}</text>\n'
    if not string:
        file.find('g', id='infra-labels').clear()
    else:
        file.find('g', id='infra-labels').contents = bs(string, 'lxml').find('body').contents

    save(file, extra_file=basefile + r'\Infrastructure.svg')

road_limit = dict()
load_data(road_limit, basefile + r'\road_limit.p')


def infra_flood_fill(a):

    if not sociology[a]['infrastructure']:
        return {}

    current = {n for n in hexes[a]['neighbors'] if sociology[n]['infrastructure']}
    visited = set([a])
    # flood fill
    while current:
        for c in list(current):
            # if c == b:
            #     return True
            if c in visited:
                current.remove(c)
                continue
            current |= {n for n in hexes[c]['neighbors'] if sociology[n]['infrastructure']}
            visited.add(c)
            current.remove(c)
            # print(current)
            # print(visited)
    return visited



def road_travel(i, j, major=False, inf=False, limit=3):

    # TODO how to handle river crossings
    # TODO add city road limit here instead

    if j in sociology[i].get('roads', set()):
        return 0
    if major and len(sociology[j].get('roads', set())) >= limit:
        return 1e9
    if major and sociology[j]['infrastructure'] < 10:
        return 1e9
    if j in cities and cities[j]['race'] == 'orc':
        return 1e9

    if not inf and abs(elevation[j]['altitude'] - elevation[i]['altitude']) < 400:
        if j in elevation[i]['drain_out']:
            return 1 / 2
        elif i in elevation[j]['drain_out']:
            return 1 / math.sqrt(2)
        else:
            pass
    alt = 0.5 * abs(elevation[j]['altitude'] - elevation[i]['altitude']) / 400
    if elevation[j]['altitude'] > 8000:
        alt += 1 + (elevation[j]['altitude'] - 8000) / 1000
    if 2 <= climate[j]['drainage'] < 10:
        alt += 1
    elif inf and hexes[j].get('drainage', 0) > 10 and j not in cities and sociology[j]['infrastructure'] < (100 * math.log(climate[j]['drainage'])):
        return 1e9
    if j in cities:
        alt /= cities[j]['population'] / 10000
    alt /= sociology[j]['infrastructure'] if inf else 1
    return 1 + alt


roadCost = {
    'high': 7.8,
    'low': 7.1,
    'cobbled': 6.1,
    'dirt': 5.0,
    'cart track': 3.9,
    'cart path': 2.8,
    'path': 1.9,
    'trail': 1.2
}


def road_travel_tao(i, j, major=False, inf=False, limit=3):

    if j not in sociology[i].get('roads', dict()):
        return 1e9
    elif i not in sociology[j].get('roads', dict()):
        return 1e9
    return (10 / roadCost.get(sociology[i].get('roads', dict()).get(j, None), 0) +
            10 / roadCost.get(sociology[j].get('roads', dict()).get(i, None), 0))


def river_travel(i, j, minD=3):

    if j in elevation[i]['drain_out']: # downstream
        D = riverProps(i, j)['depth']
        if D >= minD:
            return 20 / (riverProps(i, j)['velocity'] * 10 / 3)
        return 1e9
    elif i in elevation[j]['drain_out']:
        D = riverProps(j, i)['depth']
        if D >= minD: # upstream
            return 20 / (max(0.388, 2.071 - riverProps(j, i)['velocity']) * 10 / 3)
        return 1e9
    else:
        return 1e9


def trade_travel(i, j, minD=3):

    return min(road_travel_tao(i, j), river_travel(i, j))


def hex_path(start, end, verbose=False):

    # for n in hexes[start]['neighbors']:
    #     if road_exists(n, end) and len(hexes[n]['road_to']) < road_limit.get(n, 2) - 1:
    #         return [start, n]

    def hex_travel(i, j):

        return 1

    with stopit.ThreadingTimeout(1):
        return a_star_road(start, end, travel=hex_travel, verbose=verbose)
    return None


def roads_clear():

    for h in sociology:
        sociology[h].pop('tracks', None)
        sociology[h].pop('road', None)
        sociology[h].pop('roads', None)
        sociology[h].pop('road_to', None)
    with open(basefile + r'\Roads.svg', 'r') as f:
        file = bs(f, 'lxml')
    file.find('g', id='roads').clear()
    save(file, extra_file=basefile + r'\Roads.svg')


def roads_generate_v1(update_map=True, reset=False, limit=14):

    if reset: roads_clear()

    with open(basefile + r'\Roads.svg', 'r') as f:
        file = bs(f, 'lxml')
        string = ''.join([str(p).replace('\n', '') for p in file.find('g', id='roads').find_all('path')])

    # string = ''
    i = 0
    for a in sorted(list(cities), key=lambda x: sociology[x]['infrastructure'], reverse=True):
        i += 1
        print('{:.3%}\t{:}'.format(i / len(cities), sociology[a]['infrastructure']))
        if sociology[a]['infrastructure'] <= 50:
            break
        if len(hexes[a]['road_to']) >= road_limit.get(a, 2) - 1:
        # if len(hexes[a]['road_to']) + 1 >= math.log10(cities[a]['population']):
            continue
        road = []
        targets = sorted([
            c for c in cities if a != c and sociology[c]['infrastructure'] >= 50 and len(hexes[c]['road_to']) < road_limit.get(c, 2) - 1
        ], key=lambda x: hexDistance(a, x))
        if not targets:
            continue
        for b in targets:
            if hexDistance(a, b) > limit:
                break
            if road_exists(a, b):
                continue
            if sociology[a]['infrastructure'] < 100 and sociology[b]['infrastructure'] < 100:
                if cities[a]['race'] != cities[b]['race']:
                    continue
            with stopit.ThreadingTimeout(1):
                road = road_path(a, b)
            if not road:
                continue
            if any(len(hexes[r]['road_to']) >= road_limit.get(r, 2) - 1 for r in road):
                print(a, b)
                break
            for r in road:
                for s in road:
                    if r == s:
                        continue
                    hexes[r]['road'].add(s)
                    hexes[s]['road'].add(r)
                hexes[r]['roads'] += 1
            for r, s in zip(road[:-1], road[1:]):
                hexes[r]['road_to'].add(s)
                # hexes[r]['road_from'].add(s)
                # hexes[s]['road_from'].add(r)
                hexes[s]['road_to'].add(r)
            if path_length([hexes[r]['c'] for r in road]) > 10000:
                continue
            string += '<path d="M {:}" id="{:} to {:}" style="stroke-width:{:.2f}"/>'.format(
                ' '.join(['{:},{:}'.format(hexes[p]['x'], hexes[p]['y']) for p in road]),
                a, b,
                math.log10(sociology[a]['infrastructure'] * sociology[b]['infrastructure']) * 2
            )
            print('{:} to {:}'.format(a, b))
            break

    if update_map and string:

        with open(basefile + r'\Roads.svg', 'r') as f:
            file = bs(f, 'lxml')
        file.find('g', id='roads').contents = bs(string, 'lxml').find('body').contents
        save(file, extra_file=basefile + r'\Roads.svg')


def roads_svg():

    with open(basefile + r'\Roads.svg', 'r') as f:
        file = bs(f, 'lxml')
    string = {k: '' for k in roadInfra.keys()}
    for h in sociology:
        for i, j in sociology[h].get('roads', dict()).items():
            if j == 'trail':
                continue
            if distance(i, h) > 120:
                continue
            theta = rawAngle(h, i)
            string[j] += f" M {hexdict[h][0][0]:0.4f},{hexdict[h][0][1] + 50:0.4f} " \
                         f"{hexdict[h][0][0] + 45 * math.cos(math.radians(theta)):0.4f}," \
                         f"{hexdict[h][0][1] + 50 + 45 * math.sin(math.radians(theta)):0.4f}"
    fullString = bs(''.join(f'<g id="{t.replace(" ", "")}"/>' for t in list(string)[::-1]), 'lxml').find('body')
    for t in string:
        if string[t]:
            tt = t.replace(" ", "")
            fullString.find('g', id=tt).contents = bs(f"<path d='{string[t]}'/>", 'lxml').find('body').contents
    if not string:
        file.find('g', id='roads').clear()
    else:
        file.find('g', id='roads').contents = fullString.contents

    save(file, extra_file=basefile + r'\Roads.svg')


roadInfra = {'high': 100, 'low': 35, 'cobbled': 20, 'dirt': 12, 'cart track': 6, 'cart path': 3, 'path': 1}

totalRoads = {100: 6, 35: 6, 20: 4, 12: 4, 6: 3, 3: 2, 1: 1}


def maxRoads(h):

    i = sociology[h]['infrastructure']
    for m, v in {1: 1, 3: 2, 6: 3, 12: 4, 35: 6}.items():
        if i >= m:
            max_r = v
    return max_r


def road_types(h):

    i = sociology[h]['infrastructure']
    max_r = maxRoads(h)
    roads = dict()
    for t, min_i in roadInfra.items():
        n = i // min_i
        for k in range(n):
            roads.setdefault(t, 0)
            roads[t] += 1
            if sum(roads.values()) >= max_r:
                break
        i -= roads.get(t, 0) * min_i
    if sum(roads.values()) < max_r:
        roads['trail'] = max_r - sum(roads.values())

    return roads


def road_types_tao(h):

    i = sociology[h]['infrastructure']
    roads = road_types(h)
    primary = list(roads)[0]
    secondary = list(roadInfra)[list(roadInfra).index(primary) + 1] if list(roadInfra).index(primary) + 1 < len(roadInfra) else 0
    tertiary = list(roadInfra)[list(roadInfra).index(primary) + 2] if list(roadInfra).index(primary) + 2 < len(roadInfra) else 0
    types = {primary: roads[primary] if i > 34 else 1}
    if secondary:
        types.update({secondary: 2 if i > 34 else 1})
    if tertiary:
        types.update({tertiary: clip(maxRoads(h) - types[primary] - types[secondary], 0, 2 if i < 35 else 1 if i < 12 else None)})
    return types


def road_levels(target):

    io = sorted([i for i in all_neighbors[target] if i in sociology], key=lambda n: sociology[n].get('infrastructure', 0), reverse=True)
    io_types = {i: '' for i in io}
    for t, n in road_types_tao(target).items():
        for i in range(n):
            if not io:
                break
            io_types[io[0]] = t
            io.pop(0)

    return {k: v for k, v in io_types.items() if v}


def road_exists(a, b):

    current = set(sociology[a].get('roads', set()))
    visited = set([a])
    # flood fill
    while current:
        for c in list(current):
            # if c == b:
            #     return True
            if c in visited:
                current.remove(c)
                continue
            current |= set(sociology[c].get('roads', set()))
            visited.add(c)
            current.remove(c)
            # print(current)
            # print(visited)
    if b in visited:
        return True
    else:
        return False


def road_flood_fill(a):

    current = set(hexes[a].get('road_to', set()))
    visited = set([a])
    # flood fill
    while current:
        for c in list(current):
            # if c == b:
            #     return True
            if c in visited:
                current.remove(c)
                continue
            current |= set(hexes[c].get('road_to', set()))
            visited.add(c)
            current.remove(c)
            # print(current)
            # print(visited)
    return visited


def road_between(start, end, travel, verbose=False):

    r = {'route': [], 'AP': 0}
    io = lambda h: [i for i in set([i for i in sociology[h].get('roads', dict())] + list(elevation[h]['drain_in']) + list(elevation[h]['drain_out'])) if i in terrain]
    open_list = io(start)
    if not open_list:
        if verbose: print('No neighbors')
        return r
    closed_list = [start]
    came_from = {o: start for o in open_list}

    def g(current, s=None):

        path = [current]
        while path[-1] != start:
            try:
                path.append(came_from.get(path[-1], s))
            except KeyError:
                pass
        path = path[::-1]

        z = zip(path[:-1], path[1:])
        return round(sum(travel(i, j) for i, j in z), 3)

    def h(current):

        return hexDistance(current, end)

    def f(current):

        return g(current) + h(current) ** 2

    while open_list:
        s = sorted(open_list, key=lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        if s == end:
            # print(closed_list)
            path = [end]
            break
        for t in io(s):
            if travel(s, t) > 1e8:
                closed_list.append(t)
                continue
            if t not in closed_list:
                if t not in open_list or g(t, s=s) < h(t):
                    open_list.append(t)
                came_from.update({t: s})
        # print(f(s), closed_list)
        closed_list = list(set(closed_list))
        if verbose: print(f'{f(s):.0f} {g(s):.0f} {closed_list}')

    path = [end]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return r
    r.update({
        'route': path[::-1],
        'AP': round(sum(travel(i, j) for i, j in zip(path[::-1][:-1], path[::-1][1:])), 3)
    })
    return r


def a_star(start, end, travel, verbose=False, limit=3, major=True):

    open_list = list([i for i in all_neighbors[start] if i in terrain])
    if not open_list:
        if verbose: print('No neighbors')
        return None
    closed_list = [start]
    came_from = {o: start for o in open_list}

    def g(current, s=None):

        path = [current]
        while path[-1] != start:
            try:
                path.append(came_from.get(path[-1], s))
            except KeyError:
                pass
        path = path[::-1]

        z = zip(path[:-1], path[1:])
        return round(sum(travel(i, j, limit=limit, major=major) for i, j in z), 3)

    def h(current):

        return hexDistance(current, end)

    def f(current):

        return g(current) + h(current)

    while open_list:
        s = sorted(open_list, key=lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        if road_exists(s, end):
            path = [s]
            break
        if s == end:
            # print(closed_list)
            path = [end]
            break
        for t in [i for i in all_neighbors[s] if i in terrain]:
            if travel(s, t) > 1e8:
                closed_list.append(t)
                continue
            if t not in closed_list:
                if t not in open_list or g(t, s=s) < h(t):
                    open_list.append(t)
                came_from.update({t: s})
        # print(f(s), closed_list)
        closed_list = list(set(closed_list))
        if verbose: print(f'{f(s):.0f} {g(s):.0f} {closed_list}')

    path = [end]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return None
    return path[::-1]


def a_star_star(start, ends, travel, verbose=False, limit=3, major=True):

    open_list = list(hexes[start]['neighbors'])
    if not open_list:
        if verbose: print('No neighbors')
        return None
    closed_list = [start]
    came_from = {o: start for o in open_list}

    def g(current, s=None):

        path = [current]
        while path[-1] != start:
            try:
                path.append(came_from.get(path[-1], s))
            except KeyError:
                pass
        path = path[::-1]

        z = zip(path[:-1], path[1:])
        return round(sum(travel(i, j, limit=limit, major=major) for i, j in z), 3)

    def h(current):

        return min(hexDistance(current, end) for end in ends)

    def f(current):

        return g(current) + h(current)

    while open_list:
        s = sorted(open_list, key=lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        if any(road_exists(s, end) for end in ends):
            path = [s]
            break
        if s in ends:
            # print(closed_list)
            path = [s]
            break
        for t in hexes[s]['neighbors']:
            if travel(s, t) > 1e8:
                closed_list.append(t)
                continue
            if t not in closed_list:
                if t not in open_list or g(t, s=s) < h(t):
                    open_list.append(t)
                came_from.update({t: s})
        # print(f(s), closed_list)
        closed_list = list(set(closed_list))
        if verbose: print(f'{f(s):.0f} {g(s):.0f} {closed_list}')

    path = [s]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return None
    return path[::-1]


def size_class(a, b):
    return math.floor(math.log10(cities[a]['population'])) == math.floor(math.log10(cities[b]['population']))


def roads_total(h):

    return len(hexes[h].get('roads', set())) + len(hexes[h].get('tracks', set()))


def roads_generate_v3(clear=True, update_map=False, verbose=False, limit=3):

    if clear:
        roads_clear()
    done = set()
    # bordering capitals
    if verbose: print('Capitals...')
    capitals = sorted([
        hegemonies[h]['capital'] for h in hegemonies
    ], key=lambda x: cities[x]['t0'])
    for city in capitals:
        if roads_total(city) >= limit:
            continue
        if cities[city]['race'] == 'orc':
            continue
        heg = cities[city]['hegemony']
        targets = [hegemonies[h]['capital'] for h in hegemony_neighbors(heg)]
        for target in targets:
            if road_exists(city, target):
                continue
            if sociology[target]['infrastructure'] < 10:
                continue
            road = None
            with stopit.ThreadingTimeout(5):
                road = a_star(city, target, travel=road_travel, limit=limit, major=False)
            if not road:
                continue
            if verbose: print(city, target)
            for r, s in zip(road[:-1], road[1:]):
                hexes[r].setdefault('roads', set())
                hexes[r]['roads'] |= {s}
                hexes[s].setdefault('roads', set())
                hexes[s]['roads'] |= {r}
            if len(hexes[city].get('roads', set())) < limit:
                continue
        done.add(city)
        if verbose: print(f'{len(done) / len(capitals):.3%}')
    for heg in hegemonies:
        # big cities
        cities_t = sorted([
            city for city in hegemonies[heg]['cities'] if sociology[city]['infrastructure'] >= 50 and city not in capitals
        ], key=lambda x: cities[x]['t0'])
        for city in cities_t:
            if road_exists(city, hegemonies[heg]['capital']):
                continue
            road = None
            with stopit.ThreadingTimeout(5):
                road = a_star(hegemonies[heg]['capital'], city, travel=road_travel, limit=limit, major=True)
            if not road:
                continue
            if verbose: print(city, hegemonies[heg]['capital'])
            for r, s in zip(road[:-1], road[1:]):
                hexes[r].setdefault('roads', set())
                hexes[r]['roads'] |= {s}
                hexes[s].setdefault('roads', set())
                hexes[s]['roads'] |= {r}
        # tracks
        cities_t = sorted([
            city for city in hegemonies[heg]['cities'] if sociology[city]['infrastructure'] < 50 and city not in capitals and not roads_total(city)
        ], key=lambda x: cities[x]['t0'])
        for city in cities_t:
            if road_exists(city, hegemonies[heg]['capital']):
                continue
            if roads_total(city) >= limit:
                continue
            targets = [h for h in hegemony_flood_fill(city) if 1 <= roads_total(h) < limit and h != city]
            if not targets:
                continue
            roads = None
            with stopit.ThreadingTimeout(5):
                road = a_star_star(city, targets, travel=road_travel, limit=limit, major=True)
            if verbose: print(city, road[-1])
            for r, s in zip(road[:-1], road[1:]):
                if s in hexes[r].get('roads', set()):
                    continue
                hexes[r].setdefault('tracks', set())
                hexes[r]['tracks'] |= {s}
                hexes[s].setdefault('tracks', set())
                hexes[s]['tracks'] |= {r}
        cities_t = sorted([
            city for city in hegemonies[heg].get('hinterland', set()) if not roads_total(city)
        ], key=lambda x: sociology[x]['infrastructure'])
        for city in cities_t:
            if road_exists(city, hegemonies[heg]['capital']):
                continue
            if roads_total(city) >= limit:
                continue
            targets = [h for h in hegemonies[heg]['cities'] | hegemonies[heg].get('hinterland', set()) if 1 <= roads_total(h) < limit and h != city]
            if not targets:
                continue
            roads = []
            with stopit.ThreadingTimeout(5):
                road = a_star_star(city, targets, travel=road_travel, limit=limit, major=True)
            if verbose: print(city, road[-1])
            for r, s in zip(road[:-1], road[1:]):
                if s in hexes[r].get('roads', set()):
                    continue
                hexes[r].setdefault('tracks', set())
                hexes[r]['tracks'] |= {s}
                hexes[s].setdefault('tracks', set())
                hexes[s]['tracks'] |= {r}

    if update_map:
        roads_map()


def roads_generate_v2(clear=True, update_map=False, verbose=True, limit=3):

    roads_clear()
    done = set()
    for city in sorted([
        c for c in cities if cities[c]['race'] != 'orc' and sociology[c]['infrastructure'] >= 50
    ], key=lambda x: cities[x]['population'], reverse=True):
        if city in done:
            continue
        if len(hexes[city].get('roads_to', set())) >= limit:
            continue
        if verbose: print('City: {:}'.format(city))
        targets = [t for t in infra_flood_fill(city) if t in cities and
                   t != city and sociology[t]['infrastructure'] >= 11 and
                   hexDistance(city, t) < 25 and cities[t]['race'] != 'orc' and
                   size_class(city, t) and
                   len(hexes[t].get('roads_to', set())) < limit]
        if not targets:
            continue
        for target in sorted(targets, key=lambda t: sociology[t]['infrastructure'], reverse=True):
            road = None
            with stopit.ThreadingTimeout(20):
                road = road_path(city, target, limit=limit, major=True if math.log10(cities[city]['population']) < 5 else False)
            if not road:
                continue
            if verbose: print(city, target)
        # if not road:
        #     continue
            for r, s in zip(road[:-1], road[1:]):
                hexes[r].setdefault('road_to', set())
                hexes[r]['road_to'] |= {s}
                hexes[s].setdefault('road_to', set())
                hexes[s]['road_to'] |= {r}
            if len(hexes[target].get('roads_to', set())) < limit:
                continue
        r = True
        while r:
            r = False
            for town in sorted(
                    [t for t in infra_flood_fill(city) if
                     sociology[t]['infrastructure'] >= 11 and
                     t != city and t in cities and
                     not hexes[t].get('road_to') and
                     hexDistance(city, t) < 25 and
                     cities[t]['race'] != 'orc' and
                     len(hexes[t].get('roads_to', set())) < limit
                    ], key=lambda x: cities[x]['population'], reverse=True):
                if hexes[town].get('road_to'):
                    continue
                targets = sorted([
                    t for t in infra_flood_fill(city) if
                    t != town and
                    sociology[t]['infrastructure'] >= 11 and
                    hexDistance(town, t) < 10 and
                    hexes[t].get('road_to') and
                    len(hexes[t].get('roads_to', set())) < limit
                ], key=lambda x: sociology[x]['infrastructure'], reverse=True)
                if not targets:
                    continue
                # if verbose: print(town, end='', flush=True)
                # print(len(targets))
                for target in targets:
                    # print(target)
                    road = None
                    with stopit.ThreadingTimeout(10):
                        road = road_path(town, target, limit=limit, major=True)
                    if not road:
                        continue
                    if verbose: print(town, target)
                    r = True
                    # if not road:
                    #     continue
                    for r, s in zip(road[:-1], road[1:]):
                        hexes[r].setdefault('road_to', set())
                        hexes[r]['road_to'] |= {s}
                        hexes[s].setdefault('road_to', set())
                        hexes[s]['road_to'] |= {r}
                    break
        done |= {town for town in infra_flood_fill(city) if town in cities and sociology[town]['infrastructure'] >= 50 and hexes[town].get('road_to', set())}
        print('{:0.2%}'.format(len(done) / len([c for c in cities if sociology[c]['infrastructure'] >= 50])))
    if update_map:
        roads_map()


def roads_generate_v4(clear=True):

    if clear:
        roads_clear()

    for h in sociology:
        if sociology[h]['infrastructure']:
            sociology[h]['roads'] = road_levels(h)

    if update_map:
        roads_svg()
