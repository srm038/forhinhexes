from defs import *
from pressure import *
import scipy.stats
# from mapping import save
load_data(terrain, terrain_file)
load_data(elevation, fr"{basefile}\elevation.json")
load_data(pressure, fr"{basefile}\pressure.json")
load_data(sea_wind, fr"{basefile}\sea_wind.json")
load_data(wind, fr"{basefile}\wind.json")
load_data(wind_land, fr"{basefile}\wind_land.json")


def pressure_curl_old(season):

    discretes = {}
    print('Getting pressure cells...')

    for p in pressures[season]:
        # pass
        # id = latlong(eval(p['d'].split(' ')[1]), terr = True)
        sector = latlong(hexdict[p][0], terr=True, sector=True)
        datum = round((pressures[season][p] - 128) / 255 * 2, 3) + 0
        x = hexdict[p][0][0]
        y = hexdict[p][0][1] + 50
        # h = Hex()
        # h.x = x
        # h.y = y
        pp = parseID(p)
        latitude = round(int(pp[1]) * -90 / 207 + 90, 2)
        discretes.update({p: {'sector': sector, 'gradient': datum, 'x': x, 'y': y, 'latitude': latitude}})
        # p['id'] = id
        # hexes.update({id: h})
    for d in discretes:
        p = parseID(d)
        if discretes[d]['latitude'] == 0:
            if p[0] in [104, -104]:
                n = [
                    '207-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                    '207-' + str(abs(p[0] + 1)) + ('E' if p[0] + 1 > 0 else ('W' if p[0] + 1 < 0 else '')),
                    '206N-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                    '206S-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                ]
            else:
                n = [
                    '207-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                    '207-' + str(abs(p[0] + 1)) + ('E' if p[0] + 1 > 0 else ('W' if p[0] + 1 < 0 else '')),
                    '206N-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                    '206N-' + str(abs(p[0])) + ('E' if p[0] > 0 else ('W' if p[0] < 0 else '')),
                    '206S-' + str(abs(p[0])) + ('E' if p[0] > 0 else ('W' if p[0] < 0 else '')),
                    '206S-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                ]
        else:
            x = discretes[d]['x']
            y = discretes[d]['y']
            n = list(set([i for i in [
                latlong((round(x - 50 * math.sqrt(3) / 2, 2), y - 75 - 50), terr=True),
                latlong((round(x + 50 * math.sqrt(3) / 2, 2), y - 75 - 50), terr=True),
                latlong((round(x - 50 * math.sqrt(3) / 2, 2), y + 75 - 50), terr=True),
                latlong((round(x + 50 * math.sqrt(3) / 2, 2), y + 75 - 50), terr=True),
                latlong((round(x + 100 * math.sqrt(3) / 2, 2), y - 50), terr=True),
                latlong((round(x - 100 * math.sqrt(3) / 2, 2), y - 50), terr=True),
            ] if i in hexdict]))
        discretes[d]['neighbors'] = n

    # try an alternate method of curl derivation
    i = 0
    for d in wind[season]:
        try:
            a = (pressure[season][getTarget(d, 300)], pressure[season][getTarget(d, 240)])
            # a = (
            #     discretes[discretes[d]['neighbors'][1]]['gradient'],
            #     discretes[discretes[d]['neighbors'][0]]['gradient'])
            if a[0] - a[1] > 0:
                scale_a = round(a[0] - a[1], 3)
                a = (hexdict[getTarget(d, 300)][0][0] - hexdict[getTarget(d, 240)][0][0],
                     hexdict[getTarget(d, 300)][0][1] - hexdict[getTarget(d, 240)][0][1])
                # a = (
                #     round(discretes[discretes[d]['neighbors'][1]]['x'] - discretes[discretes[d]['neighbors'][0]]['x'],
                #           1),
                #     round(discretes[discretes[d]['neighbors'][1]]['y'] - discretes[discretes[d]['neighbors'][0]]['y'],
                #           1)
                # )
            elif a[0] - a[1] < 0:
                scale_a = round(a[1] - a[0], 3)
                a = (hexdict[getTarget(d, 240)][0][0] - hexdict[getTarget(d, 300)][0][0],
                     hexdict[getTarget(d, 240)][0][1] - hexdict[getTarget(d, 300)][0][1])
                # a = (
                #     round(discretes[discretes[d]['neighbors'][0]]['x'] - discretes[discretes[d]['neighbors'][1]]['x'],
                #           1),
                #     round(discretes[discretes[d]['neighbors'][0]]['y'] - discretes[discretes[d]['neighbors'][1]]['y'],
                #           1)
                # )
            else:
                scale_a = 0
                a = (0, 0)
        except:
            scale_a = 0
            a = (0, 0)
        try:
            b = (pressure[season][getTarget(d, 120)], pressure[season][getTarget(d, 180)])
        #     b = (
        #         discretes[discretes[d]['neighbors'][2]]['gradient'],
        #         discretes[discretes[d]['neighbors'][4]]['gradient'])
            if b[0] - b[1] > 0:
                scale_b = round(b[0] - b[1], 3)
                b = (hexdict[getTarget(d, 120)][0][0] - hexdict[getTarget(d, 180)][0][0],
                     hexdict[getTarget(d, 120)][0][1] - hexdict[getTarget(d, 180)][0][1])
        #         b = (
        #             round(discretes[discretes[d]['neighbors'][2]]['x'] - discretes[discretes[d]['neighbors'][4]]['x'],
        #                   1),
        #             round(discretes[discretes[d]['neighbors'][2]]['y'] - discretes[discretes[d]['neighbors'][4]]['y'],
        #                   1)
        #         )
            elif b[0] - b[1] < 0:
                scale_b = round(b[1] - b[0], 3)
                b = (hexdict[getTarget(d, 180)][0][0] - hexdict[getTarget(d, 120)][0][0],
                     hexdict[getTarget(d, 180)][0][1] - hexdict[getTarget(d, 120)][0][1])
        #         b = (
        #             round(discretes[discretes[d]['neighbors'][4]]['x'] - discretes[discretes[d]['neighbors'][2]]['x'],
        #                   1),
        #             round(discretes[discretes[d]['neighbors'][4]]['y'] - discretes[discretes[d]['neighbors'][2]]['y'],
        #                   1)
        #         )
        #     else:
        #         scale_b = 0
        #         b = (0, 0)
        except:
            scale_b = 0
            b = (0, 0)
        # try:
        #     c = (
        #         discretes[discretes[d]['neighbors'][3]]['gradient'],
        #         discretes[discretes[d]['neighbors'][5]]['gradient'])
        #     if c[0] - c[1] > 0:
        #         scale_c = round(c[0] - c[1], 3)
        #         c = (
        #             round(discretes[discretes[d]['neighbors'][3]]['x'] - discretes[discretes[d]['neighbors'][5]]['x'],
        #                   1),
        #             round(discretes[discretes[d]['neighbors'][3]]['y'] - discretes[discretes[d]['neighbors'][5]]['y'],
        #                   1)
        #         )
        #     elif c[0] - c[1] < 0:
        #         scale_c = round(c[1] - c[0], 3)
        #         c = (
        #             round(discretes[discretes[d]['neighbors'][5]]['x'] - discretes[discretes[d]['neighbors'][3]]['x'],
        #                   1),
        #             round(discretes[discretes[d]['neighbors'][5]]['y'] - discretes[discretes[d]['neighbors'][3]]['y'],
        #                   1)
        #         )
        #     else:
        #         scale_c = 0
        #         c = (0, 0)
        # except:
        #     scale_c = 0
        #     c = (0, 0)
        a = (scale_a * a[0], scale_a * a[1])
        b = (scale_b * b[0], scale_b * b[1])
        # c = (scale_c * c[0], scale_c * c[1])
        # t = (round(a[0] + b[0] + c[0], 3), round(a[1] + b[1] + c[1], 3))
        t = (round(a[0] + b[0], 3), round(a[1] + b[1], 3))
        wind[season][d] = t

    # for d in discretes:
    #     try:
    #         b = discretes[discretes[d]['neighbors'][1]]['gradient'] - discretes[discretes[d]['neighbors'][0]]['gradient']
    #         b /= -2
    #         b = round(b, 2)
    #         a1 = max([discretes[discretes[d]['neighbors'][2]]['gradient'], discretes[discretes[d]['neighbors'][3]]['gradient']])
    #         a2 = min([discretes[discretes[d]['neighbors'][4]]['gradient'], discretes[discretes[d]['neighbors'][5]]['gradient']])
    #         a = (a1 - a2) / 2
    #         a = round(a, 2)
    #         discretes[d]['curl'] = (a, b)
    #     except:
    #         pass

    print('Calculating vector field curl...')

    with open(windfile, 'r') as f:
        windmap = bs(f, 'lxml')

    # i = 0
    curls = []
    string = ''
    i = 0
    for d in discretes:
        if d not in hexes:
            continue
        if 'curl' in discretes[d]:
            if i % 2:
                i += 1
                continue
            theta = round(math.degrees(atan2(discretes[d]['curl'][1], discretes[d]['curl'][0])), 3)
            scale = round(math.sqrt(discretes[d]['curl'][0] ** 2 + discretes[d]['curl'][1] ** 2), 3)
            phi = 0 * sector_rotate[discretes[d]['sector']] - 90 + 30
            if 0 < scale <= 50:
                string += '<use xlink:href="#windmarker" transform="translate ({:} {:}) scale({:.3f}) rotate ({:.0f}) rotate({:})" />'.format(
                    discretes[d]['x'], discretes[d]['y'], 0.25 * scale, theta + 180, phi
                )
            winds[d][season] = (scale, theta)
        i += 1
    print('{:} s: {:}'.format(timer(1), len(string)))
    print('Adding to map...')
    # for c in curls:
    windmap.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
    # else:
    save(windmap)
    print('{:} s'.format(timer(1)))


def pressure_hamiltonian(season):

    wind[season].clear()
    for h in hexdict:
        try:
            A = [n for n in all_neighbors[h] if hexAngle(h, n) == 0][0]
            B = [n for n in all_neighbors[h] if hexAngle(h, n) == 60][0]
            C = [n for n in all_neighbors[h] if hexAngle(h, n) == 120][0]
            D = [n for n in all_neighbors[h] if hexAngle(h, n) == 180][0]
            E = [n for n in all_neighbors[h] if hexAngle(h, n) == 240][0]
            F = [n for n in all_neighbors[h] if hexAngle(h, n) == 300][0]
            dx = round(pressure[season][A] - pressure[season][D], 4)
            dy = round(statistics.mean([pressure[season][B], pressure[season][C]]) -\
                 statistics.mean([pressure[season][E], pressure[season][F]]), 4)
            g = (dx, dy)
            wind[season][h] = g
        except:
            pass


def wind_generate(season='Jul', interpolate=True, blur=False, v0=5):

    pressure_define(season=season, blur=False, save=False)
    # for h in pressure[season]:
    #     wind[season][h] = ((pressure[season][h] + 1) / 2, (pressure[season][h] + 1) / 2)
    # i = 1
    # visited = set()
    # while i:
    #     i = 0
    #     for c in wind[season]:
    #         if c in visited:
    #             continue
    #         if pressure[season].get(c, 0) in [-1, 1]:
    #             # continue
    #             if sum(pressure[season].get(n, 0) in [-1, 1] for n in all_neighbors[c]) < 3:
    #                 wind[season][c] = (0.5, 0.5)
    #                 visited.add(c)
    #                 # print(c)
    #                 i += 1
    #             # continue
    #         # elif sum(n in wind[season] for n in all_neighbors[c]) > 3:
    #         #     wind[season][h] = ((pressure[season][all_neighbors[c][0]] + 1) / 2, (pressure[season][h] + 1) / 2)
    #         #     # print(c)
    #         #     i = True
    #     print(i)
    wind[season].clear()

    HP = set()
    for h in pressure[season]:
        if pressure[season][h] == 0:
            if any(pressure[season][n] == 1 for n in all_neighbors[h]):
                HP.add(h)
    LP = set()
    for h in pressure[season]:
        if pressure[season][h] == 0:
            if any(pressure[season][n] == -1 for n in all_neighbors[h]):
                LP.add(h)

    load_data(pressure, fr"{basefile}\pressure.json")

    for h in HP:
        n = [n for n in all_neighbors[h] if pressure[season][n]]
        theta = statistics.mean([hexAngle(h, i) for i in n])
        # theta = math.degrees(math.atan(scipy.stats.linregress([hexdict[x][0][0] for x in n], [hexdict[y][0][1] for y in n]).slope))
        theta += 90 if latitude(h) >= 0 else -90
        wind[season][h] = theta % 360
        if math.isnan(wind[season][h]):
            wind[season].pop(h, None)

    for h in LP:
        n = [n for n in all_neighbors[h] if pressure[season][n]]
        theta = statistics.mean([hexAngle(h, i) for i in n])
        # theta = math.degrees(math.atan(scipy.stats.linregress([hexdict[x][0][0] for x in n], [hexdict[y][0][1] for y in n]).slope))
        theta += -90 if latitude(h) >= 0 else 90
        wind[season][h] = theta % 360
        if math.isnan(wind[season][h]):
            wind[season].pop(h, None)

    wind_svg(season=season

    for h in wind[season]:
        wind[season][h] = (v0, wind[season][h])

    if interpolate:
        wind_interpolate(season=season)

    save_data(wind, fr"{basefile}\wind.json")


def wind_flood_fill(start, sources, k=100):

    inspecting = set([start])
    visited = set()
    filtered = set()

    while inspecting and len(filtered) < k:
        for c in list(inspecting):
            if c in visited:
                inspecting.remove(c)
                continue
            if c in sources:
                inspecting.remove(c)
                visited.add(c)
                filtered.add(c)
                continue
            inspecting |= set([n for n in all_neighbors[c] if start != n])
            visited.add(c)
            inspecting.remove(c)
        # print(len(visited), len(inspecting), len(filtered))

    return filtered


def wind_interpolate(season='Jul', k=50, n=1.5, v0=5):
    '''
    Use IDW to interpolate between winds
    :param k:
    :param n:
    :return:
    '''

    sources = set(wind[season])
    filtered = [h for h in hexdict if h not in sources]
    i = 0
    for c in filtered:
        idw_neighbors = [(s, hexDistance(c, s)) for s in wind_flood_fill(c, sources, k=k) if s != c]
        if not idw_neighbors:
            continue
        # idw0 = sum(
        #     wind[season][s][0] / d ** n for s, d in idw_neighbors
        # ) / sum(
        #     1 / d ** n for s, d in idw_neighbors
        # )
        idw1 = sum(
            wind[season][s][1] / d ** n for s, d in idw_neighbors
        ) / sum(
            1 / d ** n for s, d in idw_neighbors
        )
        wind[season][c] = (v0, int(idw % 360))
        i += 1
        print(f'{i / len(filtered):.0%}')

    for c in list(wind[season]):
        if wind[season][c] is None:
            wind[season].pop(c, None)


def wind_png(season='Jul'):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in wind[season] if hexdict[h][0][1] >= hexdict['207-518W'][0][1]], key=lambda h: wind[season].get(h, 0))
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    max_c = max((math.hypot(*wind[season][h])) for h in t)
    c = np.array([colors.hex2color(f"#{screen('67a9cf', (1 - math.hypot(*wind[season][h]) / max_c) * 255)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Wind\{season}.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def wind_png_land(season='Jul'):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in wind_land[season] if hexdict[h][0][1] >= hexdict['207-518W'][0][1]], key=lambda h: wind_land[season].get(h, 0)[0])
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    max_c = max(wind_land[season][h][1] for h in t)
    c = np.array([colors.hex2color(f"#{screen('67a9cf', (1 - wind_land[season][h][1] / max_c) * 255)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Wind\land-{season}.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def wind_svg(season='Jul'):

    with open(basefile + r'\Winds.svg', 'r') as f:
        windmap = bs(f, 'html.parser')
    windmap.find(id=f'sea-{season}').clear()
    string = ''
    # max_c = max(math.hypot(*wind[season][h]) for h in wind[season])
    for h in wind[season]:
        # S = math.hypot(*wind[season][h]) / max_c * 20
        # theta = math.degrees(math.atan2(*wind[season][h]))
        S = 5
        theta = round(wind[season][h][1])
        if S != 0:
            for b in barbs:
                if b[0] <= S < b[1]:
                    barb = barbs[b]
                    break
            else:
                barb = 'barb50' if S >= 55 else None
            if barb and S >= 1:
                string += f'<use xlink:href="#{barb}" ' \
                          f'transform="translate ({hexdict[h][0][0]} {hexdict[h][0][1] + 50}) ' \
                          f'scale({1:.3f}) ' \
                          f'rotate ({theta - sector_rotate[getSector(h)]:.0f})" id="{h}" style="color:#000"/>'
    windmap.find('g', id=f'sea-{season}').contents = bs(string, 'html.parser').contents
    windmap.find('g', id=f'sea-{season}')['style'] = 'display:block'
    windmap.find('g', id=f"sea-{'Jul' if season == 'Jan' else 'Jan'}")['style'] = 'display:none'
    windmap.find('g', id=f"land-{season}")['style'] = 'display:none'
    windmap.find('g', id=f"land-{'Jul' if season == 'Jan' else 'Jan'}")['style'] = 'display:none'
    save(windmap)


def wind_svg_land(season='Jul'):

    with open(basefile + r'\WindsLand.svg', 'r') as f:
        windmap = bs(f, 'html.parser')
    windmap.find(id=f'land-{season}').clear()
    string = ''
    # max_c = max(math.hypot(*wind[season][h]) for h in wind[season])
    for h in wind_land[season]:
        # S = math.hypot(*wind[season][h]) / max_c * 20
        # theta = math.degrees(math.atan2(*wind[season][h]))
        S = round(wind_land[season][h][0], 3)
        theta = round(wind_land[season][h][1])
        if S != 0:
            for b in barbs:
                if b[0] <= S < b[1]:
                    barb = barbs[b]
                    break
            else:
                barb = 'barb50' if S >= 55 else None
            if barb and S >= 1:
                string += f'<use xlink:href="#{barb}" ' \
                          f'transform="translate ({hexdict[h][0][0]} {hexdict[h][0][1] + 50}) ' \
                          f'scale({1:.3f}) ' \
                          f'rotate ({theta - sector_rotate[getSector(h)]:.0f})" id="{h}" style="color:#0f0"/>'
    windmap.find('g', id=f'land-{season}').contents = bs(string, 'html.parser').contents
    windmap.find('g', id=f"land-{season}")['style'] = 'display:block'
    windmap.find('g', id=f"land-{'Jul' if season == 'Jan' else 'Jan'}")['style'] = 'display:none'
    save(windmap)



def wind_over_land1(r=0.99, update_map=0, hexes=hexes, sigma=5):

    timer(0)

    sea_winds = dict()
    load_data(sea_winds, basefile + r'\winds.p')

    barbs = {
        (0, 5): 'barb0',
        (5, 10): 'barb5',
        (10, 15): 'barb10',
        (15, 20): 'barb15',
        (20, 25): 'barb20',
        (25, 30): 'barb25',
        (30, 35): 'barb30',
        (35, 40): 'barb35',
        (40, 45): 'barb40',
        (45, 50): 'barb45',
        (50, 55): 'barb50',
    }
    sector_rotate = {
        1: 60 * 2,
        2: 60 * 3,
        3: -60 * 2,
        4: 60,
        5: 0,
        6: -60,
        7: -60,
        8: 0,
        9: 60 * 1,
        10: -60 * 2,
        11: -60 * 3,
        12: 60 * 2
    }

    winds = {h: {'Jan': (0, 0), 'Jul': (0, 0)} for h in hexes}

    for season in ['Jan', 'Jul']:

        received = dict()
        flowsto = dict()
        for h in hexes:
            if hexes[h]['coastal']:
                for n in neighbors(h, land=False):
                    if sea_winds[n][season][0]:
                        winds[h][season] = (25, sea_winds[n][season][1])
                        # print(winds[h][season])
                        break
        sources = set()
        visited = set()

        for h in hexes:  # get initial set
            if hexes[h]['coastal'] and winds[h][season][0]:
                sources.add(h)
        print(len(sources))

        for s in list(sources):  # remove offshore vectors
            neighbor_angles = [roundBase(degrees(atan2(hexes[s]['y'] - hexes[n]['y'], hexes[n]['x'] - hexes[s]['x'])), base=60) % 360
                               for n in hexes[s]['neighbors']]
            if not neighbor_angles:
                sources.remove(s)
                continue
            if roundBase(winds[s][season][1], base=60) not in neighbor_angles:
                sources.remove(s)
        print(len(sources))

        while len(sources):
            sources0 = list(sources)
            for source in sources0:
                if not winds[source][season][0]:
                    sources.remove(source)
                    continue
                targets = [n for n in hexes[source]['neighbors'] if n not in sources0]
                for target in targets:
                    if source in received.get(target, {}):  # check if this target has already been influenced
                        continue
                    # vec_n = (hexes[target]['x'] - hexes[source]['x'], hexes[source]['y'] - hexes[target]['y'])
                    n = hexAngle(source, target)
                    vec_n = (86.6 * cos(radians(n)), 86.6 * sin(radians(n)))
                    vec_w = [winds[source][season][0] * r, winds[source][season][1]]
                    if terrain[source]['altitude'] > terrain[target]['altitude']:
                        vec_w[0] /= r ** 2
                    else:
                        vec_w[0] *= r
                        vec_w[1] += min(90, copysign(wind_turn_aside(terrain[target]['altitude'] - terrain[source]['altitude']),
                                                     -parseID(target)[1]))
                    theta = round(degrees(acos(
                        ((vec_w[0] * cos(radians(vec_w[1])) * vec_n[0] + vec_w[0] * sin(radians(vec_w[1])) * vec_n[1])) / (
                                hypot(*vec_n) * vec_w[0])
                    )))
                    if theta % 360 < 90:
                        vec_t = (
                            vec_n[0] * (vec_w[0] * cos(radians(vec_w[1])) * vec_n[0] + vec_w[0] * sin(radians(vec_w[1])) *
                                        vec_n[1]) / hypot(*vec_n) ** 2,
                            vec_n[1] * (vec_w[0] * cos(radians(vec_w[1])) * vec_n[0] + vec_w[0] * sin(radians(vec_w[1])) *
                                        vec_n[1]) / hypot(*vec_n) ** 2,
                        )
                        vec_t0 = (
                            winds[target][season][0] * cos(radians(winds[target][season][1])),
                            winds[target][season][0] * sin(radians(winds[target][season][1]))
                        )
                        vec_t1 = (
                            (vec_t[0] + vec_t0[0]) / 2,
                            (vec_t[1] + vec_t0[1]) / 2,
                        )
                        winds[target][season] = (
                            # round(hypot(*vec_t1), 2),
                            max(round(hypot(*vec_t1), 2), round(hypot(*vec_t0), 2)),
                            round(degrees(atan2(vec_t1[1], vec_t1[0])) + random.gauss(0, sigma)) % 360
                        ) if winds[target][season][0] else (
                            round(hypot(*vec_t), 2),
                            round(degrees(atan2(vec_t[1], vec_t[0])) + random.gauss(0, sigma)) % 360
                        )
                        if winds[target][season][0] > 1:
                            sources.add(target)
                            received[target] = received.get(target, set()) | set([source])
                            flowsto[source] = flowsto.get(source, set()) | set([target])
                            # visited.add(source)
                            # print(target, theta)
                sources.remove(source)
                visited.add(source)
            print(len(visited), len(sources))

        with open(basefile + r'\winds-received-' + season + '.p', 'wb') as f:
            pickle.dump(received, f)
        with open(basefile + r'\winds-source-' + season + '.p', 'wb') as f:
            pickle.dump(flowsto, f)
        with open(basefile + r'\winds-land-' + season + '.p', 'wb') as f:
            pickle.dump(winds, f)
        if update_map:
            with open(basefile + r'\Winds-Gen.svg', 'r') as f:
                windmap = bs(f, 'lxml')
            windmap.find(id=season).clear()
            string = ''
            for h in winds:
                if winds[h][season][0]:
                    for b in barbs:
                        if b[0] <= winds[h][season][0] < b[1]:
                            barb = barbs[b]
                            break
                    else:
                        if winds[h][season][0] >= 55:
                            barb = 'barb50'
                        else:
                            barb = None
                    if barb and winds[h][season][0] >= 1:
                        string += '<use xlink:href="#{:}" transform="translate ({:} {:}) scale({:.3f}) rotate ({:.0f}) rotate({:})" id="{:}" style="color:{:}"/>'.format(
                            barb, hexes[h]['x'], hexes[h]['y'], 0.75 + 0 * 0.25 * winds[h][season][0], -winds[h][season][1],
                                                          0 * sector_rotate[hexes[h]['sector']], h, '#4f4'
                            # if winds[h][season][0] == 10 else '#000'
                        )
            windmap.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
            save(windmap)
    print('{:} s'.format(timer(1)))


def wind_over_land2(r=0.99, update_map=0, hexes=hexes, sigma=5):

    timer(0)

    sea_winds = dict()
    load_data(sea_winds, basefile + r'\winds.p')

    sector_rotate = {
        1: 60 * 2,
        2: 60 * 3,
        3: -60 * 2,
        4: 60,
        5: 0,
        6: -60,
        7: -60,
        8: 0,
        9: 60 * 1,
        10: -60 * 2,
        11: -60 * 3,
        12: 60 * 2
    }

    winds = {h: {'Jan': (sea_winds[h]['Jan'][0] / 5, sea_winds[h]['Jan'][1]), 'Jul': (sea_winds[h]['Jul'][0] / 5, sea_winds[h]['Jul'][1])} for h in hexes}

    for season in ['Jan', 'Jul']:

        winds = {h: {'Jan': (sea_winds[h]['Jan'][0] / 10, sea_winds[h]['Jan'][1]),
                     'Jul': (sea_winds[h]['Jul'][0] / 10, sea_winds[h]['Jul'][1])} for h in hexes}
        received = dict()
        flowsto = dict()
        for h in hexes:
            if hexes[h]['coastal']:
                for n in neighbors(h, land=False):
                    if sea_winds[n][season][0]:
                        winds[h][season] = (25, sea_winds[n][season][1])
                        # print(winds[h][season])
                        break
        sources = set()
        visited = set()

        for h in hexes:  # get initial set
            if hexes[h]['coastal'] and winds[h][season][0]:
                sources.add(h)
        print(len(sources))

        for s in list(sources):  # remove offshore vectors
            neighbor_angles = [hexAngle(s, n) for n in hexes[s]['neighbors']]
            if not neighbor_angles:
                sources.remove(s)
                continue
            if roundBase(winds[s][season][1], base=60) not in neighbor_angles:
                sources.remove(s)
                # winds[s][season] = (0, 0)
        print(len(sources))

        i = 0
        while len(sources):
            i += 1
            sources0 = list(sources)
            for source in sources0:
                if not winds[source][season][0]:
                    sources.remove(source)
                    continue
                targets = [n for n in hexes[source]['neighbors'] if n not in visited]
                if not targets:
                    sources.remove(source)
                    continue
                for target in targets:
                    v = [winds[source][season][0], winds[source][season][1]]
                    v[0] *= r if terrain[source]['altitude'] >= terrain[target]['altitude'] else 1
                    v[1] += min(
                        90,
                        copysign(wind_turn_aside(terrain[target]['altitude'] - terrain[source]['altitude']), -parseID(target)[1])
                    ) if terrain[source]['altitude'] >= terrain[target]['altitude'] else 0
                    # n = (round(hexes[target]['x'] - hexes[source]['x'], 2), round(hexes[source]['y'] - hexes[target]['y'], 2))
                    n = hexAngle(source, target)
                    n = (86.6 * cos(radians(n)), 86.6 * sin(radians(n)))
                    theta = roundBase(degrees(acos(min(1, max(-1,
                                                              ((v[0] * cos(radians(v[1])) * n[0] + v[0] * sin(radians(v[1])) * n[1])) / (
                                hypot(*n) * v[0])
                                                              )))), base=60)
                    if not theta % 360 < 90:
                        continue
                    vx = avg([v[0] * cos(radians(v[1])), winds[source][season][0] * cos(radians(winds[source][season][1]))])
                    vy = avg([v[0] * sin(radians(v[1])), winds[source][season][0] * sin(radians(winds[source][season][1]))])
                    winds[target][season] = (hypot(vx, vy), round(degrees(atan2(vy, vx))))
                    # winds[target][season] = (
                    #     round(avg([v[0], winds[source][season][0]]), 2),
                    #     round(avg([
                    #         degrees(atan2(v[1], v[0])), winds[source][season][1]
                    #     ]) if winds[source][season][1] != 0 else v[1]) % 360
                    # )
                    sources.add(target)
                sources.discard(source)
                visited.add(source)
            print(len(visited), len(sources))
            # if i == 5:
            #     break
        for i in range(3):
            backup = copy.deepcopy(winds)
            for w in winds:
                if not hexes[w]['neighbors']:
                    continue
                winds[w][season] = (
                    avg([backup[n][season][0] for n in hexes[w]['neighbors']]),
                    avg([backup[n][season][1] for n in hexes[w]['neighbors']]),
                )

        with open(basefile + r'\winds-land-' + season + '.p', 'wb') as f:
            pickle.dump(winds, f)
        wind_map()


def wind_over_land3(r=0.99, update_map=False, hexes=hexes, sigma=5, g=0, verbose=False):

    timer(0)

    sea_winds = dict()
    load_data(sea_winds, basefile + r'\winds.p')

    seasons = ['Jul', 'Jan']

    winds = {h: {'Jan': (1, sea_winds[h]['Jan'][1]),
                 'Jul': (1, sea_winds[h]['Jul'][1])} for h in hexes}
    # winds = {h: {'Jan': (0, 0),
    #              'Jul': (0, 0)} for h in hexes}

    # received = dict()
    # flowsto = dict()
    for season in seasons:
        sources = set()
        visited = set()
        received = dict()
        flowsto = dict()
        for h in hexes:
            if hexes[h]['coastal']:
                for n in neighbors(h, land=False):
                    if n not in hexes and sea_winds[n][season][0]:
                        a = (
                            sea_winds[n][season][0] * cos(radians(sea_winds[n][season][1])),
                            sea_winds[n][season][0] * cos(radians(sea_winds[n][season][1]))
                        )
                        b = hexAngle(h, n)
                        # if not b:
                        #     continue
                        b = (50 * cos(radians(b)), 50 * sin(radians(b)))
                        if angle_btwn(a, b) % 360 < 90:
                            continue
                        winds[h][season] = (25, sea_winds[h][season][1])
                        sources.add(h)
                        # print(winds[h][season])
                        break
        allwind = {h: [winds[h][season]] for h in hexes}
        print(len(sources))

        # sources.add(list(h for h in hexes if hexes[h]['coastal'] and winds[h][season])))
        # source = list(sources)[0]
        s = 0
        for source in sources:
            e = 0
            s += 1
            current_wind = {source: winds[source][season]}
            currents = set()
            visited = set()
            currents.add(source)
            while len(currents) and e < 5:
                currents0 = [c for c in currents]
                if all(current in visited for current in currents):
                    break
                for current in (c for c in currents0 if c not in visited):
                    # print(current, current_wind[current][0])
                    if current_wind[current][0] < 1:
                        currents.discard(current)
                        visited.add(current)
                        continue
                    a = (
                        current_wind[current][0] * cos(radians(current_wind[current][1])),
                        current_wind[current][0] * sin(radians(current_wind[current][1])),
                    )
                    if not hexes[current]['neighbors']:
                        continue
                    for neighbor in hexes[current]['neighbors']:
                        angle = hexAngle(current, neighbor)
                        b = (50 * cos(radians(angle)), 50 * sin(radians(angle)))
                        theta = angle_btwn(a, b) % 360
                        if theta > 90:
                            continue
                        ab = project(a, b)
                        if round(hypot(*ab) * r, 3) < 1:
                            continue
                        k = r if terrain[current]['altitude'] >= terrain[neighbor]['altitude'] else 1
                        # angle = round(degrees(atan2(ab[1], ab[0])) % 360)
                        angle += min(
                            90,
                            copysign(wind_turn_aside(terrain[neighbor]['altitude'] - terrain[current]['altitude']), -parseID(neighbor)[1])
                        ) if terrain[current]['altitude'] >= terrain[neighbor]['altitude'] else 0
                        current_wind[neighbor] = (round(hypot(*ab) * k, 3), int(angle) % 360)
                        currents.add(neighbor)
                        received[neighbor] = received.get(neighbor, set()) | set([current])
                        flowsto[current] = flowsto.get(current, set()) | set([neighbor])
                        if verbose: print('{:} to {:}, {:}'.format(current, neighbor, current_wind[neighbor][0]))
                    visited.add(current)
                    currents.discard(current)
                # print(len(currents), e)
                e += 1 if len(currents) < 5 else 0
            for current in visited:
                allwind[current].append(current_wind[current])
            print('{:}: {:}'.format(s, len(visited)))
        with open(basefile + r'\winds-received-' + season + '.p', 'wb') as f:
            pickle.dump(received, f)
        with open(basefile + r'\winds-source-raw-' + season + '.p', 'wb') as f:
            pickle.dump(flowsto, f)
        for source in allwind:
            if allwind[source][-1] == (0, 0):
                a = (0, 0)
            else:
                a = (
                    statistics.mean(i[0] * cos(radians(i[1])) for i in allwind[source] if i[0]),
                    statistics.mean(i[0] * sin(radians(i[1])) for i in allwind[source] if i[0]),
                )
            winds[source][season] = (
                round(hypot(*a), 3),
                int(round(degrees(atan2(a[1], a[0])), 0) % 360),
            )
        for i in range(g):
            backup = copy.deepcopy(winds)
            for w in hexes:
                if not hexes[w]['neighbors']:
                    continue
                a = (
                    statistics.mean(winds[n][season][0] * cos(radians(winds[n][season][1])) for n in hexes[w]['neighbors']),
                    statistics.mean(winds[n][season][0] * sin(radians(winds[n][season][1])) for n in hexes[w]['neighbors']),
                )
                winds[w][season] = (
                    round(hypot(*a), 3),
                    int(round(degrees(atan2(a[1], a[0])), 0) % 360),
                )
        flowstonew = dict()
        for source in winds:
            a = winds[source][season][1]
            for neighbor in hexes[source]['neighbors']:
                b = hexAngle(source, neighbor)
                if abs(a - b) % 360 < 90:
                    continue
                else:
                    flowstonew[source] = flowstonew.get(source, set()) | set([neighbor])
        with open(basefile + r'\winds-land.p', 'wb') as f:
            pickle.dump(winds, f)
        with open(basefile + r'\allwinds-land-' + season + '.p', 'wb') as f:
            pickle.dump(allwind, f)
        with open(basefile + r'\winds-source-' + season + '.p', 'wb') as f:
            pickle.dump(flowstonew, f)
    if update_map: wind_map()


def wind_over_land4(season, g=0.99, z=5000, elastic=False, v0=5, k=0.15):

    # TODO set up flowsto and received sets

    rho = math.log(89) / z

    # wind_sea = dict()
    # load_data(wind_sea, basefile + r'\winds.p')

    sea_level = seaLevelGet()
    ocean = oceanGet(seaLevel=sea_level)
    wind_land = {h: (v0, wind[season][h][1]) for h in wind[season] if h not in ocean and is_coastal(h, ocean) and getTarget(h, wind[season][h][1]) not in ocean}

    sources = set(list(wind_land))

    visited = set()
    current = {s: [wind_land[s]] for s in sources}

    print(len(sources), len(wind_land))

    while len(sources):
        for source in list(sources):
            if wind_land[source][0] < 0.1:
                sources.remove(source)
                visited.add(source)
                continue
            a = (wind_land[source][0] * math.cos(math.radians(wind_land[source][1])),
                 wind_land[source][0] * math.sin(math.radians(wind_land[source][1])))
            for target in all_neighbors[source]:
                if target in visited:
                    continue
                if target in ocean:
                    continue
                # b = (hexes[target]['x'] - hexes[source]['x'], hexes[source]['y'] - hexes[target]['y'])
                b = hexAngle(source, target)
                b = (v0 * math.cos(math.radians(b)), v0 * math.sin(math.radians(b)))
                c = project(a, b)
                # print(n, a, b, c, angle_btwn(a, b) % 360)
                if angle_btwn(a, b) % 360 > 90: # then at least target hex exists
                    continue
                dh = elevation[target]['altitude'] - elevation[source]['altitude']
                # s0 = 0.2 / (1 + math.exp(-0.00075 * dh)) + 1 - 0.2/2
                s0 = 2 - g if dh < 0 else g
                theta = wind_turn_aside(dh, r=rho)
                theta += wind_land[source][1]
                theta = (1 - k) * theta + k * wind[season][target][1]
                w = (s0 * math.hypot(*c), theta % 360)
                sources.add(target)
                current.setdefault(target, list())
                current[target].append(w)
            sources.remove(source)
            visited.add(source)
        for c in current:
            if c in visited:
                continue
            f = sum if elastic else statistics.mean
            a = f(i[0] * math.cos(math.radians(i[1])) for i in current[c])
            b = f(i[0] * math.sin(math.radians(i[1])) for i in current[c])
            # wind_land.setdefault(c, dict())
            # if c not in wind_land:
            #     wind_land[c] = dict()
            wind_land[c] = (math.hypot(a, b), math.degrees(math.atan2(b, a)))
        print(len(sources), len(wind_land), len(visited))

        # wind_rescale
        max_w = max(wind_land[h][0] for h in wind_land)
        min_w = min(wind_land[h][0] for h in wind_land)
        for h in wind_land:
            wind_land[h][season] = (
                (wind_land[h][season][0] - min_w) * (min(max_w, 50) - 1) / (max_w - min_w) + 1,
                wind_land[h][season][1]
            )
        print('{:0.2f}'.format(max_w))

        if update_map:
            pass


def wind_over_land5(season, g=0.99, z=5000, elastic=False, v0=5):

    rho = math.log(89) / z / 2
    sea_level = seaLevelGet()
    ocean = oceanGet(seaLevel=sea_level)
    wind_land[season].clear()
    wind_land[season] = {h: (v0, wind[season][h][1]) for h in wind[season] if
                         h not in ocean and getTarget(h, wind[season][h][1]) not in ocean}

    for h in wind_land[season]:
        target = getTarget(h, wind_land[season][h][1])
        dh = elevation[target]['altitude'] - elevation[source]['altitude']
        theta = wind_turn_aside(dh, r=rho)
        target1 = getTarget(h, (wind_land[season][h][1] + theta) % 360)
        target2 = getTarget(h, (wind_land[season][h][1] - theta) % 360)
        if elevation[target1]['altitude'] > elevation[target2]['altitude']:
            theta *= -1
        wind_land[season][h] = (wind_land[season][h][0], (wind_land[season][h][1] + theta) % 360)

    save_data(wind_land, fr"{basefile}\wind_land.json")


def wind_turn_aside(dh, K=90, P0=1, r=math.log(89) / 5000 / 2):

    if dh < 0:
        return 0

    return round((K * P0 * math.exp(r * dh)) / (K + P0 * (math.exp(r * dh) - 1)))


def wind_blur(g=3, r=5):

    sea_level = seaLevelGet()
    ocean = oceanGet(seaLevel=sea_level)
    for h in wind[season]:
        if h in wind_land:
            continue
        else:
            wind_land[season][h] = wind[season][h]
    i = 0
    for _ in range(g):
        data_copy = {c: float(wind_land[season][c][1]) for c in wind_land[season] if wind_land[season][c][1] is not None}
        for c in data_copy:
            i += 1
            if wind_land[season][c][1] is None:
                continue
            wind_land[season][c] = (5, round(statistics.mean([data_copy[n] for n in neighborsWithin(c, r if c in ocean else r // 2) if n in data_copy] + [data_copy[c]])))
            print(f'{i / len(data_copy) / g:.4%}')

    save_data(wind_land, fr"{basefile}\wind_land.json")
