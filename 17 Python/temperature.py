from defs import *
load_data(all_neighbors, basefile + r'\all_neighbors.p')
load_data(elevation, fr"{basefile}\elevation.json")
load_data(influences, basefile + r'\influences.json')
load_data(temperature, basefile + r'\temperature.json')
load_data(climate, basefile + r'\climate.json')


def temperature_baseline(season='Jul', max_t=90, min_tw=41, min_ts=1, sea_level=None, ocean=None):

    if season not in ['Jan', 'Jul']:
        raise Exception
    temperature[season].clear()
    if not sea_level:
        sea_level = seaLevelGet()
    if not ocean:
        ocean = oceanGet(seaLevel=sea_level)
    for h in hexdict:
        if h in ocean:
            continue
        lat = latitude(h)
        lat *= 1 if season == 'Jul' else -1
        # t = max_t * math.exp -(lat ** 2) / (42.427 ** 2)) if lat <= 0 else (max_t - 23) * math.exp -(lat ** 2) / ((42.427 * 1.85) ** 2)) + 23
        # t = max_t - (max_t - min_t) * sin(radians(lat - 10 * a - 45))
        min_t = min_tw if lat < 0 else min_ts
        t = (max_t - min_t) / 2 * math.cos(lat * 2 * math.pi/ 180) + (max_t + min_t) / 2
        temperature[season][h] = round(t, 1)
    save_data(temperature, basefile + r'\temperature.json')


def temperature_influences_v1(season, maxt=80, maxtt=85, sea_level=None, ocean=None):

    if season not in ['Jan', 'Jul']:
        raise Exception
    temperature[season].clear()
    if not sea_level:
        sea_level = seaLevelGet()
    if not ocean:
        ocean = oceanGet(seaLevel=sea_level)
    def f(lat, K, P0, r):
        return K * P0 * math.exp(r * lat) / (K + P0 * (math.exp(r * lat) - 1))
    def Tn(lat):
        return (f(lat + 61, maxt + 13, 1, 0.25) - 13) * (lat < 0) + f(-lat + 90, maxt, 14, 0.075) * (lat >= 0)
    def Th(lat):
        return (f(lat + 100, maxt + 13, 1, 0.1) - 13) * (lat < 0) + f(-lat + 90, maxt, 14, 0.075) * (lat >= 0)
    def Tm(lat):
        return (f(lat + 65, maxt + 13, 1, 0.15) - 13) * (lat < 0) + f(-lat + 80, maxt, 14, 0.085) * (lat >= 0)
    def Tc(lat):
        return (f(lat + 60, maxt + 13, 1, 0.14) - 13) * (lat < 0) + f(-lat + 40, maxt, 14, 0.17) * (lat >= 0)
    def Tct(lat):
        return (f(lat + 50, maxtt + 30, 1, 0.4) - 30) * (lat < 0) + (f(-lat + 67, maxtt - 10, 14, 0.15) - 10)* (lat >= 0)
    def Tctp(lat):
        return (f(lat + 50, maxtt + 30, 1, 0.4) - 30) * (lat < 0) + (f(-lat + 67, maxtt - 10, 14, 0.35) - 10)* (lat >= 0)
    for h in hexdict:
        if h in ocean:
            continue
        lat = latitude(h)
        if influences[season].get(h, None) is None:
            temperature[season][h] = round(Tn(lat if season == 'Jul' else -lat), 1)
        elif influences[season].get(h, None) == 'hot': # hot
            temperature[season][h] = round(Th(lat if season == 'Jul' else -lat), 1)
        elif influences[season].get(h, None) == 'cold': # cold
            temperature[season][h] = round(Tc(lat if season == 'Jul' else -lat), 1)
        elif influences[season].get(h, None) == 'cont': # cont
            temperature[season][h] = round(Tct(lat if season == 'Jul' else -lat), 1)
        elif influences[season].get(h, None) == 'cont+': # cont+
            temperature[season][h] = round(Tctp(lat if season == 'Jul' else -lat), 1)
        else: # mild
            temperature[season][h] = round(Tm(lat if season == 'Jul' else -lat))
    save_data(temperature, basefile + r'\temperature.json')


def temperature_influences_v2(season, sea_level=None, ocean=None):

    if season not in ['Jan', 'Jul']:
        raise Exception
    temperature[season].clear()
    if not sea_level:
        sea_level = seaLevelGet()
    if not ocean:
        ocean = oceanGet(seaLevel=sea_level)
    for h in hexdict:
        if h in ocean:
            continue
        lat = latitude(h)
        lat *= 1 if season == 'Jul' else -1
        t = {
            'normal': lambda x: -42.5 * (x / 90) ** 2 + 25 if x >=0 else -20 * (-x / 90) ** 2 + 25,
            'hot': lambda x: -42.5 * (x / 90) ** 2 + 25 if x >=0 else -20 * (-x / 90) ** 4 + 25,
            'cold': lambda x: -42.5 * (x / 90) ** 1.5 + 25 if x >=0 else -20 * (-x / 90) ** 1 + 25,
            'cont': lambda x: -63 * (x / 90) ** 2 + 25 if x >=0 else -56.5 * (-x / 90) ** 2 + 25,
            'cont+': lambda x: -63 * (x / 90) ** 2 + 25 if x >=0 else -20 * (-x / 90) ** 2 + 25,
        }[influences[season].get(h, 'normal')](lat) * 9/5 + 32
        temperature[season][h] = round(t, 1)
    box_blur(temperature[season], g=3, r=3)
    save_data(temperature, basefile + r'\temperature.json')


def temperature_lapse(season):

    for h in temperature[season]:
        temperature[season][h] = round(temperature[season][h] - elevation[h]['altitude'] * 3.5 / 1000, 2)
    save_data(temperature, basefile + r'\temperature.json')


def pet(season):

    for h in temperature[season]:
        climate.setdefault(h, dict())
        climate[h].setdefault('pet', dict())
        L = 12
        N = 30
        T = max(0, (temperature[season][h] - 32) * 5 / 9)
        I = sum([(max(0, ((t - 32) * 5 / 9)) / 5) ** 1.514 for t in (temperature['Jan'][h], temperature['Jul'][h])])
        if I == 0:
            climate[h]['pet'][season] = 0
            continue
        a = 6.75e-7 * I ** 3 - 7.71e-5 * I ** 2 + 1.792e-2 * I + 0.49239
        climate[h]['pet'][season] = round(16 * L / 12 * N / 30 * (10 * T / I) ** a, 1)
    save_data(climate, basefile + r'\climate.json')


def temperature_png(season='Jul'):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in temperature[season] if hexdict[h][0][1] >= hexdict['207-518W'][0][1]], key=lambda h: temperature[season].get(h, -100))
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    # max_c = max(current_sst(h, season) for h in t)
    col = lambda h: colorTemp(temperature[season][h], k=(-25, 100), col1='dddddd')
    c = np.array([colors.hex2color(f"#{col(h)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Temperature\{season}.png", dpi=150, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def pet_png(season='Jul'):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59],
         [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in climate if hexdict[h][0][1] >= hexdict['207-518W'][0][1]],
               key=lambda h: climate[h]['pet'][season])
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    # max_c = max(current_sst(h, season) for h in t)
    col = lambda h: screen('0000ff', 255 - min(255, climate[h]['pet'][season]))
    c = np.array([colors.hex2color(f"#{col(h)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Temperature\{season}-pet.png", dpi=150, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()