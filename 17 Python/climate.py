from defs import *

load_data(elevation, fr"{basefile}\elevation.json")
load_data(climate, fr"{basefile}\climate.json")
load_data(rain, fr"{basefile}\rain.json")
load_data(temperature, fr"{basefile}\temperature.json")


def holdridge():

    for h in climate:
        pavg = statistics.mean([rain[season].get(h, 0) for season in ('Jan', 'Jul')])
        pmin = min([rain[season].get(h, 0) for season in ('Jan', 'Jul')])
        pmax = max([rain[season].get(h, 0) for season in ('Jan', 'Jul')])
        tmin = (min(temperature[season][h] for season in ('Jan', 'Jul')) - 32) * 5 / 9
        tmax = (max(temperature[season][h] for season in ('Jan', 'Jul')) - 32) * 5 / 9
        tann = (statistics.mean([temperature[season][h] for season in ('Jan', 'Jul')]) - 32) * 5 / 9
        # i = 4 if climate[h]['temperature']['Jul'] > climate[h]['temperature']['Jan'] else 10
        # i = int(7 / (1 + math.exp(-(hexes[h]['latitude']) / 15))) - 3
        it = 4 if temperature['Jan'][h] == tmin else 10
        ip = 4 if rain['Jan'].get(h, 0) == pmin else 10
        ipet = 4 if climate[h]['pet']['Jan'] == min(climate[h]['pet'][s] for s in climate[h]['pet']) else 10
        pann = sum(pavg + (pmax - pmin) / 2 * math.sin(2 * math.pi/ 12 * (m - ip)) if
                   tann + (tmax - tmin) / 2 * math.sin(2 * math.pi/ 12 * (m - it)) > 0 else 0 for m in range(1, 13))
        try:
            petr = (sum([statistics.mean([climate[h]['pet'][s] for s in climate[h]['pet']]) +
                         (max([climate[h]['pet'][s] for s in climate[h]['pet']]) -
                          min([climate[h]['pet'][s] for s in climate[h]['pet']])) / 2 *
                         math.sin(2 * math.pi/ 12 * (m - ipet)) for m in range(1, 13)]) / pann)
            climate[h]['aridity'] = round(1 / petr, 2)
        except ZeroDivisionError:
            petr = 0
            climate[h]['aridity'] = 0
        precip = pann
        biotemp = sum(t if 0 < t else (30 if t > 30 else 0) for t in (tann + (tmax - tmin) / 2 * math.sin(2 * math.pi/ 12 * (m - it)) for m in range(1, 13)))
        climate[h]['biotemp'] = round(biotemp, 2)
        if petr < 0.25:
            if precip < 500:
                hold = 'superhumid polar desert'
            elif precip < 1000:
                hold = 'superhumid subpolar rain tundra'
            elif precip < 2000:
                hold = 'superhumid boreal rain forest'
            elif precip < 4000:
                hold = 'superhumid cool temperate rain forest'
            elif precip < 8000:
                hold = 'superhumid subtropical rain forest'
            else:
                hold = 'superhumid tropical rain forest'
        elif petr < 0.5:
            if precip < 250:
                hold = 'perhumid polar desert'
            elif precip < 500:
                hold = 'perhumid subpolar wet tundra'
            elif precip < 1000:
                hold = 'perhumid boreal wet forest'
            elif precip < 2000:
                hold = 'perhumid cool temperate wet forest'
            elif precip < 4000:
                hold = 'perhumid subtropical wet forest'
            else:
                hold = 'perhumid tropical wet forest'
        elif petr < 1:
            if precip < 125:
                hold = 'humid polar desert'
            elif precip < 250:
                hold = 'humid subpolar moist tundra'
            elif precip < 500:
                hold = 'humid boreal moist forest'
            elif precip < 1000:
                hold = 'humid cool temperate moist forest'
            elif precip < 2000:
                hold = 'humid subtropical moist forest'
            else:
                hold = 'humid tropical moist forest'
        elif petr < 2:
            if precip < 125:
                hold = 'subhumid subpolar dry tundra'
            elif precip < 250:
                hold = 'subhumid boreal dry scrub'
            elif precip < 500:
                hold = 'subhumid cool temperate steppe'
            elif precip < 1000:
                hold = 'subhumid subtropical dry forest'
            else:
                hold = 'subhumid tropical dry forest'
        elif petr < 4:
            if precip < 125:
                hold = 'semiarid boreal desert'
            elif precip < 250:
                hold = 'semiarid cool temperate desert scrub'
            elif precip < 500:
                hold = 'semiarid subtropical thorn steppe'
            else:
                hold = 'semiarid tropical very dry forest'
        elif petr < 8:
            if precip < 125:
                hold = 'arid cool temperate desert'
            elif precip < 250:
                hold = 'arid subtropical desert scrub'
            else:
                hold = 'arid tropical thorn woodland'
        elif petr < 16:
            if precip < 125:
                hold = 'perarid subtropical desert'
            else:
                hold = 'perarid tropical desert scrub'
        else:
            hold = 'superarid tropical desert'
        #
        # if biotemp < 1.5:
        #     if precip < 125:
        #         hold = 'superhumid polar desert'
        #     elif precip < 250:
        #         hold = 'perhumid polar desert'
        #     else:
        #         hold = 'humid polar desert'
        # elif biotemp < 3:
        #     if precip < 125:
        #         hold = 'subhumid subpolar dry tundra'
        #     elif precip < 250:
        #         hold = 'humid subpolar moist tundra'
        #     elif precip < 500:
        #         hold = 'perhumid subpolar wet tundra'
        #     else:
        #         hold = 'superhumid subpolar rain tundra'
        # elif biotemp < 6:
        #     if precip < 125:
        #         hold = 'semiarid boreal desert'
        #     elif precip < 250:
        #         hold = 'subhumid boreal dry scrub'
        #     elif precip < 500:
        #         hold = 'humid boreal moist forest'
        #     elif precip < 1000:
        #         hold = 'perhumid boreal wet forest'
        #     else:
        #         hold = 'superhumid boreal rain forest'
        # elif biotemp < 12:
        #     if precip < 125:
        #         hold = 'arid cool temperate desert'
        #     elif precip < 250:
        #         hold = 'semiarid cool temperate desert scrub'
        #     elif precip < 500:
        #         hold = 'subhumid cool temperate steppe'
        #     elif precip < 1000:
        #         hold = 'humid cool temperate moist forest'
        #     elif precip < 2000:
        #         hold = 'perhumid cool temperate wet forest'
        #     else:
        #         hold = 'superhumid cool temperate rain forest'
        # elif biotemp < 24:
        #     if precip < 125:
        #         hold = 'perarid subtropical desert'
        #     elif precip < 250:
        #         hold = 'arid subtropical desert scrub'
        #     elif precip < 500:
        #         hold = 'semiarid subtropical thorn steppe'
        #     elif precip < 1000:
        #         hold = 'subhumid subtropical dry forest'
        #     elif precip < 2000:
        #         hold = 'humid subtropical moist forest'
        #     elif precip < 4000:
        #         hold = 'perhumid subtropical wet forest'
        #     else:
        #         hold = 'superhumid subtropical rain forest'
        # else:
        #     if precip < 125:
        #         hold = 'superarid tropical desert'
        #     elif precip < 250:
        #         hold = 'perarid tropical desert scrub'
        #     elif precip < 500:
        #         hold = 'arid tropical thorn woodland'
        #     elif precip < 1000:
        #         hold = 'semiarid tropical very dry forest'
        #     elif precip < 2000:
        #         hold = 'subhumid tropical dry forest'
        #     elif precip < 4000:
        #         hold = 'humid tropical moist forest'
        #     elif precip < 8000:
        #         hold = 'perhumid tropical wet forest'
        #     else:
        #         hold = 'superhumid tropical rain forest'

        climate[h]['holdridge'] = hold
        climate[h]['annual rain'] = pann
    save_data(climate, fr"{basefile}\climate.json")


def holdridge_png():

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in climate if hexdict[h][0][1] >= hexdict['207-518W'][0][1]],
               key=lambda h: climate[h]['holdridge'])
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    c = np.array([colors.hex2color(f"#{hold2color[climate[h]['holdridge']][1:]}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Climate\Holdridge.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()


def koppen():

    for h in climate:
        #  Pw ≥ ⅔ Pann then Pth = (2 Tann), else if Ps ≥ ⅔ Pann then Pth = 2 Tann + 28 °C, else Pth = 2 Tann + 14 °C
        k = ''
        # i = int(7 / (1 + math.exp(-(hexes[h]['latitude']) / 15))) - 3
        year = range(1, 13)
        it = 4 if temperature['Jan'][h] == min([temperature[season][h] for season in ('Jan', 'Jul')]) else 10
        ip = 4 if rain['Jan'].get(h, 0) == min([rain[season].get(h, 0) for season in ('Jan', 'Jul')]) else 10
        temps = [round((temperature[season][h] - 32) * 5 / 9, 2) for season in ('Jan', 'Jul')]
        temps_year = [round(statistics.mean(temps) + (max(temps) - min(temps)) / 2 * math.sin(2 * math.pi/ 12 * (m - it)), 2) for m in year]
        rain_i = [rain[season].get(h, 0) for season in ('Jan', 'Jul')]
        rain_year = [round(statistics.mean(rain_i) + (max(rain_i) - min(rain_i)) / 2 * math.sin(2 * math.pi/ 12 * (m - ip)), 2) if temps_year[m - 1] > 0 else 0 for m in year]
        tmonb = len([t for t in temps_year if t >= 10])
        tmoncd = len([t for t in temps_year if t < 10])

        # climate[h]['precipitation_annual'] = round(sum(rain_year), 2)

        if 'S' not in h:
            summer = list(range(3, 9))
            winter = list(range(1, 3)) + list(range(9, 13))
        else:
            winter = list(range(3, 9))
            summer = list(range(1, 3)) + list(range(9, 13))

        rain_summer = [rain_year[m - 1] for m in summer]
        rain_winter = [rain_year[m - 1] for m in winter]

        if sum(rain_winter) >= 2/3 * sum(rain_year):
            pth = 2 * statistics.mean(temps_year)
        elif sum(rain_summer) >= 2/3 * sum(rain_year):
            pth = 2 * statistics.mean(temps_year) + 28
        else:
            pth = 2 * statistics.mean(temps_year) + 14

        if max(temps_year) < 10:
            k += ('E')
            if max(temps_year) >= 0:
                k += 'T'
            elif max(temps_year) < 0:
                k += 'F'
        elif sum(rain_year) < 10 * pth:
            k += ('B')
            if sum(rain_year) > 5 * pth:
                k += 'S'
            elif sum(rain_year) <= 5 * pth:
                k += 'W'
        elif min(temps_year) >= 18:
            k += ('A')
            if min(rain_year) >= 60:
                k += 'f'
            elif sum(rain_year) >= (25 * (100 - min(rain_year))):
                k += 'm'
            elif min(rain_year) in rain_summer:
                k += 's'
            elif min(rain_year) in rain_winter:
                k += 'w'
        elif -3 < min(temps_year) < 18:
            k += ('C')
            if min(rain_summer) < min(rain_winter) and max(rain_winter) > 3 * min(rain_summer) and min(rain_summer) < 40:
                k += 's'
            elif max(rain_summer) > 10 * min(rain_winter) and min(rain_winter) < min(rain_summer):
                k += 'w'
            else:
                k += 'f'
        elif min(temps_year) <= -3:
            k += ('D')
            if min(rain_summer) < min(rain_winter) and max(rain_winter) > 3 * min(rain_summer) and min(rain_summer) < 40:
                k += 's'
            elif max(rain_summer) > 10 * min(rain_winter) and min(rain_winter) < min(rain_summer):
                k += 'w'
            else:
                k += 'f'

        if statistics.mean(temps_year) >= 18 and k[0] in 'B':
            k += 'h'
        elif statistics.mean(temps_year) < 18 and k[0] in 'B':
            k += 'k'
        elif max(temps_year) >= 22 and k[0] in 'CD':
            k += 'a'
        elif max(temps_year) < 22 and tmonb > 4 and k[0] in 'CD':
            k += 'b'
        elif max(temps_year) < 22 and tmoncd > 4 and min(temps_year) > -38 and k[0] in 'CD':
            k += 'c'
        elif max(temps_year) < 22 and tmoncd > 4 and min(temps_year) <= -38 and k[0] in 'CD':
            k += 'd'

        if k == 'Cf':
            k = 'Cfa'

        climate[h]['koppen'] = k
        climate[h]['annual rain'] = int(round(sum(rain_year), 0))
    save_data(climate, fr"{basefile}\climate.json")


def koppen_png():

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in climate if hexdict[h][0][1] >= hexdict['207-518W'][0][1]],
               key=lambda h: climate[h]['holdridge'])
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    c = np.array([colors.hex2color(f"#{koppen2color[climate[h]['koppen']][1:]}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Climate\Koppen.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()
