from bs4 import BeautifulSoup as bs
from datetime import *
import os, re
from math import floor, ceil
# from mapping import timer, split_path, parse_id
# from mapping import latlong, north_pole, view_box, parse_coordinate_list

origin = (17846.63, 31066.21)
north_pole = (17846.63, 46591.22)
south_pole = (17846.64, 15541.25)
# TODO whats the equator?
view_box = (35693.257, 69168.8)
# equator = (26810.01, 31066.21)

def timer(state):
    """
    """

    global start, stop

    if state != 0 and state != 1:
        # log('Timer is broken')
        return

    if not state:
        start = datetime.now()
    else:
        stop = datetime.now()
        # print(str((stop - start).seconds) + " s")
        # log(str((stop - start).seconds) + " s")
        return (stop - start).seconds

def split_path(p, f):

    if 'Z M' not in p['d']:
        return p
    cleaned = clean_path(p['d'])
    new_paths = ' M'.join(cleaned.split('Z M')).split(' M')
    completed = []
    for n in new_paths:
        if n == new_paths[0]:
            completed.append(n + ('Z' if n[-1] == ' ' else ' Z'))
        elif n == new_paths[-1]:
            completed.append('M' + n)
        else:
            completed.append('M' + n + ('Z' if n[-1] == ' ' else ' Z'))
    return [
        f.new_tag('path', d = c, **{'style': p.get('style')}) for c in completed
    ]

def latlong(p, id = True, terr = False, sector = False):
    """

    :param p:
    :return: ID, which is the map coordinate of the center of the hex
    """

    # LONGITUDE
    x = round((p[0] - north_pole[0]) / 86.6, 1)
    if x % 1 == 0.5:
        x -= 0.5
        x += 1
    else:
        pass

    x = int(x)

    xdir = ''

    if x > 0:
        xdir = 'E'
    elif x < 0:
        xdir = 'W'
    else:
        pass

    # LATITUDE
    y = round((north_pole[1] - view_box[1] + p[1] + 50) / 75)

    ydir = ''
    if y < 207:
        ydir = 'N'
    elif y > 207:
        ydir = 'S'
    else:
        pass

    s = ''

    # assign regions and perform coordinate transformations
    if terr:
        if y < 0:
            if y / 2 - (1 - y % 2) / 2 > x:
                # a = x
                # x = abs(a)
                # x0 =
                b = abs(x) - y - floor(abs(y) / 2)
                a = 3 * b - floor(b / 2) - (b - abs(y))
                x, y = abs(a), abs(b)
                s = 1
                # print('{:}, {:}: OOB, 1'.format(y, x))
            elif - y / 2 - (1 - y % 2) / 2 < x:
                x0 = ceil(abs(y) / 2 - (1 - y % 2) / 2)
                b = abs(y) + (x - x0)
                a = ceil(2 * b - b / 2) + x0 - (x - b)
                x, y = int(abs(a)), int(abs(b))
                s = 3
                # print('{:}, {:}: OOB, 3'.format(y, x))
            else:
                b = abs(y)
                a = 3 * b + (b % 2) - abs(x) - (x <= 0) * (b % 2)
                x, y = abs(a), abs(b)
                if b % 2 and a == 3 * b and xdir == '':
                    xdir = 'W'
                s = 2
                # print('{:}, {:}: OOB, 2'.format(y, x))
        elif y <= 207:
            if y / 2 - (1 - y % 2) / 2 < x:
                x0 = ceil(y / 2 - (1 - y % 2) / 2)
                b = y + (x - x0)
                a = x + floor((x - x0) / 2) + (b % 2) * (x - x0) % 2
                x, y = int(abs(a)), int(abs(b))
                s = 6
                # print('{:}, {:}: OOB, 6'.format(y, x))
            elif -2 * x - (1 - y % 2) > y:
                x0 = ceil(y / 2 - y % 2 / 2)
                b = y - (x + x0)
                y = b
                a = x + floor((x + x0) / 2) + (b % 2) * (x - x0) % 2
                a -= (b % 2)
                x, y = int(abs(a)), int(abs(b))
                s = 4
                # print('{:}, {:}: OOB, 4'.format(y, x))
            else:
                b = y
                a = x - (y % 2) * (x <= 0)
                if x <= 0 and y % 2:
                    xdir = 'W'
                x, y = abs(a), abs(b)
                s = 5
                # print('{:}, {:}: OOB, 5'.format(y, x))
        elif 207 < y <= 414:
            if y > 2 * x + 414 - (y % 2):
                b = abs(x) + (414 - y) - floor((414 - y) / 2)
                a = ceil(b / 2) + abs(x) - abs(floor((y - 414 - (y % 2)) / 2)) + (y % 2)
                x, y = abs(a), abs(b)
                s = 7
                # print('{:}, {:}: OOB, 7'.format(y, x))
            elif y > -2 * x + 415 - (1 - y % 2):
                b = abs(x) + (414 - y) - floor((414 - y) / 2) - (y % 2)
                a = ceil(b / 2) + abs(x) - abs(floor((y - 414 - (y % 2)) / 2))
                x, y = abs(a), abs(b)
                s = 9
                # print('{:}, {:}: OOB, 9'.format(y, x))
            else:
                y = 414 - y
                x = abs(x) + (x <= 0) * (y % 2)
                if (y % 2) and xdir == '':
                    xdir = 'W'
                s = 8
                # print('{:}, {:}: OOB, 8'.format(y, x))
        elif y > 414:
            if y < -2 * x + 414 + (y % 2):
                b = y - 414 - x - ceil((y - 414) / 2 - (y % 2) / 2)
                a = 2 * b + ceil(b / 2) + x + ceil((y - 414) / 2 - (y % 2) / 2)
                x, y = abs(a), abs(b)
                s = 10
                # print('{:}, {:}: OOB, 10'.format(y, x))
            elif y > 2 * x + 414 - (y % 2):
                b = y - 414
                a = 3 * b + (b % 2) - abs(x) - (x <= 0) * (b % 2)
                if b % 2 and a == 3 * b and xdir == '':
                    xdir = 'W'
                x, y = abs(a), abs(b)
                s = 11
                # print('{:}, {:}: OOB, 11'.format(y, x))
            else:
                b = y - 414 + x - ceil((y - 414) / 2 - (1 - y % 2) / 2)
                a = ceil(2 * b - b / 2) + ceil((y - 414) / 2 - (1 - y % 2) / 2) - (x - b)
                x, y = abs(a), abs(b)
                s = 12
                # print('{:}, {:}: OOB, 12'.format(y, x))
        if y == 207:
            ydir = ''
    if sector:
        return s
    if id:
        return '{:}{:}-{:}{:}'.format(y, ydir, x, xdir)

def parse_coordinate_list(path):
    """
    converts absolute d-string paths into coordinate lists
    :param path:
    :return:
    """

    # id = path['id']
    path = path['d']
    path = clean_path(path).split(' ')
    parsed = [eval(pt) for pt in path if pt not in ['', 'M', 'Z']]
    return parsed
    # path = re.sub('([A-Za-z])(\d)', '\g<1> \g<2>', path)
    # path = re.sub('(\d)([A-Za-z])', '\g<1> \g<2>', path)
    parsed = []

def clean_path(path):

    path = re.sub('V (\d+\.?\d*) (\d+\.?\d*)', 'V \g<2>', path)
    path = re.sub('H (\d+\.?\d*) (\d+\.?\d*)', 'H \g<2>', path)
    path = re.sub('(\d+\.?\d*),(\d+\.?\d*) V (\d+\.?\d*) H (\d+\.?\d*)', '\g<1>,\g<2> \g<1>,\g<3> \g<4>,\g<3>', path)
    path = re.sub('(\d+\.?\d*),(\d+\.?\d*) H (\d+\.?\d*)', '\g<1>,\g<2> \g<3>,\g<2>', path)
    path = re.sub('(\d+\.?\d*),(\d+\.?\d*) V (\d+\.?\d*)', '\g<1>,\g<2> \g<1>,\g<3>', path)
    path = re.sub('L', '', path)
    path = re.sub(' +', ' ', path)
    if path[-1] == ' ':
        path = path[:-1]

    return path


# timer(0)
equator = (26810.01, 31066.21)
rawfile = r'D:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps\Raw Hexes.svg'
# rawmap = bs(open(rawfile, 'r'), 'lxml')
os.system('copy "' + rawfile + '" "' + rawfile[:-4] + '.copy"')
hexes = []

# i = 0
# for p in pressuremap.find_all('path', **{'style': 'fill:#ff7f00'}):
#     for q in pressuremap.find_all('path', **{'style': 'fill:#ff7f00'}):
#         try:
#             if p['d'] == q['d']:
#                 q.decompose()
#                 i += 1
#         except TypeError:
#             pass
# else:
#     print(i)
# for p in rawmap.find('g', id = 'Pressure-Jul').find_all('path'):#, **{'style': 'fill:#ff7f00'}):
#     hexes += split_path(p, rawmap)
#     # hexes.append(p)
# print(len(hexes))
# # for p in hexes:
# #     del(p['id'])
# print(hexes[0])
# for p in hexes:
#     p['d'] = p['d'].replace('Z Z Z Z', 'Z')
# for p in hexes:
#     p['d'] = p['d'].replace('Z Z', 'Z')
#
# hexes = [p['d'].replace('Z Z Z Z', 'Z').replace('Z Z', 'Z') for p in hexes]
#
# hexes = list(set(hexes))
#
# print(hexes[0])
#
# print(len(hexes))
#
# for p in rawmap.find('g', id = 'Pressure-Jul').find_all('path'):#, **{'style': 'fill:#ff7f00'}):
#     p.decompose()

# i = 0
# for p in hexes:
#     # center = eval(p.split(' ')[1])[1] + 50
#     # lat = int(parse_id(latlong(eval(p.split(' ')[1]), terr = True))[1]) * -90 / 207 + 90
#     # if lat == 0:
#     #     i += 1
#     # else:
#         # print(i)
#     rawmap.find('g', id = 'Pressure-Jul').append(rawmap.new_tag('path', **{'d': p, 'style': 'fill:#ff7f00'}))
# else:
#     print(i)
# with open(rawfile[:-4] + '.svg', 'wb') as f:
#     f.write(rawmap.prettify('utf-8'))

timer(0)
rawmap = bs(open(rawfile, 'r'), 'html.parser')
hexes = []
for p in rawmap.find('g', id = 'Pressure-Jul').find_all('path'):
    hexes.append(p)
hex_points_file = r'D:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps\hex_points.dict'
hexdict = {}
for h in hexes:
    hexdict.update({
        latlong(eval(h['d'].split(' ')[1]), terr = True): parse_coordinate_list(h)
    })
# print(hexdict['206S-231E'])
import pickle
pickle.dump(hexdict, open(hex_points_file, 'wb'))
print('{:} s'.format(timer(1)))

timer(1)