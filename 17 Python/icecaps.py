from defs import *

perm_ice = set()
load_data(perm_ice, ice_file)
temp_ice = set()
load_data(temp_ice, icet_file)


def ice_reset(perm_ice=perm_ice, temp_ice=temp_ice):

    perm_ice.clear()

    temp_ice.clear()


def ice_get(reset=True, freeze=28, max_t=110, min_tw=26, min_ts=0, perm_ice=perm_ice, temp_ice=temp_ice):

    if reset:
        ice_reset(perm_ice=perm_ice, temp_ice=temp_ice)

    ice = {'Jan': set(), 'Jul': set()}

    for season in ice:

        for h in hexdict:

            if h in hexes:
                continue

            p = parseID(h)
            lat = copysign(round(abs(p[1]) * -90 / 207 + 90, 2), p[1])
            lat *= 1 if season == 'Jul' else -1
            min_t = min_tw if lat > 0 else min_ts
            t = (max_t - min_t) / 2 * cos(radians(2 * lat)) + (max_t + min_t) / 2

            if t <= freeze:
                ice[season].add(h)

    for i in ice['Jan'] | ice['Jul']:
        if i in ice['Jan'] and i in ice['Jul']:
            perm_ice.add(i)
    for i in ice['Jan'] | ice['Jul']:
        if (i in ice['Jan']) ^ (i in ice['Jul']):
            temp_ice.add(i)


def ice_save(perm_ice=perm_ice, temp_ice=temp_ice):

    with open(ice_file, 'wb') as f:
        pickle.dump(perm_ice, f)
    with open(icet_file, 'wb') as f:
        pickle.dump(temp_ice, f)


def ice_map(perm_ice=perm_ice, temp_ice=temp_ice):

    with open(basefile + r'\Ice.svg', 'r') as f:
        file = bs(f, 'lxml')
    string = ''
    col = 'd1f2ff'
    for h in perm_ice:
        string += '<path d="M {:} Z" class="hex" id="{:}" style="color:#{:};opacity:0.5" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in hexdict[h]]),
            h,
            col
        )
    for h in temp_ice:
        if h in perm_ice:
            continue
        string += '<path d="M {:} Z" class="hex" id="{:}" style="color:#{:};opacity:0.25" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in hexdict[h]]),
            h,
            col
        )

    if not string:
        return
    file.find('g', id='ice').contents = bs(string, 'lxml').find('body').contents
    save(file, extra_file=basefile + r'\Ice.svg')