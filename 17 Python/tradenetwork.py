from defs import *

load_data(climate, climate_file)
load_data(terrain, terrain_file)

routes_file = tradefilepath + r'\routes.p'

routes = set()
load_data(routes, routes_file)


def roads_to(city):

    current = {c for c in hexes[city].get('roads', set()) | hexes[city].get('tracks', set())}
    visited = set()

    while current:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            if c in cities:
                current.remove(c)
                visited.add(c)
                continue
            current |= {n for n in hexes[c].get('roads', set()) | hexes[c].get('tracks', set())}
            visited.add(c)
            current.remove(c)
        # print(len(visited), len(current))

    return {v for v in visited if v in cities and v != city}


def rivers_from(city):

    if not terrain[city]['drain_out']:
        return {}

    current = {c for c in hexes[city].get('drain_out', set()) if climate[c]['drainage'] > 3}
    visited = set()

    while current:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            if c in cities:
                current.remove(c)
                visited.add(c)
                continue
            current |= {n for n in hexes[c].get('drain_out', set()) if terrain[c]['altitude'] - terrain[n]['altitude'] < 400 and climate[n]['drainage'] > 3}
            visited.add(c)
            current.remove(c)
        # print(len(visited), len(current))

    return {v for v in visited if v in cities and v is not city}


def road_path_existing(start, end, verbose=False):

    open_list = list(hexes[start].get('roads', set())) + list(hexes[start].get('tracks', set()))
    if not open_list:
        return None
    closed_list = [start]

    travel = lambda x, y: 1

    def g(current):

        z = [current]
        while z[-1:][0] != start:
            z.append(came_from[z[-1:][0]])
        z = z[::-1]
        return sum(travel(i, j) for i, j in zip(z[:-1], z[1:]))

    def h(current):

        return hexDistance(current, end)

    def f(current):

        return g(current) + h(current)

    came_from = {o: start for o in open_list}

    while open_list:
        s = sorted(open_list, key=lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        if s == end:
            # print(closed_list)
            path = [end]
            break
        for t in hexes[s].get('roads', set()) | hexes[s].get('tracks', set()):
            # if travel(s, t) > 1e8:
            #     closed_list.append(t)
            #     continue
            if t not in closed_list:
                if t not in open_list or g(t) < h(t):
                    open_list.append(t)
                came_from.update({t: s})
        # print(f(s), closed_list)
        if verbose: print('{:.0f} {:.0f} {:}'.format(f(s), g(s), closed_list))

    path = [end]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return None
    return path[::-1]


def river_length(i, j, upstream=False):

    c = i
    length = 0
    overhead = 1
    relationship = lambda i, j: hegemonies[cities.get(i, dict()).get('hegemony', hexes[i].get('hinterland'))][
        'conflict'].get(cities.get(j, dict()).get('hegemony', hexes[j].get('hinterland')), 0)
    while c != j:
        length += 1/2 * (sqrt(2) if upstream else 1)
        length += overhead
        length += 1e9 if relationship(i, j) > 3 else relationship(i, j)
        c = terrain[c]['drain_out'][0]
    return round(length, 2)


def seaways_to(city):
    pass


def road_length(i, j):

    road = road_path_existing(i, j)
    return road_distance(road)
# TODO roads in the hinterland


def road_distance(road):

    if not road:
        return 1e3
    length = 0
    overhead = 1
    # relationship = lambda i, j: hegemonies[cities.get(i, dict()).get('hegemony', hexes[i].get('hinterland'))]['conflict'].get(cities.get(j, dict()).get('hegemony', hexes[j].get('hinterland')), 0)
    for r, s in zip(road[:-1], road[1:]):
        alt = 0.5 * abs(terrain[s]['altitude'] - terrain[r]['altitude']) / 400
        if terrain[s]['altitude'] > 8000:
            alt += 1 + (terrain[s]['altitude'] - 8000) / 1000
        length += overhead + alt
        length += 1e9 if relationship(r, s) > 3 else relationship(r, s)
    return round(length, 2)


def river_road_distance(road):

    if not road:
        return 1e3
    length = 0
    overhead = 1
    # relationship = lambda i, j: hegemonies[cities.get(i, dict()).get('hegemony', hexes[i].get('hinterland'))]['conflict'].get(cities.get(j, dict()).get('hegemony', hexes[j].get('hinterland')), 0)
    with stopit.ThreadingTimeout(2):
        for r, s in zip(road[:-1], road[1:]):
            if s in terrain[r]['drain_out'] and abs(terrain[s]['altitude'] - terrain[r]['altitude']) < 400:
                length += 1 / 2
            elif r in terrain[s]['drain_out'] and abs(terrain[s]['altitude'] - terrain[r]['altitude']) < 400:
                length += 1 / sqrt(2)
            else:
                alt = 0.5 * abs(terrain[s]['altitude'] - terrain[r]['altitude']) / 400
                if terrain[s]['altitude'] > 8000:
                    alt += 1 + (terrain[s]['altitude'] - 8000) / 1000
                length += overhead + alt
                length += 1e9 if relationship(r, s) > 3 else relationship(r, s)
        return round(length, 2)


def roads_add(h):

    # TODO need to add actual road length since it might not always be next door
    for j in roads_to(h):
        i = 0
        while any(i in r and j in r and h in r for r in routes):
            i += 1
        # print(i , h, j)
        if road_length(h, j) < 1e9:
            routes.add((i, h, j, road_length(h, j)))


def rivers_add(h):

    for j in rivers_from(h):
        i = 0
        while any(i in r and j in r and h in r for r in routes):
            i += 1
        # print(i , h, j)
        if river_length(h, j) < 1e9:
            routes.add((i, h, j, river_length(h, j)))
            i += 1
            routes.add((i, j, h, river_length(h, j, upstream=True)))


def seaways_add(h):

    pass


def network_create(verbose=False, roads=True, rivers=True):

    routes.clear()
    c = 0
    for city in cities:
        c += 1
        if roads:
            roads_add(city)
        if rivers:
            rivers_add(city)
        if verbose: print('{:.2%}'.format(c / len(cities)))
    with open(routes_file, 'wb') as f:
        pickle.dump(routes, f)

