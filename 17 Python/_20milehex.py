# import pickle
# import csv
# from bs4 import BeautifulSoup as bs
# from math import radians, cos, sin, sqrt, floor, atan2, degrees
# from scipy.stats import norm
#
# filepath = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps'
# hexpickle = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps\hexes.p'
# coastpickle = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps\coasts.p'
from defs import *
from textwrap import wrap

basefile20 = basefile + r'\20 Mile Hexes\base.svg'
basemap20 = bs(open(basefile20, 'r'), 'xml')
hex_centers = basefile + r'\20 Mile Hexes\hex_centers.csv'


# global hexes
global subhexes

# class Hex:
#     pass
#
# hexes = pickle.load(open(hexpickle, 'rb'))
# coasts = pickle.load(open(coastpickle, 'rb'))

# hexes = dict()
# with open(demohexes, 'r') as f:
#     reader = csv.reader(f, delimiter=',')
#     next(reader, None)
#     for row in reader:
#         hexes[int(row[0])] = {
#             'altitude': int(row[1] or 0),
#             'neighbors': eval(row[2]),
#             'c': eval(row[3]),
#             'drains_from': eval(row[4]) if row[4] else None,
#             'drains_to': eval(row[5]) if row[5] else None,
#         }


# def topography(hexes=hexes):
#
#     for h in hexes:
#         if not hexes[h]['altitude']:
#             continue
#         if not hexes[h]['neighbors']:
#             continue
#         topo = []
#         plains = 0
#         hills = 0
#         highlands = 0
#         mountains = 0
#         valleys = 0
#         depressions = 0
#         rolling = 1
#         a = hexes[h]['altitude']
#         # if type(a) == str:
#         # print(hexes[h]['id'])
#         # continue
#         for n in (n for n in hexes[h]['neighbors'] if n in hexes and hexes[n]['altitude']):
#             b = hexes[n]['altitude']
#             # if type(b) == str:
#             # print(n.id)
#             # continue
#             if abs(a - b) <= 200:
#                 plains += 1
#             if 1200 >= abs(a - b) >= 200:
#                 hills += 1
#             if a - b >= 800:
#                 highlands += 1
#             if abs(a - b) >= 1200:
#                 mountains += 1
#             if a - b <= 200:
#                 valleys += 1
#             if b > a:
#                 depressions += 1
#         if plains >= 3:
#             topo.append('plains')
#             rolling = 0
#         if hills >= 3:
#             topo.append('hills')
#             rolling = 0
#         if highlands >= 3:
#             topo.append('highland')
#             rolling = 0
#         if mountains >= 3:
#             topo.append('mountain')
#             rolling = 0
#         if 5 >= valleys >= 4:
#             topo.append('valley')
#             rolling = 0
#         if depressions == len(hexes[h]['neighbors']):
#             topo.append('depression')
#             rolling = 0
#         if rolling:
#             topo.append('rolling')
#         hexes[h]['topography'] = ' '.join(topo)
#
#
# topography()


topo = {
    'mountain': 1400,
    'hills': 400,
    'plains': 200,
    'highland': 800,
    'depression': 200,
    'valley': 200,
    'rolling': 300
}

hex_group = dict(zip(range(1, 9), list(range(0, 8))[::-1]))

wild_type = dict(zip(range(7), ['reserve', 'sylvan', 'bush', 'backcountry', 'hinterland', 'boondocks', 'wild']))


def hex_corner(center, i, size = 50):
    angle = radians(60 * i + 30)
    return (
        round(center[0] + size * cos(angle), 3),
        round(center[1] + size * sin(angle), 3)
    )


def distance_sub(a, b):

    """

    :param h1: point 1
    :param h2: point 2
    :return: the distance between the two points
    """

    return round(hypot(
        subhexes[a]['c'][1] - subhexes[b]['c'][1],
        subhexes[a]['c'][0] - subhexes[b]['c'][0]
    ), 2)


def wilderness(k, target, verbose=False, rivers=[]):

    if k == 0:
        return {sh: 0 for sh in subhexes}
    j = 1
    i = 0
    n = 0
    m = 400
    big_river = []
    lakes = []
    if rivers:
        if hexes[target]['drain_in']:
            big_river = drainage_path(egress_match[egress_hex(max(hexes[target]['drain_in'], key=lambda h: hexes[h]['drainage']), target)], target)
        else:
            big_river = drainage_path(max(subhexes, key=lambda s: subhexes[s]['z']), target)
        lakes = lake(target, rivers)
    while i != k:
        x = [0] * (m - j) + [1] * j
        random.shuffle(x)
        sh_inf = {sh: inf for sh, inf in zip(subhexes, x)}
        sh_t = {sh: 8 for sh in subhexes}
        i = 0
        for sh in subhexes:
            subhexes[sh]['infra'] = 0
            if not sh_inf[sh]:
                continue
            if sh in lakes:
                continue
            if len(subhexes[sh]['neighbors']) == 6:
                t = 8 - (sh_inf[sh] + sum(sh_inf[n] for n in subhexes[sh]['neighbors']))
                i += hex_group[t]
                sh_t[sh] = t
                subhexes[sh]['infra'] = hex_group[t]
        if verbose: print(j, i)
        n += 1
        if n == 5:
            j += 1
            n = 0
        if i > k:
            j = 1
        if rivers and i == k and max(subhexes, key=lambda s: subhexes[s]['infra']) not in big_river:
            print('Center not on river')
            n = 0
            j = 1
            i = 0
        if i == 2268:
            break
    return sh_inf


def wilderness_encode(x):
    """

    :param x: either a hexadecimal number representing the wilderness layout, or a dictionary containing the infrastructure switches
    :return: either encode the dictionary or decode the number into a dictionary
    """

    if 'x' in x: # decode
        s = sorted(subhexes)
        x = {k: int(v) for k, v in zip(s, f"{int(x, 16):0400b}")}
    else: # encode
        s = sorted(x)
        x = hex(int(''.join([f'{x[i]}' for i in s]).zfill(400), 2))
    return x


subhexes = {}

with open(hex_centers, 'r') as f:
    for h in f.readlines():
        subhexes.update({
            h.split(',')[0].zfill(2) + h.split(',')[1].zfill(2): {
                'c': (
                    round(eval(h.split(',')[2].lower()), 3),
                    eval(h.split(',')[3]),
                )
        }})
# for h in subhexes:
#     subhexes[h]['neighbors'] = [x for x in [
#         str(int(h[:2]) + 0).zfill(2) + str(int(h[2:]) - 1).zfill(2),
#         str(int(h[:2]) + 0).zfill(2) + str(int(h[2:]) + 1).zfill(2),
#         str(int(h[:2]) + 1).zfill(2) + str(int(h[2:]) + 0).zfill(2),
#         str(int(h[:2]) + 1).zfill(2) + (str(int(h[2:]) - 1).zfill(2) if not int(h[:2]) // 2 else str(int(h[2:]) + 1).zfill(2)),
#         str(int(h[:2]) - 1).zfill(2) + (str(int(h[2:]) - 1).zfill(2) if not int(h[:2]) // 2 else str(int(h[2:]) + 1).zfill(2)),
#         str(int(h[:2]) - 1).zfill(2) + str(int(h[2:]) + 0).zfill(2),
#     ] if x in subhexes]
for h in subhexes:
    subhexes[h]['neighbors'] = []
    for i in range(6):
        x = subhexes[h]['c'][0] + 50 * sqrt(3) * cos(radians(60 * i))
        y = subhexes[h]['c'][1] + 50 * sqrt(3) * sin(radians(60 * i))
        for s in subhexes:
            if abs(x - subhexes[s]['c'][0]) < 0.1:
                if abs(y - subhexes[s]['c'][1]) < 0.1:
                    subhexes[h]['neighbors'].append(s)
                    break

egress_match = {
    '0719': '0700', '0919': '0900', '1119': '1100', '1319': '1300', '1519': '1500', '1719': '1700', '0010': '1900',
    '0619': '0600', '0819': '0800', '1019': '1000', '1219': '1200', '1419': '1400', '1619': '1600', '1819': '1800',
    '1918': '0009', '2017': '0107', '2115': '0206', '2214': '0304', '2312': '0403', '2411': '0501', '0011': '1901',
    '0112': '2003', '0214': '2103', '0315': '2206', '0417': '2307', '0518': '2409', '2018': '0108', '2116': '0207',
    '2215': '0305', '2313': '0404', '2412': '0506', '2510': '0601', '2002': '0111', '2103': '0213', '2205': '0314',
    '2306': '0416', '2408': '0517', '2509': '0619', '0900': '0919', '1100': '1119', '1300': '1319', '1500': '1519',
    '1700': '1719', '1900': '0010', '0600': '0619', '0800': '0819', '1000': '1019', '1200': '1219', '1400': '1419',
    '1600': '1619', '1800': '1819', '0009': '1918', '0107': '2017', '0206': '2115', '0304': '2214', '0403': '2312',
    '0501': '2411', '1901': '0011', '2003': '0112', '2103': '0214', '2206': '0315', '2307': '0417', '2409': '0518',
    '0108': '2018', '0207': '2116', '0305': '2215', '0404': '2313', '0506': '2412', '0601': '2510', '0111': '2002',
    '0213': '2103', '0314': '2205', '0416': '2306', '0517': '2408', '0619': '2509'
}

for c in list(egress_match):
    if len(subhexes[c]['neighbors']) == 3:
        continue
    else:
        egress_match[egress_match[c]] = c
        del egress_match[c]

ingress_match = {egress_match[c]: c for c in egress_match}


def idw(s, h, k=2, x=2): # https://www.e-education.psu.edu/geog486/node/1877

    x = 2
    if s == '1210':
        return hexes[h]['altitude']
    w = [(1 / distance_sub(s, '1210')) ** x]
    zw = [hexes[h]['altitude'] / (distance(subhexes[s]['c'], subhexes['1210']['c']) ** x)]
    dists = {
        n: int(distance((
            round(subhexes['1210']['c'][0] + 1000 * sqrt(3) * cos(radians(round(degrees(atan2(
            hexes[n]['c'][1] - hexes[h]['c'][1],
            hexes[n]['c'][0] - hexes[h]['c'][0],
        )), 0))), 3),
            round(subhexes['1210']['c'][1] + 1000 * sqrt(3) * sin(radians(round(degrees(atan2(
            hexes[n]['c'][1] - hexes[h]['c'][1],
            hexes[n]['c'][0] - hexes[h]['c'][0],
        )), 0))), 3),
        ), subhexes[s]['c'])) for n in hexes[h]['neighbors']
    }
    for n in sorted(list(dists), key=lambda i: dists[i])[:k]:
        # only use k-nearest
        d = dists[n]
        zw.append(hexes[n]['altitude'] / (d ** x))
        w.append(1 / (d ** x))
    return floor(sum(zw) / sum(w))


def r2(target, k=2, x=2):

    z = statistics.mean([subhexes[s]['z'] for s in subhexes])
    sstot = sum((subhexes[s]['z'] - z) ** 2 for s in subhexes)
    ssres = sum((subhexes[s]['z'] - idw(s, target, k=k, x=x)) ** 2 for s in subhexes)
    r2 = 1 - ssres / sstot
    return r2


def drainage_path(a, target):

    if hexes[target]['drain_out']:
        egress = egress_hex(target, hexes[target]['drain_out'][0])
    else:
        egress = min(subhexes, key=lambda s: subhexes[s]['z'])
    ingresses = [egress_match[egress_hex(i, target)] for i in hexes[target]['drain_in']]
    path = [a]
    while path[-1] != egress:
        try:
            path.append(subhexes[path[-1]]['drains_to'])
        except KeyError:
            print(path[-1])
            return
    return path


def map_gen(target, update_rivers=True, dev=0.25, jitter=True, update_map=True, k=5, x=2, erode=0, verbose=False, wild=True, update_roads=True, update_tracks=True):

    deviation = 1
    # get the average deviation amount based on topography
    t = statistics.mean([topo.get(i, 0) for i in hexes[target]['topography'].split(' ')]) / 3
    i = 0
    while deviation > dev:
        i += 1
        for s in subhexes:
            # subhexes[s].clear()
            if 'drains_from' in subhexes[s]: subhexes[s]['drains_from'].clear()
            if 'drains_to' in subhexes[s]: del subhexes[s]['drains_to']
        for sh in subhexes:
            subhexes[sh]['z'] = 0
            while subhexes[sh]['z'] <= 0:
                # apply IDW to determine the height
                subhexes[sh]['z'] = round(idw(sh, target, k=k, x=x) + jitter * random.gauss(mu = 0, sigma = t))
        rivers = []
        if update_rivers:
            rivers = river_random(target)
            if not rivers:
                deviation = 1
                if verbose: print(f'{deviation:0.4f}')
                continue
            if rivers:
                # check = False
                # with stopit.ThreadingTimeout(30):
                #     rivers_carve(target, rivers)
                #     check = True
                # if not check:
                #     if verbose: print('rivers took too long')
                #     continue
                river_ensure(target, rivers)
                sub_drainage(target, rivers)
                for _ in range(erode):
                    sub_erode(target, rivers)
                    river_ensure(target, rivers)
                # rivers_carve(target, rivers)
                sub_drainage(target, rivers)
        deviation = abs(1 - (sum(idw(s, target, k=k, x=x) for s in subhexes) / sum(subhexes[s]['z'] for s in subhexes)) ** 2)
        if verbose: print(f'{deviation:0.4f}')
        while deviation > dev:
            for s in subhexes:
                if s in sum(rivers, []):
                    continue
                i = idw(s, target, k=k, x=x)
                subhexes[s]['z'] = round(subhexes[s]['z'] - 0.1 * (subhexes[s]['z'] - i))
            deviation = abs(1 - (sum(idw(s, target, k=k, x=x) for s in subhexes) / sum(
                subhexes[s]['z'] for s in subhexes)) ** 2)
            if verbose: print(f'{deviation:0.4f}')
        if update_rivers:
            sub_drainage(target, rivers)
        # if verbose: print(deviation)
        # if i == 10: # if it is taking too long, make the acceptable error higher
        #     i = 0
        #     dev += 0.25
        #     print(f'new target deviation: {dev:0.4f}')
    if update_map:
        map_write(target, update_rivers=update_rivers)
    if wild:
        wilderness_subs_draw(target, hexes[target]['infrastructure'], rivers=rivers, verbose=verbose)
    if rivers:
        print(f'Rivers: {rivers_encode(rivers)}')
    if update_roads:
        roads = roads_generate(target, rivers=rivers, verbose=verbose)
        if verbose: print(roads)
        print(f'Roads: {rivers_encode(roads)}')
        if update_map:
            roads_draw(target, roads)
    if update_tracks:
        tracks = tracks_generate(target, roads, rivers=rivers, verbose=verbose)
        print(f'Tracks: {rivers_encode(tracks)}')
        if update_map:
            tracks_draw(target, tracks, roads)


def map_write(target, update_rivers=True, river_mark=False, scale=1):
    """
    Saves target svg in new file (or overwrites existing file)
    :param target:
    :param update_rivers:
    :param river_mark: places a colored hex at the egress and ingress hexes; helpful for troubleshooting
    :param scale: make the rivers larger if necessary
    :return:
    """

    targetfile = basefile + '\\20 Mile Hexes\\' + target + '.svg'
    with open(targetfile if os.path.isfile(targetfile) else basefile20, 'r') as f:
        targetmap = bs(f, 'xml')
    targetmap.find('g', id='altitude').clear()
    targetmap.find('g', id='subhexes').clear()
    string = ''
    for h in subhexes:
        col = screen('000000', subhexes[h]['z'] // 100)
        if river_mark and update_rivers and h == egress_hex(target, hexes[target]['drain_out'][0] if hexes[target]['drain_out'] else min(subhexes, key=lambda s: subhexes[s]['z'])):
            col = 'f00'
        if river_mark and update_rivers and h in [egress_match[egress_hex(i, target)] for i in hexes[target]['drain_in']]:
            col = '00f'
        string += f"<path " \
            f"d='{'M ' + ' '.join(['{:},{:}'.format(hex_corner(subhexes[h]['c'], i)[0], hex_corner(subhexes[h]['c'], i)[1]) for i in [4, 5, 0, 1, 2, 3]]) + ' Z '}'" \
            f" style='fill:#{col}' id='{h}-z' />"
    targetmap.find('g', id='altitude').contents = bs(string, 'lxml').find('body').contents
    if update_rivers:
        string = ''
        # td = max(subhexes[d]['drainage'] for d in subhexes)
        td = max(d['drainage'] for d in hexes.values())
        def width(r): return max(0.3, - ((12 - 1) * log(subhexes[r].get('drainage', 0) + 1) + log(td) - 12 * log(3)) / (log(3) - log(td)))
        for r in subhexes:
            if 'drains_to' not in subhexes[r]:
                continue
            s = subhexes[r]['drains_to']
            string += f"<path d='{'M {:},{:}'.format(*subhexes[r]['c']) + '  {:},{:}'.format(*subhexes[s]['c'])}' " \
                f"style='stroke-width:{scale * width(r):0.3f}' class='river' />"
        if hexes[target]['drain_out']:
            r = egress_hex(target, hexes[target]['drain_out'][0])
            egress_angle = (round(degrees(atan2(
                hexes[hexes[target]['drain_out'][0]]['c'][1] - hexes[target]['c'][1],
                hexes[hexes[target]['drain_out'][0]]['c'][0] - hexes[target]['c'][0],
            )), 0)) % 360
            string += f"<path d='{'M  {:},{:}'.format(*subhexes[r]['c']) + '  {:.3f},{:.3f}'.format(subhexes[r]['c'][0] + 50 * sqrt(3) * cos(radians(egress_angle)),subhexes[r]['c'][1] + 50 * sqrt(3) * sin(radians(egress_angle)))}' "f"style='stroke-width:{scale * (width(r) + 1):0.3f};' class='river' />"
        targetmap.find('g', id='rivers').contents = bs(string, 'lxml').find('body').contents
    with open(targetfile, 'wb') as f:
        try:
            targetmap.find('body').replace_with_children()
        except AttributeError:
            pass
        try:
            targetmap.find('html').replace_with_children()
        except AttributeError:
            pass
        f.write(targetmap.prettify('utf-8'))


def river_ensure(target, rivers):

    # rivers = river_in_out(target=target)
    sub_drainage(target, rivers)
    for river in rivers:
        z = np.linspace(subhexes[river[0]]['z'], subhexes[river[-1]]['z'], num=len(river))
        for r, hex in enumerate(river):
            if subhexes[hex].get('drains_from', None):
                subhexes[hex]['z'] = min(round(z[r]), subhexes[hex]['z'], subhexes[min(subhexes[hex]['drains_from'], key=lambda x: subhexes[x]['z'])]['z'] - 1)
            else:
                subhexes[hex]['z'] = min(round(z[r]), subhexes[hex]['z'])
    # for river in rivers:
    #     for hex in river:
    #         try:
    #             m = max(subhexes[n]['z'] for n in subhexes[hex]['neighbors'] if n not in sum(rivers, []) and hex not in subhexes[n].get('drains_to', {}))
    #         except ValueError:
    #             continue
    #         # print(hex, m, [(n, subhexes[n]['z']) for n in subhexes[hex]['neighbors']])
    #         for n in subhexes[hex]['neighbors']:
    #             if n in river:
    #                 continue
    #             subhexes[n]['z'] = max(subhexes[n]['z'], m) + 1
    sub_drainage(target, rivers)


def egress_id(a, b, conn='river'):

    i = sum(ord(j) for j in a + b) + (1 if conn == 'road' else 0)
    return i


def egress_hex(a, b, conn='river'):

    # drains_to = list(hexes[target]['drains_to'])[0]
    eid = egress_id(a, b, conn=conn)
    egress_hex = []
    egress_angle = (round(degrees(atan2(
        hexes[b]['c'][1] - hexes[a]['c'][1],
        hexes[b]['c'][0] - hexes[a]['c'][0],
    )), 0)) % 360
    for s in subhexes:
        if s not in egress_match:
            continue
        angle = (round(degrees(atan2(
            subhexes[s]['c'][1] - subhexes['1210']['c'][1],
            subhexes[s]['c'][0] - subhexes['1210']['c'][0],
        )), 0)) % 360
        if egress_angle == roundBase(angle, base=60) % 360:
            # print(s, round_base(angle, base=60) % 360)
            egress_hex.append(s)
    egress_hex = sorted(egress_hex)[eid % len(egress_hex)]
    return egress_hex


def river_check(target):

    pass


def river_continuity(target=None):

    # assuming that there is at least an egress
    egress_hex = []
    ingress_hexes = []

    drains_to = list(hexes[target]['drains_to'])[0]
    egress_angle = (round(degrees(atan2(
            hexes[drains_to]['c'][1] - hexes[target]['c'][1],
            hexes[drains_to]['c'][0] - hexes[target]['c'][0],
        )), 0)) % 360
    if 'river ingress' in hexes[drains_to]:
        for i in hexes[drains_to]['river ingress']:
            pass
    else:
        for s in subhexes:
            if s not in egress_match:
                continue
            angle = (round(degrees(atan2(
                subhexes[s]['c'][1] - subhexes['1210']['c'][1],
                subhexes[s]['c'][0] - subhexes['1210']['c'][0],
            )), 0)) % 360
            if egress_angle == roundBase(angle, base=60) % 360:
                # print(s, round_base(angle, base=60) % 360)
                egress_hex.append(s)
            # print(s, round_base(angle, base=60) % 360)
        egress_hex = random.choice(egress_hex)
        subhexes[egress_hex]['z'] = int(
            statistics.mean([hexes[target]['altitude'], hexes[list(hexes[target]['drains_to'])[0]]['altitude']]))
        if 'drains_to' in subhexes[egress_hex]: subhexes[egress_hex]['drains_to']
        for n in subhexes[egress_hex]['neighbors']:
            if subhexes[n]['z'] < subhexes[egress_hex]['z']:
                subhexes[n]['z'] = subhexes[egress_hex]['z'] + 1

    for drains_from in hexes[target]['drains_from']:
        ingress_angle = 360 - (round(degrees(atan2(
            hexes[drains_from]['c'][1] - hexes[target]['c'][1],
            hexes[drains_from]['c'][0] - hexes[target]['c'][0],
        )), 0)) % 360
        if 'river egress' in hexes[drains_from]:
            pass
        else:
            ingress_hexes_t = []
            for s in subhexes:
                if s not in ingress_match:
                    continue
                angle = 360 - (round(degrees(atan2(
                    subhexes[s]['c'][1] - subhexes['1210']['c'][1],
                    subhexes[s]['c'][0] - subhexes['1210']['c'][0],
                )), 0)) % 360
                if ingress_angle == roundBase(angle, base=60) % 360:
                    # print(s, round_base(angle, base=60) % 360)
                    # if 'drains_to' in subhexes[s]:
                    #     continue
                    ingress_hexes_t.append(s)
            ingress_hexes.append((random.choice(ingress_hexes_t), hexes[target]['drains_from'][drains_from]))
            subhexes[ingress_hexes[-1][0]]['z'] = int(
                statistics.mean([hexes[target]['altitude'], hexes[drains_from]['altitude']]))
            for n in subhexes[ingress_hexes[-1][0]]['neighbors']:
                if subhexes[n]['z'] < subhexes[ingress_hexes[-1][0]]['z']:
                    subhexes[n]['z'] = subhexes[ingress_hexes[-1][0]]['z'] + 1

    if not egress_hex:
        egress_hex = None
    if not ingress_hexes:
        ingress_hexes = None
    return egress_hex, ingress_hexes


def river_in_out(target=None):

    if hexes[target]['drain_out']:
        egress = egress_hex(target, hexes[target]['drain_out'][0])
    else:
        egress = min(subhexes, key=lambda s: subhexes[s]['z'])
    rivers = []
    # maybe to get it to follow preexisting paths, copy the heights, set them all to 0 on each run, then restore z
    ingresses = [egress_match[egress_hex(i, target)] for i in hexes[target]['drain_in']]
    for ingress in ingresses:
        path = a_star_sub(ingress, egress)
        rivers.append(path)
    return rivers


def rivers_carve(target, rivers):

    for river in rivers:
        for a, b in zip(river[:-1], river[1:]):
            subhexes[a]['drains_to'] = b
            subhexes[b]['drains_from'] = subhexes[b].get('drains_from', set()) | {a}
    if hexes[target]['drain_out']:
        egress = egress_hex(target, hexes[target]['drain_out'][0])
    else:
        egress = min(subhexes, key=lambda s: subhexes[s]['z'])
    if hexes[target]['drain_out']:
        subhexes[egress]['z'] = round(round(statistics.mean([hexes[target]['altitude'], hexes[hexes[target]['drain_out'][0]]['altitude']])))
    ingresses = [(i, egress_match[egress_hex(i, target)]) for i in hexes[target]['drain_in']]
    for ingress in ingresses:
        subhexes[ingress[1]]['z'] = int(
            round(statistics.mean([hexes[target]['altitude'], hexes[ingress[0]]['altitude']]))) - 1
    for river in sorted(rivers, key=len, reverse=True):
        if rivers[-1] == egress:
            for i, j in enumerate(river):
                m = round(np.linspace(subhexes[river[0]]['z'], subhexes[river[-1]]['z'], len(river))[i])
                subhexes[j]['z'] = m
        else:
            if subhexes[river[0]]['z'] - subhexes[river[-1]]['z'] >= len(river):
                for i, j in enumerate(river):
                    m = round(np.linspace(subhexes[river[0]]['z'], subhexes[river[-1]]['z'], len(river))[i])
                    subhexes[j]['z'] = m
            else:
                for i, j in enumerate(river + drainage_path(river[-1], target)[1:]):
                    m = round(np.linspace(subhexes[river[0]]['z'], subhexes[river[-1]]['z'],
                                       len(river + drainage_path(river[-1], target)[1:]))[i])
                    subhexes[j]['z'] = m
    # for ingress in sorted(ingresses, key=lambda x: (subhexes[x]['z'] - subhexes[egress]['z']) / len(drainage_path(x, target)), reverse=True):
    #     for i, j in enumerate(drainage_path(ingress, target)):
    #         m = round(linspace(subhexes[ingress]['z'], subhexes[egress]['z'], len(drainage_path(ingress, target)))[i])
    #         subhexes[j]['z'] = m
    i = 0
    while any(
            any(subhexes[n]['z'] < subhexes[r]['z'] for n in subhexes[r]['neighbors'] if n not in sum(rivers, [])) for r
            in sum(rivers, [])):
        i += 1
        pt = sum(distance_sub('1210', s) + 1 for s in subhexes)
        p = [(distance_sub('1210', s) + 1) / pt for s in sorted(subhexes, key=lambda s: distance_sub('1210', s))][::-1]
        shuff = npr.choice(sorted(subhexes, key=lambda s: distance_sub('1210', s)), len(subhexes) // 3, replace=False, p=p)
        for s in shuff:
            if s in sum(rivers, []):
                continue
            for n in subhexes[s]['neighbors']:
                if n in sum(rivers, []):
                    if subhexes[s]['z'] > subhexes[n]['z']:
                        break
            subhexes[s]['z'] += random.randint(1, 5)
            # print(i)
            # if any(any(subhexes[n]['z'] < subhexes[r]['z'] for n in subhexes[r]['neighbors'] if n not in sum(rivers, [])) for r in sum(rivers, [])):
            #     break


def river_random(target, verbose=False):

    rivers = []
    if hexes[target]['drain_out']:
        egress = egress_hex(target, hexes[target]['drain_out'][0])
    else:
        egress = min(subhexes, key=lambda s: subhexes[s]['z'])
    ingresses = [egress_match[egress_hex(i, target)] for i in hexes[target]['drain_in']]
    if not ingresses:
        ingresses = [max(subhexes, key=lambda s: subhexes[s]['z'])]
    for ingress in sorted(ingresses, key=lambda t: distance_sub(egress, t)):
        if (subhexes[ingress]['z'] - subhexes[egress]['z']) < 20:
            return None
        path = [ingress]
        while egress not in path:
            targets = [s for s in subhexes[path[-1]]['neighbors'] if s not in path]
            targets = [t for t in targets if t not in sum([subhexes[p]['neighbors'] for p in path[:-1]], [])]
            if not targets:
                path = [ingress]
                continue
            if egress in targets:
                if len(path) > (subhexes[ingress]['z'] - subhexes[egress]['z']):
                    print(len(path), (subhexes[ingress]['z'] - subhexes[egress]['z']))
                    path = [ingress]
                    continue
                path.append(egress)
                break
            if any(t in sum(rivers, []) for t in targets):
                path.append(min([t for t in targets if t in sum(rivers, [])], key=lambda x: distance_sub(egress, x)))
                break
            targets = [t for t in targets if len(subhexes[t]['neighbors']) == 6]
            if not targets:
                path = [ingress]
                continue
            pt = sum([distance_sub(egress, t) * distance_sub(t, '1210') for t in targets])
            if pt == 0:
                path = [ingress]
                continue
            ps = [(distance_sub(egress, t) * distance_sub(t, '1210')) / pt for t in targets][::-1]
            if verbose: print(targets, ps)
            t = npr.choice(targets, 1, p=ps).item()
            path.append(t)
            if verbose: print(path)
        rivers.append(path)
    return rivers


def lake(target, rivers):

    if not hexes[target].get('lake'):
        return []
    if hexes[target]['drain_out']:
        egress = egress_hex(target, hexes[target]['drain_out'][0])
    else:
        egress = min(subhexes, key=lambda s: subhexes[s]['z'])
    confluences = []
    for river in rivers:
        if river[-1] != egress:
            confluences.append(river[-1])
    lakes = set()
    for conflux in sorted(confluences, key=lambda c: subhexes[c]['z'], reverse=True):
        current = {conflux}
        visited = set()
        while current:
            for c in list(current):
                if c in visited:
                    current.remove(c)
                    continue
                current |= set([n for n in subhexes[c]['neighbors'] if
                                subhexes[n]['z'] < subhexes[conflux]['z']])
                visited.add(c)
                current.remove(c)
            # print(conflux, len(visited), lakes)
        lakes |= visited

    return lakes


def rivers_encode(x):

    if not x:
        return

    if type(x) == list:
        x = [i for i in x if i]
        r = 'x'.join([''.join(i) for i in x])
    else:
        r = []
        for i in x.split('x'):
            r.append(wrap(i, 4))
    return r


def sub_drainage(target, rivers):

    for s in subhexes:
        subhexes[s]['drainage'] = 1 / 400
        if s in sum(rivers, []):
            continue
        subhexes[s]['drains_from'] = set()
        try:
            del subhexes[s]['drains_to']
        except:
            pass
    for river in rivers:
        for a, b in zip(river[:-1], river[1:]):
            subhexes[a]['drains_to'] = b
            subhexes[b]['drains_from'] = subhexes[b].get('drains_from', set()) | {a}
    if hexes[target]['drain_out']:
        egress = egress_hex(target, hexes[target]['drain_out'][0])
    else:
        egress = min(subhexes, key=lambda s: subhexes[s]['z'])
    ingresses = [(i, egress_match[egress_hex(i, target)]) for i in hexes[target]['drain_in']]
    for ingress in ingresses:
        subhexes[ingress[1]]['drainage'] = hexes[ingress[0]]['drainage']
    subhexes[egress]['drainage'] = hexes[target]['drainage']
    for source in sorted(subhexes, key=lambda x: subhexes[x]['z'], reverse=True):
        if source in sum(rivers, []):
            continue
        try:
            t = min([n for n in subhexes[source]['neighbors'] if
                          n not in subhexes[source]['drains_from'] and
                          subhexes[source]['z'] > subhexes[n]['z']], key=lambda x: subhexes[x]['z'])
        except ValueError:
            continue
        subhexes[t]['drainage'] += subhexes[source]['drainage']
        subhexes[source]['drains_to'] = t
        subhexes[t]['drains_from'] = subhexes[t].get('drains_from', set()) | {source}
    confluences = {river[-1]: river[0] for river in rivers if river[-1] != egress}
    rivers.sort(key=len, reverse=True)
    for ingress in ingresses:
        subhexes[ingress[1]]['drainage'] = hexes[ingress[0]]['drainage']
    for river in rivers:
        for r in drainage_path(river[0], target)[:-1]:
            subhexes[subhexes[r]['drains_to']]['drainage'] = 0
    for river in rivers:
        for r in drainage_path(river[0], target)[:-1]:
            subhexes[subhexes[r]['drains_to']]['drainage'] += subhexes[river[0]]['drainage']
    # for river in sorted(rivers, key=len, reverse=False):
    #     for r, s in zip(river[:-1], river[1:]):
    #         subhexes[s]['drainage'] += subhexes[r]['drainage']


def sub_basins(egress_hex):

    return sum(subhexes[s]['drainage'] for s in subhexes if 'drains_to' not in subhexes[s] and s != egress_hex)


def sub_erode(target, rivers):

    sub_drainage(target, rivers)
    sources = [s for s in subhexes if subhexes[s].get('drains_to') and s not in sum(rivers, [])]
    sources.sort(key=lambda x: subhexes[x]['z'], reverse=False)
    for source in sources:
        tz = subhexes[subhexes[source]['drains_to']]['z']
        subhexes[source]['z'] = round(statistics.mean([subhexes[source]['z'], subhexes[source]['z'], subhexes[source]['z'], tz]))
    sub_drainage(target, rivers)

    # sources = [s for s in subhexes if not subhexes[s]['drains_from'] and s not in sum(rivers, [])]
    # streams = sorted([s for s in subhexes if 'drains_to' in subhexes[s] and s not in sum(rivers, [])], key=lambda x: subhexes[x]['drainage'], reverse=True)
    # for source in streams:
    #     t = subhexes[source].get('drains_to')
    #     if not t:
    #         continue
    #     slope = subhexes[source]['z'] - subhexes[t]['z']
    #     if slope == 1:
    #         continue
    #     dh = (5.198e-12 - 1.778e-15 * sqrt(subhexes[source]['drainage'] * 9.674e9) * slope) * 7.889e12
    #     if dh < -slope: dh = 1 - slope
    #     # dh = max(-10 ** -4.1 * sqrt(subhexes[source]['drainage']) * , 1 - slope)
    #     subhexes[source]['z'] += dh
    #     # subhexes[random.choice(sources)]['z'] -= dh
    # sub_drainage(target, rivers)
    #     # print(source, subhexes[source]['z'], subhexes[target]['z'], dh, 1 - slope)


def wilderness_subs_draw(target, infra, verbose=False, seed=None, rivers=[]):

    targetfile = basefile + '\\20 Mile Hexes\\' + target + '.svg'
    with open(targetfile if os.path.isfile(targetfile) else basefile20, 'r') as f:
        targetmap = bs(f, 'xml')
    targetmap.find('g', id='infrastructure').clear()
    targetmap.find('g', id='subhexes').clear()

    colors = {
        'primary': None,
        'secondary': None,
        'settled': None,
        'lake': '819da9'
    }

    colors['primary'] = terr2color[hexes[target]['terrain']]
    colors['settled'] = terr2color[settled[hexes[target]['terrain']]]
    # get secondary terrain: look at neighbors
    try:
        colors['secondary'] = terr2color[random.choice(list(set([hexes[t]['terrain'] for t in hexes[target]['neighbors'] if hexes[t]['terrain'] != hexes[target]['terrain']])))]
    except IndexError:
        colors['secondary'] = colors['primary']

    ws = {'primary': '', 'settled': '', 'secondary': '', 'lake': ''}
    wild = wilderness_encode(seed) if seed else wilderness(k = infra, target=target, verbose=verbose, rivers=rivers)
    print(wilderness_encode(wild))
    string = ''
    lakestring = ''
    lakes = lake(target, rivers)
    for h, w in wild.items():
        t = 'settled' if w else ('secondary' if random.random() < 0.25 else 'primary')
        if t == 'secondary' and colors['secondary'] == colors['primary']:
            t = 'primary'
        if h in lakes:
            t = 'lake'
        ws[t] += 'M ' + ' '.join(
            ['{:},{:}'.format(hex_corner(subhexes[h]['c'], i)[0], hex_corner(subhexes[h]['c'], i)[1]) for i in
             [4, 5, 0, 1, 2, 3]]) + ' Z'
    for w in ws:
        if w == 'lake':
            lakestring = f"<path d='{ws[w]}' style='fill:#{colors[w]}' class='{w}' />"
        else:
            string += f"<path d='{ws[w]}' style='fill:#{colors[w]}' class='{w}' />"
    if string:
        targetmap.find('g', id='infrastructure').contents = bs(string, 'lxml').find('body').contents
    if lakestring:
        targetmap.find('g', id='rivers').contents += bs(lakestring, 'lxml').find('body').contents
    string = ''
    for h, w in wild.items():
        if w:
            continue
        if h in lakes:
            continue
        wt = sum(1 - wild[n] for n in subhexes[h]['neighbors'])
        wt += 6 - len(subhexes[h]['neighbors'])
        ws = 'M ' + ' '.join(
            ['{:},{:}'.format(hex_corner(subhexes[h]['c'], i)[0], hex_corner(subhexes[h]['c'], i)[1]) for i in
             [4, 5, 0, 1, 2, 3]]) + ' Z'
        string += f"<path d='{ws}' fill='url(#wild{wt})'/>"
    if string:
        targetmap.find('g', id='wilds').contents = bs(string, 'lxml').find('body').contents
    with open(targetfile, 'wb') as f:
        try:
            targetmap.find('body').replace_with_children()
        except AttributeError:
            pass
        try:
            targetmap.find('html').replace_with_children()
        except AttributeError:
            pass
        f.write(targetmap.prettify('utf-8'))


# TODO erode endorheic hexes until the drainage egresses and ingresses are correct

def endorheics(target, n=72):

    # assuming for now there is no ingress
    end = hexes[target]['river egress']
    sub_drainage()
    basins = [s for s in subhexes if 'drains_to' not in subhexes[s] and s != end]
    basins.sort(key=lambda b: len(a_star(b, end)))
    while True:
        b = basins[0]
        # print(b, subhexes[b]['z'], ' ', end='', flush=True)
        subhexes[b]['z'] = int(min([subhexes[n]['z'] for n in subhexes[b]['neighbors']]) + 50)
        # print(subhexes[b]['z'])
        sub_drainage()
        basins = [s for s in subhexes if 'drains_to' not in subhexes[s] and s != end]
        basins.sort(key=lambda b: len(a_star(b, end)))
        # print(len(basins), sub_basins(end))
        if sub_basins(end) < n:
            break
    # while True:
    #     b = basins[0]
    #     # print(b)
    #     path = a_star(b, end)[::-1]
    #     if subhexes[b]['z'] > (subhexes[end]['z'] + len(path)):
    #         dh = (subhexes[b]['z'] - subhexes[end]['z']) / len(path)
    #     else:
    #         dh = 1
    #     for i, p in enumerate(path[1:]):
    #         subhexes[p]['z'] = int(subhexes[end]['z'] + i * dh)
    #     sub_drainage()
    #     basins = [s for s in subhexes if 'drains_to' not in subhexes[s] and s != end]
    #     basins.sort(key=lambda b: distance(subhexes[b]['c'], subhexes[end]['c']))
    #         # print(subhexes[p]['z'])
    #     print(len(basins), sub_basins(end))


def travel_bare(a, b, rivers=[]):

    return distance_sub(a, b) + 1e9 * (subhexes[a]['z'] < subhexes[b]['z'])


def travel_infra(a, b, rivers, lakes=[]):

    d = abs(subhexes[b]['z'] - subhexes[a]['z'])
    if subhexes[b].get('infra', 0) > 0:
        return subhexes[b].get('infra')
    d *= 10 if subhexes[b].get('infra', 0) < 1 else 1
    if b in lakes:
        return 1e10
    if a not in sum(rivers, []) and b in sum(rivers, []) and subhexes[b].get('infra', 0) < 1:
        return 1e10
    elif a in sum(rivers, []) and b in sum(rivers, []):
        return 1e10
    elif a in sum(rivers, []) and b not in sum(rivers, []) and subhexes[a].get('infra', 0) < 1:
        return 0
    return d


def a_star_sub(start, end, travel=travel_bare, rivers=[], lakes=[], verbose=False):

    open_list = list(subhexes[start]['neighbors'])
    closed_list = [start]
    came_from = {o: start for o in open_list}

    def g(current, s=None):

        path = [current]
        while path[-1] != start:
            try:
                path.append(came_from.get(path[-1], s))
            except KeyError:
                pass
        path = path[::-1]

        z = zip(path[:-1], path[1:])
        return round(sum(travel(i, j, rivers=rivers, lakes=lakes) for i, j in z), 3)

    def h(current):

        return distance(subhexes[current]['c'], subhexes[end]['c'])

    def f(current):

        return round(g(current) + h(current), 3)

    while open_list:
        s = sorted(open_list, key = lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        if s == end:
            # print(closed_list)
            break
        for t in subhexes[s]['neighbors']:
            if travel(s, t, rivers=rivers, lakes=lakes) > 1e8:
                closed_list.append(t)
                continue
            if t not in closed_list:
                if t not in open_list or g(t, s=s) < h(t):
                    open_list.append(t)
                came_from[t] = s
        closed_list = list(set(closed_list))
        if verbose: print(f(s), closed_list)

    path = [end]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return None
    return path[::-1]


def a_star_star_sub(start, ends, travel=travel_bare, rivers=[], lakes=[], verbose=False):

    open_list = list(subhexes[start]['neighbors'])
    closed_list = [start]
    came_from = {o: start for o in open_list}

    def g(current, s=None):

        path = [current]
        while path[-1] != start:
            try:
                path.append(came_from.get(path[-1], s))
            except KeyError:
                pass
        path = path[::-1]

        z = zip(path[:-1], path[1:])
        return round(sum(travel(i, j, rivers=rivers, lakes=lakes) for i, j in z), 3)

    def h(current):

        return min(distance(subhexes[current]['c'], subhexes[end]['c']) for end in ends)

    def f(current):

        return round(g(current) + h(current), 3)

    while open_list:
        s = sorted(open_list, key = lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        if s in ends:
            # print(closed_list)
            path = [s]
            break
        for t in subhexes[s]['neighbors']:
            if travel(s, t, rivers=rivers, lakes=lakes) > 1e8:
                closed_list.append(t)
                continue
            if t not in closed_list:
                if t not in open_list or g(t, s=s) < h(t):
                    open_list.append(t)
                came_from[t] = s
        closed_list = list(set(closed_list))
        if verbose: print(f(s), closed_list)

    path = [s]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return None
    return path[::-1]


def whole_shebang(target, verbose=False):

    map_gen(target=target, erode=0, verbose=verbose)
    sub_drainage(target=target)
    river_ensure(target=target)
    map_write(target=target)


def sub_desirability():

    pass


def roads_generate(target, rivers=[], lakes=[], verbose=False):

    roads = []
    big_river = []
    if rivers:
        if hexes[target]['drain_in']:
            big_river = drainage_path(egress_match[egress_hex(max(hexes[target]['drain_in'], key=lambda h: hexes[h]['drainage']), target)], target)
        else:
            big_river = drainage_path(max(subhexes, key=lambda s: subhexes[s]['z']), target)
        lakes = lake(target, rivers)
    a = max(subhexes, key=lambda s: subhexes[s].get('infra', 0) * (s in big_river))
    for road in hexes[target].get('roads', set()):
        if hexes[road]['altitude'] < hexes[target]['altitude']:
            b = egress_match[egress_hex(road, target, conn='road')]
        else:
            b = egress_hex(target, road, conn='road')
        if verbose: print(road, b)
        roads.append(a_star_sub(a, b, travel=travel_infra, rivers=rivers, lakes=lakes, verbose=verbose))
    return roads


def tracks_generate(target, roads, rivers=[], lakes=[], verbose=False):

    tracks = []
    big_river = []
    if rivers:
        if hexes[target]['drain_in']:
            big_river = drainage_path(egress_match[egress_hex(max(hexes[target]['drain_in'], key=lambda h: hexes[h]['drainage']), target)], target)
        else:
            big_river = drainage_path(max(subhexes, key=lambda s: subhexes[s]['z']), target)
        lakes = lake(target, rivers)
    a = max(subhexes, key=lambda s: subhexes[s].get('infra', 0) * (s in big_river))
    for road in hexes[target].get('tracks', set()):
        b = egress_hex(road, target, conn='road')
        if hexes[road]['altitude'] > hexes[target]['altitude']:
            b = egress_match[egress_hex(target, road, conn='road')]
        tracks.append(a_star_sub(a, b, travel=travel_infra, rivers=rivers, lakes=lakes, verbose=verbose))
    targets = sum([r for r in roads if r] or [[]], []) + sum([t for t in tracks if t] or [[]], [])
    if not targets:
        return
    for s in sorted(subhexes, key=lambda x: subhexes[x]['infra'], reverse=True):
        if not subhexes[s].get('infra'):
            continue
        if s in targets:
            continue
        else:
            # targets = [a_star_sub(s, r, travel=travel_infra, rivers=rivers) for r in sum(roads, []) + sum(tracks, [])]
            # target = min(targets, key=lambda path: sum(travel_infra(i, j, rivers=rivers) for i, j in zip(path[:-1], path[1:])))
            target = a_star_star_sub(s, targets, travel=travel_infra, rivers=rivers, lakes=lakes)
            if verbose: print(target)
        tracks.append(target)
    return tracks


def roads_draw(target, roads):

    targetfile = basefile + '\\20 Mile Hexes\\' + target + '.svg'
    with open(targetfile if os.path.isfile(targetfile) else basefile20, 'r') as f:
        targetmap = bs(f, 'xml')
    targetmap.find('g', id='roads').clear()
    targetmap.find('g', id='subhexes').clear()
    string = ''
    done = set()

    for road in roads:
        if not road:
            continue
        for r, s in zip(road[:-1], road[1:]):
            if (r, s) in done or (s, r) in done:
                continue
            string += f"<path d='{'M {:.3f},{:.3f}'.format(*subhexes[r]['c']) + ' {:.3f},{:.3f}'.format(*subhexes[s]['c'])}' style='stroke-dasharray:10,10;' />"
            done.add((r, s))
            done.add((s, r))

    for road in hexes[target].get('roads', set()):
        if hexes[road]['altitude'] < hexes[target]['altitude']:
            r = egress_match[egress_hex(road, target, conn='road')]
        else:
            continue
            r = egress_hex(target, road, conn='road')
        egress_angle = (round(degrees(atan2(
            hexes[road]['c'][1] - hexes[target]['c'][1],
            hexes[road]['c'][0] - hexes[target]['c'][0],
        )), 0)) % 360
        string += f"<path d='M {subhexes[r]['c'][0]:.3f},{subhexes[r]['c'][1]:.3f} " \
            f"{subhexes[r]['c'][0] + 50 * sqrt(3) * cos(radians(egress_angle)):.3f}," \
            f"{subhexes[r]['c'][1] + 50 * sqrt(3) * sin(radians(egress_angle)):.3f}' />"

    if string:
        targetmap.find('g', id='roads').contents = bs(string, 'lxml').find('body').contents
    with open(targetfile, 'wb') as f:
        try:
            targetmap.find('body').replace_with_children()
        except AttributeError:
            pass
        try:
            targetmap.find('html').replace_with_children()
        except AttributeError:
            pass
        f.write(targetmap.prettify('utf-8'))


def tracks_draw(target, tracks, roads):

    if not tracks:
        tracks = []
    if not roads:
        roads = []

    targetfile = basefile + '\\20 Mile Hexes\\' + target + '.svg'
    with open(targetfile if os.path.isfile(targetfile) else basefile20, 'r') as f:
        targetmap = bs(f, 'xml')
    targetmap.find('g', id='tracks').clear()
    targetmap.find('g', id='subhexes').clear()
    string = ''
    done = set()
    for road in roads:
        if not road:
            continue
        for r, s in zip(road[:-1], road[1:]):
            done.add((r, s))
            done.add((s, r))

    for road in tracks:
        if not road:
            continue
        for r, s in zip(road[:-1], road[1:]):
            if (r, s) in done or (s, r) in done:
                continue
            string += f"<path d='{'M {:},{:}'.format(*subhexes[r]['c']) + ' {:},{:}'.format(*subhexes[s]['c'])}' style='stroke-dasharray:5,5;' />"
            done.add((r, s))
            done.add((s, r))

    for road in hexes[target].get('tracks', set()):
        if hexes[road]['altitude'] < hexes[target]['altitude']:
            r = egress_match[egress_hex(road, target, conn='road')]
        else:
            continue
        egress_angle = (round(degrees(atan2(
            hexes[road]['c'][1] - hexes[target]['c'][1],
            hexes[road]['c'][0] - hexes[target]['c'][0],
        )), 0)) % 360
        string += f"<path d='M {subhexes[r]['c'][0]:.3f},{subhexes[r]['c'][1]:.3f} " \
            f"{subhexes[r]['c'][0] + 50 * sqrt(3) * cos(radians(egress_angle)):.3f}," \
            f"{subhexes[r]['c'][1] + 50 * sqrt(3) * sin(radians(egress_angle)):.3f}' />"

    if string:
        targetmap.find('g', id='tracks').contents = bs(string, 'lxml').find('body').contents
    with open(targetfile, 'wb') as f:
        try:
            targetmap.find('body').replace_with_children()
        except AttributeError:
            pass
        try:
            targetmap.find('html').replace_with_children()
        except AttributeError:
            pass
        f.write(targetmap.prettify('utf-8'))


def combine_neighbors(center, targets, verbose=False):

    for target in targets + [center]:
        targetfile = basefile + '\\20 Mile Hexes\\' + target + '.svg'
        if not os.path.isfile(targetfile):
            map_gen(target, verbose=verbose)
    c0 = hexes[center]['c'][0] * 20, hexes[center]['c'][1] * 20
    centerfile = basefile + '\\20 Mile Hexes\\' + center + '.svg'
    with open(centerfile, 'r') as f:
        centermap = bs(f, 'xml')
    for p in centermap.find_all('path'):
        p['transform'] = f"translate ({c0[0] - c0[0]:.3f} {c0[1] - c0[1]:.3f})"
    for target in targets:
        targetfile = basefile + '\\20 Mile Hexes\\' + target + '.svg'
        c = hexes[target]['c'][0] * 20, hexes[target]['c'][1] * 20
        with open(targetfile, 'r') as f:
            targetmap = bs(f, 'xml')
        for p in targetmap.find_all('path'):
            p['transform'] = f"translate ({c[0] - c0[0]:.3f} {c[1] - c0[1]:.3f})"
        for g in targetmap.find_all('g'):
            centermap.find('g', id=g['id']).contents += g.contents

    x0 = subhexes['1400']['c'][0] - 86.6 + min(hexes[n]['c'][0] for n in targets + [center]) * 20
    x1 = subhexes['1419']['c'][0] + 86.6 + max(hexes[n]['c'][0] for n in targets + [center]) * 20
    y0 = subhexes['0009']['c'][1] - 86.6 + min(hexes[n]['c'][1] for n in targets + [center]) * 20
    y1 = subhexes['2510']['c'][1] + 86.6 + max(hexes[n]['c'][1] for n in targets + [center]) * 20

    centermap.find('svg')['width'] = f"{abs(x1 - x0):.3f}"
    centermap.find('svg')['height'] = f"{abs(y1 - y0):.3f}"
    centermap.find('svg')['viewBox'] = f"{x0-c0[0]} {y0-c0[1]} {abs(x1 - x0) + 86.6:.3f} {abs(y1 - y0):.3f}"

    with open(fr"{basefile}\{20} Mile Hexes\{center} neighbors.svg", 'wb') as f:
        try:
            centermap.replace_with_children()
        except:
            pass
        try:
            centermap.find('html').replace_with_children()
        except:
            pass
        f.write(centermap.prettify('utf-8'))


