from defs import *
from market import *
from word_gen import *

load_data(sociology, sociology_file)

def p(k, level):
    """Returns the level distribution"""

    return 1 / sum(1 / k ** (n - 1) for n in range(0, 20+1)) * k ** (1 - level)


def B(x, n, k):
    """Bernstein polynomial"""

    return math.factorial(n) / math.factorial(k) / math.factorial(n - k) * x ** k * (1 - x) ** (n - k)


def P(x, xm, r):
    """Returns the age distribution"""

    r = {'HIGH': 1.5, 'LOW': 1, 'DECLINE': 0.75}[r]
    n = 5
    Y = [r, 1, 1, 1, 0, 0]
    X = list(np.linspace(start=15, stop=xm, num=n+1))
    f = 0
    for k in range(n+1):
        f += Y[k] * B((x - 15) / (xm - 15), n, k)
    L = 0
    for i in range(15, xm+1):
        for k in range(n + 1):
            L += Y[k] * B((i - 15) / (xm - 15), n, k)
    L = 1/L
    return L * f

orc_names = markov_combine([name_base + r'\arabic.txt', name_base + r'\somali.txt'])
elf_names = markov_combine([name_base + r'\protosamic.txt', name_base + r'\egyptian.txt'])
dwarf_names = markov_combine([name_base + r'\hmong.txt', name_base + r'\germanic.txt'])
human_names = markov_combine([name_base + r'\welsh.txt', name_base + r'\polish.txt'])
halfling_names = markov_combine([name_base + r'\bokmal.txt', name_base + r'\russian.txt'])

# TODO check max name length, seems a bit too long for some languages

names = {
    'orc': orc_names,
    'elf': elf_names,
    'dwarf': dwarf_names,
    'human': human_names,
    'halfling': halfling_names
}

k = 3.039
p_level = [p(k, level) for level in range(21)]

xm = { # max age
    'elf': 125,
    'dwarf': 150,
    'human': 80,
    'orc': 55,
    'halfling': 70
}

p_age = {
    'elf': [P(a, xm['elf'], 'LOW') for a in range(15, xm['elf']+1)],
     'dwarf': [P(a, xm['dwarf']+1, 'DECLINE') for a in range(15, xm['dwarf']+1)],
     'human': [P(a, xm['human']+1, 'HIGH') for a in range(15, xm['human']+1)],
     'halfling': [P(a, xm['human']+1, 'LOW') for a in range(15, xm['halfling']+1)],
     'orc': [P(a, xm['human']+1, 'DECLINE') for a in range(15, xm['orc']+1)]
}

p_age = {p: [i / sum(p_age[p]) for i in p_age[p]] for p in p_age}

classes = dict()


def get_class(city):

    classes = {c: 1 for c in [
        'barbarian',
        'bard',
        'cleric',
        'druid',
        'fighter',
        'paladin',
        'ranger',
        'rogue',
        'sorcerer',
        'wizard',
        'warlock'
    ]}

    # Type I
    university = G.node[city]['pricing references'].get('university', 0) / max(G.node[n]['pricing references'].get('university', 0) for n in G)
    classes.update({'wizard': university})
    seminary = G.node[city]['pricing references'].get('seminary', 0) / max(G.node[n]['pricing references'].get('seminary', 0) for n in G)
    classes.update({'cleric': seminary})
    college = G.node[city]['pricing references'].get('college', 0) / max(G.node[n]['pricing references'].get('college', 0) for n in G)
    classes.update({'bard': college})
    # Type II
    for tech in tech_infra:
        if sociology[city]['infrastructure'] < tech_infra[tech]:
            tech -= 1
            break
    classes.update({'barbarian': -1 / 1728 * (tech - 17) ** 3})
    classes.update({'druid': -1 / 1728 * (tech - 17) ** 3})
    classes.update({'ranger': -1 / 1728 * (tech - 17) ** 3})
    classes.update({'rogue': 1 / 1728 * (tech - 17) ** 3 + 1})
    # Type III
    classes.update({'paladin': 0.01 * random.random()})
    classes.update({'sorcerer': 0.01 * random.random()})
    classes.update({'warlock': 0.01 * random.random()})
    # Type IV
    classes.update({'fighter': 3})

    classes = {c: round(classes[c], 5) for c in classes}
    classes = {c: classes[c] / sum(classes.values()) for c in classes}

    return classes


def generate_scores(level):

    if level > 9:
        return [roll('4d6l1') for _ in range(6)]
    else:
        return {
            # 0: [roll('2d6') for _ in range(6)],
            # 1: [roll('2d6') for _ in range(5)] + [roll('3d6') for _ in range(1)],
            # 2: [roll('2d6') for _ in range(4)] + [roll('3d6') for _ in range(2)],
            # 3: [roll('2d6') for _ in range(3)] + [roll('3d6') for _ in range(3)],
            0: [roll('2d6') for _ in range(2)] + [roll('3d6') for _ in range(4)],
            1: [roll('3d6') for _ in range(6)],
            2: [roll('3d6') for _ in range(6)],
            3: [roll('3d6') for _ in range(5)] + [roll('4d6l1') for _ in range(1)],
            4: [roll('3d6') for _ in range(5)] + [roll('4d6l1') for _ in range(1)],
            5: [roll('3d6') for _ in range(4)] + [roll('4d6l1') for _ in range(2)],
            6: [roll('3d6') for _ in range(4)] + [roll('4d6l1') for _ in range(2)],
            7: [roll('3d6') for _ in range(3)] + [roll('4d6l1') for _ in range(3)],
            8: [roll('3d6') for _ in range(3)] + [roll('4d6l1') for _ in range(3)],
            9: [roll('3d6') for _ in range(2)] + [roll('4d6l1') for _ in range(4)],
        }[level]


def racial_scores(race, scores):

    if race == 'dwarf':
        scores['STR'] += 1
        scores['CON'] += 2
    elif race == 'elf':
        scores['DEX'] += 2
        scores['INT'] += 1
    elif race == 'halfling':
        scores['DEX'] += 1
        scores['CON'] += 1
        scores['CHA'] += 1
    elif race == 'human':
        for s in scores:
            scores[s] += 1
    elif race == 'orc':
        scores['STR'] += 2
        scores['CHA'] -= 2

    return scores


def age_scores(scores, age, race):

    age = int(age * xm[race] / xm['human'])
    if age < 60:
        return scores
    for a in range(60, age + 1):
        for s in ['STR', 'DEX', 'CON']:
            if roll('1d20') < scores[s]:
                scores[s] -= 1
        for s in ['INT', 'WIS', 'CON']:
            if roll('1d20') > scores[s]:
                scores[s] += 1
    return scores


def assign_scores(c, level, race):

    primary = {
        'barbarian': ['STR', 'CON'],
        'bard': ['CHA'],
        'cleric': ['WIS', 'CHA'],
        'druid': ['WIS'],
        'fighter': ['STR', 'CON', 'DEX', 'CHA'],
        'paladin': ['CHA', 'CON', 'STR', 'CHA'],
        'ranger': ['WIS', 'DEX', 'CHA'],
        'rogue': ['DEX'],
        'sorcerer': ['CHA'],
        'wizard': ['INT'],
        'warlock': ['CHA']
    }

    raw = generate_scores(level)
    raw.sort(reverse=True)
    scores = {s: 0 for s in ['STR', 'DEX', 'CON', 'INT', 'WIS', 'CHA']}
    for s in scores:
        if s in primary[c]:
            scores[s] = raw[0]
            del raw[0]
    for s in scores:
        if scores[s]:
            continue
        r = random.choice(raw)
        scores[s] = r
        del raw[raw.index(r)]

    return racial_scores(race, scores)


minimum_age = { # relative to human
    'barbarian': 15,
    'bard': 26,
    'cleric': 21,
    'druid': 23,
    'fighter': 15,
    'paladin': 20,
    'ranger': 18,
    'rogue': 19,
    'sorcerer': 26,
    'wizard': 26,
    'warlock': 26
}


def generate_population(city):

    pop = cities[city]['population']
    p_race = [G.node[city]['demographics'][r] for r in names]
    p_race = [p / sum(p_race) for p in p_race]
    city_classes = get_class(city)
    city_classes = [list(city_classes), list(city_classes.values())]
    people = dict()
    for person in range(pop):
        race = npr.choice(list(names), 1, p=p_race)[0]
        while True:
            name = markov_word(names[race]).capitalize()
            if name not in people:
                break
        level = npr.choice(range(21), 1, p=p_level)[0]
        gender = random.choice(['male', 'female'])
        player_class = npr.choice(city_classes[0], p=city_classes[1])
        while True:
            age = npr.choice(range(15, xm[race]+1), 1, p=p_age[race])[0]
            if age >= minimum_age[player_class] * xm[race] / xm['human'] + level * 0.75:
                break
        scores = assign_scores(player_class, level, race)
        scores = age_scores(scores, age, race)
        # print(f'{name}, a {age} year old {gender} {race}, {level}')
        people.update({
            name: {
                'age': age,
                'gender': gender,
                'race': race,
                'level': level,
                'class': player_class,
                'stats': scores,
                'mods': [int((scores[s] - 10)/2) for s in scores]
            }
        })
    return people


def display_person(name, people):

    person = people[name]
    return f'{name}, a {person["age"]} year old {person["race"]} {person["gender"]}, {person["class"].capitalize()} {person["level"]}' #+ \
           # '/'.join([f'{person["stats"][s]}' for s in person["stats"]]) + ' ' + \
           # '/'.join([f'{s}' for s in person["mods"]])


def product(list):
    p = 1
    for i in list:
        p *= i
    return p


def locate_party(party, party_races):

    threshold = (1 / len(party)) ** len(party) / 1000
    for city in G:
        classes = get_class(city)
        p = product([classes[c] > 0 for c in party]) * product(G.node[city]['demographics'][r] > 0 for r in party_races)
        print(f'{city}, {p:.5f}, {p >= threshold}')
        if p >= threshold:
            return