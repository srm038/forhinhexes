from defs import *
# from history import *
from topography import *
from mapping import temperature_baseline
import copy

load_data(seaways, seaways_file)
# load_data(all_neighbors, all_neighbors_file)
load_data(currents, basefile + r'\currents.json')
load_data(currentsTemp, basefile + r'\currents_temp.json')
load_data(ice, basefile + r'\ice.json')
# load_data(temp_ice, basefile + r'\ice_temp.json')

def currentsGenerate(i=0.15):

    seaLevel = seaLevelGet(verbose=False)
    ocean = oceanGet(seaLevel=seaLevel)

    def influence(s):

        def f(x, d):
            r = 0.036345098772829118529
            return 180 * math.exp(r * (x - d)) / (180 + (math.exp(r * (x - d) - 1)))

        lat = abs(parseID(s)[1])

        return roundBase(f(lat, 311/2) - f(-lat, -311/2), base=60)

    visited = set()
    sources = set()
    baseCurrents = {h for h in ocean if '311-' in h or '155S-' in h or '155N-' in h}
    currents.clear()
    received = dict()
    for source in baseCurrents:
        currents[source] = influence(source) % 360
        sources.add(source)
    oldSources = set()

    for source in list(sources):
        if not nearCoast(source, ocean):
            sources.discard(source)
            currents[source] = None

    while len(sources) and oldSources != sources and oldSources | visited != sources | visited:
        oldSources = copy.deepcopy(sources)
        for source in list(sources):
            if source not in ocean or source in visited:
                sources.discard(source)
                continue

            targets = [n for n in neighbors(source) if n not in visited and n in ocean and nearCoast(n, ocean)]

            influenceAngle = influence(source) % 360
            angle = currents[source] % 360
            target = getTarget(source, angle)

            if currents[source] == influenceAngle:

                if target in visited:
                    sources.discard(source)
                    continue

                if target not in hexMap:
                    angle += 60 if 90 < influence(source) < 270 else -60
                    target = getTarget(source, angle)

                # if target and getSector(target) != getSector(source):
                #     angle = (currents[source] - sectorRotate[getSector(source)] + sectorRotate[getSector(target)]) % 360

                if target and target in ocean:
                    visited.add(source)
                    sources.discard(source)
                    if target not in visited:
                        print(f'{source: <10}-> {target: <10}, {len(sources)}, {len(visited)}, {angle:0.0f}')
                        sources.add(target)
                        currents[target] = angle % 360
                        received[target] = source

            for target in targets:
                print('{: <10}-> {: <10}, {:}, {:}'.format(source, target, len(sources), len(visited)))
                sources.add(target)
                visited.add(source)
                sources.discard(source)
                currents[target] = hexAngle(source, target)
                received[target] = source

    for c in list(currents):
        if currents[c] is None:
            currents.pop(c, None)

    currentsPNG(currents=currents)

    for c in currents:
        if nearCoast(c, ocean):
            continue
        influenceAngle = influence(source) % 360
        currents[c] = round(i * influenceAngle + (1 - i) * currents[c])

    currentsPNG(currents=currents)

    save_data(currents, basefile + r'\currents.json')


def currentsPNG(currents, s=0.25):

    gc.collect()
    fig, ax = newMap(0)

    b = [hexMap[i] for i in ['311-156E', '310S-465E', '310S-775E', '310S-775W', '310S-465W', '311-156W',
                             '311-467W', '311-778W', '311-778E', '311-467E']]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#000000'))

    t = list(c for c in currents if currents[c] is not None)
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    c = [hexColor(f"{screen('0000ff', currents[h] / 360 * 255)}") for h in t]
    ax.scatter(x=x, y=y, marker='h', c=c, s=s, edgecolors=c, zorder=100, linewidths=1 / 8)

    fig.savefig(fr"{basefile}\Currents\currents.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()
    del x, y, t
    gc.collect()


def currentsTempPNG(currentsTemp, season='Jul', s=0.25):

    gc.collect()

    seaLevel = seaLevelGet(verbose=False)
    ocean = oceanGet(seaLevel=seaLevel)

    fig, ax = newMap(0)

    b = [hexMap[i] for i in ['311-156E', '310S-465E', '310S-775E', '310S-775W', '310S-465W', '311-156W',
                             '311-467W', '311-778W', '311-778E', '311-467E']]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#000000'))

    t = list(c for c in currentsTemp[season] if currentsTemp[season][c] is not None)
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    minT = min(currentsTemp[season][c] for c in currents)
    maxT = max(currentsTemp[season][c] for c in currents)
    c = [hexColor(f"{colorTemp(currentsTemp[season][h], k=(minT, maxT))}") for h in t]
    ax.scatter(x=x, y=y, marker='h', c=c, s=s, edgecolors=c, zorder=100, linewidths=1 / 8)

    t = list(c for c in currentsTemp[season] if currentsTemp[season][c] is not None and nearCoast(c, ocean))
    t = [c for c in t if abs(currentsTemp[season][c] - currentSST(c, season)) > 10]
    y = [hexMap[h][0] for h in t]
    x = [hexMap[h][1] for h in t]
    c = [hexColor(f"{colorTemp(currentsTemp[season][h], k=(minT, maxT))}") for h in t]
    ax.scatter(x=x, y=y, marker='h', c=c, s=s, edgecolors=c, zorder=100, linewidths=1 / 8)

    fig.savefig(fr"{basefile}\Currents\currentsTemp{season}.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()
    del x, y, t
    gc.collect()


def convexity(hex1, hex2):

    x1, y1 = hexdict[hex1][0][0], hexdict[hex1][0][1] + 50
    x2, y2 = hexdict[hex2][0][0], hexdict[hex2][0][1] + 50
    n = hexDistance(hex1, hex2)
    checked = set()
    m = int(abs(floor(log(1 / n, 2))))
    for i in range(1, m + 1):
        for j in range(1, i + 1):
            for k in range(1, 2 ** j):
                t = k * 2 ** -j
                if t in checked:
                    continue
                checked |= {t}
                x = x1 + (x2 - x1) * t
                y = y1 + (y2 - y1) * t
                if latlong((x, y)) in hexes:
                    return False
    else:
        return True


def currentsFloodFill(start, sources, k=100):

    inspecting = set([start])
    visited = set()
    filtered = set()

    while inspecting and len(filtered) < k:
        for c in list(inspecting):
            if c in visited:
                inspecting.remove(c)
                continue
            if c in sources:
                inspecting.remove(c)
                visited.add(c)
                filtered.add(c)
                continue
            inspecting |= set([n for n in neighbors(c) if start != n])
            visited.add(c)
            inspecting.remove(c)
        # print(len(visited), len(inspecting), len(filtered))

    return filtered


def currentsInterpolate(k=50, n=2):
    '''
    Use IDW to interpolate between currents
    :param k:
    :param n:
    :return:
    '''

    seaLevel = seaLevelGet(verbose=False)
    ocean = oceanGet(seaLevel=seaLevel)

    # clear everything
    for c in list(currents):
        if not nearCoast(c, ocean) and abs(parseID(c)[1]) not in [311, 311//2, 310]:
            currents.pop(c, None)
    # first, get the remaining coastal currents
    sources = set(currents)
    filtered = [h for h in ocean if nearCoast(h, ocean) and h not in sources]
    i = 0
    for c in filtered:
        idw_neighbors = [(s, hexDistance(c, s)) for s in currentsFloodFill(c, sources, k=k) if s != c]
        if not idw_neighbors:
            continue
        idw = sum(
            currents[s] / d ** n for s, d in idw_neighbors
        ) / sum(
            1 / d ** n for s, d in idw_neighbors
        )
        currents[c] = int(idw)
        i += 1
        angle = roundBase(currents[c], base=60)
        target = getTarget(c, angle)
        if target in hexMap:
            m = -1 if parseID(c)[1] < 0 else 1
            for j in range(1, 7):
                angle = roundBase(currents[c] + 60 * m * j, base=60)
                target = getTarget(c, angle)
                if target in ocean:
                    # print(j)
                    currents[c] = angle
                    break
        print(f'{i / len(filtered):.4%}')

    currentsPNG(currents=currents)

    sources = set(currents)
    filtered = [h for h in ocean if h not in sources]
    i = 0
    for c in filtered:
        idw_neighbors = [(s, hexDistance(c, s)) for s in currentsFloodFill(c, sources, k=k) if s != c]
        if not idw_neighbors:
            continue
        idw = sum(
            currents[s] / d ** n for s, d in idw_neighbors if d
        ) / sum(
            1 / d ** n for s, d in idw_neighbors if d
        )
        currents[c] = int(idw)
        i += 1
        print(f'{i / len(filtered):.4%}')

    for c in list(currents):
        if currents[c] is None or c not in ocean:
            currents.pop(c, None)

    currentsPNG(currents=currents)

    save_data(currents, basefile + r'\currents.json')


def currentsBlend(g=3):
    '''
    Apply a Gaussian blur to all currents
    :param g:
    :return:
    '''

    seaLevel = seaLevelGet(verbose=False)
    ocean = oceanGet(seaLevel=seaLevel)
    # c_neighbors = {c: all_neighbors[c] for c in currents if currents[c]}
    for _ in range(g):
        i = 0
        currentsCopy = {c: int(currents[c]) for c in currents if currents[c]}
        for c in currentsCopy:
            i += 1
            if not currents[c]:
                continue
            if nearCoast(c, ocean):
                currents[c] = int(statistics.mean([currentsCopy[n] for n in neighbors(c) if n in currentsCopy and nearCoast(
                    n, ocean)] + [currentsCopy[c]]))
            else:
                currents[c] = int(statistics.mean([currentsCopy[n] for n in neighbors(c) if n in currentsCopy] + [currentsCopy[c]]))
            # print(f'{i / len(currentsCopy):4%}')

    for c in list(currents):
        if currents[c] is None or c not in sea:
            currents.pop(c, None)

    currentsPNG(currents=currents)

    save_data(currents, basefile + r'\currents.json')


def currents_map_downsample(n=10, season='Jul'):

    # load_data(currents, basefile + r'\currents.json')
    ocean = oceanGet()
    string = ''
    i = 1
    for s in currents:
        if currents[s] is not None:
            # col = '000'
            if s not in ocean and i % n != 0:
                i += 1
                continue
            col = colorTemp(temperature_baseline(s, season))
            string += f'<use xlink:href="#current" transform="translate ({hexdict[s][0][0]} {hexdict[s][0][1] + 50}) scale({2.5:.3f}) rotate ({currents[s]:.0f})" id="{s}" style="color:#{col}"/>'
            i += 1
    with open(basefile + r'\Currents.svg', 'r') as f:
        currentmap = bs(f, 'lxml')
    currentmap.find('g', id='Currents-Gen').contents = bs(string, 'lxml').find('body').contents
    save(currentmap)


def currentSST(h, season='Jul'):

    if season not in ['Jan', 'Jul']:
        raise Exception
    lat = latitude(h)
    lat *= 1 if season == 'Jul' else -1
    t = -(50 if lat > 0 else 57) * (lat / 90) ** (4 if lat > 0 else 2) + 30
    t = 1.8 * t + 32
    return round(t)


def currentsTemperature(d=80, season='Jul'):

    currentsTemp.setdefault(season, dict())
    currentsTemp[season].clear()
    # c = '0N-0'
    temporary = dict()
    j = 0
    for c in currents:
        # t = temperature_baseline(c, season)
        t = currentSST(c, season)
        active = set([c])
        visited = set()
        i = 0
        while i < d and active:
            for a in list(active):
                if a in visited:
                    active.discard(a)
                    continue
                target = getTarget(a, currents[a] % 360)
                if target in currents:
                    temporary.setdefault(target, dict())
                    temporary[target][c] = t
                    active.add(target)
                    active.discard(a)
                    visited.add(a)
            i += 1
        j += 1
        print(f"{len(temporary) / len(currents):0.2%}")

    seaLevel = seaLevelGet(verbose=False)
    ocean = oceanGet(seaLevel=seaLevel)

    for h in temporary:
        t = list(temporary[h].values())
        if all(i == t[0] for i in t):
            temporary[h] = {list(temporary[h].keys())[0]: t[0]}

    i = 1
    n = 10
    for s in currents:
        if currents[s] is not None:
            # if s not in ocean and i % n != 0:
            #     i += 1
            #     continue
            t = statistics.mean(temporary[s].values() if s in temporary else [currentSST(c, season)])
            if s in temporary: temporary.pop(s, None)
            # if t > 90:
            #     print(s, t, temporary[s].values() if s in temporary else [currentSST(c, season)])
            currentsTemp[season][s] = round(t, 2)
            # if s in temporary:
            #     col = 'ff0' if t > currentSST(s, season) + 5 else '0ff' if t < currentSST(s,
            #                                                                               season) - 5 else colorTemp(
            #         currentSST(
            #             s, season))
            # else:
            #     col = colorTemp(t if s in temporary else currentSST(s, season))
            # string += f'<use xlink:href="#current" transform="translate ({hexdict[s][0][0]} {hexdict[s][0][1] + 50}) scale({2.5:.3f}) rotate ({currents[s]:.0f})" id="{s}" style="color:#{col}"/>'
            # i += 1

    currentsTempPNG(currentsTemp=currentsTemp, season=season)

    # with open(basefile + r'\Currents.svg', 'r') as f:
    #     currentmap = bs(f, 'lxml')
    # currentmap.find('g', id='Currents-Gen').contents = bs(string, 'lxml').find('body').contents
    # save(currentmap)

    save_data(currentsTemp, basefile + r'\currents_temp.json')

    # return temporary


def currentsTemperatureBlur(g=3, season='Jul'):

    for _ in range(g):
        old = {c: currentsTemp[season][c] for c in currents}
        for c in currents:
            u = old[c]
            n = statistics.mean([u] + [old[n] for n in neighbors(c) if n in currents])
            currentsTemp[season][c] = n


def sea_travel(i, j):

    if j in hexes:
        return 1e9

    try:
        d = abs(rawAngle(i, j) - roundBase(currents[i], base=60))
    except TypeError:
        d = 180
    t = 0.5 + d * 0.5 / 180

    if nearCoast(j, ):
        t /= 100

    return round(t, 2)


# with open(ice_file, 'rb') as f:
#     perm_ice = pickle.load(f)
# with open(icet_file, 'rb') as f:
#     temp_ice = pickle.load(f)


def ice_caps():

    ice['Jan'].clear()
    ice['Jul'].clear()
    ice['permanent'].clear()

    for c in currents:
        # if c in currentsTemp['Jul'] and c in currentsTemp['Jan']:
        if abs(latitude(c)) > 70:
            if c in currentsTemp['Jul'] and currentsTemp['Jul'][c] <= 32:
                ice['Jul'].append(c)
            if c in currentsTemp['Jan'] and currentsTemp['Jan'][c] <= 32:
                ice['Jan'].append(c)

    for season in ['Jan', 'Jul']:
        active = set(ice[season])
        while active:
            for a in list(active):
                for n in all_neighbors[a]:
                    if n in ice[season] or n not in currents:
                        continue
                    if abs(latitude(n)) > abs(latitude(a)):
                        active.add(n)
                        ice[season].append(n)
                active.discard(a)
            print(len(ice[season]), len(active))

        i = True
        while i:
            i = False
            for c in ice[season] + sum([all_neighbors[h] for h in ice[season]], []):
                if c in ice[season]:
                    # continue
                    if sum(n in ice[season] for n in all_neighbors[c]) < 3:
                        ice[season].remove(c)
                        # print(c)
                        i = True
                    # continue
                elif sum(n in ice[season] for n in all_neighbors[c]) > 3:
                    ice[season].append(c)
                    # print(c)
                    i = True
            print(len(ice[season]))

    ice['permanent'] = [c for c in ice['Jul'] if c in ice['Jan'] and c in currents]

    for season in ['Jan', 'Jul']:
        for p in ice[season]:
            if p in ice['permanent']:
                ice[season].remove(p)
            elif p not in currents:
                ice[season].remove(p)

    save_data(ice, basefile + r'\ice.json')

    ice_maps()


def ice_maps(season='Jul'):

    string = ''
    col = screen('8cf0ff', 255 * 0.4)
    # col = '0ff'
    for c in ice[season]:
        string += f"<path d='M {' '.join([','.join([str(i) for i in p]) for p in hexdict[c]])} Z' id='{c}' class='hex' style='color:#{col}' />"
    col = screen('8cf0ff', 255 * 0)
    # col = '00f'
    for c in ice['permanent']:
        string += f"<path d='M {' '.join([','.join([str(i) for i in p]) for p in hexdict[c]])} Z' id='{c}' class='hex' style='color:#{col}' />"
    with open(basefile + r'\Ice.svg', 'r') as f:
        icemap = bs(f, 'lxml')
    icemap.find('g', id='ice').contents = bs(string, 'lxml').find('body').contents
    save(icemap)


def a_star_sea(start, end, travel=sea_travel, verbose=False, limit=100):

    open_list = [n for n in all_neighbors[start] if n not in hexes and n not in temp_ice]
    if not open_list:
        if verbose: print('No neighbors')
        return None
    closed_list = [start]
    if not limit:
        limit = 1000
    came_from = {o: start for o in open_list}

    def g(current, s=None):

        path = [current]
        while path[-1] != start:
            try:
                path.append(came_from.get(path[-1], s))
            except KeyError:
                pass
        path = path[::-1]

        z = zip(path[:-1], path[1:])
        return round(sum(travel(i, j) for i, j in z), 3)

    def h(current):

        return hexDistance(current, end)

    def f(current):

        return g(current) + h(current)

    came_from = {o: start for o in open_list}

    while open_list:
        s = sorted(open_list, key=lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        if s == end:
            # print(closed_list)
            path = [end]
            break
        for t in (n for n in all_neighbors[s] if n not in hexes and n not in temp_ice):
            if travel(s, t) > 1e8:
                closed_list.append(t)
                continue
            if t not in closed_list:
                if t not in open_list or g(t, s=s) < h(t):
                    open_list.append(t)
                came_from.update({t: s})
        if verbose: print('{:.0f} {:.0f} {:}'.format(f(s), g(s), closed_list))

    path = [end]
    while path[-1:][0] != start:
        try:
            path.append(came_from[path[-1:][0]])
        except KeyError:
            return None
    if route_length(path[::-1]) >= limit:
        return None
    return path[::-1]


def sea_route(a, b, limit=100, verbose=False):

    if a == b:
        return [[a], 0]

    seaway = a_star_sea(a, b, travel=sea_travel, limit=limit, verbose=verbose)

    return [seaway, max(1, route_length(seaway))]


def sea_routes_map():

    string = ''

    # temp = []
    # for t in seaways:
    #     if seaways[t] not in temp:
    #         temp.append(seaways[t])
    # seaways = temp

    for s in seaways:
        string += '<path d="M {:}" id="{:} to {:}"/>'.format(
            ' '.join(['{:},{:}'.format(hexdict[r][0][0], hexdict[r][0][1] + 50) for r in seaways[s][0]]),
            s[0], s[1]
            # s,
            # log10(sociology[h]['infrastructure'] + sociology[i]['infrastructure']) * 5
        )
    if not string:
        with open(basefile + r'\Roads.svg', 'r') as f:
            file = bs(f, 'lxml')
        file.find('g', id='searoutes').clear()
        save(file)
        return
    with open(basefile + r'\Roads.svg', 'r') as f:
        file = bs(f, 'lxml')
    file.find('g', id='searoutes').contents = bs(string, 'lxml').find('body').contents
    save(file)
    # save_seaways(seaways)


def route_length(seaway):

    if not seaway:
        return 0
    t = 1
    for i, j in zip(seaway[:-1], seaway[1:]):
        try:
            d = abs(rawAngle(i, j) - roundBase(currents[i], base=60))
        except TypeError:
            d = 180
        t += 0.5 + d / 180 / math.sqrt(2)
    return math.ceil(t)


def save_seaways():

    with open(seaways_file, 'wb') as f:
        pickle.dump(seaways, f)


def seaways_clear():

    seaways.clear()
    for city in cities:
        if 'seaways' in cities[city]:
            cities[city]['seaways'] = set()


def seaways_generate(clear=True, update_map=False, verbose=True):

    if clear: seaways_clear()
    ports = [c for c in cities if hexes[c]['coastal'] and sociology[c]['infrastructure'] > 25 and cities[c]['race'] != 'orc' and hegemony_tech_level(cities[c]['hegemony']) >= 7]
    # timer(0)
    if verbose:
        print(f"{len(ports)} ports identified")
    for city in sorted(ports, key=lambda x: cities[x]['population']):
        if any(n in perm_ice | temp_ice for n in all_neighbors[city]):
            continue
        targets = [p for p in ports if hexDistance(p, city) < 50]
        if not targets:
            continue
        print(city, cities[city]['name'])
        a = hexes[city].setdefault('harbor', random.choice([n for n in all_neighbors[city] if n not in hexes]))
        # hexes[city]['harbor'] = a
        for target in sorted(targets, key=lambda x: hexDistance(city, x), reverse=True):
            if seaway_check(city, target):
                continue
            if any(n in perm_ice | temp_ice for n in all_neighbors[target]):
                continue
            b = hexes[target].setdefault('harbor', random.choice([n for n in all_neighbors[target] if n not in hexes]))
            # print(target)
            seaway = [None]
            with stopit.ThreadingTimeout(10):
                seaway = sea_route(a, b)
            if seaway[0]:
                # print(route_length(seaway))
                seaway_stops(seaway)
                seaway = [None]
                with stopit.ThreadingTimeout(10):
                    seaway = sea_route(b, a)
                if seaway[0]:
                    # print(route_length(seaway))
                    seaway_stops(seaway)
        save_seaways()
    if update_map:
        sea_routes_map()
        save_seaways()


def seaway_check(a, b):

    if 'harbor' not in hexes[a]:
        return False
    if 'harbor' not in hexes[b]:
        return False
    return (a, b) in seaways.keys()


def seaway_stops(seaway):

    breaks = []
    harbors = dict()
    for s in seaway[0]:
        if s in hexes:
            continue
        if not nearCoast(s, ):
            continue
        for n in all_neighbors[s]:
            if n in cities:
                if sociology[n]['infrastructure'] < 25:
                    continue
                if s == cities[n].get('harbor'):
                    # breaks.append((s, n))
                    breaks.append(n)
                    harbors.update({n: s})
                    continue
                if not cities[n].get('harbor'):
                    cities[n]['harbor'] = s
                    breaks.append(n)
                    harbors.update({n: s})
                    continue
    for a, b in zip(breaks[:-1], breaks[1:]):
        r = sea_route(cities[a]['harbor'], cities[b]['harbor'])
        if not r[0]:
            continue
        cities[a].setdefault('seaways', set()).add(b)
        cities[b].setdefault('seaways', set()).add(a)
        r[0] = [a] + r[0] + [b]
        r[1] = max(1, r[1])
        seaways.update({(a, b): r})


def seaways_flood_fill(start):

    current = {start}
    visited = set()

    while current:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            if c not in cities:
                current.remove(c)
                continue
            current |= cities[c].get('seaways', set())
            visited.add(c)
            current.remove(c)
        # print(len(visited), len(current))

    return visited


def seaways_clean():

    destroyed = []
    for seaway in seaways:
        if seaway[0][0] not in cities or seaway[0][-1] not in cities:
            destroyed.append(seaway)
            continue
        if 'harbor' not in cities[seaway[0][0]] or 'harbor' not in cities[seaway[0][-1]]:
            destroyed.append(seaway)
    seaways[:] = [s for s in seaways if s not in destroyed]