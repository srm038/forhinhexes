from defs import *

load_data(knowledge, fr"{basefile}\knowledge.json")
load_data(discoveries, fr"{tradefilepath}\discoveries.json")

def get_knowledges():

    discoveries.clear()
    file = fr"{tradefilepath}\knowledges.csv"
    with open(file, 'r') as f:
        headers = f.readline().strip().split(',')
        for row in f.readlines():
            if not row.strip().split(',')[0]:
                continue
            discoveries.update({row.strip().split(',')[0]: dict(zip(headers[1:], row.strip().split(',')[1:]))})
    for d in discoveries:
        for c in discoveries[d]:
            discoveries[d][c] = eval(discoveries[d][c].replace(';', ',')) if discoveries[d][c] else ''