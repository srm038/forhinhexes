import random
import re
# from big_phoney import BigPhoney
# from arpabetandipaconvertor import arpabet2phoneticalphabet
# import jellyfish

def makewords():
    vowels = ['i', 'a', 'e', 'ii']
    consonants = ['b', 'd', 'g', 'j', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'z', 'bj', 'dj', 'gj', 'jj', 'lj', 'mj',
                  'nj', 'pj', 'rj', 'sj', 'tj', 'vj', 'zj', 'jr']
    endings = ['\'ir', 'rn', 'd', 'g', 'j', 'm', 'n', 'r', 't', 'v', 'z', 'dj', 'gj', 'jj', 'rj', 'vj', 'zj']
    words = []
    wordsfile = r'tjavanwords.txt'
    open(wordsfile, 'w').close()

    def writeword(w):

        with open(wordsfile, 'a') as f:
            f.write(w + '\n')

    for c0 in consonants:
        for v0 in vowels:
            for c1 in endings:
                # words.append(c0 + v0 + c1)
                writeword(c0 + v0 + c1)

    for c0 in consonants:
        for v0 in vowels:
            for c1 in consonants:
                for v1 in vowels:
                    for c2 in endings:
                        # words.append(c0 + v0 + c1 + v1 + c2)
                        writeword(c0 + v0 + c1 + v1 + c2)

    for c0 in consonants:
        for v0 in vowels:
            for c1 in consonants:
                for v1 in vowels:
                    for c2 in consonants:
                        for v2 in vowels:
                            for c3 in endings:
                                # words.append(c0 + v0 + c1 + v1 + c2 + v2 + c3)
                                writeword(c0 + v0 + c1 + v1 + c2 + v2 + c3)

    print(len(words))


name_base = r'G:\forhinhexes\11 Characters'


def makeword_old():
    vowels = ['i'] * 12 + ['a'] * 6 + ['e'] * 4 + ['ii'] * 3 + [''] * 2
    consonants = ['tj'] * 414 + ['m'] * 331 + ['v'] * 265 + ['bj'] * 212 + ['r'] * 169 + ['n'] * 136 + ['pj'] * 108 + \
                 ['b'] * 87 + ['d'] * 69 + ['mj'] * 56 + ['g'] * 44 + ['j'] * 36 + ['l'] * 28 + ['p'] * 23 + [
                     's'] * 18 + \
                 ['dj'] * 15 + ['t'] * 12 + ['gj'] * 9 + ['z'] * 7 + ['lj'] * 6 + ['nj'] * 5 + ['rj'] * 4 + ['sj'] * 3 + \
                 ['vj'] * 2 + ['zj'] * 2 + ['jr'] * 2 + ['jj'] * 1
    endings = ['dj'] * 554 + ['n'] * 382 + ['rn'] * 263 + ['m'] * 182 + [''] * 150 + ['rj'] * 125 + ['d'] * 86 + \
              ['\'ir'] * 60 + ['v'] * 41 + ['s'] * 30 + ['gj'] * 28 + ['g'] * 20 + ['j'] * 13 + ['r'] * 9 + [
                  't'] * 6 + ['z'] * 4 + \
              ['vj'] * 3 + ['zj'] * 2 + ['jj'] * 1

    word = [random.choice(consonants), random.choice(vowels)]
    wordlength = random.randrange(1, 4)
    if wordlength == 1:
        word.append(random.choice(endings))
    elif wordlength == 2:
        word.append(random.choice(consonants))
        word.append(random.choice(vowels))
        word.append(random.choice(endings))
    elif wordlength == 3:
        word.append(random.choice(consonants))
        word.append(random.choice(vowels))
        word.append(random.choice(consonants))
        word.append(random.choice(vowels))
        word.append(random.choice(endings))
    print(''.join(word).capitalize())


# global vowels, consonants, finals, glides, sibilants, syllables, avoid
#
# language = 'Giant'
# language = 'Dwarfen'
# language = 'Tjavan'
#
# if language == 'Tjavan':
#     vowels = ['i', 'a', 'e']
#     consonants = ['tj', 'm', 'v', 'bj', 'r', 'n', 'g', 'j', 'l', 'p', 'dj', 's', 't', 'z', 'nj', 'jr']
#     finals = ["'ir", 'rn']
#     glides = ['r', 'l']
#     sibilants = ['s']
#     syllables = [
#         'C.V.C.',
#         # 'C.V.V?C.',
#         # 'C.V.V.C?',
#         'C.V.C?',
#         'C.V.',
#         'V.C.',
#         'C.V.F.',
#         'C?V.C.',
#         'C.V.F?',
#         'C.L?V.C.'
#         'C.L?V.F.',
#         'S?C.V.C.',
#         'S?C.V.F.',
#         'S?C.V.C?',
#         'C?V.F.',
#         'C?V.C?',
#         'C?V.F?',
#         'C?L?V.C.',
#         'C.V.L?C?',
#         'C?V.L?C.',
#         'C?V.L.C?',
#     ]
#     avoid = ['tl', 'ml', 'nl', 'rl', 'sv', 'jl']
#     for a in vowels + consonants + sibilants + finals + glides:
#         avoid.append(a + a)
#     for a in ['tj', 'bj', 'pj', 'dj', 'nj', 'jr', 'jj'] + ['tl', 'ml', 'nl', 'rl', 'sv', 'jl']:
#         for b in consonants + sibilants + ['rn'] + glides:
#             avoid.append(a + b)
#         avoid.append(b + a)
# elif language == 'Giant':
#     vowels = ['i', 'a', 'e', 'o', 'u']
#     consonants = ['m', "p", "b", "p'", "f", "v", "n", "t", "d", "t'", "ts", "j", "c'", "s", "z", "l", "r", "tsh", "dj",
#                   "sh", "zh", "y", "k", "g", "kh", "x", "h"]
#     finals = consonants
#     glides = ['r', 'l']
#     sibilants = ['s', 'z', 'sh', 'tsh', "ts", "c'", "zh", "dj"]
#     syllables = [
#         'C.V.C.',
#         # 'C.V.V?C.',
#         # 'C.V.V.C?',
#         'C.V.C?',
#         'C.V.',
#         'V.C.',
#         'C.V.F.',
#         'C?V.C.',
#         'C.V.F?',
#         'C.L?V.C.'
#         'C.L?V.F.',
#         'S?C.V.C.',
#         'S?C.V.F.',
#         'S?C.V.C?',
#         'C?V.F.',
#         'C?V.C?',
#         'C?V.F?',
#         'C?L?V.C.',
#         'C.V.L?C?',
#         'C?V.L?C.',
#         'C?V.L.C?',
#     ]
#     avoid = ['tl', 'ml', 'nl', 'rl', 'jl']
#     for a in vowels + consonants + sibilants + finals + glides:
#         avoid.append(a + a + a)
#         avoid.append(a + a + a + a)
#     for a in ['tj', 'bj', 'pj', 'dj', 'nj', 'jr', 'jj'] + ['tl', 'ml', 'nl', 'rl', 'jl']:
#         for b in consonants + sibilants + ['rn'] + glides:
#             avoid.append(a + b)
#         avoid.append(b + a)
# elif language == 'Dwarfen':
#     vowels = ['i', 'y', 'u', 'e', 'o', 'a']
#     consonants = ['m', 'p', 'b', 'f', 'v', 'n', 't', 'd', 'j', 'ng', 'k', 'g', 'x']
#     finals = consonants
#     glides = ['r', 'l']
#     sibilants = ['s', 'z', 'ts', 'tch', 'ch']
#     syllables = [
#         'C.V.C.',
#         # 'C.V.V?C.',
#         # 'C.V.V.C?',
#         'C.V.C?',
#         'C.V.',
#         'V.C.',
#         'C.V.F.',
#         'C?V.C.',
#         'C.V.F?',
#         'C.L?V.C.'
#         'C.L?V.F.',
#         'S?C.V.C.',
#         'S?C.V.F.',
#         'S?C.V.C?',
#         'C?V.F.',
#         'C?V.C?',
#         'C?V.F?',
#         'C?L?V.C.',
#         'C.V.L?C?',
#         'C?V.L?C.',
#         'C?V.L.C?',
#     ]
#     avoid = ['tl', 'ml', 'nl', 'rl', 'jl']
#     for a in vowels + consonants + sibilants + finals + glides:
#         avoid.append(a + a + a)
#         avoid.append(a + a + a + a)
#     for a in consonants + sibilants + glides:
#         avoid.append(a + a)
#         # for b in consonants + sibilants + glides:
#         #     avoid.append(a + b)
#         #     avoid.append(b + a)
#     # for a in ['tj', 'bj', 'pj', 'dj', 'nj', 'jr', 'jj'] + ['tl', 'ml', 'nl', 'rl', 'jl']:
#     #     for b in consonants + sibilants + ['rn'] + glides:
#     #         avoid.append(a + b)
#     #     avoid.append(b + a)


def makesyllable(fin=0):
    end = False
    while not end:
        structure = random.choice(syllables)
        if not fin and 'F' in structure:
            end = False
        else:
            end = True
    length_ok = False
    avoid_ok = False
    while not length_ok or not avoid_ok:
        avoid_ok = True
        syllable = []
        for a, b in pairwise(structure):
            if b == '?' and roll(2) > 1:
                continue
            else:
                if a == 'C':
                    syllable.append(random.choice(consonants))
                elif a == 'V':
                    syllable.append(random.choice(vowels))
                elif a == 'L':
                    syllable.append(random.choice(glides))
                elif a == 'S':
                    syllable.append(random.choice(sibilants))
                elif a == 'F':
                    syllable.append(random.choice(finals))
        syllable = ''.join(syllable)
        # print(len(syllable))
        length_ok = 12 > len(syllable) > 2
        for n in avoid:
            if n in syllable:
                avoid_ok = False
                # print(n)
    return syllable


def pairwise(it):
    it = iter(it)
    while True:
        yield next(it), next(it)


def makeword(city=False):
    # word_length = random.randrange(1, 3) if language == 'Tjavan' else random.randrange(3, 5)
    if language == 'Tjavan':
        word_length = random.randrange(1, 3)
    elif language == 'Giant':
        word_length = random.randrange(3, 5)
    elif language == 'Dwarfen':
        word_length = random.randrange(2, 4)
    if not city:
        avoid.append('\'ir')

    avoid_ok = False
    while not avoid_ok:
        avoid_ok = True
        word = []
        for i in range(word_length):
            word.append(makesyllable(fin=(i == word_length - 1)))
        word = ''.join(word)
        for n in avoid:
            if n in word:
                avoid_ok = False
    return word.capitalize()


def markov_old(file, n=1):
    from markovwordgen import MarkovWordGen
    with open(file, 'r') as f:
        data = f.read().split('\n')
    word_factory = MarkovWordGen(data)
    return [word_factory.generate() for _ in range(n)]


def markov_chain(file, n=None):
    """
    Creates a 3rd order Markov chain
    :param file: source file for words
    :param n: maximum number of words to use
    :return:
    """

    with open(file, 'r', encoding='utf-8') as f:
        data = f.read().split('\n')
    maxlength = max([len(d) for d in data])
    existing = set([d.lower() for d in data])
    if n:
        random.shuffle(data)
        data = data[:n]
    data = ['^^' + d.lower() + '.' for d in data]
    chain = {}
    # TODO compare 2 and 3 order chain
    for i, word in enumerate(data):
        if len(word) < 4:
            continue
        for j, letter1 in enumerate(word[:-2]):
            letter2 = word[j + 1]
            letter3 = word[j + 2]
            if (letter1, letter2) not in chain:
                chain[(letter1, letter2)] = [letter3]
            else:
                chain[(letter1, letter2)].append(letter3)
    return chain, maxlength#, existing
    # for i, word in enumerate(data):
    #     if len(word) < 4:
    #         continue
    #     for j, letter1 in enumerate(word[:-3]):
    #         letter2 = word[j + 1]
    #         letter3 = word[j + 2]
    #         letter4 = word[j + 3]
    #         if (letter1, letter2, letter3) not in chain:
    #             chain[(letter1, letter2, letter3)] = [letter4]
    #         else:
    #             chain[(letter1, letter2, letter3)].append(letter4)
    # return chain, maxlength


def markov_combine(files):
    master_chain = dict()
    # normalize sizes
    n = 1e9
    for file in files:
        with open(file, 'r', encoding='utf-8') as f:
            data = f.read().split('\n')
        if len(data) < n:
            n = len(data)
    chains = [markov_chain(file, n) for file in files]
    for chain in chains:
        for key in chain[0]:
            master_chain[key] = master_chain.get(key, []) + chain[0][key]
    return master_chain, max(chain[1] for chain in chains)#, set().union(*[chain[2] for chain in chains])


def markov_word(chain, word_length=None):

    if not word_length:
        word_length = max(4, int(random.gauss(6, 2)))

    key = ('^', '^')
    word = ''
    good = False
    while not good:
        good = False
        word = ''
        key = ('^', '^')
        while '.' not in key:
            w = random.choice(chain[0][key])
            word += w
            key = (key[1], w)
        if not word_length and 5 < len(word) <= chain[1]:# and word not in chain[2]:
            good = True
        if word_length and len(word) == word_length + 1:
            good = True
    return cluster_reduction(word[:-1]) #TODO I suspect this is causing part of the slow-down


def get_similar(chain, a, word_length=None, maxtries=100):

    i = 0
    while i < maxtries:
        word = markov_word(chain, word_length=word_length)
        if jellyfish.levenshtein_distance(a, word) < 2:
            return word
        i += 1
    return ''

# def to_ipa(word):
#
#     convertor = arpabet2phoneticalphabet.ARPAbet2PhoneticAlphabetConvertor()
#
#     return convertor.convert_to_international_phonetic_alphabet(BigPhoney().phonize(word))
# from string import ascii_lowercase
# consonants = set(ascii_lowercase) - set('aeiou')

def cluster_reduction(word):

    checks = [('^nt', 't'), ('^([b-df-hj-np-tvxz])([b-df-hj-np-tvxz])([b-df-hj-np-tvxz])', '\g<1>\g<3>'), ('^fg', 'g'),
        ('^c([fv])', '\g<1>'), ('^np', 'anp'), ('^[zg]([b-df-hj-km-npqs-tvxz])', '\g<1>'), ('hh', 'h'),
        ('vv', 'v'), ('^([b-df-hj-km-npqs-tvxz])z', '\g<1>s'), ('ww', 'w')]
    for c in checks:
        word = re.sub(c[0], c[1], word)
    return word
