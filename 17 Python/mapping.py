from defs import *

global worldmap

load_data(climate, climate_file)
load_data(coasts, coastpickle)

# global hexdict
# global coasts

def long(p):
    """

    :param p: page coordinate
    :return: map coordinate
    """
    # TODO determine which hex zone it's in
    x = round((p[0] - north_pole[0]) / 86.6, 2)
    if x % 1 == 0.5:
        x -= 0.5
        x += 1
    else:
        pass

    x = int(x)

    dir = ''

    if x > 0:
        dir = 'E'
    elif x < 0:
        dir = 'W'
    else:
        pass

    return str(x) + dir


def lat(p):
    """

    :param p: page coordinate
    :return: map coordinate
    """

    # y-coordinate is odd thanks to inkscape's spec
    x = round((north_pole[1] - view_box[1] + p[1] + 50) / 75) + 1

    dir = ''
    if x < 207:
        dir = 'N'
    elif x > 207:
        dir = 'S'
    else:
        pass

    x = -abs(x - 207) + 207

    x = int(x)

    return str(x) + dir


def transformation(x1, y1, x2, y2, a1, b1, a2, b2):
    """
    xy is what it says it is
    ab is what it actually is
    :param x1:
    :param y1:
    :param x2:
    :param y2:
    :param a1:
    :param b1:
    :param a2:
    :param b2:
    :return:
    """

    return (
        (a1 * y2 - a2 * y1) / (x1 * y2 - x2 * y1),
        (a2 * x1 - a1 * x2) / (x1 * y2 - x2 * y1),
        (b1 * y2 - b2 * y1) / (x1 * y2 - x2 * y1),
        (b2 * x1 - b1 * x2) / (x1 * y2 - x2 * y1),
    )


def latlong2coords(c):
    pass


def roll(i):
    random.seed()
    return random.randint(1, i)


def parse_style(s):
    style = dict()
    s = s.split(';')
    for i in s:
        j = i.split(':')
        style[j[0]] = j[1]
    return style


def gen():
    r = roll(6)
    subtypes = [1] * 7 + [0] * r + [1] * (6 - r)
    random.shuffle(subtypes)

    print('   ' + str(subtypes[0]))
    print(' '.join(map(str, subtypes[1:5])))
    print(' ' + ' '.join(map(str, subtypes[5:8])))
    print(' '.join(map(str, subtypes[8:12])))
    print('   ' + str(subtypes[-1]))
    # print('   ' + ' '.join(map(str, subtypes[0:2])))
    # print(' '.join(map(str, subtypes[2:7])))
    # print(' ' + ' '.join(map(str, subtypes[7:11])))
    # print(' '.join(map(str, subtypes[11:16])))
    # print(' ' + ' '.join(map(str, subtypes[16:20])))
    # print('    ' + str(subtypes[20]))

    # return subtypes


def snow_line(lat):
    lat = abs(float(lat))

    return int(
        -2.298e-04 * lat ** 5 +
        4.376e-02 * lat ** 4 +
        - 2.715e+00 * lat ** 3 +
        5.304e+01 * lat ** 2 +
        - 7.804e+01 * lat +
        1.586e+04
    )


def timberline(lat):
    lat = float(lat)

    return int(
        -3 * lat ** 2 +
        37 * lat +
        12622
    )




def unparse_id(id):
    x = id[0]
    y = id[1]
    lat, long = '', ''
    if y:
        lat = 'N' if y > 0 else 'S'
    if x:
        long = 'E' if x > 0 else 'W'
    return '{:}{:}-{:}{:}'.format(y, lat, x, long)



global equatorials
# equatorials = [(hexdict[h][0][0], hexdict[h][0][1] + 50) for h in hexes if '207-' in h]
equatorials = [[h for h in hexes if '207-' in h], [h for h in hexes if '206S-' in h]]




    # return math.sqrt((float(h1[0]) - float(h2[0])) ** 2 + (float(h1[1]) - float(h2[1])) ** 2)




def angle(p1, p2, p3):
    """

    :param p1: point 1
    :param p2: point 2
    :param p3: point 3
    :return: the angle between three points in math.degrees
    """

    c = distance(p1, p2)
    a = distance(p2, p3)
    b = distance(p1, p3)

    if a == 0 or c == 0:
        return 0

    try:
        rad = math.acos(round((a ** 2 + c ** 2 - b ** 2) / (2 * a * c), 5))
    except ValueError:
        print(a, b, c, str(a ** 2 + c ** 2 - b ** 2), str(2 * a * c), str((a ** 2 + c ** 2 - b ** 2) / (2 * a * c)))
        return None

    return math.degrees(rad)


# global hexes
# global coasts
# global hexes_by_id
# global jul_temps, jan_temps
# hexes = {}
# coasts = []
# hexes_by_id = {}
# jul_temps = {}
# jan_temps = {}


# def fix_svg():
#     """
#     Cleans out a bunch of inkscape crap
#     :return:
#     """
#
#     timer(0)
#     print('Cleaning up SVG...')
#     initial = 0
#     # for f in [worldmapfile, labelfile, influencesfile, temperaturesfile, elevationfile, windfile, riverfile, terrainfile, coastfile, pressurefile]:
#     for f in [worldmap, labelmap, influencesmap, temperaturesmap, elevationmap, windmap, rivermap,
#               terrainmap, coastmap, pressuremap]:
#         initial += sys.getsizeof(str(f))
#         for p in f.find_all('path'):
#             del (p['inkscape:connector-curvature'])
#             del (p['sodipodi:nodetypes'])
#
#     for p in terrainmap.find('g', id='Terrain').find_all('path'):
#         del (p['class'])
#         try:
#             p['style'] = 'fill:' + get_property(p, 'fill')
#         except ValueError:
#             pass
#
#     # for p in pressuremap.find_all('path'):
#     #     del (p['id'])
#     #     try:
#     #         p['style'] = 'fill:' + get_property(p, 'fill')
#     #     except ValueError:
#     #         pass
#
#     for p in elevationmap.find('g', id='Elevation').find_all('path'):
#         del (p['class'])
#         try:
#             p['style'] = 'fill:' + get_property(p, 'fill')
#         except ValueError:
#             pass
#
#     # for p in windmap.find('g').find_all('path'):
#     #     # color = get_property(p, 'fill')
#     #     del (p['id'])
#
#     for p in labelmap.find('g', id='Hexes').find_all('text'):
#         # color = get_property(p, 'fill')
#         del (p['style'])
#
#     for p in labelmap.find('g', id='Altitude').find_all('text'):
#         # color = get_property(p, 'fill')
#         del (p['style'])
#
#     for p in coastmap.find('g', id='Coast-Rough').find_all('path'):
#         # color = get_property(p, 'fill')
#         del (p['style'])
#         del (p['id'])
#
#     for p in coastmap.find('g', id='Coast-Fine').find_all('path'):
#         # color = get_property(p, 'fill')
#         del (p['style'])
#         del (p['id'])
#
#     # for p in influencesmap.find('g', id = 'Influences').find_all('path'):
#     #     # color = get_property(p, 'fill')
#     #     del (p['id'])
#     #     del (p['class'])
#     #     try:
#     #         p['style'] = 'fill:' + get_property(p, 'fill')
#     #     except ValueError:
#     #         pass
#
#     for p in temperaturesmap.find('g', id='Temperature').find_all('path'):
#         # color = get_property(p, 'fill')
#         del (p['id'])
#         del (p['class'])
#         try:
#             p['style'] = 'fill:' + get_property(p, 'fill')
#         except ValueError:
#             pass
#
#     # for p in worldmap.find_all('g'):
#     #     try:
#     #         p['class']
#     #         p['style'] = 'display:' + get_property(p, 'display')
#     #     except KeyError:
#     #         pass
#     #     except AttributeError:
#     #         pass
#
#     final = 0
#     # for f in [worldmapfile, labelfile, influencesfile, temperaturesfile, elevationfile, windfile, riverfile,
#     #           terrainfile, coastfile, pressurefile]:
#     for f in [worldmap, labelmap, influencesmap, temperaturesmap, elevationmap, windmap, rivermap,
#               terrainmap, coastmap, pressuremap]:
#         final += sys.getsizeof(str(f))
#         save(f)
#     print('{:.3f} MB saved'.format((initial - final) / 1e6))
#     print('{:} s'.format(timer(1)))


def timer(state):
    """
    """

    global start, stop

    if state != 0 and state != 1:
        # math.log('Timer is broken')
        return

    if not state:
        start = datetime.now()
    else:
        stop = datetime.now()
        # print(str((stop - start).seconds) + " s")
        # math.log(str((stop - start).seconds) + " s")
        return (stop - start).seconds


# def load_frompickle():
#
#     global hexes, hexes_by_id

# print(len(hexes))
# for h in hexes:
#     hexes_by_id.update({h.id: h})


def load(labels=False):
    global hexes, coasts
    # hexes = []

    # TODO automagically calculate the points of the hexes

    if 0:
        fix_svg()

    # load_temps()
    load_hexes()
    load_coasts()
    assign_coasts()

    if labels:
        add_hex_labels()
        add_drainage_labels()
        add_elevation_labels()

    with open(worldmapfile, 'r') as f:
        worldmap = bs(f, 'lxml')

    save(worldmap)


def avg(x):
    """
    :param x: a list
    :return: the average of the list
    """

    s = 0
    n = 0

    for i in x:
        if i is None:
            s = s
            n = n
        else:
            s += float(i)
            n += 1

    if n == 0:
        return 0
    else:
        return s / n




import noise








import subprocess


def export_png(f, n=0):

    timer(0)
    # change to Inkscape program dir
    os.chdir(r'G:\Program Files\Inkscape')
    fname = basefile + r'\{:}.svg'.format(f.find('svg')['sodipodi:docname'][:-4])
    fname_png = basefile + r'{:}\{:}.png'.format(f.find('svg')['sodipodi:docname'][:-4], n)
    proc = subprocess.Popen(['inkscape.exe', "-f", fname, "-e", fname_png, "-d", '10', "-b", "#ffffff"])
    proc.wait()
    print('exported to png, {:} s'.format(timer(1)))


# from hydromath.logy import reset_elevation



from math import atan




def current_temp():

    with open(basefile + r'\currents.p', 'rb') as f:
        currents = pickle.load(f)
    current_temps = dict()

    sector_rotate = {
        1: 60 * 2,
        2: 60 * 3,
        3: -60 * 2,
        4: 60,
        5: 0,
        6: -60,
        7: -60,
        8: 0,
        9: 60 * 1,
        10: -60 * 2,
        11: -60 * 3,
        12: 60 * 2
    }

    for c in currents:
        if nearCoast(c, ) and currents[c] is not None and c in hexdict:
            target = latlong((
                round(hexdict[c][0][0] + 50 * cos(radians(currents[c])) * math.sqrt(3), 2),
                round(hexdict[c][0][1] + 50 * sin(radians(currents[c])), 2)
            ))
            if target not in hexes and target in hexdict and not nearCoast(target, ):
                continue
            lat = math.copysign(90 - int(abs(parseID(c)[1]) * 90 / 207), parseID(c)[1])
            t = round(0.69 / 2 * atan(radians(-10 * abs(lat) + 450)) + 0.5, 2)
            theta = currents[c] - sector_rotate[latlong((hexdict[c][0][0], hexdict[c][0][1]), id=True, sector=True)]
            theta %= 360
            temp = min(1, max(0, t))
            if 0 < lat <= 45:
                if 45 <= theta <= 135:
                    temp = 0.5
                elif 225 <= theta <= 315:
                    temp = 1
            elif 45 < lat <= 90:
                if 45 <= theta <= 135:
                    temp = 0
                elif 225 <= theta <= 315:
                    temp = 0.5
            elif -45 < lat <= 0:
                if 45 <= theta <= 135:
                    temp = 1
                elif 225 <= theta <= 315:
                    temp = 0.5
            elif -90 < lat <= 45:
                if 45 <= theta <= 135:
                    temp = 0.5
                elif 225 <= theta <= 315:
                    temp = 0
            current_temps.update({c: temp})

    string = ''
    for s in currents:
        if currents[s] is not None:
            if s in current_temps:
                col = '#{:02x}{:02x}{:02x}'.format(
                    int((255 - 0) * current_temps.get(s, 0) + 0),
                    int((0 - 0) * current_temps.get(s, 0) + 0),
                    int((0 - 255) * current_temps.get(s, 1) + 255),
                )
            else:
                col = '#bbb'
            string += '<use xlink:href="#current" transform="translate ({:} {:}) scale({:.3f}) rotate ({:.0f}) rotate({:})" id="{:}" style="color:{:}"/>'.format(
                hexdict[s][0][0], hexdict[s][0][1] + 50, 2.5, currents[s], 0, s, col
            )
    with open(basefile + r'\Currents.svg', 'r') as f:
        currentmap = bs(f, 'lxml')
    currentmap.find('g', id='Currents-Gen').contents = bs(string, 'lxml').find('body').contents
    save(currentmap)

    with open(basefile + r'\currenttemps.p', 'wb') as f:
        pickle.dump(current_temps, f)


def wind_over_sea(hexes=hexes):

    timer(0)
    sector_rotate = {
        1: 60 * 2,
        2: 60 * 3,
        3: -60 * 2,
        4: 60,
        5: 0,
        6: -60,
        7: -60,
        8: 0,
        9: 60 * 1,
        10: -60 * 2,
        11: -60 * 3,
        12: 60 * 2
    }

    winds = {h: {'Jan': (0, 0), 'Jul': (0, 0)} for h in hexdict}
    seasons = ['Jan', 'Jul']
    # TODO land pressure centers
    # hp = {
    #     'Jan': {'138S-106E', '137S-147E', '137S-272E', '138S-409W', '138S-178W', '172N-102W', '102N-88E' , '197N-247E',
    #             '142N-228E', '153N-347E', '156N-375W', '123S-75W', '153S-231E', '144S-358W'},
    #     'Jul': {'137N-79E', '137N-190E', '138N-299E', '138N-406W', '137N-114W'}
    # }
    # lp = {
    #     'Jan': {'0N-0', '123S-75W', '153S-231E', '144S-358W', '0S-0'},
    #     'Jul': {'172N-102W', '102N-88E' , '197N-247E', '142N-228E', '153N-347E', '156N-375W'}
    # }
    pressures = {'Jan': {}, 'Jul': {}}

    # setup initial pressures
    # go 6 around or 2 additional?
    # for cell in hp['Jan']:
    #     if 'N' in cell:
    #         i = 1 # cw
    #     elif 'S' in cell:
    #         i = 0 # ccw
    #     for n in neighbors(cell, land=False):
    #         for p in all_neighbors[n]:
    #             for q in all_neighbors[p]:
    #                 for r in all_neighbors[q]:
    #                     for s in all_neighbors[r]:
    #                         for t in all_neighbors[s]:
    #                             for u in all_neighbors[t]:
    #                                 theta = (round(math.degrees(atan2(
    #                                     hexdict[cell][0][1] - hexdict[u][0][1],
    #                                     hexdict[u][0][0] - hexdict[cell][0][0],
    #                                 ))) + 90 + i * 180) % 360
    #                                 winds[u]['Jan'] = (100, theta)
    # for cell in lp['Jan']:
    #     if 'N' in cell:
    #         i = 0 # ccw
    #     elif 'S' in cell:
    #         i = 1 # cw
    #     for n in neighbors(cell, land=False):
    #         for p in all_neighbors[n]:
    #             for q in all_neighbors[p]:
    #                 for r in all_neighbors[q]:
    #                     for s in all_neighbors[r]:
    #                         for t in all_neighbors[s]:
    #                             for u in all_neighbors[t]:
    #                                 theta = (round(math.degrees(atan2(
    #                                     hexdict[cell][0][1] - hexdict[u][0][1],
    #                                     hexdict[u][0][0] - hexdict[cell][0][0],
    #                                 ))) + 90 + i * 180) % 360
    #                                 winds[u]['Jan'] = (20, theta)

    with open(basefile + r'\Pressure.svg', 'r') as f:
        pressuremap = bs(f, 'lxml')
    for season in seasons:
        # string = ''
        # windmap.find(id=season).clear()
        visited = set()
        for p in pressuremap.find('g', id=season).find_all('path'):
            path = parse_coordinate_list(clean_path(p['d']))
            for a, b in zip(path[:-1], path[1:]):
                h = latlong(a)
                if h in visited:
                    continue
                else:
                    visited.add(h)
                ang = int(math.degrees(atan2(b[1] - a[1], b[0] - a[0]))) % 360
                winds[h][season] = (20, ang)
        #         print(h, int(math.degrees(atan2(b[1] - a[1], b[0] - a[0]))) % 360)
        #         for b in barbs:
        #             if b[0] <= 20 < b[1]:
        #                 barb = barbs[b]
        #                 break
        #         else:
        #             barb = None
        #         if barb and 20 >= 1:
        #             string += '<use xlink:href="#{:}" transform="translate ({:} {:}) scale({:.3f}) rotate ({:.0f}) rotate({:})" id="{:}" style="color:{:}"/>'.format(
        #                 barb, hexdict[h][0][0], hexdict[h][0][1] + 50, 0.75, ang,
        #                                               0, h, '#44f'
        #                 # if winds[h][season][0] == 10 else '#000'
        #             )
        #     windmap.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
        #     save(windmap)

    # trade_winds
    for w in winds:
        lat = parseID(w)[1]
        if abs(lat) in [207, 206]:
            winds[w]['Jan'] = (20, (180 - sector_rotate[getSector(w)]) % 360)
            winds[w]['Jul'] = (20, (180 - sector_rotate[getSector(w)]) % 360)
        elif abs(lat) in [103]:
            winds[w]['Jan'] = (20, (0 - sector_rotate[getSector(w)]) % 360)
            winds[w]['Jul'] = (20, (0 - sector_rotate[getSector(w)]) % 360)
        elif abs(parseID(w)[1]) in [20]:
            winds[w]['Jan'] = (20, (0 - sector_rotate[getSector(w)]) % 360)
            winds[w]['Jul'] = (20, (0 - sector_rotate[getSector(w)]) % 360)

        # elif lat in [138] and w not in hexes: # NH/HP/Jan
        #     winds[w]['Jan'] = (20, (0 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [139] and w not in hexes: # NH/HP/Jan
        #     winds[w]['Jan'] = (20, (180 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [80] and w not in hexes: # NH/LP/Jan
        #     winds[w]['Jan'] = (20, (180 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [81] and w not in hexes: # NH/LP/Jan
        #     winds[w]['Jan'] = (20, (0 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [56] and w not in hexes: # NH/LP/Jul
        #     winds[w]['Jul'] = (20, (180 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [57] and w not in hexes: # NH/LP/Jul
        #     winds[w]['Jul'] = (20, (0 - sector_rotate[get_sector(w)]) % 360)
        #
        # elif lat in [-138] and w not in hexes: # SH/HP/Jul
        #     winds[w]['Jul'] = (20, (0 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [-139] and w not in hexes: # SH/HP/Jul
        #     winds[w]['Jul'] = (20, (180 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [-80] and w not in hexes: # SH/LP/Jul
        #     winds[w]['Jul'] = (20, (180 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [-81] and w not in hexes: # SH/LP/Jul
        #     winds[w]['Jul'] = (20, (0 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [-56] and w not in hexes: # SH/LP/Jan
        #     winds[w]['Jan'] = (20, (180 - sector_rotate[get_sector(w)]) % 360)
        # elif lat in [-57] and w not in hexes: # SH/LP/Jan
        #     winds[w]['Jan'] = (20, (0 - sector_rotate[get_sector(w)]) % 360)

        else:
            continue
    # westerlies
    # easterlies

    for season in seasons:

        sources = [w for w in winds if winds[w][season][0] != 0]
        n = 3
        for w in winds:
            if not nearCoast(w, ) or (w in hexes and not hexes[w]['coastal']):
                continue
            if w in sources:
                continue
            idw_neighbors = [(s, round(distance(w, s), 2)) for s in sources if w != s]
            # idw_neighbors = [(s, hex_distance(w, s)) for s in sources if w != s]
            if not idw_neighbors:
                continue
            idw = sum(
                winds[s][season][1] / d ** n for s, d in idw_neighbors if d
            ) / sum(
                1 / d ** n for s, d in idw_neighbors if d
            )
            winds[w][season] = (20, int(idw))
        with open(basefile + r'\winds.p', 'wb') as f:
            pickle.dump(winds, f)

        with open(basefile + r'\Winds-Gen.svg', 'r') as f:
            windmap = bs(f, 'lxml')
        string = ''
        windmap.find(id=season + '-Sea').clear()
        for h in winds:
            if h in hexes:
                continue
            if winds[h][season]:
                for b in barbs:
                    if b[0] <= winds[h][season][0] < b[1]:
                        barb = barbs[b]
                        break
                else:
                    barb = None
                if barb and winds[h][season][0] >= 1:
                    string += '<use xlink:href="#{:}" transform="translate ({:} {:}) scale({:.3f}) rotate ({:.0f}) rotate({:})" id="{:}" style="color:{:}"/>'.format(
                        barb, hexdict[h][0][0], hexdict[h][0][1] + 50, 0.75 + 0 * 0.25 * winds[h][season][0], -winds[h][season][1],
                                                      0, h, '#44f'
                        # if winds[h][season][0] == 10 else '#000'
                    )
        windmap.find('g', id=season + '-Sea').contents = bs(string, 'lxml').find('body').contents
        save(windmap)
    print('{:} s'.format(timer(1)))


def find_low_coast():
    print('Finding extremely low coasts...', end='', flush=True)
    timer(0)
    threshold = 50
    global hexes
    string = ''
    for h in hexes:
        if terrain[h]['altitude'] <= threshold:
            # col = '{:02x}'.format(int(terrain[h]['altitude']) // 100) * 3 if terrain[h]['altitude'] > threshold else 'ff0000'
            string += '<path d="M {:} Z" id="{:}" style="fill:#ff0000" />'.format(
                ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]), h
            )
    else:
        with open(basefile + r'\Coast.svg', 'r') as f:
            file = bs(f, 'lxml')
        file.find('g', id='lowland').contents = bs(string, 'lxml').find('body').contents
        save(file, extra_file=basefile + r'\Coast.svg')
        print('{:} s'.format(timer(1)))
    print('There are {:} hex(es) lower than 10 feet'.format(len([h for h in hexes if terrain[h]['altitude'] <= threshold])))


def load_temps():
    global jul_temps, jan_temps
    jul_temps = {}
    for p in temperaturesmap.find('g', attrs={'inkscape:label': 'Temperature'}).find('g', attrs={
        'inkscape:label': 'Jul'}).find_all('path'):
        id = latlong(eval(p.get('d').split(' ')[1]))
        t = dict(x.split(':') for x in p.get('style').split(';'))['fill']
        jul_temps.update({id: t})
    print('# of July temperature hexes: {:}'.format(len(jul_temps)))

    # get the January temperatures
    jan_temps = {}
    for p in temperaturesmap.find('g', attrs={'inkscape:label': 'Temperature'}).find('g', attrs={
        'inkscape:label': 'Jan'}).find_all('path'):
        id = latlong(eval(p.get('d').split(' ')[1]))
        t = dict(x.split(':') for x in p.get('style').split(';'))['fill']
        jan_temps.update({id: t})
    print('# of January temperature hexes: {:}'.format(len(jan_temps)))


def jitter(pt):
    x, y = pt[0], pt[1]
    for j in range(-500, 500):
        for k in range(-500, 500):
            x, y = x + j / 1000, y + k / 1000
            id = latlong((math.floor(x), math.floor(y)), terr=True)
            if id in hexdict:
                return id
            # x, y = x - j / 1000, y + k / 1000
            # id = latlong((x, y), terr = True)
            # if id in hexdict:
            #     return id
            # x, y = x + j / 1000, y - k / 1000
            # id = latlong((x, y), terr = True)
            # if id in hexdict:
            #     return id
            # x, y = x - j / 1000, y - k / 1000
            # id = latlong((x, y), terr = True)
            # if id in hexdict:
            #     return id
    else:
        print('Point not found in hexdict: {:}'.format(pt))


def load_hexes():
    split_terrain()
    timer(0)  # 20 s
    i = 0
    ids = set()
    # global hexes
    hexes.clear()
    # hexes_by_id = {}
    for p in terrainmap.find('g', id='Terrain').find_all('path'):
        if len(p['d']) == 1:
            p.decompose()
            continue
        try:
            id = latlong(eval(p['d'].split(' ')[1]), terr=True)
        except SyntaxError:
            print('{:}'.format(len(p['d'])))
            continue
        except IndexError:
            print('{:}'.format(len(p['d'])))
            continue
        if id not in hexdict:
            print('Hexdict error: {:}, {:}'.format(id, p['d']))
            p['style'] = 'fill:#00f'
            continue
        h = Hex()
        h.x = eval(p.get('d').split(' ')[1])[0]
        h.y = eval(p.get('d').split(' ')[1])[1] + 50
        # try:
        #     hexdict[id]
        # except KeyError:
        #     for j in range(0, 50):
        #         for k in range(0, 50):
        #             h.x, h.y = h.x + j / 100, h.y + k / 100
        #             id = latlong((h.x, h.y), terr = True)
        #             if id in hexdict:
        #                 break
        #             h.x, h.y = h.x - j / 100, h.y - k / 100
        #             id = latlong((h.x, h.y), terr = True)
        #             if id in hexdict:
        #                 break
        #     print('{:} error'.format(id))
        # if id not in hexdict:
        #     hexdict.update({id: parse_coordinate_list(p)})
        h.id = id
        h.sector = latlong(eval(p.get('d').split(' ')[1]), terr=True, sector=True)
        h.c = (h.x, h.y)
        h.pts = hexdict[id]
        # p['d'] = 'M ' + ' '.join(['{:},{:}'.format(i[0], i[1]) for i in h.pts]) + ' Z'd
        if id in ids:
            print('Duplication Error: {:}, {:}'.format(i, id))
            p['id'] = id + '-duplicate'
            p['style'] = 'fill:#f00'
            # TODO if there is a duplicate, we need to check to see which is the real one and fix it
            # TODO something is rotten in the state of denmark - sectors 9 & 12
            # test_id = str(id)
            # this_id = eval(p['d'].split(' ')[1])
            # other_id = eval(hexes[id]['pts'][0])
            # if jitter(this_id) == test_id:
            #     # this is the real one
            #     pass
            # p['id'] = 'terr' + str(i)
        else:
            p['id'] = id
            ids.add(id)
        h.latitude = round(int(parseID(id)[1]) * -90 / 207 + 90, 2)
        try:
            # TODO eventually we won't need to read this, we'll set it from the climate data
            h.terrain += color2terr[dict(x.split(':') for x in p.get('style').split(';'))['fill'].upper()]
        except KeyError:
            pass
        if id in ids:
            hexes.update({id: h})
        # print(h.id, h.terrain
        i += 1
    else:
        fix_latlong()
        # assign_neighbors()
        # terrainmap.find('g', id = 'Terrain').sorted(key = lambda p: p['id'])
        save(terrainmap)
        # pickle.dump(hexdict, open(hex_points_file, 'wb'))
    # TODO delete the ones that aren't there
    print('# of terrain hexes: {:} ({:} square miles)'.format(len(hexes), len(hexes) * 346))
    print('{:.0%} land, {:.0%} ocean'.format(len(hexes) / len(hexdict), 1 - len(hexes) / len(hexdict)))
    print('{:} s'.format(timer(1)))


def assign_neighbors():
    timer(0)
    global hexes
    print('Wont you be my neighbor? ', end='', flush=True)
    for h in hexes:
        hexes[h]['neighbors'] = neighbors(h)
    else:
        with open(hexpickle, 'wb') as f:
            pickle.dump(hexes, f)
    print('{:} s'.format(timer(1)))


def simplify_coasts():
    """
    Can't handle complex paths
    :return:
    """

    timer(0)
    print('Getting coasts...')
    # global coasts
    # coasts = []
    for coast in coastmap.find('g', id='Coast-Fine').find_all('path'):
        c = parse_coordinate_list(coast)
        original = path_length(c)
        simple = []
        for i, j, k in list(zip(c[0::2], c[1::2], c[2::2])):
            if distance(i, k) > 50 or distance(j, k) > 50:
                simple.append(i)
                simple.append(j)
                continue
            if angle(i, j, k) > 150:
                simple.append(i)
            else:
                simple.append(i)
                simple.append(j)
            if k == c[-1]:
                simple.append(k)
        fixed = path_length(simple)
        # if fixed != original:
        #     print('{:.0%}'.format(fixed / original))
        fixed = 'M ' + ' '.join([','.join(str(j) for j in i) for i in simple]) + ' Z'
        coast['d'] = fixed
    else:
        save(coastmap)
    for coast in coastmap.find('g', id='Coast-Fine').find_all('path'):
        c = parse_coordinate_list(coast)
        original = path_length(c)
        simple = []
        for i, j, k in list(zip(c[0::2], c[1::2], c[2::2])):
            if distance(i, k) > 50 or distance(j, k) > 50:
                simple.append(i)
                simple.append(j)
                continue
            if angle(i, j, k) > 175:
                simple.append(i)
            else:
                simple.append(i)
                simple.append(j)
            if k == c[-1]:
                simple.append(k)
        fixed = path_length(simple)
        # if fixed != original:
        #     print('{:.0%}'.format(fixed / original))
        fixed = 'M ' + ' '.join([','.join(str(j) for j in i) for i in simple]) + ' Z'
        coast['d'] = fixed
    else:
        save(coastmap)
    print('{:} s'.format(timer(1)))


def create_coastal_mask():
    """
    For reasons unknown to me, Inkscape cannot understand clippath...but it knows clipPath.
    To get this to display correctly:
        the url(#clipCoast) element must be in the style header, NOT in the actual tag
        the entire clipping path must be copied over to the defs under the use tag
        clipPath must be in camelCase
    :return:
    """

    with open(worldmapfile, 'r') as f:
        worldmap = bs(f, 'lxml')
    with open(coastfile, 'r') as f:
        coastmap = bs(f, 'lxml')
    with open(basefile + r'\Terrain.svg', 'r') as f:
        terrainmap = bs(f, 'lxml')

    try:
        terrainmap.find('clipPath', id='clipCoast').decompose()
        worldmap.find('clipPath', id='clipCoast').decompose()
    except:
        pass
    total_coast = ' '.join([c['d'] for c in coastmap.find('g', id='Coast-Fine').find_all('path')])
    worldmap.find('defs').append(
        worldmap.new_tag('clipPath', **{'id': 'clipCoast'})
    )
    worldmap.find('clipPath').append(
        worldmap.new_tag('path', **{'d': total_coast, 'clip-rule': 'evenodd'})
    )
    terrainmap.find('defs').append(
        worldmap.new_tag('clipPath', **{'id': 'clipCoast'})
    )
    terrainmap.find('clipPath').append(
        worldmap.new_tag('path', **{'d': total_coast, 'clip-rule': 'evenodd'})
    )
    save(worldmap)
    save(terrainmap)


def load_coasts():
    global hexes, coasts

    with open(coastfile, 'r') as f:
        coastmap = bs(f, 'lxml')

    timer(0)
    print('Getting coasts...', end='', flush=True)
    coasts.clear()
    for p in coastmap.find('g', id='Coast-Fine').find_all('path'):
        p['d'] = clean_path(p['d'])
        # p['d'] = 'M ' + ' '.join(['{:.2f},{:.2f}'.format(pt[0], pt[1]) for pt in parse_coordinate_list(p)]) + ' Z'
    for p in coastmap.find('g', id='Coast-Fine').find_all('path'):
        try:
            if 'Z M' in p['d']:
                for a in split_path(p, coastmap):
                    coasts.append(parse_coordinate_list(a))
            else:
                coasts.append(parse_coordinate_list(p))
        except TypeError:
            print(p.get('id'), p)
        except AttributeError:
            print(p.get('id'), p)
    else:
        save(coastmap)
        with open(coastpickle, 'wb') as f:
            pickle.dump(coasts, f)
    print('{:} s'.format(timer(1)))


def assign_coasts(hexes=hexes):

    timer(0)
    print('Assigning coastal regions...')
    i = 0
    with open(coastpickle, 'rb') as f:
        coasts = pickle.load(f)
    coastchain = sum(coasts, [])
    for h in hexes:
        hexes[h]['coastal'] = 0
    print('Brute force...')
    for h in sorted(list(hexes), key=lambda x: len(hexes[x]['neighbors'])):
        # TODO this is actually a terrible way to do this
        if len([n for n in hexes[h]['neighbors'] if n in hexes]) < 6:
            hexes[h]['coastal'] = 1
            terrain[h]['terrain'] += 'o'
            continue
    print('Interior...')
    for c in coastchain:
        h = latlong(c)
        if h not in hexes or '207-' in h:
            continue
        hexes[h]['coastal'] = 1
        terrain[h]['terrain'] += 'o'
    for h in hexes:
        terrain[h]['terrain'] = ''.join(set(terrain[h]['terrain']))
    n = sum(1 for h in hexes if hexes[h]['coastal'])
    print('# of coasts: {:} (~{:.0f} miles)'.format(len(coasts), sum([path_length(c) for c in coasts]) / 86.6 * 20))
    print('Coastal hexes: {:}'.format(n))
    print('{:} s'.format(timer(1)))




def fix_coasts():
    """
    Removes hexes that are outside the coastline - saves a trip into the very large crashy SVG
    :return:
    """

    timer(0)
    to_clean = []
    split_terrain()
    print('Creating temporary mpl Paths...')
    coast_paths = [mplPath.Path(np.array(c), closed=True) for c in coasts]
    hex_paths = {h: mplPath.Path(np.array(hexes[h]['pts']), closed=True) for h in hexes}
    print('{:} s'.format(timer(1)))
    for h in hexes:
        hexes[h]['land'] = hexes[h]['coastal']
        if hexes[h]['land'] == 0:
            for c in coast_paths:
                if c.intersects_path(hex_paths[h]):
                    hexes[h]['land'] += 1
                    # break
        if hexes[h]['land'] in [0, 2]:
            to_clean.append(h)
    else:
        print('{:} extra hexes ({:} s)'.format(len(to_clean), timer(1)))
        timer(0)
        # print('{:} s'.format(timer(1)))
    # for c in to_clean:
    #     try:
    #         terrainmap.find('path', id = 'terrain: ' + c)['style'] = 'fill:#ff0000'
    #     except NameError:
    #         print(c)
    #     except TypeError:
    #         print(c)
    for p in terrainmap.find_all('path'):
        if p['id'] in to_clean:
            p['style'] = 'fill:#ff0000'
    else:
        save(terrainmap)
        del coast_paths
        del hex_paths
    print('{:} s'.format(timer(1)))


def add_hex_labels():
    """
    Deletes all existing hex labels.
    Adds text to the label SVG for coordinate values
    :return:
    """

    timer(0)  # 232 s
    print('Fixing hex labels...')
    with open(basefile + r'\Labels.svg', 'r') as f:
        file = bs(f, 'lxml')
    # TODO only fix the ones that are wrong...should be quicker than this since I/O is heavy?
    style = "font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;font-size:5.3333333px;" \
            "font-family:fontin;-inkscape-font-specification:'fontin Bold';text-align:center;" \
            "letter-spacing:0px;word-spacing:0px;text-anchor:middle;fill:#000000;fill-opacity:1;stroke:none;" \
            "stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;"
    # clear the existing hex label
    file.find('g', id='Hexes').clear()
    string = ''
    for h in elevation:
        if elevation[h]['altitude'] <= sea_level:
            continue
        string += '<text id="{:}" x="{:.02f}" y="{:.02f}">{:}</text>\n'.format(
            h, hexdict[h][0][0], hexdict[h][0][1] + 50, h
        )
        # # try:
        # t = labelmap.new_tag('text', id=hexes[h]['id'], x=hexes[h]['x'], y=hexes[h]['y'])
        # t.string = hexes[h]['id']
        # labelmap.find('g', id='Hexes').append(t)
        # t.new_tag('tspan')
        # except AttributeError:
        #     pass
    else:
        # print('Saving...')
        file.find('g', id='Hexes').contents = bs(string, 'lxml').find('body').contents
        save(file)
    print('{:} s'.format(timer(1)))


def add_drainage_labels():
    """
    Deletes all existing drainage labels.
    Adds text to the label SVG for drainage values
    :return:
    """

    timer(0)  # 232 s
    print('Fixing drainage labels...')
    with open(basefile + r'\Labels.svg', 'r') as f:
        file = bs(f, 'lxml')
    # clear the existing hex label
    file.find('g', id='Drainage').clear()
    string = ''
    for h in (x for x in hexes if terrain[x]['altitude'] and climate[x]['drainage'] > 2):
        try:
            string += '<text id="{:}-drainage" x="{:.02f}" y="{:.02f}">{:}</text>\n'.format(
                h, hexes[h]['x'], hexes[h]['y'] + 245, climate[h]['drainage']
            )
            # if climate[h]['drainage'] > 2:
            # t = labelmap.new_tag('text', id=h + ' drainage', x=hexes[h]['x'], y=hexes[h]['y'] + 45)
            # t.string = str(climate[h]['drainage'])
            # labelmap.find(id='Drainage').append(t)
        except AttributeError:
            pass
    else:
        # print('Saving...')
        file.find('g', id='Drainage').contents = bs(string, 'lxml').find('body').contents
        save(file)
    print('{:} s'.format(timer(1)))


def load_cities():
    pass


def update_cities():
    pass


def update_list():
    """

    :return:
    """

    already_added = set()
    with open(resource_file, 'r') as f:
        next(f)
        for line in f:
            if not int(line.split(',')[0]):
                already_added.add(line.split(',')[1] + '-' + line.split(',')[2])
    with open(resource_file, 'a') as f:
        for h in hexes:
            if hexes[h]['id'] not in already_added:
                f.write('{:},{:},{:},{:},{:}\n'.format(2, hexes[h]['id'].split('-')[0], hexes[h]['id'].split('-')[1], 0,
                                                       terrain[h]['terrain']))


def split_path(p, f):
    if 'Z M' not in p['d']:
        return [p]
    cleaned = clean_path(p['d'])
    new_paths = ' M'.join(cleaned.split('Z M')).split(' M')
    completed = []
    for n in new_paths:
        if n == new_paths[0]:
            completed.append(n + ('Z' if n[-1] == ' ' else ' Z'))
        elif n == new_paths[-1]:
            completed.append('M' + n)
        else:
            completed.append('M' + n + ('Z' if n[-1] == ' ' else ' Z'))
    return [
        f.new_tag('path', d=c, **{'style': p.get('style')}) for c in completed
    ]


def fix_terrain_colors():
    """
    Adjusts hex colors based on actual terrain/topography/climate
    :return:
    """

    timer(0)
    for h in hexes:
        if type(terrain[h]['altitude']) == int and snow_line(hexes[h]['latitude']) <= terrain[h]['altitude']:
            try:
                terrainmap.find('path', id='terrain: ' + h)['style'] = 'fill:#ddffff'
            except TypeError:
                print(h)
    else:
        save(terrainmap)
    print('{:} s'.format(timer(1)))


# pressures = {'Jul': {}, 'Jan': {}}
# pressures = pickle.load(open(pressurepickle, 'rb'))


# def fix_pressure_old(season):


def fix_pressure(season):
    """
    Grabs pressure values from the pressure SVG
    :param season: Jan (winter) or Jul (summer)
    :return:
    """

    global pressures

    with open(basefile + r'\Pressure-' + season + '.svg', 'r') as f:
        pressuremap = bs(f, 'lxml')

    if not season:
        print('No season selected!')
        return

    pressures[season].clear()

    timer(0)
    print('Breaking apart paths...')
    i = 0
    j = 0
    cleanup = []
    broken = []
    for p in pressuremap.find('g', id='Pressure-' + season + '-Raw').find_all('path'):
        if 'Z M' in p['d'] or ' M' in p['d']:
            for a in split_path(p, pressuremap):
                broken.append(a)
                j += 1
            else:
                cleanup.append(p)
                i += 1
    else:
        print('{:} paths broken into {:} parts ({:} s)'.format(i, j, timer(1)))
        # for b in broken:
        if len(broken) > 0:
            pressuremap.find('g', id='Pressure-' + season + '-Raw').contents = broken
        # for c in cleanup:
        #     c.decompose()
        del broken
        save(pressuremap)
    print('New paths added')
    print('Cleaning artifacts...')

    with open(basefile + r'\Pressure-' + season + '.svg', 'r') as f:
        pressuremap = bs(f, 'lxml')

    for p in pressuremap.find('g', id='Pressure-' + season + '-Raw').find_all('path'):
        p['d'] = clean_path(p['d'])
    else:
        save(pressuremap)
    i = 0
    for p in pressuremap.find('g', id='Pressure-' + season + '-Raw').find_all('path'):
        if path_length(p) < 120:
            p.decompose()
            i += 1
    else:
        save(pressuremap)
        print(str(i) + ' artifacts cleaned')

    print('Getting raw pressure cells...')
    raws = []
    for p in pressuremap.find('g', id='Pressure-' + season + '-Raw').find_all('path'):
        coords = parse_coordinate_list(p)
        xmax, xmin = max([p[0] for p in coords]), min([p[0] for p in coords])
        ymax, ymin = max([p[1] for p in coords]), min([p[1] for p in coords])
        raws.append((
            coords, int(get_property(p, 'fill')[-2:], 16), (xmin, xmax, ymin, ymax)
        ))
    raws = sorted(raws, key=lambda x: x[0][0])
    print('...' + str(timer(1)) + ' s')
    print('Fixing pressure...')
    i = 0
    with open(hex_points_file, 'rb') as f:
        hexdict = pickle.load(f)
    for h in sorted(hexdict, key=lambda h: hexdict[h][0]):
        c = (
            hexdict[h][1][0],
            hexdict[h][1][1] + 50
        )
        for p in raws:
            if c[0] < p[2][0] or c[0] > p[2][1] or c[1] < p[2][2] or c[1] > p[2][3]:
                continue
            try:
                if point_in_path(p[0], c):
                    pressures[season].update({h: p[1]})
                    # cleanup.append(p)
                    i += 1
                    break
            except AssertionError:
                print(h + ' error')
        else:
            pressures[season].update({h: 128})
            i += 1
        if not i % (len(hexdict) // 100):
            # pickle.dump(pressures, open(pressurepickle, 'wb'))
            print('{:.0%}, {:} s'.format(i / len(hexdict), timer(1)))
            # save(pressuremap)
            # print('{:.0f} s, {:.3%}'.format(timer(1), len(pressures[season]) / len(hexdict)))
    else:
        # for c in cleanup:
        #     c.decompose()
        save(pressuremap)
        with open(pressurepickle, 'wb') as f:
            pickle.dump(pressures, f)
    print(str(timer(1)) + ' s')


# winds = {h: {'Jan': None, 'Jul': None} for h in hexes}

def pressure_curl_old(season):
    timer(0)
    global winds
    discretes = {}
    print('Getting pressure cells...')
    with open(hex_points_file, 'rb') as f:
        hexdict = pickle.load(f)
    for p in pressures[season]:
        # pass
        # id = latlong(eval(p['d'].split(' ')[1]), terr = True)
        sector = latlong(hexdict[p][0], terr=True, sector=True)
        datum = round((pressures[season][p] - 128) / 255 * 2, 3) + 0
        x = hexdict[p][0][0]
        y = hexdict[p][0][1] + 50
        # h = Hex()
        # h.x = x
        # h.y = y
        pp = parseID(p)
        latitude = round(int(pp[1]) * -90 / 207 + 90, 2)
        discretes.update({p: {'sector': sector, 'gradient': datum, 'x': x, 'y': y, 'latitude': latitude}})
        # p['id'] = id
        # hexes.update({id: h})
    for d in discretes:
        p = parseID(d)
        if discretes[d]['latitude'] == 0:
            if p[0] in [104, -104]:
                n = [
                    '207-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                    '207-' + str(abs(p[0] + 1)) + ('E' if p[0] + 1 > 0 else ('W' if p[0] + 1 < 0 else '')),
                    '206N-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                    '206S-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                ]
            else:
                n = [
                    '207-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                    '207-' + str(abs(p[0] + 1)) + ('E' if p[0] + 1 > 0 else ('W' if p[0] + 1 < 0 else '')),
                    '206N-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                    '206N-' + str(abs(p[0])) + ('E' if p[0] > 0 else ('W' if p[0] < 0 else '')),
                    '206S-' + str(abs(p[0])) + ('E' if p[0] > 0 else ('W' if p[0] < 0 else '')),
                    '206S-' + str(abs(p[0] - 1)) + ('E' if p[0] - 1 > 0 else ('W' if p[0] - 1 < 0 else '')),
                ]
        else:
            x = discretes[d]['x']
            y = discretes[d]['y']
            n = list(set([i for i in [
                latlong((round(x - 50 * math.sqrt(3) / 2, 2), y - 75 - 50), terr=True),
                latlong((round(x + 50 * math.sqrt(3) / 2, 2), y - 75 - 50), terr=True),
                latlong((round(x - 50 * math.sqrt(3) / 2, 2), y + 75 - 50), terr=True),
                latlong((round(x + 50 * math.sqrt(3) / 2, 2), y + 75 - 50), terr=True),
                latlong((round(x + 100 * math.sqrt(3) / 2, 2), y - 50), terr=True),
                latlong((round(x - 100 * math.sqrt(3) / 2, 2), y - 50), terr=True),
            ] if i in hexdict]))
        discretes[d]['neighbors'] = n
    sector_rotate = {
        1: 60,
        2: 0,
        3: -60,
        4: 120,
        5: 180,
        6: -120,
        7: -120,
        8: 180,
        9: 120,
        10: -60,
        11: 0,
        12: 60
    }

    # try an alternate method of curl derivation
    i = 0
    for d in discretes:
        try:
            a = (
                discretes[discretes[d]['neighbors'][1]]['gradient'],
                discretes[discretes[d]['neighbors'][0]]['gradient'])
            if a[0] - a[1] > 0:
                scale_a = round(a[0] - a[1], 3)
                a = (
                    round(discretes[discretes[d]['neighbors'][1]]['x'] - discretes[discretes[d]['neighbors'][0]]['x'],
                          1),
                    round(discretes[discretes[d]['neighbors'][1]]['y'] - discretes[discretes[d]['neighbors'][0]]['y'],
                          1)
                )
            elif a[0] - a[1] < 0:
                scale_a = round(a[1] - a[0], 3)
                a = (
                    round(discretes[discretes[d]['neighbors'][0]]['x'] - discretes[discretes[d]['neighbors'][1]]['x'],
                          1),
                    round(discretes[discretes[d]['neighbors'][0]]['y'] - discretes[discretes[d]['neighbors'][1]]['y'],
                          1)
                )
            else:
                scale_a = 0
                a = (0, 0)
        except:
            scale_a = 0
            a = (0, 0)
        try:
            b = (
                discretes[discretes[d]['neighbors'][2]]['gradient'],
                discretes[discretes[d]['neighbors'][4]]['gradient'])
            if b[0] - b[1] > 0:
                scale_b = round(b[0] - b[1], 3)
                b = (
                    round(discretes[discretes[d]['neighbors'][2]]['x'] - discretes[discretes[d]['neighbors'][4]]['x'],
                          1),
                    round(discretes[discretes[d]['neighbors'][2]]['y'] - discretes[discretes[d]['neighbors'][4]]['y'],
                          1)
                )
            elif b[0] - b[1] < 0:
                scale_b = round(b[1] - b[0], 3)
                b = (
                    round(discretes[discretes[d]['neighbors'][4]]['x'] - discretes[discretes[d]['neighbors'][2]]['x'],
                          1),
                    round(discretes[discretes[d]['neighbors'][4]]['y'] - discretes[discretes[d]['neighbors'][2]]['y'],
                          1)
                )
            else:
                scale_b = 0
                b = (0, 0)
        except:
            scale_b = 0
            b = (0, 0)
        try:
            c = (
                discretes[discretes[d]['neighbors'][3]]['gradient'],
                discretes[discretes[d]['neighbors'][5]]['gradient'])
            if c[0] - c[1] > 0:
                scale_c = round(c[0] - c[1], 3)
                c = (
                    round(discretes[discretes[d]['neighbors'][3]]['x'] - discretes[discretes[d]['neighbors'][5]]['x'],
                          1),
                    round(discretes[discretes[d]['neighbors'][3]]['y'] - discretes[discretes[d]['neighbors'][5]]['y'],
                          1)
                )
            elif c[0] - c[1] < 0:
                scale_c = round(c[1] - c[0], 3)
                c = (
                    round(discretes[discretes[d]['neighbors'][5]]['x'] - discretes[discretes[d]['neighbors'][3]]['x'],
                          1),
                    round(discretes[discretes[d]['neighbors'][5]]['y'] - discretes[discretes[d]['neighbors'][3]]['y'],
                          1)
                )
            else:
                scale_c = 0
                c = (0, 0)
        except:
            scale_c = 0
            c = (0, 0)
        a = (scale_a * a[0], scale_a * a[1])
        b = (scale_b * b[0], scale_b * b[1])
        c = (scale_c * c[0], scale_c * c[1])
        t = (round(a[0] + b[0] + c[0], 3), round(a[1] + b[1] + c[1], 3))
        discretes[d]['curl'] = t

    # for d in discretes:
    #     try:
    #         b = discretes[discretes[d]['neighbors'][1]]['gradient'] - discretes[discretes[d]['neighbors'][0]]['gradient']
    #         b /= -2
    #         b = round(b, 2)
    #         a1 = max([discretes[discretes[d]['neighbors'][2]]['gradient'], discretes[discretes[d]['neighbors'][3]]['gradient']])
    #         a2 = min([discretes[discretes[d]['neighbors'][4]]['gradient'], discretes[discretes[d]['neighbors'][5]]['gradient']])
    #         a = (a1 - a2) / 2
    #         a = round(a, 2)
    #         discretes[d]['curl'] = (a, b)
    #     except:
    #         pass

    print('Calculating vector field curl...')

    with open(windfile, 'r') as f:
        windmap = bs(f, 'lxml')

    # i = 0
    curls = []
    string = ''
    i = 0
    for d in discretes:
        if d not in hexes:
            continue
        if 'curl' in discretes[d]:
            if i % 2:
                i += 1
                continue
            theta = round(math.degrees(atan2(discretes[d]['curl'][1], discretes[d]['curl'][0])), 3)
            scale = round(math.sqrt(discretes[d]['curl'][0] ** 2 + discretes[d]['curl'][1] ** 2), 3)
            phi = 0 * sector_rotate[discretes[d]['sector']] - 90 + 30
            if 0 < scale <= 50:
                string += '<use xlink:href="#windmarker" transform="translate ({:} {:}) scale({:.3f}) rotate ({:.0f}) rotate({:})" />'.format(
                    discretes[d]['x'], discretes[d]['y'], 0.25 * scale, theta + 180, phi
                )
            winds[d][season] = (scale, theta)
        i += 1
    print('{:} s: {:}'.format(timer(1), len(string)))
    print('Adding to map...')
    # for c in curls:
    windmap.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
    # else:
    save(windmap)
    print('{:} s'.format(timer(1)))


sector_rotate = {
    1: 60,
    2: 0,
    3: -60,
    4: 120,
    5: 180,
    6: -120,
    7: -120,
    8: 180,
    9: 120,
    10: -60,
    11: 0,
    12: 60
}




def influences_define(coast_cutoff=5, cont_cutoff=10, contplus_cutoff=50, update_map=False, no_increase=True, hexes=hexes):

    timer(0)

    current_temps = dict()
    load_data(current_temps, basefile + r'\currenttemps.p')
    with open(basefile + r'\Influences.svg', 'rb') as f:
        influencemap = bs(f, 'lxml')

    influences = {h: {'Jan': None, 'Jul': None} for h in hexes}

    for season in ['Jan', 'Jul']:
        print('solving for {:}'.format(season))

        with open(basefile + r'\winds-received-' + season + '.p', 'rb') as f:
            received = pickle.load(f)
        with open(basefile + r'\winds-source-' + season + '.p', 'rb') as f:
            flowsto = pickle.load(f)
        with open(basefile + r'\winds-land-' + season + '.p', 'rb') as f:
            winds = pickle.load(f)

        for c in current_temps:
            for n in all_neighbors[c]:
                if n in hexes and influences[n][season] is None:
                    influences[n][season] = current_temps[c]

        visited = set()
        sources = {i for i in influences if influences[i][season] is not None}
        # it's possible there are loops in the mappings
        while len(sources):
            for source in sorted(list(sources), key=lambda x: winds[x][season][0], reverse=True):
                if source in visited:
                    sources.discard(source)
                    continue
                if not winds[source][season] or winds[source][season][0] <= coast_cutoff:
                    visited.add(source)
                    sources.discard(source)
                    continue
                if not flowsto.get(source):
                    visited.add(source)
                    sources.discard(source)
                    continue
                for target in flowsto[source]:
                    if no_increase and winds[source][season][0] < winds[target][season][0]:
                        continue
                    if target not in hexes:
                        continue
                    influences[target][season] = influences[source][season]
                    sources.add(target)
                visited.add(source)
                sources.discard(source)
            print(len(sources))

        for h in hexes:
            lat = 207 - abs(parseID(h)[1])
            if season == 'Jan':
                if 'N' in h and hexes[h]['dist_to_coast']:
                    if hexes[h]['dist_to_coast'] >= cont_cutoff and abs(lat) >= cont_cutoff / 2:
                        influences[h][season] = -1
                    if hexes[h]['dist_to_coast'] >= contplus_cutoff and abs(lat) >= contplus_cutoff / 2:
                        influences[h][season] = -2
            elif season == 'Jul':
                if 'S' in h and hexes[h]['dist_to_coast']:
                    if hexes[h]['dist_to_coast'] >= cont_cutoff and abs(lat) >= cont_cutoff / 2:
                        influences[h][season] = -1
                    if hexes[h]['dist_to_coast'] >= contplus_cutoff and abs(lat) >= contplus_cutoff / 2:
                        influences[h][season] = -2

        if not update_map:
            continue

        print('saving map...')
        influencemap.find(id=season).clear()
        string = ''
        for i in influences:
            if influences[i][season] is None:
                continue
            else:
                col = screen(
                    'ff0000' if influences[i][season] == 1 else (
                        '1b41cc' if influences[i][season] == 0 else ('00ff6c')
                    ), 0
                    # min(255, max(0, -16.467 * winds[i][season][0] + 329.33))
                ) if influences[i][season] >= 0 else screen(
                    'ffe400' if influences[i][season] == -1 else 'b09f0d', 0
                    # min(255, max(0, -7.719 * min(hexes[i]['dist_to_coast'], 2 * (207 - abs(parse_id(i)[1]))) + 401.375))
                )
                string += '<path d="M ' + ' '.join(
                    [','.join([str(i) for i in p]) for p in hexes[i]['pts']]
                ) + ' Z" id="' + i + '-' + season + '" style="fill:#' + col + '" />\n'
        influencemap.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
        save(influencemap)

    with open(basefile + r'\influences.p', 'wb') as f:
        pickle.dump(influences, f)

    print('{:} s'.format(timer(1)))


def rain_baseline(update_map=False, hexes=hexes, base=255/2):

    with open(basefile + r'\influences.p', 'rb') as f:
        influences = pickle.load(f)
    seasons = ['Jan', 'Jul']
    sigma = 19.75
    for season in seasons:
        s = 15 if season == 'Jul' else season == 'Jan'
        with open(basefile + r'\winds-land-' + season + '.p', 'rb') as f:
            wind = pickle.load(f)
        for h in hexes:
            climate[h]['precipitation'][season] = 0
            if not hexes[h]['coastal']:
                continue
            if influences[h][season] == 0:
                continue
            for n in hexes[h]['neighbors']:
                if roundBase(wind[h][season][1], base=30) == hexAngle(h, n):
                    i = 1
                    break
                elif roundBase(wind[h][season][1], base=60) == hexAngle(h, n):
                    i = 0.5
                    break
            else:
                continue
            x = abs(hexes[h]['latitude'])
            climate[h]['precipitation'][season] = int(i * base * (
                math.exp(-(x - s) ** 2 / sigma ** 2) +
                0.5 * math.exp(-(x - 45 - s) ** 2 / sigma ** 2) +
                0.5 * math.exp(-(x + 45 + s) ** 2 / sigma ** 2)
            ) * (90 - x) * (90 + x) / 90 ** 2)
    if update_map:
        with open(basefile + r'\Rain.svg', 'rb') as f:
            rain = bs(f, 'lxml')
    for season in ['Jan', 'Jul']:
        if not update_map:
            continue
        else:
            string = ''
            rain.find(id=season).clear()
            for h in hexes:
                if climate[h]['precipitation'][season] == 0:
                    continue
                col = screen('0000ff', 255 - climate[h]['precipitation'][season])
                string += '<path d="M ' + ' '.join(
                    [','.join([str(i) for i in p]) for p in hexes[h]['pts']]
                ) + ' Z" id="' + h + '-' + season + '" style="fill:#' + col + '" />\n'
            rain.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
            save(rain)


def rain_blow(hexes=hexes, update_map=False, g=0, reset=True, verbose=True, r=0.946, k=2, base=255/2):

    if reset: rain_baseline(base=base)
    seasons = ['Jan', 'Jul']
    for season in seasons:
        visited = set()
        available = {h: climate[h]['precipitation'][season] / 255 for h in hexes}
        with open(basefile + r'\winds-received-' + season + '.p', 'rb') as f:
            received = pickle.load(f)
        with open(basefile + r'\winds-source-' + season + '.p', 'rb') as f:
            flowsto = pickle.load(f)
        i = 1
        while len(flowsto) > len(visited) != i:
            i = len(visited)
            for source in flowsto:
                if source not in hexes:
                    continue
                if climate[source]['precipitation'][season] == 0:
                    continue
                if available[source] < 0.005:
                    continue
                if source in visited:
                    continue
                if all(target in visited for target in flowsto[source]):
                    for target in [n for n in hexes[source]['neighbors'] if n not in visited]:
                        dh = terrain[target]['altitude'] - terrain[source]['altitude']
                        j = abs(dh) / 25599 / 5 * r
                        a = max(0, available[source] - j * (k if dh > 0 else 1))
                        p = int(255 * (a + (k * j if dh < 0 else j))) * (a > 0)
                        available[target] = a if available[target] == 0 else statistics.mean([
                            available[target],
                            a, a
                        ])
                        climate[target]['precipitation'][season] = int(
                            p if climate[target]['precipitation'][season] == 0 else statistics.mean([
                                climate[target]['precipitation'][season],
                                p
                            ]))
                    visited.add(source)
                    continue
                for target in flowsto[source]:
                    if target not in hexes:
                        continue
                    dh = terrain[target]['altitude'] - terrain[source]['altitude']
                    j = abs(dh) / 25599 / 5 * r
                    a = max(0, available[source] - j * (k if dh > 0 else 1))
                    p = int(255 * (a + (k * j if dh < 0 else j))) * (a > 0)
                    available[target] = a if available[target] == 0 else statistics.mean([
                        available[target],
                        a, a
                    ])
                    climate[target]['precipitation'][season] = int(
                        p if climate[target]['precipitation'][season] == 0 else statistics.mean([
                            climate[target]['precipitation'][season],
                            p
                        ]))
                visited.add(source)
            if verbose: print(climate[source]['precipitation'][season], len(flowsto) - len(visited))
        for i in range(g):
            backup = copy.deepcopy(climate)
            for w in hexes:
                if not hexes[w]['neighbors']:
                    continue
                climate[w]['precipitation'][season] = int(statistics.mean([backup[n]['precipitation'][season] for n in hexes[w]['neighbors']]))
    if update_map:
        with open(basefile + r'\Rain.svg', 'rb') as f:
            rain = bs(f, 'lxml')
    for season in ['Jan', 'Jul']:
        if not update_map:
            continue
        else:
            string = ''
            rain.find(id=season).clear()
            for h in hexes:
                if climate[h]['precipitation'][season] == 0:
                    continue
                col = screen('0000ff', 255 - min(255, climate[h]['precipitation'][season]))
                string += '<path d="M {:} Z" class="hex" id="{:}-{:}" style="color:#{:}"/>'.format(
                    ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]),
                    h, season,
                    col
                )
            rain.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
    if update_map: save(rain)

    year = range(1, 13)
    for h in hexes:
        it = 4 if climate[h]['temperature']['Jan'] == min(climate[h]['temperature'][s] for s in climate[h]['temperature']) else 10
        ip = 4 if climate[h]['precipitation']['Jan'] == min(
            climate[h]['precipitation'][s] for s in climate[h]['precipitation']) else 10
        temps = [round((climate[h]['temperature'][s] - 32) * 5 / 9, 2) for s in climate[h]['temperature']]
        temps_year = [round(avg(temps) + (max(temps) - min(temps)) / 2 * sin(2 * math.pi/ 12 * (m - it)), 2) for m in year]
        rain = [climate[h]['precipitation'][s] for s in climate[h]['precipitation']]
        rain_year = [round(avg(rain) + (max(rain) - min(rain)) / 2 * sin(2 * math.pi/ 12 * (m - ip)), 2) if
                     temps_year[m - 1] > 0 else 0 for m in year]
        climate[h]['precipitation_annual'] = round(sum(rain_year), 2)


def temperature_influences(update_map=False, hexes=hexes, maxt=80, maxtt=85):

    timer(0)
    with open(basefile + r'\influences.p', 'rb') as f:
        influences = pickle.load(f)
    def f(lat, K, P0, r):
        return K * P0 * math.exp(r * lat) / (K + P0 * (math.exp(r * lat) - 1))
    def Tn(lat):
        return (f(lat + 61, maxt + 13, 1, 0.25) - 13) * (lat < 0) + f(-lat + 90, maxt, 14, 0.075) * (lat >= 0)
    def Th(lat):
        return (f(lat + 100, maxt + 13, 1, 0.1) - 13) * (lat < 0) + f(-lat + 90, maxt, 14, 0.075) * (lat >= 0)
    def Tm(lat):
        return (f(lat + 65, maxt + 13, 1, 0.15) - 13) * (lat < 0) + f(-lat + 80, maxt, 14, 0.085) * (lat >= 0)
    def Tc(lat):
        return (f(lat + 60, maxt + 13, 1, 0.14) - 13) * (lat < 0) + f(-lat + 40, maxt, 14, 0.17) * (lat >= 0)
    def Tct(lat):
        return (f(lat + 50, maxtt + 30, 1, 0.4) - 30) * (lat < 0) + (f(-lat + 67, maxtt - 10, 14, 0.15) - 10)* (lat >= 0)
    def Tctp(lat):
        return (f(lat + 50, maxtt + 30, 1, 0.4) - 30) * (lat < 0) + (f(-lat + 67, maxtt - 10, 14, 0.35) - 10)* (lat >= 0)
    for h in hexes:
        for season in ['Jan', 'Jul']:
            lat = hexes[h]['latitude']
            climate[h]['base_temperature'][season] = int(Tn(lat if season == 'Jul' else -lat))
            if influences[h][season] is None:
                # climate[h]['temperature'][season] = climate[h]['base_temperature'][season] + int(random.gauss(mu=0, sigma=3))
                climate[h]['temperature'][season] = climate[h]['base_temperature'][season]
            elif influences[h][season] == 1: # hot
                climate[h]['temperature'][season] = int(Th(lat if season == 'Jul' else -lat))
                # if abs(hexes[h]['latitude']) < 23.5:
                #     climate[h]['temperature'][season] = climate[h]['base_temperature'][season] + int(random.gauss(mu=0, sigma=3))
                #     continue
                # climate[h]['temperature'][season] = climate[h]['base_temperature'][season] + (
                #     10 if (season == 'Jan' and hexes[h]['latitude'] >= 0) or (season == 'Jul' and hexes[h]['latitude'] <= 0) else 0
                # )
            elif influences[h][season] == 0: # cold
                climate[h]['temperature'][season] = int(Tc(lat if season == 'Jul' else -lat))
                # climate[h]['temperature'][season] = climate[h]['base_temperature'][season] - 15
            elif influences[h][season] == -1: # cont
                climate[h]['temperature'][season] = int(Tct(lat if season == 'Jul' else -lat))
                # climate[h]['temperature'][season] = climate[h]['base_temperature'][season] + (
                #     15 if (season == 'Jan' and hexes[h]['latitude'] <= 0) or (season == 'Jul' and hexes[h]['latitude'] >= 0) else -15
                # )
            elif influences[h][season] == -2: # cont+
                climate[h]['temperature'][season] = int(Tctp(lat if season == 'Jul' else -lat))
                # climate[h]['temperature'][season] = climate[h]['base_temperature'][season] + (
                #     20 if (season == 'Jan' and hexes[h]['latitude'] <= 0) or (season == 'Jul' and hexes[h]['latitude'] >= 0) else -15
                # )
            else: # mild
                climate[h]['temperature'][season] = int(Tm(lat if season == 'Jul' else -lat))
                # climate[h]['temperature'][season] = climate[h]['base_temperature'][season] + (
                #     10 if (season == 'Jan' and hexes[h]['latitude'] >= 0) or (season == 'Jul' and hexes[h]['latitude'] <= 0) else -5
                # )

    with open(basefile + r'\Temp.svg', 'rb') as f:
        temp = bs(f, 'lxml')
    for season in ['Jan', 'Jul']:
        if not update_map:
            continue
        else:
            string = ''
            temp.find(id=season).clear()
            for h in hexes:
                string += '<path d="M ' + ' '.join(
                    [','.join([str(i) for i in p]) for p in hexes[h]['pts']]
                ) + ' Z" id="' + h + '-' + season + '" style="fill:#' + '{:02x}'.format(climate[h]['temperature'][season] + 40) * 3 + '" />\n'
            temp.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
            save(temp)
    print('{:} s'.format(timer(1)))


def temperature_lapse(hexes=hexes, update_map=False):

    timer(0)
    for h in hexes:
        for season in ['Jan', 'Jul']:
            climate[h]['temperature'][season] -= int(terrain[h]['altitude'] * 3.5 / 1000)
    with open(basefile + r'\Temp.svg', 'rb') as f:
        temp = bs(f, 'lxml')
    for season in ['Jan', 'Jul']:
        if not update_map:
            continue
        else:
            string = ''
            temp.find(id=season).clear()
            for h in hexes:
                string += '<path d="M {:} Z" class="hex" id="{:}-{:}" style="color:#{:}" />'.format(
                    ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]),
                    h, season,
                    '{:02x}'.format(climate[h]['temperature'][season] + 37) * 3
                )
            temp.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
            save(temp)
    print('{:} s'.format(timer(1)))


def temperature_baseline(h, season, max_t=90, min_tw=41, min_ts=1):

    # load_data(climate, basefile + r'\climate.json')
    # with open(basefile + r'\Temp.svg', 'rb') as f:
    #     temp = bs(f, 'lxml')

    # for h in hexdict:
    climate.setdefault(h, dict())
    climate[h]['base_temperature'] = dict()

    if season not in ['Jan', 'Jul']:
        raise Exception
    lat = latitude(h)
    lat *= 1 if season == 'Jul' else -1
    # t = max_t * math.exp -(lat ** 2) / (42.427 ** 2)) if lat <= 0 else (max_t - 23) * math.exp -(lat ** 2) / ((42.427 * 1.85) ** 2)) + 23
    # t = max_t - (max_t - min_t) * sin(radians(lat - 10 * a - 45))
    min_t = min_tw if lat < 0 else min_ts
    t = (max_t - min_t) / 2 * math.cos(lat  * 2 * math.pi/ 180) + (max_t + min_t) / 2
    return int(t)


def temperature_blend(hexes=hexes, update_map=False, g=3):

    timer(0)
    for i in range(g):
        for season in ['Jan', 'Jul']:
            temps = {h: int(climate[h]['temperature'][season]) for h in hexes}
            for h in hexes:
                climate[h]['temperature'][season] = int(avg([temps[n] for n in hexes[h]['neighbors']]) + random.gauss(0, 2))
    with open(basefile + r'\Temp.svg', 'rb') as f:
        temp = bs(f, 'lxml')
    for season in ['Jan', 'Jul']:
        if not update_map:
            continue
        else:
            string = ''
            temp.find(id=season).clear()
            for h in hexes:
                string += '<path d="M {:} Z" class="hex" id="{:}-{:}" style="color:#{:}" />'.format(
                    ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]),
                    h, season,
                    '{:02x}'.format(climate[h]['temperature'][season] + 37) * 3
                )
            temp.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
            save(temp)
    print('{:} s'.format(timer(1)))





def fix_elevation():
    """
    Takes rough hexes from the Raw Elevation layer, finds the matching hex in the Elevation layer (which must already
    exist), and copies the color over into the correct hex. Pink hexes (#f781bf). Any skipped hexes are then colored black.
    Afterwards, the raw hex is deleted.
    :return:
    """

    timer(0)
    split_elevation()

    # WARNING: REMOVES MATERIAL THAT YOU CANT GET BACK
    print('Cleaning paths...')
    for p in elevationmap.find_all('path'):
        p['d'] = clean_path(p['d'])
    else:
        save(elevationmap)
    print('Breaking apart paths...')
    i = 0
    j = 0
    cleanup = []
    broken = []
    for p in elevationmap.find('g', id='Elevation-Raw').find_all('path'):
        if 'Z M' in p['d'] or ' M' in p['d']:
            for a in split_path(p, elevationmap):
                broken.append(a)
                j += 1
            else:
                cleanup.append(p)
                i += 1
    else:
        for b in broken:
            elevationmap.find('g', id='Elevation-Raw').append(b)
        for c in cleanup:
            c.decompose()
        del broken
        print(str(i) + ' paths broken into ' + str(j) + ' parts')
        save(elevationmap)
    print('Cleaning artifacts...')
    for p in elevationmap.find('g', id='Elevation-Raw').find_all('path'):
        p['d'] = clean_path(p['d'])
    else:
        save(elevationmap)
    i = 0
    for p in elevationmap.find('g', id='Elevation-Raw').find_all('path'):
        if path_length(p) < 120:
            p.decompose()
            i += 1
    else:
        save(elevationmap)
        print(str(i) + ' artifacts cleaned')
    # cleanup = []
    i = 0
    print('Presorting arrays...')
    elevationmap.find(id='Elevation').contents = sorted(elevationmap.find(id='Elevation').find_all('path'),
                                                        key=lambda h: h.get('d').split(' ')[1])
    elevationmap.find(id='Elevation-Raw').contents = sorted(elevationmap.find(id='Elevation-Raw').find_all('path'),
                                                            key=lambda h: h.get('d').split(' ')[1])
    save(elevationmap)

    print('Fixing elevation...')
    timer(0)
    amounts = []
    for p in elevationmap.find('g', id='Elevation-Raw').find_all('path'):
        coords = parse_coordinate_list(p)
        xmax, xmin = max([p[0] for p in coords]), min([p[0] for p in coords])
        ymax, ymin = max([p[1] for p in coords]), min([p[1] for p in coords])
        amounts.append((
            coords, int(get_property(p, 'fill')[-2:], 16), (xmin, xmax, ymin, ymax)
        ))
    # TODO what unit is this in?
    i = 1
    for h in hexes:
        terrain[h]['altitude'] = 0
        for p in amounts:
            if hexes[h]['c'][0] < p[2][0] or hexes[h]['c'][0] > p[2][1] or hexes[h]['c'][1] < p[2][2] or hexes[h]['c'][1] > p[2][3]:
                continue
            if point_in_path(p[0], hexes[h]['c']):
                terrain[h]['altitude'] = p[1] * 100
                i += 1
                break
        if i % (len(hexes) // 100) == 0:
            print('{:.0f}%, {:} s'.format(100 * i / len(hexes), timer(1)))
    else:
        # for c in cleanup:
        #     c.decompose()
        save(elevationmap)
    print('{:} s'.format(timer(1)))
    print('Generating elevation file...', end='', flush=True)
    string = ''
    for h in hexes:
        string += '<path d="M ' + ' '.join(
            [','.join([str(i) for i in p]) for p in hexes[h]['pts']]) + ' Z" id="' + h + '" style="fill:#' + \
                  ('{:02x}'.format(max(0, terrain[h]['altitude'] // 100))) * 3 + '" />\n'
    elevationmap.find('g', id='Elevation').contents = bs(string, 'lxml').find('body').contents
    save(elevationmap)
    print('{:} s'.format(timer(1)))
    merge_elevation()
    merge_terrain()


# TODO only load the heavy memory items (like hexdict) as needed


def adjust_elevation(split=0, merge=0, hexes=hexes):
    """
    Grabs elevation data from the SVG and adds it into the hex object. Unfortunately not persistent (yet)
    :return:
    """

    timer(0)

    with open(elevationfile, 'r') as f:
        elevationmap = bs(f, 'lxml')

    if split: split_elevation()
    scaling = 0
    # TODO scale everything to 255ish?
    print('Adjusting elevation...')
    for h in elevationmap.find(id='Elevation').find_all('path'):
        id = latlong(eval(h.get('d').split(' ')[1]), terr=True)
        color = get_property(h, 'fill')
        height = int(color[-2:], 16) * 100
        try:
            a = sum([random.randint(0, 4) for i in range(16)]) * sum([random.randint(0, 4) for i in range(16)]) * 4 / (8192 / 100) - 5
            terrain[id]['altitude'] = height + int(a)
        except KeyError:
            pass
    else:
        save(elevationmap)
    print('Lowering coasts...', end='', flush=True)
    # coastal hexes must be lower than at least one non-coastal neighbor
    for h in (g for g in hexes if hexes[g]['coastal']):
        a = int(terrain[h]['altitude'])
        for n in (i for i in hexes[h]['neighbors'] if not hexes[i]['coastal']):
            if terrain[n]['altitude'] < a:
                a = max(1, terrain[n]['altitude'] - random.randrange(1, 5))
        terrain[h]['altitude'] = a
    # rescale_altitude()
    if split and merge:
        merge_elevation()
    save(elevationmap)
    print('{:} s'.format(timer(1)))


def lower_coasts(update_map=False, hexes=hexes):

    # TODO fix this so that a coastal is lower than all non-coastal neighbors
    print('Lowering coasts...', end='', flush=True)
    # coastal hexes must be lower than at least one non-coastal neighbor
    # for h in hexes:
    #     # dist = math.exp(-terrain[h]['fault'][0] / 500) + 0.1 * cos(2 * math.pi/ 200 * terrain[h]['fault'][0]) + 0.1
    #     d = hexes[h]['dist_to_coast']
    #     terrain[h]['altitude'] *= (0.99 * (1 - math.exp(-d / random.randint(1, 100))) + 0.01)
    #     terrain[h]['altitude'] = int(terrain[h]['altitude'])
    for h in (g for g in hexes if hexes[g]['coastal']):
        a = int(terrain[h]['altitude'])
        if hexes[h]['neighbors']:
            a = max(1, min(terrain[n]['altitude'] for n in hexes[h]['neighbors']) - random.randrange(30, 100))
        else:
            continue
            a *= noise.pnoise2(hexes[h]['c'][0], hexes[h]['c'][1], octaves=1, repeatx=500, repeaty=500, base=random.randrange(1, 10))
        terrain[h]['altitude'] = int(a)
    if update_map: save_new_elevation()


# TODO convert hex class to dict

class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = None

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = self._original_stdout








import copy










import matplotlib.pyplot as plt


def test_neighbors():
    h = '93N-60E'

    d = 3
    i = 0
    itinerary = {n for n in hexes[h]['neighbors']}
    visited = set()
    while i < d:
        fresh = set()
        for n in itinerary:
            if n not in visited:
                print(i, n)
                visited.add(n)
                for k in hexes[n]['neighbors']:
                    fresh.add(k)
        itinerary |= fresh
        i += 1


def find_in_terrain(id):
    # hexes[id]['pts'][0]
    for p in terrainmap.find_all('path'):
        pass




# def lookup(id):
# try:
#     return hexes_by_id[id]
# except KeyError:
#     print('! hex not in list', id)
#     return 0
# for h in hexes:
#     if h.id == id:
#         return hexes_by_id[h.id]


def lookup_by_xy(x, y, g=True, hexes=hexes):

    if not g:
        temp = hexes
    else:
        temp = [h for h in hexes if
                abs(parseID(h)[0] - parseID(g)[0]) < 10 and abs(parseID(h)[1] - parseID(g)[1]) < 10]
    for h in temp:
        if x - 1 < hexes[h]['x'] < x + 1 and y - 1 < hexes[h]['y'] < y + 1:
            return h
    else:
        return None


global total_drain






def fix_rain(season):
    timer(0)

    if not season:
        print('No season selected!')
        return

    print('Loading map data...', end=' ', flush=True)

    with open(basefile + r'\Rain-' + season + '.svg', 'r') as f:
        rain = bs(f, 'lxml')

    print('{:} s'.format(timer(1)))

    print('Cleaning paths...', end=' ', flush=True)
    i = 0
    for p in rain.find('g', id='Raw').find_all('path'):
        p['d'] = clean_path(p['d'])
        # i += 1
        # if not i % 100:
        #     print(i)
    else:
        save(rain, extra_file=basefile + r'\Rain-' + season + '.svg')
    print('{:} s'.format(timer(1)))
    print('Breaking apart paths...', end=' ', flush=True)
    i = 0
    j = 0
    cleanup = []
    broken = []
    for p in rain.find('g', id='Raw').find_all('path'):
        if 'Z M' in p['d'] or ' M' in p['d']:
            for a in split_path(p, rain):
                broken.append(a)
                j += 1
            else:
                cleanup.append(p)
                i += 1
    else:
        # for b in broken:
        if len(broken) > 0:
            rain.find('g', id='Raw').contents = broken
        # for c in cleanup:
        #     c.decompose()
        del broken
        print('{:} s'.format(timer(1)))
        print(str(i) + ' paths broken into ' + str(j) + ' parts')
        save(rain, basefile + r'\Rain-' + season + '.svg')

    with open(basefile + r'\Rain-' + season + '.svg', 'r') as f:
        rain = bs(f, 'lxml')

    print('Cleaning artifacts...', end=' ', flush=True)
    for p in rain.find('g', id='Raw').find_all('path'):
        p['d'] = clean_path(p['d'])
    else:
        save(rain, basefile + r'\Rain-' + season + '.svg')
    i = 0
    for p in rain.find('g', id='Raw').find_all('path'):
        if path_length(p) < 120:
            p.decompose()
            i += 1
    else:
        save(rain, basefile + r'\Rain-' + season + '.svg')
        print('{:} s'.format(timer(1)))
        print(str(i) + ' artifacts cleaned')
    cleanup = []
    i = 0
    print('Presorting arrays...', end=' ', flush=True)
    # rain.find('g', id='Rain').contents = sorted(rain.find('g', id='Rain').find_all('path'),
    #                                             key=lambda h: h.get('d').split(' ')[1])
    rain.find('g', id='Raw').contents = sorted(rain.find('g', id='Raw').find_all('path'),
                                               key=lambda h: h.get('d').split(' ')[1])
    save(rain, basefile + r'\Rain-' + season + '.svg')
    print('{:} s'.format(timer(1)))
    print('Finding rain amounts...')
    timer(0)
    amounts = []
    for p in rain.find('g', id='Raw').find_all('path'):
        coords = parse_coordinate_list(p)
        xmax, xmin = max([p[0] for p in coords]), min([p[0] for p in coords])
        ymax, ymin = max([p[1] for p in coords]), min([p[1] for p in coords])
        amounts.append((
            coords, 4 * int(get_property(p, 'fill')[-2:], 16) + random.randrange(1, 6), (xmin, xmax, ymin, ymax)
        ))
    # TODO what unit is this in?
    i = 1
    for h in hexes:
        climate[h]['precipitation'][season] = 250 + random.randrange(1, 6)
        climate[h]['base_precipitation'][season] = 250 + random.randrange(1, 6)
        for p in amounts:
            if hexes[h]['c'][0] < p[2][0] or hexes[h]['c'][0] > p[2][1] or hexes[h]['c'][1] < p[2][2] or hexes[h]['c'][1] > p[2][3]:
                continue
            if point_in_path(p[0], hexes[h]['c']):
                climate[h]['precipitation'][season] = int(p[1])
                climate[h]['base_precipitation'][season] = int(p[1])
                i += 1
                break
        else:
            climate[h]['precipitation'][season] = 10
            climate[h]['base_precipitation'][season] = 10
            i += 1
        if i % (len(hexes) // 100) == 0:
            print('{:.0f}%, {:} s'.format(100 * i / len(hexes), timer(1)))
    else:
        save(rain, basefile + r'\Rain-' + season + '.svg')
        print('{:} s'.format(timer(1)))
        del rain


def fix_temp(season):
    timer(0)

    if not season:
        print('No season selected!')
        return

    print('Loading map data...', end=' ', flush=True)

    with open(basefile + r'\Temp-' + season + '.svg', 'r') as f:
        temp = bs(f, 'lxml')

    os.system('copy "' + basefile + r'\Temp-' + season + '.svg' + '" "' + basefile + r'\Temp-' + season + '.copy"')
    print('{:} s'.format(timer(1)))

    print('Cleaning paths...', end=' ', flush=True)
    # i = 0
    for p in temp.find('g', id='Raw').find_all('path'):
        p['d'] = clean_path(p['d'])
        # i += 1
        # if not i % 100:
        #     print(i)
    else:
        save(temp, extra_file=basefile + r'\Temp-' + season + '.svg')
    print('{:} s'.format(timer(1)))
    print('Breaking apart paths...', end=' ', flush=True)
    timer(0)
    i = 0
    j = 0
    cleanup = []
    broken = []
    for p in temp.find('g', id='Raw').find_all('path'):
        if 'Z M' in p['d'] or ' M' in p['d']:
            for a in split_path(p, temp):
                broken.append(a)
                j += 1
            else:
                cleanup.append(p)
                i += 1
    else:
        for b in broken:
            temp.find('g', id='Raw').append(b)
        for c in cleanup:
            c.decompose()
        del broken
        print('{:} s'.format(timer(1)))
        print(str(i) + ' paths broken into ' + str(j) + ' parts')
        save(temp, basefile + r'\Temp-' + season + '.svg')
    print('Cleaning artifacts...', end=' ', flush=True)
    for p in temp.find('g', id='Raw').find_all('path'):
        p['d'] = clean_path(p['d'])
    else:
        save(temp, basefile + r'\Temp-' + season + '.svg')
    i = 0
    for p in temp.find('g', id='Raw').find_all('path'):
        if path_length(p) < 120:
            p.decompose()
            i += 1
    else:
        save(temp, basefile + r'\Temp-' + season + '.svg')
        print('{:} s'.format(timer(1)))
        print(str(i) + ' artifacts cleaned')
    cleanup = []
    i = 0
    print('Presorting arrays...', end=' ', flush=True)
    # temp.find('g', id='Temp').contents = sorted(temp.find('g', id='Temp').find_all('path'),
    #                                             key=lambda h: h.get('d').split(' ')[1])
    temp.find('g', id='Raw').contents = sorted(temp.find('g', id='Raw').find_all('path'),
                                               key=lambda h: h.get('d').split(' ')[1])
    save(temp, basefile + r'\Temp-' + season + '.svg')
    print('{:} s'.format(timer(1)))
    print('Finding temperatures...')
    timer(0)
    amounts = []
    for p in temp.find('g', id='Raw').find_all('path'):
        coords = parse_coordinate_list(p)
        xmax, xmin = max([p[0] for p in coords]), min([p[0] for p in coords])
        ymax, ymin = max([p[1] for p in coords]), min([p[1] for p in coords])
        amounts.append((
            coords, int(get_property(p, 'fill')[-2:], 16), (xmin, xmax, ymin, ymax)
        ))
    # TODO what unit is this in?
    i = 1
    for h in hexes:
        climate[h]['temperature'][season] = 0
        climate[h]['base_temperature'][season] = 0
        for p in amounts:
            if hexes[h]['c'][0] < p[2][0] or hexes[h]['c'][0] > p[2][1] or hexes[h]['c'][1] < p[2][2] or hexes[h]['c'][1] > p[2][3]:
                continue
            if point_in_path(p[0], hexes[h]['c']):
                climate[h]['temperature'][season] = int(p[1]) - 47
                climate[h]['base_temperature'][season] = int(p[1]) - 47
                i += 1
                break
        if i % (len(hexes) // 100) == 0:
            print('{:.0f}%, {:} s'.format(100 * i / len(hexes), timer(1)))
    else:
        save(temp, basefile + r'\Temp-' + season + '.svg')
        print('{:} s'.format(timer(1)))
        del temp


def pet(month):

    print('Finding potential evapotranspiration for {:}...'.format(month), end=' ', flush=True)
    for h in hexes:
        L = 24
        N = 30
        T = max(0, (climate[h]['temperature'][month] - 32) * 5 / 9)
        I = sum([(max(0, ((climate[h]['temperature'][t] - 32) * 5 / 9)) / 5) ** 1.514 for t in climate[h]['temperature']])
        if I == 0:
            climate[h]['pet'][month] = 0
            continue
        a = 6.75e-7 * I ** 3 - 7.71e-5 * I ** 2 + 1.792e-2 * I + 0.49239
        climate[h]['pet'][month] = int(16 * L / 12 * N / 30 * (10 * T / I) ** a)
    print('{:} s'.format(timer(1)))
    timer(0)
    print('Saving map...', end='', flush=True)
    string = ''
    for h in hexes:
        pt = min(max(0, climate[h]['pet'][month] // 2), 255)
        if pt == 0:
            continue
        string += '<path d="M ' + ' '.join(
            [','.join([str(i) for i in p]) for p in hexes[h]['pts']]) + ' Z" id="' + h + '" style="color:#' + \
                  '{:02x}'.format(pt) * 3 + '" />\n'

    with open(basefile + r'\PET-' + month + '.svg', 'r') as f:
        file = bs(f, 'lxml')

    file.find('g', id='PET').contents = bs(string, 'lxml').find('body').contents
    save(file, basefile + r'\PET-' + month + '.svg')
    print('{:} s'.format(timer(1)))


def elevate_rain():
    for h in hexes:
        for season in ['Jan', 'Jul']:
            climate[h]['precipitation'][season] = (
                    int(climate[h]['base_precipitation'][season]) + max(0, int(terrain[h]['altitude'] * 0.005 - 7.6))
            )
        # koppen(h)
        # holdridge(h)
    # todo scale 0-255?




from math import sin, cos, atan2, radians


def meander_phi(s, w, L):
    return w * sin(2 * math.pi* s / L)


def fractalize(x1, y1, x2, y2, smoothness):
    """ Calculate the vector from (x1,y1) to (x2,y2) """
    x3 = x2 - x1
    y3 = y2 - y1
    """ Calculate the point half-way between the two points """
    hx = x1 + x3 / 2
    hy = y1 + y3 / 2
    """ Calculate normalized vector perpendicular to the vector (x3,y3) """
    length = math.sqrt(x3 * x3 + y3 * y3)
    if length != 0:
        nx = -y3 / length
        ny = x3 / length
    else:
        nx = 1
        ny = 0
    """ Scale perpendicular vector by random factor """
    r = random.uniform(-length / (1 + smoothness), length / (1 + smoothness))
    nx = nx * r
    ny = ny * r
    """ add scaled perpendicular vector to the half-way point to get the final
    displaced subdivision point """
    x = round(hx + nx, 2)
    y = round(hy + ny, 2)
    return [x, y]




def collinear(p1, p2, p3):
    if type(p1) != tuple:
        p1 = hexes[p1]
        p2 = hexes[p2]
        p3 = hexes[p3]

        x1, y1 = p1.x, p1.y
        x2, y2 = p2.x, p2.y
        x3, y3 = p3.x, p3.y
    else:
        x1, y1 = p1[0], p1[1]
        x2, y2 = p2[0], p2[1]
        x3, y3 = p3[0], p3[1]

    c = round(abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)), 5)

    return c == 0


def population(founded, now=1535):
    # r =random.randint(25, 50) / 10000
    r = 0.49 / 100
    p0 = 30
    return round(p0 * math.exp(r * (now - founded)))


def founded(pop, now=1535):
    r = (random.randint(-10, 0) + random.randint(0, 10) + 49) / 10000
    # r = 0.49 / 100
    p0 = 30
    return round(now - math.log(pop / p0) / r)


def merge_terrain():

    f = terrainmap
    print('Joining like colors to please Inkscape...')
    for t in color2terr:
        timer(0)  # 10 min
        joined_paths = ''
        cleanup = []
        for p in f.find_all('path', **{'style': 'fill:' + t.lower()}):
            # for p in f.find_all('path', **{'style': 'fill:#76c84f'}):
            # s = latlong(eval(p['d'].split(' ')[1]), terr = True, sector = True)
            # if s != 12 and s != 9:
            if True:
                joined_paths += p['d']
                joined_paths += ' '
                cleanup.append(p)
        for c in cleanup:
            c.decompose()
        else:
            # f.append(f.new_tag('path', **{'d': joined_paths, 'style': 'fill:#76c84f'}))
            if len(joined_paths) > 1:
                f.find('g', id='Terrain').append(
                    f.new_tag('path', **{'d': joined_paths, 'style': 'fill:' + t.lower()}))
                save(f)
                # print('{:}: {:} s ({:})'.format(t.lower(), timer(1), len(joined_paths)))


def generate_precipitation_file(season):
    if not season:
        return

    open(basefile + r'test.txt', 'w').close()
    with open(basefile + r'test.txt', 'w') as f:
        for h in hexes:
            if climate[h]['precipitation'][season] == 0 or climate[h]['temperature'][season] <= 32:
                continue
            f.write('<path d="M ' + ' '.join(
                [','.join([str(i) for i in p]) for p in hexes[h]['pts']]) + ' Z" id="' + h + '" style="fill:#' +
                    '{:02x}'.format(max(0, min(255, 255 - climate[h]['precipitation'][season]))) * 2 + 'ff" />\n')


def generate_temperature_file(season):
    if not season:
        return

    timer(0)
    print('Generating temperature file...', end='', flush=True)
    string = ''
    for h in hexes:
        string += '<path d="M ' + ' '.join(
            [','.join([str(i) for i in p]) for p in hexes[h]['pts']]) + ' Z" id="' + h + '" style="fill:#' + \
                  ('{:02x}'.format(max(0, 47 + climate[h]['temperature'][season]))) * 3 + '" />\n'

    with open(basefile + r'\Temp.svg', 'r') as f:
        file = bs(f, 'lxml')

    file.find('g', id=season).contents = bs(string, 'lxml').find('body').contents
    save(file, basefile + r'\Temp.svg')
    print('{:} s'.format(timer(1)))


def merge_elevation():
    timer(0)
    print('Joining like colors to please Inkscape...')
    string = ''
    for z in range(0, 256):
        alt = '#' + '{:x}'.format(z).zfill(2) * 3
        paths = ''
        for h in hexes:
            if z * 100 <= terrain[h]['altitude'] <= z * 100 + 99:
                paths += "M " + ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]) + ' Z '
        string += (
                '<path d="' +
                paths +
                '" id="' + h +
                '" style="fill:' + alt +
                '" />\n'
        )

    with open(basefile + r'\Elevation.svg', 'r') as f:
        file = bs(f, 'lxml')

    file.find('g', id='Elevation').contents = bs(string, 'lxml').find('body').contents
    save(file, basefile + r'\Elevation.svg')
    print('{:} s'.format(timer(1)))


def merge_elevation_raw():
    f = elevationmap
    print('Joining like colors to please Inkscape...')
    for z in range(0, 256):
        alt = '#' + '{:x}'.format(z).zfill(2) * 3
        timer(0)  # 10 min
        joined_paths = ''
        cleanup = []
        for p in f.find(id='Elevation-Raw').find_all('path', **{'style': 'fill:' + alt}):
            if True:
                joined_paths += p['d']
                joined_paths += ' '
                cleanup.append(p)
        for c in cleanup:
            c.decompose()
        else:
            if len(joined_paths) > 1:
                f.find(id='Elevation-Raw').append(f.new_tag('path', **{'d': joined_paths, 'style': 'fill:' + alt}))
                print('{:}: {:} s ({:})'.format(z, timer(1), len(joined_paths)))
    else:
        save(f)


def split_terrain():
    # TODO add id to the new hexes
    timer(0)  # s
    print('Splitting terrain...')
    f = terrainmap
    paths = []
    cleanup = []
    for p in f.find('g', id='Terrain').find_all('path'):
        if 'Z M' in p['d']:
            for a in split_path(p, f):
                a['id'] = latlong(eval(a['d'].split(' ')[1]), terr=True)
                paths.append(a)
            cleanup.append(p)
    else:
        for c in cleanup:
            c.decompose()
        for a in paths:
            f.find('g', id='Terrain').append(a)
        for a in f.find(id='Terrain').find_all('path'):
            a['id'] = latlong(eval(a['d'].split(' ')[1]), terr=True)
        else:
            save(f)
            print('{:} s'.format(timer(1)))


def split_elevation():
    # TODO add id to the new hexes
    timer(0)  # s
    print('Splitting elevation...')

    with open(elevationfile, 'r') as file:
        f = bs(file, 'lxml')

    paths = []
    cleanup = []
    for p in f.find('g', id='Elevation').find_all('path'):
        if 'Z M' in p['d']:
            for a in split_path(p, f):
                paths.append(a)
            cleanup.append(p)
    else:
        for c in cleanup:
            c.decompose()
        for a in paths:
            f.find(id='Elevation').append(a)
        for a in f.find(id='Elevation')('path'):
            a['id'] = latlong(eval(a['d'].split(' ')[1]), terr=True)
        else:
            save(f)
            print('{:} s'.format(timer(1)))


def koppen(h):
    #  Pw ≥ ⅔ Pann then Pth = (2 Tann), else if Ps ≥ ⅔ Pann then Pth = 2 Tann + 28 °C, else Pth = 2 Tann + 14 °C
    k = ''
    # i = int(7 / (1 + math.exp(-(hexes[h]['latitude']) / 15))) - 3
    year = range(1, 13)
    it = 4 if climate[h]['temperature']['Jan'] == min(climate[h]['temperature'][s] for s in climate[h]['temperature']) else 10
    ip = 4 if climate[h]['precipitation']['Jan'] == min(climate[h]['precipitation'][s] for s in climate[h]['precipitation']) else 10
    temps = [round((climate[h]['temperature'][s] - 32) * 5 / 9, 2) for s in climate[h]['temperature']]
    temps_year = [round(avg(temps) + (max(temps) - min(temps)) / 2 * sin(2 * math.pi/ 12 * (m - it)), 2) for m in year]
    rain = [climate[h]['precipitation'][s] for s in climate[h]['precipitation']]
    rain_year = [round(avg(rain) + (max(rain) - min(rain)) / 2 * sin(2 * math.pi/ 12 * (m - ip)), 2) if
                 temps_year[m - 1] > 0 else 0 for m in year]
    tmonb = len([t for t in temps_year if t >= 10])
    tmoncd = len([t for t in temps_year if t < 10])

    climate[h]['precipitation_annual'] = round(sum(rain_year), 2)

    if 'S' not in h:
        summer = list(range(3, 9))
        winter = list(range(1, 3)) + list(range(9, 13))
    else:
        winter = list(range(3, 9))
        summer = list(range(1, 3)) + list(range(9, 13))

    rain_summer = [rain_year[m - 1] for m in summer]
    rain_winter = [rain_year[m - 1] for m in winter]

    if sum(rain_winter) >= 2/3 * sum(rain_year):
        pth = 2 * avg(temps_year)
    elif sum(rain_summer) >= 2/3 * sum(rain_year):
        pth = 2 * avg(temps_year) + 28
    else:
        pth = 2 * avg(temps_year) + 14

    if max(temps_year) < 10:
        k += ('E')
        if max(temps_year) >= 0:
            k += 'T'
        elif max(temps_year) < 0:
            k += 'F'
    elif sum(rain_year) < 10 * pth:
        k += ('B')
        if sum(rain_year) > 5 * pth:
            k += 'S'
        elif sum(rain_year) <= 5 * pth:
            k += 'W'
    elif min(temps_year) >= 18:
        k += ('A')
        if min(rain_year) >= 60:
            k += 'f'
        elif sum(rain_year) >= (25 * (100 - min(rain_year))):
            k += 'm'
        elif min(rain_year) in rain_summer:
            k += 's'
        elif min(rain_year) in rain_winter:
            k += 'w'
    elif -3 < min(temps_year) < 18:
        k += ('C')
        if min(rain_summer) < min(rain_winter) and max(rain_winter) > 3 * min(rain_summer) and min(rain_summer) < 40:
            k += 's'
        elif max(rain_summer) > 10 * min(rain_winter) and min(rain_winter) < min(rain_summer):
            k += 'w'
        else:
            k += 'f'
    elif min(temps_year) <= -3:
        k += ('D')
        if min(rain_summer) < min(rain_winter) and max(rain_winter) > 3 * min(rain_summer) and min(rain_summer) < 40:
            k += 's'
        elif max(rain_summer) > 10 * min(rain_winter) and min(rain_winter) < min(rain_summer):
            k += 'w'
        else:
            k += 'f'

    if avg(temps_year) >= 18 and k[0] in 'B':
        k += 'h'
    elif avg(temps_year) < 18 and k[0] in 'B':
        k += 'k'
    elif max(temps_year) >= 22 and k[0] in 'CD':
        k += 'a'
    elif max(temps_year) < 22 and tmonb > 4 and k[0] in 'CD':
        k += 'b'
    elif max(temps_year) < 22 and tmoncd > 4 and min(temps_year) > -38 and k[0] in 'CD':
        k += 'c'
    elif max(temps_year) < 22 and tmoncd > 4 and min(temps_year) <= -38 and k[0] in 'CD':
        k += 'd'

    if k == 'Cf':
        k = 'Cfa'

    return k


def generate_koppen_file(hexes=hexes, g=3):

    timer(0)
    for h in hexes:
        climate[h]['koppen'] = koppen(h)
    else:
        with open(hexpickle, 'wb') as f:
            pickle.dump(hexes, f)
    koppen_mode(g=g)
    print('Generating Koppen map...', end='', flush=True)
    string = ''
    for h in hexes:
        try:
            string += (
                    '<path d="M ' +
                    ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]) +
                    ' Z" id="' + h +
                    '" class="hex" style="color:' + koppen2color[climate[h]['koppen']] +
                    '" />\n'
            )
        except KeyError:
            print(h, climate[h]['koppen'])
            continue

    with open(basefile + r'\Koppen.svg', 'r') as f:
        file = bs(f, 'lxml')

    file.find('g', id='Koppen').contents = bs(string, 'lxml').find('body').contents
    save(file, basefile + r'\Koppen.svg')
    print('{:} s'.format(timer(1)))

from statistics import mode, StatisticsError


def holdridge_mode(hexes=hexes, g=3):

    for _ in range(g):
        holdcopy = {h: climate[h]['holdridge'] for h in hexes}
        for h in hexes:
            if not hexes[h]['neighbors']:
                continue
            try:
                climate[h]['holdridge'] = mode([holdcopy[n] for n in hexes[h]['neighbors']] + [holdcopy[h]])
            except StatisticsError:
                if holdcopy[h] in [holdcopy[n] for n in hexes[h]['neighbors']]:
                    pass
                else:
                    climate[h]['holdridge'] = holdcopy[hexes[h]['neighbors'][0]]


def koppen_mode(hexes=hexes, g=3):

    for _ in range(g):
        koppencopy = {h: climate[h]['koppen'] for h in hexes}
        for h in hexes:
            if not hexes[h]['neighbors']:
                continue
            try:
                climate[h]['koppen'] = mode([koppencopy[n] for n in hexes[h]['neighbors']] + [koppencopy[h]])
            except StatisticsError:
                if koppencopy[h] in [koppencopy[n] for n in hexes[h]['neighbors']]:
                    pass
                else:
                    climate[h]['koppen'] = koppencopy[hexes[h]['neighbors'][0]]


def holdridge(h):
    """

    :param h:
    :return: the Holdridge classification
    """

    # global hexes # TODO can this be a class method instead? probably should.
    hold = ''
    pavg = avg([climate[h]['precipitation'][s] for s in climate[h]['precipitation']])
    pmin = min(climate[h]['precipitation'][s] for s in climate[h]['precipitation'])
    pmax = max(climate[h]['precipitation'][s] for s in climate[h]['precipitation'])
    tmin = (min(climate[h]['temperature'][s] for s in climate[h]['temperature']) - 32) * 5 / 9
    tmax = (max(climate[h]['temperature'][s] for s in climate[h]['temperature']) - 32) * 5 / 9
    tann = (avg([climate[h]['temperature'][s] for s in climate[h]['temperature']]) - 32) * 5 / 9
    # i = 4 if climate[h]['temperature']['Jul'] > climate[h]['temperature']['Jan'] else 10
    # i = int(7 / (1 + math.exp(-(hexes[h]['latitude']) / 15))) - 3
    it = 4 if climate[h]['temperature']['Jan'] == min(climate[h]['temperature'][s] for s in climate[h]['temperature']) else 10
    ip = 4 if climate[h]['precipitation']['Jan'] == min(climate[h]['precipitation'][s] for s in climate[h]['precipitation']) else 10
    ipet = 4 if climate[h]['pet']['Jan'] == min(climate[h]['pet'][s] for s in climate[h]['pet']) else 10
    pann = sum(pavg + (pmax - pmin) / 2 * sin(2 * math.pi/ 12 * (m - ip)) if
               tann + (tmax - tmin) / 2 * sin(2 * math.pi/ 12 * (m - it)) > 0 else 0 for m in range(1, 13))
    try:
        petr = (sum([avg([climate[h]['pet'][s] for s in climate[h]['pet']]) +
                     (max([climate[h]['pet'][s] for s in climate[h]['pet']]) -
                      min([climate[h]['pet'][s] for s in climate[h]['pet']])) / 2 *
                     sin(2 * math.pi/ 12 * (m - ipet)) for m in range(1, 13)]) /
                sum([pavg + (pmax - pmin) / 2 * sin(2 * math.pi/ 12 * (m - ip)) if
                     tann + (tmax - tmin) / 2 * sin(2 * math.pi/ 12 * (m - it)) > 0 else 0 for m in range(1, 13)])
                )
        climate[h]['aridity'] = 1 / petr
    except ZeroDivisionError:
        petr = 0
        climate[h]['aridity'] = 0
    precip = sum(pavg + (pmax - pmin) / 2 * sin(2 * math.pi/ 12 * (m - ip)) if
                 tann + (tmax - tmin) / 2 * sin(2 * math.pi/ 12 * (m - it)) > 0 else 0 for m in range(1, 13))
    biotemp = sum(
        t if 0 < t else (30 if t > 30 else 0) for t in (tann + (tmax - tmin) / 2 * sin(2 * math.pi/ 12 * (m - it)) for m in range(1, 13)))
    climate[h]['biotemp'] = round(biotemp, 2)
    if petr < 0.25:
        if precip < 500:
            hold = 'superhumid polar desert'
        elif precip < 1000:
            hold = 'superhumid subpolar rain tundra'
        elif precip < 2000:
            hold = 'superhumid boreal rain forest'
        elif precip < 4000:
            hold = 'superhumid cool temperate rain forest'
        elif precip < 8000:
            hold = 'superhumid subtropical rain forest'
        else:
            hold = 'superhumid tropical rain forest'
    elif petr < 0.5:
        if precip < 250:
            hold = 'perhumid polar desert'
        elif precip < 500:
            hold = 'perhumid subpolar wet tundra'
        elif precip < 1000:
            hold = 'perhumid boreal wet forest'
        elif precip < 2000:
            hold = 'perhumid cool temperate wet forest'
        elif precip < 4000:
            hold = 'perhumid subtropical wet forest'
        else:
            hold = 'perhumid tropical wet forest'
    elif petr < 1:
        if precip < 125:
            hold = 'humid polar desert'
        elif precip < 250:
            hold = 'humid subpolar moist tundra'
        elif precip < 500:
            hold = 'humid boreal moist forest'
        elif precip < 1000:
            hold = 'humid cool temperate moist forest'
        elif precip < 2000:
            hold = 'humid subtropical moist forest'
        else:
            hold = 'humid tropical moist forest'
    elif petr < 2:
        if precip < 125:
            hold = 'subhumid subpolar dry tundra'
        elif precip < 250:
            hold = 'subhumid boreal dry scrub'
        elif precip < 500:
            hold = 'subhumid cool temperate steppe'
        elif precip < 1000:
            hold = 'subhumid subtropical dry forest'
        else:
            hold = 'subhumid tropical dry forest'
    elif petr < 4:
        if precip < 125:
            hold = 'semiarid boreal desert'
        elif precip < 250:
            hold = 'semiarid cool temperate desert scrub'
        elif precip < 500:
            hold = 'semiarid subtropical thorn steppe'
        else:
            hold = 'semiarid tropical very dry forest'
    elif petr < 8:
        if precip < 125:
            hold = 'arid cool temperate desert'
        elif precip < 250:
            hold = 'arid subtropical desert scrub'
        else:
            hold = 'arid tropical thorn woodland'
    elif petr < 16:
        if precip < 125:
            hold = 'perarid subtropical desert'
        else:
            hold = 'perarid tropical desert scrub'
    else:
        hold = 'superarid tropical desert'
    #
    # if biotemp < 1.5:
    #     if precip < 125:
    #         hold = 'superhumid polar desert'
    #     elif precip < 250:
    #         hold = 'perhumid polar desert'
    #     else:
    #         hold = 'humid polar desert'
    # elif biotemp < 3:
    #     if precip < 125:
    #         hold = 'subhumid subpolar dry tundra'
    #     elif precip < 250:
    #         hold = 'humid subpolar moist tundra'
    #     elif precip < 500:
    #         hold = 'perhumid subpolar wet tundra'
    #     else:
    #         hold = 'superhumid subpolar rain tundra'
    # elif biotemp < 6:
    #     if precip < 125:
    #         hold = 'semiarid boreal desert'
    #     elif precip < 250:
    #         hold = 'subhumid boreal dry scrub'
    #     elif precip < 500:
    #         hold = 'humid boreal moist forest'
    #     elif precip < 1000:
    #         hold = 'perhumid boreal wet forest'
    #     else:
    #         hold = 'superhumid boreal rain forest'
    # elif biotemp < 12:
    #     if precip < 125:
    #         hold = 'arid cool temperate desert'
    #     elif precip < 250:
    #         hold = 'semiarid cool temperate desert scrub'
    #     elif precip < 500:
    #         hold = 'subhumid cool temperate steppe'
    #     elif precip < 1000:
    #         hold = 'humid cool temperate moist forest'
    #     elif precip < 2000:
    #         hold = 'perhumid cool temperate wet forest'
    #     else:
    #         hold = 'superhumid cool temperate rain forest'
    # elif biotemp < 24:
    #     if precip < 125:
    #         hold = 'perarid subtropical desert'
    #     elif precip < 250:
    #         hold = 'arid subtropical desert scrub'
    #     elif precip < 500:
    #         hold = 'semiarid subtropical thorn steppe'
    #     elif precip < 1000:
    #         hold = 'subhumid subtropical dry forest'
    #     elif precip < 2000:
    #         hold = 'humid subtropical moist forest'
    #     elif precip < 4000:
    #         hold = 'perhumid subtropical wet forest'
    #     else:
    #         hold = 'superhumid subtropical rain forest'
    # else:
    #     if precip < 125:
    #         hold = 'superarid tropical desert'
    #     elif precip < 250:
    #         hold = 'perarid tropical desert scrub'
    #     elif precip < 500:
    #         hold = 'arid tropical thorn woodland'
    #     elif precip < 1000:
    #         hold = 'semiarid tropical very dry forest'
    #     elif precip < 2000:
    #         hold = 'subhumid tropical dry forest'
    #     elif precip < 4000:
    #         hold = 'humid tropical moist forest'
    #     elif precip < 8000:
    #         hold = 'perhumid tropical wet forest'
    #     else:
    #         hold = 'superhumid tropical rain forest'

    return hold


def generate_holdridge_file(g=3):

    timer(0)
    for h in hexes:
        climate[h]['holdridge'] = holdridge(h)
    else:
        with open(hexpickle, 'wb') as f:
            pickle.dump(hexes, f)
    holdridge_mode(g=g)
    print('Generating Holdridge map...', end='', flush=True)
    string = ''
    for h in hexes:
        try:
            string += (
                    '<path d="M ' +
                    ' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']]) +
                    ' Z" class="hex" id="' + h +
                    '" style="color:' + hold2color[climate[h]['holdridge']] +
                    '" />\n'
            )
        except KeyError:
            print(climate[h]['holdridge'])
            continue

    with open(basefile + r'\Holdridge.svg', 'r') as f:
        file = bs(f, 'lxml')

    file.find('g', id='Holdridge').contents = bs(string, 'lxml').find('body').contents
    save(file, extra_file=basefile + r'\Holdridge.svg')
    print('{:} s'.format(timer(1)))


def dist_to_seg(c, a, b):
    v = (b[0] - a[0], b[1] - a[1])
    w = (c[0] - a[0], c[1] - a[1])
    c1 = np.dot(w, v)
    c2 = np.dot(v, v)

    if c1 <= 0:  # before P0
        return distance(c, a)
    if c2 <= c1:  # after P1
        return distance(c, b)

    b = c1 / c2
    Pb = (a[0] + b * v[0], a[1] + b * v[1])
    return distance(c, Pb)






def distance_to_coasts_old(hexes=hexes, verbose=False):

    timer(0)
    print('Finding coastal distances...', end='', flush=True)
    coastals = [(hexdict[c][0][0], hexdict[c][0][1] + 50) for c in hexes if hexes[c]['coastal']]
    i = 0
    for h in hexes:
        t = min(hexDistance(h, f) for f in coastals)
        hexes[h]['dist_to_coast'] = int(t) if t else None
        if verbose:
            print(h, int(t))
        i += 1
        if not i / len(hexes) % 0.01:
            print('{:.0%}, {:} s'.format(i / len(hexes), timer(1)))
    with open(hexpickle, 'wb') as f:
        pickle.dump(hexes, f)
    print('{:} s'.format(timer(1)))


def distance_to_coasts(hexes=hexes, verbose=False):

    timer(0)

    coastals = [c for c in hexes if hexes[c]['coastal']]
    sources = set(coastals)
    visited = set()

    d = 0
    while len(sources):
        for source in list(sources):
            hexes[source]['dist_to_coast'] = d
            sources.discard(source)
            sources |= set([n for n in hexes[source]['neighbors'] if n not in visited])
            visited.add(source)
        d += 1
        print(len(visited), len(sources))

    with open(hexpickle, 'wb') as f:
        pickle.dump(hexes, f)
    print('{:} s'.format(timer(1)))




def snow_line(lat):
    """

    :param lat: the latitude of the hex
    :return: the altitude above which there can be snow
    """

    x = float(lat)

    return int(15000 * math.exp(-(x - 24) ** 2 / 2 / 60 ** 2))


def timber_line(lat):
    """

    :param lat: the latitude of the hex
    :return: the altitude below which there can be trees
    """

    x = float(lat)

    return int(14500 * math.exp(-(x - 20) ** 2 / 2 / 37 ** 2))


def terrain_type(h):

    if climate[h]['koppen'] == 'EF':
        return 'i'
    try:
        if 2 < hexes[h]['fault raw']['D'][1] < 5:
            if hexes[h]['dist_to_coast'] < 10:
                if 'mountain' in terrain[h]['topography']:
                    return 'v'  # ifrandom.randint(1, 100) < 50 else ' '
    except AttributeError:
        pass
    if terrain[h]['altitude'] > timber_line(hexes[h]['latitude']):
        if terrain[h]['altitude'] > snow_line(hexes[h]['latitude']):
            return 'i'
        else:
            return 'k'
    if climate[h]['aridity'] > 1 and climate[h]['precipitation_annual'] > 700:
        if climate[h]['drainage'] > 475 and 'plains' == terrain[h]['topography']:
            if len([n for n in hexes[h]['neighbors'] if 'plains' == terrain[n]['topography']]) > 0:
                if ('forest' in climate[h]['holdridge']) ^ ('woodland' in climate[h]['holdridge']):
                    return 'w'
                else:
                    return 'm'
        if hexes[h]['coastal'] and terrain[h]['altitude'] < 50 and 'plains' == terrain[h]['topography']:
            return 'm'
        if 'depression' in terrain[h]['topography'] and not hexes[h]['coastal']:
            return 'f'
        if 'plains' in terrain[h]['topography'] and ('highland' in terrain[h]['topography'] or 'valley' in terrain[h]['topography']):
            if climate[h]['precipitation_annual'] > 1100:
                if not hexes[h]['coastal']:
                    return 'b'
                elif 'b' in ''.join([terrain[n]['terrain'] for n in hexes[h]['neighbors']]):
                    return 'b'
        if not len(terrain[h]['drain_in']) and climate[h]['drainage'] > 4 and 'plain' == terrain[h]['topography']:
            return 'b'
    else:
        if climate[h]['drainage'] > 750 and 'plains' == terrain[h]['topography']:
            if len([n for n in hexes[h]['neighbors'] if 'plains' == terrain[n]['topography']]) > 0:
                if ('forest' in climate[h]['holdridge']) ^ ('woodland' in climate[h]['holdridge']):
                    return 'w'
                else:
                    return 'm'
    for t in ['w', 'm', 'b', 'f']:
        if 'plains' == terrain[h]['topography'] and len([n for n in hexes[h]['neighbors'] if t in terrain[n]['terrain']]) > 2:
            return t  # ifrandom.randint(1, 100) < 10 else ' '
    if 'Aw' in climate[h]['koppen']:
        return 'p'
    if 'rain forest' in climate[h]['holdridge'] or 'Af' in climate[h]['koppen']:
        if hexes[h].get('infrastructure', 0) >= 2268/2:
            return 'g'
        return 'r'
    if 'D' in climate[h]['koppen']:
        if hexes[h].get('infrastructure', 0) >= 2268/2:
            return 's'
        return 'c'
    if 'BS' in climate[h]['koppen']:
        return 'g'
    if 'B' in climate[h]['koppen']:
        return 'd'
    if 'steppe' in climate[h]['holdridge']:
        return 's'
    if climate[h]['koppen'] == 'ET':
        return 'b'
    if 'thorn' in climate[h]['holdridge']:
        return 'n'
    if 'scrub' in climate[h]['holdridge']:
        return 'n'
    if 'forest' in climate[h]['holdridge']:
        if hexes[h].get('infrastructure', 0) >= 2268/2:
            return 'g'
        return 'j'


def assign_terrain(update_map=False, r=3, verbose=False):

    timer(0)
    print('Recoloring map...')
    for h in hexes:
        terrain[h]['terrain'] = ' '
    terrain_check0 = ['']
    terrain_check = []
    i = 1
    while terrain_check0 != terrain_check:
        string = ''
        terrain_check0 = ''.join(terrain[h]['terrain'] for h in sorted(list(hexes)))
        print('Pass #{:}, {:} s'.format(i, timer(1)))
        for h, hex in hexes.items():
            terr = terrain_type(h)
            if not terr or terr == ' ':
                continue
            hex['terrain'] = terr
            string += '<path d="M {:} Z" id="{:}" style="color:#{:}" class="hex"/>'.format(
                ' '.join([','.join([str(i) for i in p]) for p in hex['pts']]),
                h, screen(terr2color[terr], hex['altitude'] // 100)
            )
        terrain_check = ''.join(hex['terrain'] for h in sorted(list(hexes)))
        i += 1
        if i == r:
            break
    if update_map:
        with open(basefile + r'\Terrain.svg', 'r') as f:
            file = bs(f, 'lxml')
        file.find('g', id='Terrain').contents = bs(string, 'lxml').find('body').contents
        save(file, extra_file=basefile + r'\Terrain.svg')
    if verbose: print(f'{timer(1)} s')


def generateHexes():

    hexMap.clear()
    hexMap.update({'0N-0': (0, 0)})
    hexMap.update({'0S-0': hexReflect(0, 0)})
    nRings = math.ceil(90 / (hexMiles / 69))
    for ring in range(1, nRings + 1):
        # print(ring)
        theta = -120
        # c = n(*m(ring, theta), theta)
        c = latitudeY(ring)
        WE = math.ceil(ring / 2)
        hexN = f"{ring}{'N' if ring < nRings else ''}-{WE}{'W'}"
        hexMap.update({hexN: c})
        if ring < nRings:
            hexS = f"{ring}{'S'}-{WE}{'W'}"
            hexMap.update({hexS: hexReflect(*hexMap[hexN])})
        phi = 0
        i = -1
        while True:
            c = hexNeighbor(*hexMap[hexN], phi)
            dir = 'W' if c[0] < 0 else 'E' if c[0] != 0 else ''
            WE = math.ceil(ring / 2) + i + (1 if dir == 'E' else 0) + (1 if dir == '' and i > 1 else 0)
            hexN = f"{ring}{'N' if ring < nRings else ''}-{WE}{dir}"
            alpha = round(math.degrees(math.atan2(c[1] - hexMap['0N-0'][1], c[0] - hexMap['0N-0'][0])), 3)
            if not alpha % 60:
                phi += 60
                phi = phi % 360
            if hexN in hexMap:
                i = 0
                break
            hexMap.update({hexN: c})
            print(hexN, c, i)
            if ring < nRings:
                hexS = f"{ring}{'S'}-{WE}{dir}"
                hexMap.update({hexS: hexReflect(*hexMap[hexN])})
            i += {'W': -1, 'E': 1, '': 0}[dir]
            if dir == '' and not i:
                i -= 1


def rawHexSVG():

    string = ''
    for h in hexMap:
        string += '<path d="M {:} Z" id="{:}" class="hex" style="color:#f00" />'.format(
            ' '.join([','.join([str(i) for i in p]) for p in [hexPts(*hexMap[h], i*60) for i in range(6)]]),
            h
        )
    else:
        with open(basefile + r'\Raw Hexes.svg', 'r') as f:
            file = bs(f, 'lxml')
        file.find('g', id='Raw').contents = bs(string, 'lxml').find('body').contents
        save(file, extra_file=basefile + r'\Raw Hexes.svg')


def rawHexPNG():

    fig, ax = newMap(0)

    t = list(hexMap)
    yh = {h: hexMap[h][0] for h in t}
    xh = {h: hexMap[h][1] for h in t}
    y = [yh[h] for h in yh.keys()]
    x = [xh[h] for h in xh.keys()]
    c = [colors.hex2color(f"#{screen('ff0000', roundBase(255 - 255 * hexDistance('0N-0', h) / 622, base=16))}") for h in t]
    ax.scatter(x=x, y=y, marker='*', c=c, s=5, edgecolors=c, zorder=100, linewidths=0.25)

    fig.savefig(fr"{basefile}\Terrain\raw.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()
