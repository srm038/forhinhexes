from defs import *
from demographics import *
from desirability import *
import gc

load_data(sociology, fr"{basefile}\sociology.json")
load_data(tribes, fr"{basefile}\tribes.json")
load_data(currents, fr"{basefile}\currents.json")

triballog = list()


def writelog(triballog, file=basefile + r'\tribalhistory.txt'):

    # print(string)
    with open(file, 'a', encoding='utf-8') as f:
        for i in triballog:
            f.write(f'{i[0]}, {i[1]}\n')
    triballog.clear()


def prehistory(start=0, end=1000):

    race = 'human'
    if not start:
        tribes.clear()
        triballog.clear()
        open(fr"{basefile}\tribalhistory.txt", 'w').close()
        h0 = random.choice([h for h in sociology if sociology[h]['desirability_raw'][race] >= 0.5])
        tribe_new(h0, 0, race)
    t = list(tribes.keys())[0]
    # fig = plt.figure()
    for y in range(start, end + 1):
        print(f"{y}, {len(tribes)}")
        # print(f"{y}, {t} population: {tribes[t]['population']} ({tribes[t]['location']} "
        #       f"{sociology[tribes[t]['location']]['support']} "
        #       f"{tribe_k(tribes[t]['location'], t)})")
        update_support()
        for t in list(tribes):
            if t not in tribes:
                continue
            tribe_manage(t, y)
        if not y % 10:
            tribes_draw(y, s=1)
            plt.cla()
            plt.clf()
            plt.close("all")
            writelog(triballog)
    save_data(tribes, tribes_file)


def update_support():
    """
    As tribes remain in one location too long, resources are be used up. Once the tribe leaves, it can begin to recover
    """

    populated = [tribes[h]['location'] for h in tribes]
    for h in populated:
        sociology[h].setdefault('support', 1)
        sociology[h]['support'] *= (sociology[h]['desirability_raw'][sociology[h]['race']]) ** 0.1
        sociology[h]['support'] = round(sociology[h]['support'], 4)
    for h in sociology:
        if 'support' not in sociology[h] or sociology[h]['support'] == 1:
            continue
        if 'tribe' not in sociology[h]:
            P = sociology[h]['support']
            sociology[h]['support'] += 0.005 * P * (1 - P)
            sociology[h]['support'] = round(sociology[h]['support'], 4)


def tribe_new(h, y, race, n=15):

    name = markov_word(names[race], word_length=random.randint(4, 6)).title()
    while name in tribes:
        name = markov_word(names[race], word_length=random.randint(4, 6)).title()
    tribes.update({name: {
        'location': h,
        't0': y,
        'population': n,
        'race': race,
        'color': randcolor()
    }})
    sociology[h]['tribe'] = name
    sociology[h]['support'] = 1
    sociology[h]['race'] = race
    triballog.append((y, f"{name} tribe founded ({race})"))


def tribe_split(t, y=0):

    h = tribes[t]['location']
    targets = all_neighbors[h]
    targets = [target for target in targets if target in sociology and 'tribe' not in sociology[target]]
    if not targets:
        # print(f"{t} has no place to go")
        return
    target = max(targets, key=lambda h: tribe_k(h, t))
    p = random.randrange(15, math.floor(tribes[t]['population'] / 2))
    triballog.append((y, f"{t} undergoes a split"))
    tribe_new(target, y=y, race=tribes[t]['race'], n=p)
    tribes[t]['population'] -= p


def tribe_k(h, t, p0=300):

    return math.floor(p0 * sociology[h]['desirability_raw'][tribes[t]['race']] * sociology[h].get('support', 1))


def tribe_attack(t, u, y=0):

    if t not in tribes:
        return False
    if u not in tribes:
        return True
    pt = tribes[t]['population']
    pu = tribes[u]['population']
    origin = tribes[t]['location']
    target = tribes[u]['location']
    mt = pt * 0.05
    mu = pu * 0.05
    if mt > mu:
        tribes[t]['population'] = pt - mu + math.floor((pu - mu) / 2)
        tribe_delete(u, y=y)
        tribes[t]['location'] = target
        sociology[target].setdefault('support', 1)
        sociology[target]['race'] = tribes[t]['race']
        sociology[target]['tribe'] = t
        del sociology[origin]['tribe']
        del sociology[origin]['race']
        triballog.append((y, f"{t} defeated {u}"))
        return True
    else:
        tribes[t]['population'] = pt - mt
        tribes[u]['population'] = pu - mt
        triballog.append((y, f"{u} repelled {t}"))
        return False


def tribe_migrate(t, y=0):

    h = tribes[t]['location']
    n = all_neighbors[h]
    if False and elevation[h]['dist_to_coast'] == 1 and random.random() < 0.25:
        a = random.choice([n for n in all_neighbors[h] if n not in sociology])
        visited = {a}
        seaway = []
        b = a
        while b not in sociology:
            if b not in hexdict:
                return
            b = check_target(b, theta=roundBase(currents[b] + random.randint(-30, 30), base=60))
            visited.add(b)
            if b in seaway:
                return
            # print(b)
            seaway.append(b)
        target = b
        if b == a:
            return
        if 'tribe' in sociology[target]:
            defenders = sociology[target]['tribe']
            if defenders == t:
                return
            success = tribe_attack(t, defenders, y=y)
            if not success:
                tribe_delete(t, y=y)
            else:
                triballog.append((y, f"{t} migrated across the sea"))
            return
        else:
            tribes[t]['location'] = target
            sociology[target].setdefault('support', 1)
            sociology[target]['race'] = tribes[t]['race']
            sociology[target]['tribe'] = t
            del sociology[h]['tribe']
            del sociology[h]['race']
            triballog.append((y, f"{t} migrated across the sea"))
            return
    targets = [target for target in n if target in sociology and 'tribe' not in sociology.get(target, dict()) and tribe_k(target, t) >= tribe_k(h, t)]
    if not targets:
        # print(f"{t} has no place to go")
        targets = [target for target in n if target in sociology and tribe_k(target, t) >= tribe_k(h, t)]
        if not targets:
            return
        target = max(targets, key=lambda i: tribe_k(i, t))
        tribe_attack(t, sociology[target]['tribe'], y=y)
        return
    target = max(targets, key=lambda i: tribe_k(i, t))
    tribes[t]['location'] = target
    sociology[target].setdefault('support', 1)
    sociology[target]['race'] = tribes[t]['race']
    sociology[target]['tribe'] = t
    del sociology[h]['tribe']
    del sociology[h]['race']


def tribe_grow(t, y=0):

    P = tribes[t]['population']
    K = max(tribe_k(tribes[t]['location'], t), 1)
    r = 0.015 / 2 * sociology[tribes[t]['location']]['desirability_raw'][tribes[t]['race']] * sociology[tribes[t]['location']]['support']
    b = births(P, K, r) if P < K else -min(math.floor(0.05 * P), 1)
    tribes[t]['population'] = math.floor(tribes[t]['population'] + b)



def tribe_delete(t, y=0):

    if t not in tribes:
        return
    h = tribes[t]['location']
    del sociology[h]['tribe']
    del sociology[h]['race']
    del tribes[t]


def tribe_manage(t, y=0):

    tribe_grow(t, y)
    h = tribes[t]['location']
    if tribe_k(h, t) <= tribes[t]['population']:
        # print(f"{t} has outgrown its area")
        if random.random() < 0.15:
            if tribes[t]['population'] >= 100:
                tribe_split(t, y)
        elif random.random() < 0.80:
            tribe_migrate(t, y)
    if t not in tribes or tribes[t]['population'] < 2:
        triballog.append((y, f"{t} disappears"))
        tribe_delete(t, y)


def tribes_draw(year, w=250, k=0.15, s=1/2):

    m = max(tribes[t]['population'] for t in tribes)
    fig, ax = plt.subplots()
    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    # ax = plt.Axes(fig, [0., 0., 1., 1.])
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b])-50, max([i[1] for i in b])+50
    ylim = min([i[0] for i in b])-50, max([i[0] for i in b])+50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#bad9ff'))

    yh = {h: hexdict[h][0][0] for h in sociology.keys() if hexdict[h][0][1] >= hexdict['207-518W'][0][1]}
    xh = {h: hexdict[h][0][1] + 50 for h in sociology.keys() if hexdict[h][0][1] >= hexdict['207-518W'][0][1]}
    y = [yh[h] for h in yh.keys() if h in sociology]
    x = [xh[h] for h in xh.keys() if h in sociology]
    ax.scatter(x=x, y=y, marker='h', c='w', edgecolors='w', s=1/2, zorder=100, linewidths=1/2)

    t = sorted(list(tribes.keys()), key=lambda h: tribes[h]['population'])
    y = [hexdict[tribes[h]['location']][0][0] for h in t]
    x = [hexdict[tribes[h]['location']][0][1] + 50 for h in t]
    c = {h: tribes[h]['color'] for h in t}
    c = [colors.hex2color(f"#{screen(c[h], w - w * (tribes[h]['population'] / m) ** k)}") for h in c]
    # c += [colors.hex2color(f"#{screen(hegemonies.get(sociology[h]['hinterland'], dict()).get('color', 'ffffff'), w - w * ((sociology[h].get('population', 0)) / m) ** k)}") for h in hinterlands]
    ax.scatter(x=x, y=y, marker='h', c=c, s=s, edgecolors=c, zorder=100, linewidths=s/2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    # fig.set_size_inches(6, 4)
    ax.annotate(f"{year: 5}", (0.0, 0.0), xycoords='axes fraction', size=12, fontfamily='monospace')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(basefile + fr'\Tribes\gif\{year:05}.png', dpi=150, bbox_inches='tight', pad_inches = 0)
    ax.clear()
    fig.clear()
    del ax
    gc.collect()


