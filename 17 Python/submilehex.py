import pickle
import csv
from bs4 import BeautifulSoup as bs
from math import radians, cos, sin, sqrt, floor, atan2, degrees
from random import randint, randrange, random, shuffle
from scipy.stats import norm
from numpy.random import choice

filepath = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps'
hexpickle = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps\hexes.p'
coastpickle = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps\coasts.p'
basefile = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps\20 Mile Hexes\base.svg'
basemap = bs(open(basefile, 'r'), 'lxml')
hex_centers = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Images\Maps\20 Mile Hexes\hex_centers.csv'
demohexes = r'G:\Drive\D&D\Behind the Veil\2 - Documents and Images\Documents\Blog\demohexes.csv'


global hexes
global subhexes
global parent

# class Hex:
#     pass
#
# hexes = pickle.load(open(hexpickle, 'rb'))
# coasts = pickle.load(open(coastpickle, 'rb'))

hexes = dict()
with open(demohexes, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None)
    for row in reader:
        hexes[int(row[0])] = {
            'altitude': int(row[1] or 0),
            'neighbors': eval(row[2]),
            'c': eval(row[3]),
            'drains_from': dict(),
            'drains_to': eval(row[5]) if row[5] else None,
            'subdrainage_in': dict(),
            'river ingress': dict(),
            'subdrainage_out': None,
            'river egress': None
        }


def topography(hexes=hexes):

    for h in hexes:
        if not terrain[h]['altitude']:
            continue
        if not hexes[h]['neighbors']:
            continue
        topo = []
        plains = 0
        hills = 0
        highlands = 0
        mountains = 0
        valleys = 0
        depressions = 0
        rolling = 1
        a = terrain[h]['altitude']
        # if type(a) == str:
        # print(hexes[h]['id'])
        # continue
        for n in (n for n in hexes[h]['neighbors'] if n in hexes and hexes[n]['altitude']):
            b = hexes[n]['altitude']
            # if type(b) == str:
            # print(n.id)
            # continue
            if abs(a - b) <= 200:
                plains += 1
            if 1200 > abs(a - b) >= 200:
                hills += 1
            if a - b >= 800:
                highlands += 1
            if abs(a - b) >= 1200:
                mountains += 1
            if a - b <= -200:
                valleys += 1
            if b > a:
                depressions += 1
        if plains >= 3:
            topo.append('plains')
            rolling = 0
        if hills >= 3 and mountains < 3:
            topo.append('hills')
            rolling = 0
        if highlands >= 3:
            topo.append('highland')
            rolling = 0
        if mountains >= 3:
            topo.append('mountain')
            rolling = 0
        if valleys in [4, 5]:
            topo.append('valley')
            rolling = 0
        if depressions == len(hexes[h]['neighbors']):
            topo.append('depression')
            rolling = 0
        if rolling:
            topo.append('rolling')
        hexes[h].update({'topography': ' '.join(topo)})


topography()

for h in hexes:
    if not hexes[h]['drains_to']:
        continue
    for d in hexes[h]['drains_to']:
        hexes[d]['drains_from'].update({h: hexes[h]['drains_to'][d]})

for h in hexes:
    if not hexes[h]['drains_to']:
        continue
    for i in hexes[h]['drains_to']:
        d = hexes[h]['drains_to'][i]
    hexes[h].update({'drainage': d})


topo = {
    'mountain': 3000,
    'hills': 1500,
    'plains': 200,
    'highland': 800,
    'depression': 200,
    'valley': 300,
    'rolling': 1000,
}


def hex_corner(center, i, size = 50):
    angle = radians(60 * i + 30)
    return (
        round(center[0] + size * cos(angle), 3),
        round(center[1] + size * sin(angle), 3)
    )


def distance(h1, h2):
    """

    :param h1: point 1
    :param h2: point 2
    :return: the distance between the two points
    """

    return sqrt((float(h1[0]) - float(h2[0])) ** 2 + (float(h1[1]) - float(h2[1])) ** 2)


def wilderness(k = 0, m = 400):

    if m != 400:
        k = int(k * 400 / m) + randrange(0, int(400 / m))
        m = 400
    x = [0] * (m - k) + [1] * k
    shuffle(x)

    return x

subhexes = dict()

with open(hex_centers, 'r') as f:
    for h in f.readlines():
        subhexes.update({
            h.split(',')[0].zfill(2) + h.split(',')[1].zfill(2): {
                'c': (
                    round(eval(h.split(',')[2].lower()), 3),
                    eval(h.split(',')[3]),
                )
        }})
# for h in subhexes:
#     subhexes[h]['neighbors'] = [x for x in [
#         str(int(h[:2]) + 0).zfill(2) + str(int(h[2:]) - 1).zfill(2),
#         str(int(h[:2]) + 0).zfill(2) + str(int(h[2:]) + 1).zfill(2),
#         str(int(h[:2]) + 1).zfill(2) + str(int(h[2:]) + 0).zfill(2),
#         str(int(h[:2]) + 1).zfill(2) + (str(int(h[2:]) - 1).zfill(2) if not int(h[:2]) // 2 else str(int(h[2:]) + 1).zfill(2)),
#         str(int(h[:2]) - 1).zfill(2) + (str(int(h[2:]) - 1).zfill(2) if not int(h[:2]) // 2 else str(int(h[2:]) + 1).zfill(2)),
#         str(int(h[:2]) - 1).zfill(2) + str(int(h[2:]) + 0).zfill(2),
#     ] if x in subhexes]
for h in subhexes:
    subhexes[h].update({'neighbors': list()})
    for i in range(6):
        x = subhexes[h]['c'][0] + 50 * sqrt(3) * cos(radians(60 * i))
        y = subhexes[h]['c'][1] + 50 * sqrt(3) * sin(radians(60 * i))
        for s in subhexes:
            if abs(x - subhexes[s]['c'][0]) < 0.1:
                if abs(y - subhexes[s]['c'][1]) < 0.1:
                    subhexes[h]['neighbors'].append(s)
                    break

egress_match = {
    '0919': '0900', '1119': '1100', '1319': '1300', '1519': '1500', '1719': '1700',
    '0819': '0800', '1019': '1000', '1219': '1200', '1419': '1400', '1619': '1600',
    '2017': '0107', '2115': '0206', '2214': '0304', '2312': '0403', '2411': '0501',
    '0112': '2003', '0214': '2103', '0315': '2206', '0417': '2307', '2018': '0108', '2116': '0207',
    '2215': '0305', '2313': '0404', '2412': '0502', '2002': '0111', '2103': '0213', '2205': '0314',
    '2306': '0416', '2408': '0517', '0900': '0919', '1100': '1119', '1300': '1319', '1500': '1519',
    '1700': '1719', '0800': '0819', '1000': '1019', '1200': '1219', '1400': '1419',
    '1600': '1619', '0107': '2017', '0206': '2115', '0304': '2214', '0403': '2312',
    '0501': '2411', '2003': '0112', '2103': '0214', '2206': '0315', '2307': '0417',
    '0108': '2018', '0207': '2116', '0305': '2215', '0404': '2313', '0502': '2412', '0111': '2002',
    '0213': '2103', '0314': '2205', '0416': '2306', '0517': '2408',
}

for c in list(egress_match):
    if len(subhexes[c]['neighbors']) == 3:
        continue
    else:
        egress_match[egress_match[c]] = c
        del egress_match[c]

ingress_match = {egress_match[c]: c for c in egress_match}

def idw(s, h, k=2): # https://www.e-education.psu.edu/geog486/node/1877

    x = 2
    if s == '1210':
        return hexes[h]['altitude']
    w = [(1 / distance(subhexes[s]['c'], subhexes['1210']['c'])) ** x]
    zw = [hexes[h]['altitude'] / (distance(subhexes[s]['c'], subhexes['1210']['c']) ** x)]
    dists = {
        n: int(distance((
            round(subhexes['1210']['c'][0] + 1000 * sqrt(3) * cos(radians(round(degrees(atan2(
            hexes[n]['c'][1] - hexes[h]['c'][1],
            hexes[n]['c'][0] - hexes[h]['c'][0],
        )), 0))), 3),
            round(subhexes['1210']['c'][1] + 1000 * sqrt(3) * sin(radians(round(degrees(atan2(
            hexes[n]['c'][1] - hexes[h]['c'][1],
            hexes[n]['c'][0] - hexes[h]['c'][0],
        )), 0))), 3),
        ), subhexes[s]['c'])) for n in hexes[h]['neighbors']
    }
    for n in sorted(list(dists), key=lambda i: dists[i])[:k]:
        # only use k-nearest
        d = dists[n]
        zw.append(hexes[n]['altitude'] / (d ** x))
        w.append(1 / (d ** x))
    return floor(sum(zw) / sum(w))


def r2(parent, subhexes=subhexes):

    z = statistics.mean([subhexes[s]['z'] for s in subhexes])
    sstot = sum((subhexes[s]['z'] - z) ** 2 for s in subhexes)
    ssres = sum((subhexes[s]['z'] - idw(s, parent, k=3)) ** 2 for s in subhexes)
    r2 = 1 - ssres / sstot
    return r2

import statistics

def map_gen(parent, subhexes=subhexes, hexes=hexes, rivers=True, erode=25, jitter=True, endo=False):

    deviation = 1
    # for i in hexes[parent]['topography'].split(' '):
    #     if i in topo:
    #         t = topo[i]
    t = mean([topo.get(i) for i in hexes[parent]['topography'].split(' ')]) / 3
    egress_hex = '0'
    i = 0
    while deviation > 0.35:
        for sh in subhexes:
            subhexes[sh].update({'z': 0})
            while subhexes[sh]['z'] <= 0:
                subhexes[sh].update({'z': int(idw(sh, parent, k=3) + jitter * (gauss(mu = 0, sigma = t)))})
                # TODO check if the neighbors have the same height
                # TODO the ingress/egress hexes need to match in height
        if rivers:
            egress_hex, ingress_hexes = river_continuity(parent=parent, subhexes=subhexes)
            hexes[parent].update({'river egress': egress_hex})
            hexes[parent].update({'river ingress': ingress_hexes})
            print(egress_hex, ingress_hexes)
            # if sub_basins(egress_hex) > 72:
            #     deviation = 1
            sub_drainage(parent=parent, subhexes=subhexes)
            river_ensure(parent, subhexes=subhexes)
            if endo: endorheics(parent=parent, n=200)
            for i in range(erode):
                river_ensure(parent, subhexes=subhexes)
                if not hexes[parent]['river ingress']:
                    continue
                sub_erode(parent=parent, subhexes=subhexes)
            # sub_drainage(parent=parent, subhexes=subhexes)
            # river_ensure(parent, subhexes=subhexes)
        deviation = abs(1 - sum([subhexes[sh]['z'] for sh in subhexes]) / 400 / hexes[parent]['altitude'])
        if rivers and not river_check(parent=parent, subhexes=subhexes):
            deviation = 1
        if deviation != 1 and i > 50:
            break
        print('dev: {:0.3f}, r2: {:0.3f}'.format(deviation, r2(parent=parent)))
        i += 1

def scaling(s, subhexes=subhexes, hexes=hexes):

    # c = subhexes[s]['drainage']
    # a = min(subhexes[i[0]]['drainage'] for i in hexes[parent]['river ingress'])
    # b = subhexes[hexes[parent]['river egress']]['drainage']
    # y = min(hexes[parent]['drains_from'][i] for i in hexes[parent]['drains_from'])
    # z =  min(hexes[parent]['drains_to'][i] for i in hexes[parent]['drains_to'])
    # return (c - a) * (z - y) / (b - a) + y

    return 13.2635 * subhexes[s]['drainage'] ** 0.4459 / 5280 * 100

def map_write(parent, subhexes=subhexes, hexes=hexes, rivers=True, scale=1, height=False):

    basemap.find('g').clear()
    open(filepath + '\\20 Mile Hexes\\' + str(parent) + '.svg', 'wb').close()
    for h in subhexes:
        col = ('{:02x}'.format(floor(min(25599, subhexes[h]['z']) / 100))) * 3
        # if rivers and h == hexes[parent]['river egress']:
        #     col = 'f00'
        # if rivers and hexes[parent]['river ingress'] and h in [i[0] for i in hexes[parent]['river ingress']]:
        #     col = '00f'
        basemap.find('g').append(basemap.new_tag('path', **{
            'd': 'M ' + ' '.join(['{:},{:}'.format(hex_corner(subhexes[h]['c'], i)[0], hex_corner(subhexes[h]['c'], i)[1]) for i in [4, 5, 0, 1, 2, 3]]) + ' Z',
            'style': 'fill:#' + col + ';stroke:#' + col,
            'id': h
        }))
    if rivers:
        for h in subhexes:
            try:
                subhexes[h]['drains_to']
            except KeyError:
                continue
            if subhexes[h]['drainage'] < 2 or not subhexes[h]['drains_to']:
                continue
            r = scaling(h)
            basemap.find('g').append(basemap.new_tag('path', **{
                'd': 'M ' + '{:},{:}'.format(*subhexes[h]['c']) + ' ' + '{:},{:}'.format(*subhexes[subhexes[h]['drains_to']]['c']),
                'style': 'stroke-width:' + '{:0.3f}'.format(r) + 'px',
                'id': h + '-' + subhexes[h]['drains_to'],
                'class': 'river'
            }))
        s = hexes[parent]['river egress']
        angle = round_base((round(degrees(atan2(
            subhexes[s]['c'][1] - subhexes['1210']['c'][1],
            subhexes[s]['c'][0] - subhexes['1210']['c'][0],
        )), 0)) % 360, base=60)
        out = (
            subhexes[s]['c'][0] + 50 * sqrt(3) * cos(radians(angle)),
            subhexes[s]['c'][1] + 50 * sqrt(3) * sin(radians(angle)),
        )
        r = scaling(s)
        basemap.find('g').append(basemap.new_tag('path', **{
            'd': 'M ' + '{:},{:}'.format(*subhexes[s]['c']) + ' ' + '{:},{:}'.format(
                *out),
            'style': 'stroke-width:' + '{:0.3f}'.format(r) + 'px',
            'id': h + '-out',
            'class': 'river'
        }))
    if height:
        string = ''
        for h in subhexes:
            string += '<text id="{:}-ft" x="{:.02f}" y="{:.02f}" class="height">{:}</text>\n'.format(
                h, subhexes[h]['c'][0], subhexes[h]['c'][1], subhexes[h]['z']
            )
        basemap.find('g').contents += bs(string, 'lxml').find('body').contents
    with open(filepath + '\\20 Mile Hexes\\' + str(parent) + '.svg', 'wb') as f:
        try:
            basemap.find('body').replace_with_children()
        except AttributeError:
            pass
        try:
            basemap.find('html').replace_with_children()
        except AttributeError:
            pass
        f.write(basemap.prettify('utf-8'))


def screen(col, bw):

    r, g, b = int(col[:2], 16), int(col[2:4], 16), int(col[4:], 16)
    r, g, b = r / 255, g / 255, b / 255

    bw /= 255
    bw = max(0, min(bw, 255))

    r = 1 - (1 - r) * (1 - bw)
    g = 1 - (1 - g) * (1 - bw)
    b = 1 - (1 - b) * (1 - bw)

    r, g, b = r * 255, g * 255, b * 255

    col = '{:02x}{:02x}{:02x}'.format(int(r), int(g), int(b))

    return col


def map_write_big(parent, subhexes=subhexes, hexes=hexes, rivers=True, scale=1, height=False):

    hexstring = ''
    riverstring = ''
    for h in subhexes:
        col = screen('586c44', subhexes[h]['z'] / 100)
        hexstring += '<path d="M {:} Z" style="color:#{:}" id="{:}-{:}" transform="translate({:0.2f} {:0.2f})" class="hex"/>'.format(
            ' '.join(['{:},{:}'.format(hex_corner(subhexes[h]['c'], i)[0], hex_corner(subhexes[h]['c'], i)[1]) for i in [4, 5, 0, 1, 2, 3]]),
            col,
            parent,
            h,
            20 * (hexes[parent]['c'][0] - 450),
            20 * (hexes[parent]['c'][1] - 675)
        )
    if rivers:
        for h in subhexes:
            try:
                subhexes[h]['drains_to']
            except KeyError:
                continue
            if subhexes[h]['drainage'] < 2 or not subhexes[h]['drains_to']:
                continue
            r = scaling(h)
            riverstring += '<path d="M {:},{:} {:},{:}" class="river" id="{:}-{:}-{:}" transform="translate({:0.2f} {:0.2f})" style="stroke-width:{:0.3f}px"/>'.format(
                subhexes[h]['c'][0], subhexes[h]['c'][1], subhexes[subhexes[h]['drains_to']]['c'][0], subhexes[subhexes[h]['drains_to']]['c'][1],
                parent, h, subhexes[h]['drains_to'],
                20 * (hexes[parent]['c'][0] - 450),
                20 * (hexes[parent]['c'][1] - 675),
                r
            )
        s = hexes[parent]['river egress']
        angle = round_base((round(degrees(atan2(
            subhexes[s]['c'][1] - subhexes['1210']['c'][1],
            subhexes[s]['c'][0] - subhexes['1210']['c'][0],
        )), 0)) % 360, base=60)
        out = (
            subhexes[s]['c'][0] + 50 * sqrt(3) * cos(radians(angle)),
            subhexes[s]['c'][1] + 50 * sqrt(3) * sin(radians(angle)),
        )
        r = scaling(s)
        riverstring += '<path d="M {:},{:} {:},{:}" class="river" id="{:}-{:}-out" transform="translate({:0.2f} {:0.2f})" style="stroke-width:{:0.3f}px"/>'.format(
                subhexes[s]['c'][0], subhexes[s]['c'][1], out[0], out[1],
                parent, s,
                20 * (hexes[parent]['c'][0] - 450),
                20 * (hexes[parent]['c'][1] - 675),
                r
            )
    all_map_file = filepath + r'\20 Mile Hexes\all.svg'
    with open(all_map_file, 'r') as f:
        all_map = bs(f, 'lxml')
    all_map.find('g', id='hexes').contents += bs(hexstring, 'lxml').find('body').contents
    all_map.find('g', id='rivers').contents += bs(riverstring, 'lxml').find('body').contents
    with open(all_map_file, 'wb') as f:
        all_map.find('body').replace_with_children()
        all_map.find('html').replace_with_children()
        f.write(all_map.prettify('utf-8'))


all_map_file = filepath + r'\20 Mile Hexes\all.svg'
with open(all_map_file, 'rb') as f:
    all_map = bs(f, 'lxml')
for g in all_map.find_all('g'):
    g.clear()
with open(all_map_file, 'wb') as f:
    all_map.find('body').replace_with_children()
    all_map.find('html').replace_with_children()
    f.write(all_map.prettify('utf-8'))

from random import gauss, random
from numpy import linspace

def round_base(x, prec=2, base=.05):
    return round(base * round(float(x) / base))


def river_check(parent, subhexes=subhexes):

    if not hexes[parent]['river ingress']:
        return True
    rivers = river_in_out(parent=parent, subhexes=subhexes)
    for river in rivers:
        for r, riv in enumerate(river[:-1]):
            if 'drains_to' not in subhexes[riv]:
                return False
            if river[r + 1] != subhexes[riv]['drains_to']:
                return False
    else:
        return True


def river_ensure(parent, subhexes=subhexes):

    if not hexes[parent]['drains_from']:
        return
    rivers = river_in_out(parent=parent, subhexes=subhexes)
    rivers.sort(key=lambda x: subhexes[x[0]]['z'], reverse=False)
    for river in rivers:
        h0 = [h[1] for h in hexes[parent]['river ingress'] if h[0] == river[0]][0]
        hf = list(hexes[parent]['drains_to'])[0]
        h0 = int(mean([hexes[parent]['altitude'], hexes[h0]['altitude']]))
        hf = int(mean([hexes[parent]['altitude'], hexes[hf]['altitude']]) + 1)
        dh = int((hf - h0) / len(river))
        subhexes[river[0]].update({'z': h0})
        subhexes[river[-1]].update({'z': hf})
        print(h0, hf, dh, subhexes[river[0]]['z'], subhexes[river[-1]]['z'])
        for r, riv in enumerate(river[:-1]):
            # print(subhexes[river[r + 1]]['z'] - subhexes[riv]['z'])
            if dh <= subhexes[river[r + 1]]['z'] - subhexes[riv]['z'] < 0:
                continue
            subhexes[river[r + 1]].update({'z': subhexes[riv]['z'] + dh})
        subhexes[river[0]].update({'z': h0})
        subhexes[river[-1]].update({'z': hf})
    # print(subhexes[hexes[parent]['river egress']]['z'] == int(
    #     mean([hexes[parent]['altitude'], hexes[list(hexes[parent]['drains_to'])[0]]['altitude']])) + 1)

    sub_drainage(parent=parent, subhexes=subhexes)

def river_continuity(parent, subhexes=subhexes):

    # assuming that there is at least an egress
    egress_hex = []
    ingress_hexes = []

    drains_to = list(hexes[parent]['drains_to'])[0]
    egress_angle = (round(degrees(atan2(
            hexes[drains_to]['c'][1] - hexes[parent]['c'][1],
            hexes[drains_to]['c'][0] - hexes[parent]['c'][0],
        )), 0)) % 360
    if hexes[drains_to]['river ingress']:
        for i in hexes[drains_to]['river ingress']:
            egress_hex.append(ingress_match[i[0]])
    else:
        for s in subhexes:
            if s not in egress_match:
                continue
            angle = (round(degrees(atan2(
                subhexes[s]['c'][1] - subhexes['1210']['c'][1],
                subhexes[s]['c'][0] - subhexes['1210']['c'][0],
            )), 0)) % 360
            if egress_angle == round_base(angle, base=60) % 360:
                # print(s, round_base(angle, base=60) % 360)
                egress_hex.append(s)
            # print(s, round_base(angle, base=60) % 360)
    egress_hex = choice(egress_hex)
    subhexes[egress_hex].update({'z': int(
        mean([hexes[parent]['altitude'], hexes[list(hexes[parent]['drains_to'])[0]]['altitude']]))})
    if 'drains_to' in subhexes[egress_hex]: subhexes[egress_hex]['drains_to']
    for n in subhexes[egress_hex]['neighbors']:
        if subhexes[n]['z'] < subhexes[egress_hex]['z']:
            subhexes[n].update({'z': subhexes[egress_hex]['z'] + 1})

    for drains_from in hexes[parent]['drains_from']:
        ingress_angle = 360 - (round(degrees(atan2(
            hexes[drains_from]['c'][1] - hexes[parent]['c'][1],
            hexes[drains_from]['c'][0] - hexes[parent]['c'][0],
        )), 0)) % 360
        ingress_hexes_t = []
        if hexes[drains_from]['river egress']:
            ingress_hexes_t.append(egress_match[hexes[drains_from]['river egress']])
        else:
            for s in subhexes:
                if s not in ingress_match:
                    continue
                angle = 360 - (round(degrees(atan2(
                    subhexes[s]['c'][1] - subhexes['1210']['c'][1],
                    subhexes[s]['c'][0] - subhexes['1210']['c'][0],
                )), 0)) % 360
                if ingress_angle == round_base(angle, base=60) % 360:
                    # print(s, round_base(angle, base=60) % 360)
                    # if 'drains_to' in subhexes[s]:
                    #     continue
                    ingress_hexes_t.append(s)
        ingress_hexes.append((choice(ingress_hexes_t), hexes[parent]['drains_from'][drains_from]))
        subhexes[ingress_hexes[-1][0]].update({'z': int(
            mean([hexes[parent]['altitude'], hexes[drains_from]['altitude']]))})
        for n in subhexes[ingress_hexes[-1][0]]['neighbors']:
            if subhexes[n]['z'] < subhexes[ingress_hexes[-1][0]]['z']:
                subhexes[n].update({'z': subhexes[ingress_hexes[-1][0]]['z'] + 1})

        if hexes[drains_from]['subdrainage_out']:
            hexes[parent]['subdrainage_in'][ingress_hexes[-1][0]] = hexes[drains_from]['subdrainage_out']

    if not egress_hex:
        egress_hex = None
    if not ingress_hexes:
        ingress_hexes = None
    return egress_hex, ingress_hexes


def river_in_out(parent, subhexes=subhexes):

    egress = hexes[parent]['river egress']
    if not egress:
        return []
    rivers = []
    # maybe to get it to follow preexisting paths, copy the heights, set them all to 0 on each run, then restore z
    for ingress in hexes[parent]['river ingress']:
        path = a_star(ingress[0], egress)
        for f, t in zip(path[:-1], path[1:]):
            subhexes[f].update({'drains_to': t})
            subhexes[t]['drains_from'].add(t)
        rivers.append(path)
    return rivers


def sub_drainage(parent, subhexes=subhexes):

    hexes[parent].update({'subdrainage_out': None})
    for s in subhexes:
        subhexes[s].update({'drainage': 1})
        subhexes[s].update({'drains_from': set()})
        subhexes[s].update({'drains_to': None})
        # try:
        #     del subhexes[s]['drains_to']
        # except:
        #     pass
    try:
        hexes[parent]
    except:
        print(parent)
        return
    for i in hexes[parent]['subdrainage_in']:
        subhexes[i]['drainage'] += hexes[parent]['subdrainage_in'][i]
    for source in sorted(subhexes, key=lambda x: subhexes[x]['z'], reverse=True):
        try:
            target = min([n for n in subhexes[source]['neighbors'] if
                          n not in subhexes[source]['drains_from'] and
                          subhexes[source]['z'] > subhexes[n]['z']], key=lambda x: subhexes[x]['z'])
        except ValueError:
            target = None
        if target:
            subhexes[target]['drainage'] += subhexes[source]['drainage']
            subhexes[target]['drains_from'].add(source)
        subhexes[source].update({'drains_to': target})

    if hexes[parent]['river egress']:
        hexes[parent].update({'subdrainage_out': subhexes[hexes[parent]['river egress']]['drainage']})


def sub_basins(egress_hex, subhexes=subhexes):

    return sum(subhexes[s]['drainage'] for s in subhexes if not subhexes[s]['drains_to'] and s != egress_hex)



def sub_erode(parent, subhexes=subhexes):

    # TODO add deposition in basins?
    sub_drainage(parent=parent)
    sources = [s for s in subhexes if not subhexes[s]['drains_from']]
    rivers = sorted([s for s in subhexes if subhexes[s]['drains_to']], key=lambda x: subhexes[x]['drainage'], reverse=True)
    big_rivers = river_in_out(parent=parent, subhexes=subhexes) if hexes[parent]['river ingress'] else [sources]
    for source in rivers:
        target = subhexes[source]['drains_to']
        if not target:
            river_ensure(parent, subhexes=subhexes)
            sub_drainage(parent, subhexes=subhexes)
            target = subhexes[source]['drains_to']
        slope = subhexes[source]['z'] - subhexes[target]['z']
        if slope == 1:
            continue
        dh = max(-10 ** -4.1 * sqrt(subhexes[source]['drainage']), 1 - slope)
        subhexes[source]['z'] += dh
        subhexes[choice(sum(big_rivers, []))]['z'] -= dh
        sub_drainage(parent=parent)
        # print(source, subhexes[source]['z'], subhexes[parent]['z'], dh, 1 - slope)

def wilderness_subs(infra):

    basemap.find('g').clear()
    open(filepath + '\\20 Mile Hexes\\' + parent + '.svg', 'wb').close()
    ws = {1: '', 0: ''}
    for h, w in zip(subhexes, wilderness(k = infra, m = 100)):
        ws[w] += 'M ' + ' '.join(
            ['{:},{:}'.format(hex_corner(subhexes[h]['c'], i)[0], hex_corner(subhexes[h]['c'], i)[1]) for i in
             [4, 5, 0, 1, 2, 3]]) + ' Z'
    for w in ws:
        basemap.find('g').append(basemap.new_tag('path', **{
            'd': ws[w],
            'style': 'fill:#' + ('c0d765' if w == 1 else '6aa84f')
        }))
        # basemap.find('g').append(basemap.new_tag('path', **{
        #     'd': 'M ' + ' '.join(
        #         ['{:},{:}'.format(hex_corner(subhexes[h]['c'], i)[0], hex_corner(subhexes[h]['c'], i)[1]) for i in
        #          [4, 5, 0, 1, 2, 3]]) + ' Z',
        #     'style': 'fill:#' + ('c0d765' if w == 1 else '6aa84f'),
        #     'id': h
        # }))
    with open(filepath + '\\20 Mile Hexes\\' + parent + '.svg', 'wb') as f:
        try:
            basemap.find('body').replace_with_children()
        except AttributeError:
            pass
        try:
            basemap.find('html').replace_with_children()
        except AttributeError:
            pass
        f.write(basemap.prettify('utf-8'))


def unflatten(parent, subhexes=subhexes):

    i = True
    g = 0
    while i:
        for s in subhexes:
            i = False
            if s == hexes[parent]['river egress'] or s in hexes[parent].get('river ingress', {}):
                continue
            while subhexes[s]['z'] in [subhexes[n]['z'] for n in subhexes[s]['neighbors']]:
                i = True
                subhexes[s]['z'] += 1
                subhexes[choice(subhexes[b]['neighbors'])]['z'] -= 1


def endorheics(parent, subhexes=subhexes, n=72):

    # assuming for now there is no ingress
    end = hexes[parent]['river egress']
    sub_drainage(parent=parent)
    basins = [s for s in subhexes if not subhexes[s]['drains_to'] and s != end]
    basins.sort(key=lambda b: len(a_star(b, end)))
    while True:
        b = basins[0]
        # print(b, subhexes[b]['z'], ' ', end='', flush=True)
        # m = avg(subhexes[b]['neighbors'], key=lambda x: subhexes[x]['z'])[2]
        dh = max(1, int(mean([subhexes[n]['z'] for n in subhexes[b]['neighbors']])) - subhexes[b]['z'])
        subhexes[b]['z'] += dh
        m = choice(list(s for s in subhexes if s not in basins))
        subhexes[m]['z'] -= dh
        # print(b, subhexes[b]['z'], m, subhexes[m]['z'], dh)
        # subhexes[b]['z'], subhexes[m]['z'] = subhexes[m]['z'], subhexes[b]['z']
        unflatten(parent=parent)
        river_ensure(parent=parent)
        sub_drainage(parent=parent)
        basins = [s for s in subhexes if not subhexes[s]['drains_to'] and s != end]
        basins.sort(key=lambda b: len(a_star(b, end)))
        print(len(basins), sub_basins(end))
        if sub_basins(end) < n:
            break
    # while True:
    #     b = basins[0]
    #     # print(b)
    #     path = a_star(b, end)[::-1]
    #     if subhexes[b]['z'] > (subhexes[end]['z'] + len(path)):
    #         dh = (subhexes[b]['z'] - subhexes[end]['z']) / len(path)
    #     else:
    #         dh = 1
    #     for i, p in enumerate(path[1:]):
    #         subhexes[p]['z'] = int(subhexes[end]['z'] + i * dh)
    #     sub_drainage()
    #     basins = [s for s in subhexes if 'drains_to' not in subhexes[s] and s != end]
    #     basins.sort(key=lambda b: distance(subhexes[b]['c'], subhexes[end]['c']))
    #         # print(subhexes[p]['z'])
    #     print(len(basins), sub_basins(end))



def travel(a, b):

    return subhexes[b]['z'] - subhexes[a]['z']


def a_star(start, end):

    def g(current, closed_list=[]):

        z = zip(closed_list, closed_list[1:] + [current])
        return sum(travel(i, j) for i, j in z)

    def h(current):

        return distance(subhexes[current]['c'], subhexes[end]['c'])

    def f(current):

        return g(current) + h(current)

    open_list = list(subhexes[start]['neighbors'])
    closed_list = [start]
    came_from = {sorted(open_list, key = lambda h: f(h))[0]: start}

    while open_list:
        s = sorted(open_list, key = lambda h: f(h))[0]
        open_list.remove(s)
        closed_list.append(s)
        if s == end:
            # print(closed_list)
            break
        for t in subhexes[s]['neighbors']:
            if t not in closed_list:
                if t not in open_list or g(t) < h(t):
                    open_list.append(t)
                came_from[t] = s
        # print(closed_list)

    path = [end]
    while path[-1:][0] != start:
        path.append(came_from[path[-1:][0]])
    return path[::-1]

def whole_shebang(parent, subhexes=subhexes, hexes=hexes, scale=4.5/400, endo=False, erode=0, rivers=True):

    map_gen(parent=parent, erode=erode, endo=endo, hexes=hexes, rivers=rivers)
    map_write(parent=parent, scale=scale, rivers=rivers)