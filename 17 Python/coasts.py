from defs import *
# from shapely.ops import cascaded_union, polygonize
# from scipy.spatial import Delaunay
# import shapely.geometry as geometry

# def alpha_shape(points, alpha):
#
#     """
#     Compute the alpha shape (concave hull) of a set
#     of points.
#     @param points: Iterable container of points.
#     @param alpha: alpha value to influence the
#         gooeyness of the border. Smaller numbers
#         don't fall inward as much as larger numbers.
#         Too large, and you lose everything!
#     """
#     if len(points) < 4:
#         # When you have a triangle, there is no sense
#         # in computing an alpha shape.
#         return geometry.MultiPoint(list(points)).convex_hull
#     def add_edge(edges, edge_points, coords, i, j):
#         """
#         Add a line between the i-th and j-th points,
#         if not in the list already
#         """
#         if (i, j) in edges or (j, i) in edges:
#             # already added
#             return
#         edges.add((i, j))
#         edge_points.append(coords[[i, j]])
#     coords = array([point.coords[0] for point in points])
#     tri = Delaunay(coords)
#     edges = set()
#     edge_points = []
#     # loop over triangles:
#     # ia, ib, ic = indices of corner points of the
#     # triangle
#     for ia, ib, ic in tri.vertices:
#         pa = coords[ia]
#         pb = coords[ib]
#         pc = coords[ic]
#         # Lengths of sides of triangle
#         a = sqrt((pa[0]-pb[0])**2 + (pa[1]-pb[1])**2)
#         b = sqrt((pb[0]-pc[0])**2 + (pb[1]-pc[1])**2)
#         c = sqrt((pc[0]-pa[0])**2 + (pc[1]-pa[1])**2)
#         # Semiperimeter of triangle
#         s = (a + b + c)/2.0
#         # Area of triangle by Heron's formula
#         area = sqrt(s*(s-a)*(s-b)*(s-c))
#         circum_r = a*b*c/(4.0*area)
#         # Here's the radius filter.
#         #print circum_r
#         if circum_r < 1.0/alpha:
#             add_edge(edges, edge_points, coords, ia, ib)
#             add_edge(edges, edge_points, coords, ib, ic)
#             add_edge(edges, edge_points, coords, ic, ia)
#     m = geometry.MultiLineString(edge_points)
#     triangles = list(polygonize(m))
#     return cascaded_union(triangles), edge_points


# def alpha_shape(points, alpha, only_outer=True):
#     """
#     Compute the alpha shape (concave hull) of a set of points.
#     :param points: np.array of shape (n,2) points.
#     :param alpha: alpha value.
#     :param only_outer: boolean value to specify if we keep only the outer border
#     or also inner edges.
#     :return: set of (i,j) pairs representing edges of the alpha-shape. (i,j) are
#     the indices in the points array.
#     """
#     assert points.shape[0] > 3, "Need at least four points"
#
#     def add_edge(edges, i, j):
#         """
#         Add a line between the i-th and j-th points,
#         if not in the list already
#         """
#         if (i, j) in edges or (j, i) in edges:
#             # already added
#             assert (j, i) in edges, "Can't go twice over same directed edge right?"
#             if only_outer:
#                 # if both neighboring triangles are in shape, it's not a boundary edge
#                 edges.remove((j, i))
#             return
#         edges.add((i, j))
#
#     tri = Delaunay(points)
#     edges = set()
#     # Loop over triangles:
#     # ia, ib, ic = indices of corner points of the triangle
#     for ia, ib, ic in tri.vertices:
#         pa = points[ia]
#         pb = points[ib]
#         pc = points[ic]
#         # Computing radius of triangle circumcircle
#         # www.mathalino.com/reviewer/derivation-of-formulas/derivation-of-formula-for-radius-of-circumcircle
#         a = sqrt((pa[0] - pb[0]) ** 2 + (pa[1] - pb[1]) ** 2)
#         b = sqrt((pb[0] - pc[0]) ** 2 + (pb[1] - pc[1]) ** 2)
#         c = sqrt((pc[0] - pa[0]) ** 2 + (pc[1] - pa[1]) ** 2)
#         s = (a + b + c) / 2.0
#         area = sqrt(s * (s - a) * (s - b) * (s - c))
#         circum_r = a * b * c / (4.0 * area)
#         if circum_r < alpha:
#             add_edge(edges, ia, ib)
#             add_edge(edges, ib, ic)
#             add_edge(edges, ic, ia)
#     return edges


start = '129N-17E'

def c(a, b):

    # return ((round_base(a[0], base=0.25), round_base(a[1], base=0.25)), (round_base(b[0], base=0.25), round_base(b[1], base=0.25)))
    return list(a) + list(b)
#
checked = set()
string = ''

for start in hexes:
    if start in checked:
        continue
    print(start)
    current = flood_fill(start)
    print(len(current))
    checked |= current
    coastraw = [h for h in current if len(hexes[h]['neighbors']) < 6]
    all_edges = [[c(a, b) for a, b in zip(hexes[h]['pts'], hexes[h]['pts'][1:] + hexes[h]['pts'][:1])] for h in current]
    # all_edges = [[(a, b) for a, b in zip(hexes[h]['pts'], hexes[h]['pts'][1:] + hexes[h]['pts'][:1])] for h in flood_fill(start)]
    all_edges = sum(all_edges, [])
    print(len(all_edges))

    for h in coastraw:
        for a, b in zip(hexes[h]['pts'], hexes[h]['pts'][1:] + hexes[h]['pts'][:1]):
            if sum(all(isclose(j, k, abs_tol=0.5) for j, k in zip(i, c(a, b))) or all(isclose(j, k, abs_tol=0.5) for j, k in zip(i, c(b, a))) for i in all_edges) > 1:
                continue
                print(i)
            string += '<path d="M {:}" />'.format(
                ' '.join(['{:},{:}'.format(p[0], p[1]) for p in [a, b]])
            )
    print(len(checked) / len(hexes))

with open(coastfile, 'r') as f:
    coastmap = bs(f, 'lxml')
# coastmap.find('g', id='coast-gen').contents = bs('<path d="{:}" style="stroke:black;stroke-width:15px" />'.format(string), 'lxml').find('body').contents
coastmap.find('g', id='coast-gen').contents = bs(string, 'lxml').find('body').contents
save(coastmap)

# # allhexes = [Polygon(hexes[h]['pts']) for h in flood_fill(start)]
# n = 2
# allpts = sum([hexes[h]['pts'] for h in coastraw], [])
# allpts = [
#     [
#         (a, b) for a, b in zip([
#         np.linspace(a[0], b[0], num=n) for a, b in zip(hexes[h]['pts'], hexes[h]['pts'][1:] + hexes[h]['pts'][:1])
#     ], [np.linspace(a[1], b[1], num=n) for a, b in zip(hexes[h]['pts'], hexes[h]['pts'][1:] + hexes[h]['pts'][:1])
#         ])
#     ] for h in coastraw
# ]
# allpts = sum(allpts, [])
# allpts = [[(a,b) for a, b in zip(i[0], i[1])] for i in allpts]
# allpts = array(sum(allpts, []))
# allpts = geometry.MultiPoint(list(allpts))

def reorder(concave_hull):

    a = list(concave_hull)[0]
    coast = [a]
    while coast[0][0] != coast[-1][1]:
        a = coast[-1]
        for edge in concave_hull:
            if edge in coast:
                continue
            if a == edge:
                continue
            if a[1] == edge[0]:
                coast.append(edge)
                break

# for i in range(1, 500):
#     alpha = i / 1000
#     concave_hull, edges = alpha_shape(allpts, alpha=alpha)
#     try:
#         concave_hull.exterior.coords
#     except AttributeError:
#         # print(0.02 + i/1000)
#         continue
#
#     cleaned = [e for e in edges if hypot(e[0][0] - e[1][0], e[0][1] - e[1][1]) < 60]
#
#     # string = ' '.join(['M {:},{:} {:},{:}'.format(p[0][0], p[0][1], p[1][0], p[1][1]) for p in cleaned])
#     string = 'M' +  ' '.join(['{:},{:}'.format(p[0], p[1]) for p in concave_hull.geoms[0].exterior.coords])
#     # string = ''
#     # for pt in allpts:
#     #     string += '<circle cx="{:.2f}" cy="{:.2f}" r="5" />'.format(pt[0], pt[1])
#