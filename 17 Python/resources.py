from defs import *

# import matplotlib.pyplot as plt
# import networkx as nx
# import forceatlas2 as fa2

global cities, currency, stage, labor

load_data(climate, fr"{basefile}\climate.json")
load_data(terrain, fr"{basefile}\terrain.json")
load_data(topography, fr"{basefile}\topography.json")
load_data(all_neighbors, all_neighbors_file)

# from market import gold_ref

# TODO totally redo this file to work with the new system
# TODO needs to generate resources - might need to do these manually

resourcefile = tradefilepath + r'\resources.csv'
commodityfile = tradefilepath + r'\commodities.csv'
# references_file = r'G:\Drive\D&D\Behind the Veil\Maps\tjavan hex_resources.csv'
referencesfile = tradefilepath + r'\references.csv'
# hex_resources, currency, stage, labor, commodity, cities = [], [], [], [], [], []

# cities = dict()

def load_cities():

    with open(citiesfile, 'r') as f:
        next(f)
        for line in f:
            temp = line.strip().split('\t')
            cities.update({temp[1]: {
                'qr': temp[0],
                'terrain': list(temp[2]),
                'topography': temp[3].split(' '),
                'koppen': temp[4]
            }})


commodity = dict()

def load_commodities(clear=True):

    global gp_per_oz, currency_ratio, gold_ref
    if clear: commodity.clear()
    commodity.update({'gold': {'amount': 1500, 'unit': 'oz'}})

    gp_per_oz = 0.48
    currency_ratio = 0.8
    gold_ref = 1500 / gp_per_oz

    with open(commodityfile, 'r') as f:
        headers = f.readline().strip().split(',')
        for row in f.readlines():
            if not row.strip().split(',')[1]:
                continue
            # print(row.strip().split(',')[0])
            commodity.update({row.strip().split(',')[0]: dict(zip(headers[1:], row.strip().split(',')[1:]))})
    for c in list(commodity):
        if not commodity[c]['cp/unit']:
            del commodity[c]
            continue
        # print(c)
        commodity[c]['stage'] = int(commodity[c]['stage']) if commodity[c]['stage'] not in ['p', 'x'] else commodity[c]['stage']
        commodity[c]['tech level'] = int(commodity[c]['tech level'])
        commodity[c]['weight'] = float(commodity[c]['weight']) if commodity[c]['stage'] not in ['p', 'x'] else None
        commodity[c]['occurrence'] = float(commodity[c]['occurrence']) if commodity[c]['occurrence'] else None
        commodity[c]['terrain'] = commodity[c]['terrain'].split(';')
        commodity[c]['koppen'] = commodity[c]['koppen'].split(';') if commodity[c]['koppen'] else []
        commodity[c]['coincident'] = commodity[c]['coincident'].split(';')
        commodity[c]['topography'] = commodity[c]['topography'].split(';')
        commodity[c]['unit/ref'] = float(commodity[c]['unit/ref']) if commodity[c]['unit/ref'] else None
        commodity[c]['ref/acre'] = float(commodity[c]['ref/acre']) if commodity[c]['ref/acre'] else None
        commodity[c]['acre/pop'] = float(commodity[c]['acre/pop']) if commodity[c]['acre/pop'] else None
        commodity[c]['ref/1000pop'] = float(commodity[c]['ref/1000pop']) if commodity[c]['ref/1000pop'] else None
        commodity[c]['recipe'] = eval(re.sub(';', ',', commodity[c]['recipe']) or '{}')
        commodity[c]['cp/unit'] = round(100 * gold_ref / commodity[c]['unit/ref'], 4) if commodity[c]['stage'] == 0 else (float(commodity[c]['cp/unit']) if commodity[c]['stage'] == 'p' else None)
        # commodity[c]['unit/gref'] = round(100 * gold_ref / commodity[c]['cp/unit'], 4) if commodity[c]['stage'] == 0 else None

    # for c in commodity:
    #     if commodity[c]['lat'] is not None:
    #         try:
    #             commodity[c]['lat'][0][0]
    #         except TypeError:
    #             commodity[c].update({'lat': [commodity[c]['lat']]})

    global currency, stage, labor
    currency = ['gold']
    stage = [[] for i in range(0, 6)]
    labor = []
    for c in commodity:
        if c in currency:
            continue
        if commodity[c]['stage'] in ['p']:
            labor.append(c)
            commodity[c]['unit/ref'] = round(100 * gold_ref / commodity[c]['cp/unit'], 4)
            continue
        if commodity[c]['stage'] in ['x']:
            continue
        try:
            stage[commodity[c]['stage']].append(c)
        except IndexError:
            print(c, commodity[c]['stage'])
        except TypeError:
            print(c, commodity[c]['stage'])

    verify_commodities()



def verify_commodities():

    for i in commodity:
        # try:
        s = commodity[i]['stage']
        if s == 'p':
            continue
        for j in commodity[i]['recipe']:
            if j == 'labor':
                continue
            if j in ['min', 'max', 'min1', 'min2']:
                if all(commodity[k]['stage'] >= s for k in commodity[i]['recipe'][j]):
                    for k in commodity[i]['recipe'][j]:
                        if commodity[k]['stage'] >= s:
                            print(i + ' comes before ' + k + ' (stage)')
                continue
            if j in ['avg']:
                for k in commodity[i]['recipe'][j]:
                    if commodity[k]['stage'] >= s:
                        print(i + ' comes before ' + k + ' (stage)')
                continue
            if commodity[j]['stage'] >= s:
                print(i + ' comes before ' + j + ' (stage)')
                    # log(i + ' comes before ' + j + ' (stage)')
        # except KeyError:
        #     pass
        # try:
        s = commodity[i]['tech level']
        for j in commodity[i]['recipe']:
            if j == 'labor':
                continue
            if j in ['min', 'max', 'min1', 'min2']:
                if all(commodity[k]['stage'] >= s for k in commodity[i]['recipe'][j]):
                    for k in commodity[i]['recipe'][j]:
                        if commodity[k]['tech level'] > s:
                            print(i + ' comes before ' + k + ' (stage)')
                continue
            if j in ['avg']:
                for k in commodity[i]['recipe'][j]:
                    if commodity[k]['tech level'] > s:
                        print(i + ' comes before ' + k + ' (stage)')
                continue
            if commodity[j]['tech level'] > s:
                print(i + ' comes before ' + j + ' (tech)')
                # log(i + ' comes before ' + j + ' (tech)')
        # except KeyError:
        #     pass


def load_hex_resources():

    global hex_resources
    hex_resources = []
    with open(resource_file, 'r', encoding = 'UTF-8') as f:
        next(f)
        for line in f:
            hex_resources.append(dict(zip(
                ['update', 'q', 'r', 'ht', 'terrain', 'resources'],
                line.rstrip('\n').split(',')
            )))
    for h in hex_resources:
        h['update'] = int(h['update'])
        h['ht'] = int(h['ht'])
        h['terrain'] = list(h['terrain'])
        if 'N' in h['q']:
            h.update({'lat': round(90 - int(h['q'].strip('N')) * 90 / 207, 1)})
        elif 'S' in h['q']:
            h.update({'lat': -round(90 - int(h['q'].strip('S')) * 90 / 207, 1)})
        else:
            h.update({'lat': 0})

    for c in cities:
        for h in hex_resources:
            # TODO O(n3) process!!!
            if h['q'] == c['q'] and h['r'] == c['r']:
                h.update({'pop': c['pop']})
                h.update({'name': c['name']})
                h.update({'urbanization': c['urbanization']})
                for t in tech_levels:
                    if c['pop'] in tech_levels[t]:
                        h.update({'tech level': t})

tech_levels = {
    5: range(0, 172),
    6: range(172, 360),
    7: range(360, 756),
    8: range(756, 1588),
    9: range(1588, 3334),
    10: range(3334, 7001),
    11: range(7001, 12851),
    12: range(12851, 23959),
    13: range(23959, 44432),
    14: range(44432, 88644),
    15: range(88644, 186151),
    16: range(186151, 409531),
    17: range(409531, 990001)
}

tech_infra = dict(zip(range(5, 18), [0, 1, 2, 5, 10, 20, 37, 69, 128, 160, 534, 1180, 2882]))

tech_infra = dict(zip(range(5, 18), [round(i / 346.5 * 5) for i in [172, 360, 756, 1588, 3334, 7001, 12851, 23959, 44432, 88644, 186151, 409531, 990001]]))

def industry_occurence(pop, sv):

    return (pop >= sv) * (p - sv) / (p - sv / 2)


def update():

    allav = set()
    while not all(i in allav for i in ['limonite', 'gold', 'silver']):
        allav = set()
        for city in cities:
            availablity(city)
            allav |= cities[city]['available resources']
    for city in cities:
        print(city, cities[city]['available resources'])

load_data(hex_resources, tradefilepath + r'\resources.p')


def resources_assign_base(clear=True, new=None):

    for h in terrain:
        if clear and not new:
            hex_resources.setdefault(h, set())
            hex_resources[h].clear()
        if h not in hex_resources:
            hex_resources.setdefault(h, set())
            hex_resources[h].clear()
        hex_resources[h].add(availability(h, clear=clear, new=new))
    save_data(hex_resources, tradefilepath + r'\resources.p')


def availability(hex, clear=True, new=None):

    cs = list(commodity.keys())
    random.shuffle(cs)
    # TODO: special production locations for raw materials or industry, coastal/river/lake, geographical area restriction
    for c in cs:
        if new and c not in new:
            continue
        if commodity[c]['stage']:
            continue
        p = commodity[c]['occurrence']
        if not p:
            p = 1
            for criterion in ['terrain', 'koppen', 'topography']:
                if not commodity[c].get(criterion) or not commodity[c].get(criterion)[0]:
                    continue
                # print(any(i in commodity[c].get(criterion) for i in cities[hex].get(criterion, {})))
                if criterion == 'terrain':
                    p *= any(i in commodity[c].get(criterion) for i in terrain[hex])
                elif criterion == 'koppen':
                    p *= climate[hex].get(criterion, {}) in commodity[c].get(criterion)
                elif criterion == 'topography':
                    p *= any(i in commodity[c].get(criterion) for i in topography[hex].split(' '))
        else:
            if any(i in commodity[c]['coincident'] for i in hex_resources[hex]):
                p *= 1.5
            for criterion in ['terrain', 'koppen', 'topography']:
                if not commodity[c].get(criterion) or not commodity[c].get(criterion)[0]:
                    continue
                # print(any(i in commodity[c].get(criterion) for i in cities[hex].get(criterion, {})))
                if criterion == 'terrain':
                    p *= 1.5 if any(i in commodity[c].get(criterion) for i in terrain[hex]) else 1
                elif criterion == 'koppen':
                    p *= 1.5 if climate[hex].get(criterion, {}) in commodity[c].get(criterion) else 1
                elif criterion == 'topography':
                    p *= 1.5 if any(i in commodity[c].get(criterion) for i in topography[hex].split(' ')) else 1
        if any(c in hex_resources.get(n, {}) for n in all_neighbors[h]):
            p *= 1.75
        # print(c, p)
        if random.random() < p:
            return c



def reference(pop, c):

    if c in stage[0]:
        if commodity[c]['acre/pop'] and commodity[c]['/acre']:
            per_ref = commodity[c]['amount']
            acre_pop = commodity[c]['acre/pop']
            per_acre = commodity[c]['/acre']
            acres = random.randrange(1, acre_pop * 1000)
            return round(acres * per_acre / per_ref, 4)
        if not commodity[c]['acre/pop'] and not commodity[c]['/acre'] and commodity[c]['roll']:
            return parse_roll(commodity[c]['roll'])
    if c in labor:
        try:
            if pop >= commodity[c]['recipe']['SV']:
                return 'x'
            else:
                return ''
        except KeyError:
            pass
            # print(c)


# def parse_roll(dieroll):
#
#     dieroll = dieroll.split('+')
#     mod = int(dieroll[1]) if len(dieroll) > 1 else 0
#     die = [int(i) for i in dieroll[0].split('d')]
#     total = mod
#     for d in range(die[0]):
#         total += roll(die[1])
#     return total


def resources_save():

    save_data(hex_resources, tradefilepath + r'\resources.p')


def map_resource_svg(r, threshold=0.15, col='ff0000', w=255, k=1, s=1/4, freq=500, g=3):

    """
    Write a heatmap of reserves to file
    :param races:
    :return:
    """

    with open(basefile + r'\Reserves.svg', 'r') as f:
        file = bs(f, 'lxml')
    for group in file.find_all('g'):
        group.clear()
    t = sorted([h for h in hexes if r in hex_resources[h]], key=lambda h: noise.pnoise2(hexes[h]['c'][0] / freq, hexes[h]['c'][1] / freq, octaves=2, repeatx=63000, repeaty=63000), reverse=True)
    string = ''
    # m = max(sociology[h]['desirability_raw'][race] for h in hexes if h not in cities)
    c = {h: noise.pnoise2(hexes[h]['c'][0] / freq, hexes[h]['c'][1] / freq, octaves=1, repeatx=63000, repeaty=63000) / 2 + 0.5 for h in t}
    min_c, max_c = min(c.values()), max(c.values())
    c = {h: rescale(c[h], min_c, max_c, 0.01, 1) for h in c}
    for _ in range(g):
        backup = copy.deepcopy(c)
        for h in t:
            if not hexes[h]['neighbors']:
                continue
            c[h] = statistics.mean([backup[n] if n in t else 0 for n in hexes[h]['neighbors']] + [backup[h]])
    min_c, max_c = min(c.values()), max(c.values())
    c = {h: rescale(c[h], min_c, max_c, 0.01, 1) for h in c}
    c = {h: f"{screen(col, w - w * (c[h] / 1) ** k)}" for h in t}
    for h in hexes:
        if h not in c:
            continue
        if c[h] == 'ffffff':
            continue
        string += f"<path d='M {' '.join([','.join([str(i) for i in p]) for p in hexes[h]['pts']])} Z' class='hex' id='{h}' style='color:#{c[h]}' />"

    if string:
        file.find('g', id='reserves').contents = bs(string, 'lxml').find('body').contents
    else:
        file.find('g', id='reserves').clear()
    save(file, extra_file=basefile + r'\Reserves.svg')


def perlin(h, base=0, freq=500):

    n = noise.pnoise2(hexdict[h][0][0] / freq, hexdict[h][0][1] / freq, octaves=5, repeatx=63000, repeaty=63000, base=base)
    return n


def map_resource_png(r, reserve=False, map_file=fr"{tradefilepath}\Maps", col='ff0000', w=255, k=1, s=1/4, freq=500, base=0, verbose=False, occ=None):

    fig, ax = plt.subplots()
    # fig = plt.figure()
    # ax = plt.Axes(fig, [0., 0., 1., 1.])
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#bad9ff'))

    yh = {h: hexdict[h][0][0] for h in terrain.keys() if hexdict[h][0][1] >= hexdict['207-518W'][0][1]}
    xh = {h: hexdict[h][0][1] + 50 for h in terrain.keys() if hexdict[h][0][1] >= hexdict['207-518W'][0][1]}
    y = [yh[h] for h in yh.keys() if h in terrain]
    x = [xh[h] for h in xh.keys() if h in terrain]
    ax.scatter(x=x, y=y, marker='h', c='w', edgecolors='w', s=1 / 4, zorder=100, linewidths=s / 2)

    # if base == 0:
    #     base = int(statistics.mean(ord(i) for i in r))

    # t = sorted([h for h in terrain if r in hex_resources[h]], key=lambda h: perlin(h), reverse=True)
    t = sorted([h for h in terrain if climate[h]['koppen'] in commodity[r]['koppen']])
    if occ:
        # c = {h: perlin(h) for h in t}
        # min_c, max_c = min(c.values()), max(c.values())
        # c = {h: rescale(c[h], min_c, max_c, 0, 1) for h in c}
        # t = [h for h in t if c[h] > 1 - occ]
        t = sorted(t, key=lambda h: perlin(h, freq=freq), reverse=True)[:int(len(t) * occ)]
    y = [hexdict[h][0][0] for h in t]
    x = [hexdict[h][0][1] + 50 for h in t]
    c = {h: perlin(h, freq=freq) for h in t}
    min_c, max_c = min(c.values()), max(c.values())
    c = {h: rescale(c[h], min_c, max_c, 0, 1) for h in c}
    c = [colors.hex2color(f"#{screen(col, w * (1 - c[h] / 1) ** k)}") for h in t]
    # c += [colors.hex2color(f"#{screen(hegemonies.get(sociology[h]['hinterland'], dict()).get('color', 'ffffff'), w - w * ((sociology[h].get('population', 0)) / m) ** k)}") for h in hinterlands]
    ax.scatter(x=x, y=y, marker='h', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    # fig.set_size_inches(6, 4)
    # ax.annotate(f"{year: 5}", (0.0, 0.0), xycoords='axes fraction', size=12, fontfamily='monospace')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{map_file}\{r}.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()

# load_cities()
# load_hex_resources()
# load_commodities()