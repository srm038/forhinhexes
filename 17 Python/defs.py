import json
import math
import os
import pickle
import random
import re
import statistics
import sys
from datetime import datetime

import matplotlib as mpl
import matplotlib.colors as colors
import matplotlib.path as mplPath
import matplotlib.pyplot as plt
import numpy as np
import scipy.spatial.distance as ssd
import xdice
import gc

mpl.use('Agg')

os.chdir('..\\')

basefile = os.getcwd() + r'\02 Documents and Images\Images\Maps'
worldmapfile = basefile + r'\World Map v7.svg'
labelfile = basefile + r'\Labels.svg'
influencesfile = basefile + r'\Influences.svg'
# temperaturesfile = basefile + r'\Temperature.svg'
elevationfile = basefile + r'\Elevation.svg'
windfile = basefile + r'\Winds.svg'
riverfile = basefile + r'\Rivers.svg'
coastfile = basefile + r'\Coast.svg'
terrainfile = basefile + r'\Terrain.svg'
pressurefile = basefile + r'\Pressure.svg'
rawhexfile = basefile + r'\Raw Hexes.svg'

hex_file = basefile + r'\hexes.csv'
hex_points_file = basefile + r'\hex_points.dict'
all_neighbors_file = basefile + r'\all_neighbors.p'
pressurepickle = basefile + r'\pressures.json'
hexpickle = basefile + r'\hexes.p'
coastpickle = basefile + r'\coasts.p'
cities_file = basefile + r'\cities.json'
hegemonies_file = basefile + r'\hegemonies.p'
seaways_file = basefile + r'\seaways.p'
destroyed_file = basefile + r'\destroyed.p'
wars_file = basefile + r'\wars.p'
city_size_file = basefile + r'\citysize.p'
hegemony_size_file = basefile + r'\hegemonysize.p'
battles_file = basefile + r'\battles.p'
asabiya_file = basefile + r'\asabiya.p'
climate_file = basefile + r'\climate.json'
terrain_file = basefile + r'\terrain.json'
sociology_file = basefile + r'\sociology.json'
tribes_file = basefile + r'\tribes.json'

tradefilepath = os.getcwd() + r'\15 Locations\Trade'

hexes = dict()
hexdict = dict()
hexMap = dict()
all_neighbors = dict()
cities = dict()
hegemonies = dict()
destroyed = dict()
seaways = dict()
currents = dict()
currentsTemp = dict()
climate = dict()
terrain = dict()
sociology = dict()
coasts = list()
wars = dict()
citysizes = dict()
hegemonysizes = dict()
asabiya = dict()
battles = dict()
tribes = dict()
tectonics = dict()
faults = dict()
plates = dict()
hexPlates = dict()
plateTypes = dict()
plateRotations = dict()
temperature = dict()
pressure = dict()
plateInfluences = dict()
elevation = dict()
ice = dict()
wind_land = dict()
wind = dict()
rain = dict()
influences = dict()
topography = dict()
hex_resources = dict()

origin = (17846.63, 31066.21)
north_pole = (17846.63, 46591.22)
south_pole = (17846.64, 15541.25)
view_box = (35693.257, 69168.8)
equator = (26810.01, 31066.21)

temp_ranges = {
    '#9e0142': range(95, 150),
    '#d53e4f': range(83, 95),
    '#f46d43': range(72, 83),
    '#fdae61': range(65, 72),
    '#fee08b': range(50, 65),
    '#e6f598': range(32, 50),
    '#abdda4': range(14, 32),
    '#66c2a4': range(-13, 14),
    '#3288bd': range(-36, 13),
    '#5e4fa2': range(-50, -36),
}

barbs = {
    (0, 5): 'barb0',
    (5, 10): 'barb5',
    (10, 15): 'barb10',
    (15, 20): 'barb15',
    (20, 25): 'barb20',
    (25, 30): 'barb25',
    (30, 35): 'barb30',
    (35, 40): 'barb35',
    (40, 45): 'barb40',
    (45, 50): 'barb45',
    (50, 55): 'barb50',
}

hold2color = dict(zip(
    ['humid polar desert', 'perhumid polar desert', 'superhumid polar desert', 'subhumid subpolar dry tundra',
     'humid subpolar moist tundra', 'perhumid subpolar wet tundra', 'superhumid subpolar rain tundra',
     'semiarid boreal desert', 'subhumid boreal dry scrub', 'humid boreal moist forest', 'perhumid boreal wet forest',
     'superhumid boreal rain forest', 'arid cool temperate desert', 'semiarid cool temperate desert scrub',
     'subhumid cool temperate steppe', 'humid cool temperate moist forest', 'perhumid cool temperate wet forest',
     'superhumid cool temperate rain forest', 'perarid subtropical desert', 'arid subtropical desert scrub',
     'semiarid subtropical thorn steppe', 'subhumid subtropical dry forest', 'humid subtropical moist forest',
     'perhumid subtropical wet forest', 'superhumid subtropical rain forest', 'superarid tropical desert',
     'perarid tropical desert scrub', 'arid tropical thorn woodland', 'semiarid tropical very dry forest',
     'subhumid tropical dry forest', 'humid tropical moist forest', 'perhumid tropical wet forest',
     'superhumid tropical rain forest'],
    ['#FFFFFF', '#FFFFFF', '#FFFFFF', '#808080', '#608080', '#408090', '#2080C0', '#A0A080', '#80A080', '#60A080',
     '#40A090', '#20A0C0', '#C0C080', '#A0C080', '#80C080', '#60C080', '#40C090', '#20C0C0', '#E0E080', '#C0E080',
     '#A0E080', '#80E080', '#60E080', '#40E090', '#20E0C0', '#FFFF80', '#E0FF80', '#C0FF80', '#A0FF80', '#80FF80',
     '#60FF80', '#40FF90', '#20FFA0']
))

color2hold = {hold2color[key]: key for key in hold2color}

koppen2color = dict(zip(
    ['Af', 'Am', 'Aw', 'As', 'BWh', 'BWk', 'BSh', 'BSk', 'Csa', 'Csb', 'Csc', 'Cwa', 'Cwb', 'Cwc', 'Cfa', 'Cfb', 'Cfc',
     'Dsa', 'Dsb', 'Dsc', 'Dsd', 'Dwa', 'Dwb', 'Dwc', 'Dwd', 'Dfa', 'Dfb', 'Dfc', 'Dfd', 'ET', 'EF'],
    ['#0000FE', '#0077FF', '#46A9FA', '#46A9FA', '#FE0000', '#FE9695', '#F5A301', '#FFDB63', '#FFFF00', '#C6C700',
     '#C6C700', '#96FF96', '#63C764', '#63C764', '#C6FF4E', '#66FF33', '#33C701', '#FF00FE', '#C600C7', '#963295',
     '#966495', '#ABB1FF', '#5A77DB', '#4C51B5', '#320087', '#00FFFF', '#38C7FF', '#007E7D', '#00455E', '#B2B2B2',
     '#686868']
))

color2koppen = {koppen2color[key]: key for key in koppen2color}

color2terr = {
    # '76C84F': 'f',
    'e6ede9': 'i',
    # '#FF0000': 'x',
    'ab9c58': 'w',
    'deaa88': 'd',
    '0c442d': 'r',
    '586c44': 't',
    '637f79': 'm',
    '4a4d3b': 'f',
    '6b7462': 'b',
    '4e2728': 'v',
    '8e8583': 'k',
    '5b8466': 'c',
    '38914a': 'j',
    'd3ecdb': 's',
    '658e6c': 'n',
    'ded5c2': 'p',
    '91cb7d': 'g',
}

terr2color = {color2terr[key]: key for key in color2terr}

settled = {
    'j': 'g',
    'r': 'g',
    'n': 'g',
    'c': 'g',
    'w': 'g',
    'b': 'g',
    'p': 'p',
    'd': 'p',
    'g': 'g',
    'k': 's',
    's': 's'
}

influences = {
    '#b09f0d': 'cont+',
    '#ffe400': 'cont',
    '#1b41cc': 'cold',
    '#00ff6c': 'mild',
    '#ff0000': 'hot'

}

sector_rotate = {
    1: 60,
    2: 0,
    3: -60,
    4: 120,
    5: 180,
    6: -120,
    7: -120,
    8: 180,
    9: 120,
    10: -60,
    11: 0,
    12: 60
}

sectorRotate = {
    1: 60 * 2,
    2: 60 * 3,
    3: -60 * 2,
    4: 60,
    5: 0,
    6: -60,
    7: -60,
    8: 0,
    9: 60 * 1,
    10: -60 * 2,
    11: -60 * 3,
    12: 60 * 2
}

hexMiles = 20
hexArea = 2 * math.sqrt(3) * (hexMiles / 2) ** 2
hexPx = 90
hexr = hexPx / 2
hexR = 2 / math.sqrt(3) * hexr
nRings = 311
hexPts = lambda x, y, theta: (round(x + hexR * math.cos(math.radians(theta)), 5), round(y + hexR * math.sin(math.radians(theta)), 5))
hexNeighbor = lambda x, y, theta: (round(x + 2 * hexr * math.cos(math.radians(theta)), 5), round(y + 2 * hexr * math.sin(math.radians(theta)), 5))
latitudeY = lambda ring: (round(ring * 2 * hexr * math.cos(math.radians(60)), 5), round(ring * 2 * hexr * math.sin(math.radians(60)), 5))
hexReflect = lambda x, y: (x, round(2 * hexMap['311-1W'][1] - y, 5))


def load_data(data, file):

    """
    Updates an existing dict, list, or set from data which has already been saved in a pickle or json file. This is
    useful to preserve memory references to needed structures across files.
    :param data: variable to be updated
    :param file: location of pickle/json file
    :return:
    """

    p = json if 'json' in file else pickle

    with open(file, 'rb' if p == pickle else 'r') as f:
        temp = p.load(f)
    data.clear()
    if type(data) == dict:
        for t in temp:
            data[t] = temp[t]
    elif type(data) == list:
        for t in temp:
            data.append(t)
    elif type(data) == set:
        for t in temp:
            data.add(t)


def save_data(data, file):

    """
    Saves an existing dict, list, or set
    :param data: variable to be saved
    :param file: location of pickle/json file
    :return:
    """

    p = json if 'json' in file else pickle

    with open(file, 'wb' if p == pickle else 'w') as f:
        p.dump(data, f)


# load_data(hexes, hexpickle)
load_data(hexdict, hex_points_file)
load_data(hexMap, fr"{basefile}\hexMap.json")
hexMapPt = {tuple(v): k for k, v in hexMap.items()}
# load_data(all_neighbors, all_neighbors_file)
# load_data(cities, cities_file)
# load_data(hegemonies, hegemonies_file)
# load_data(seaways, seaways_file)
# load_data(destroyed, destroyed_file)


def population_rural(c):

    k = 0.0027674264924352600978
    L = 60 * 2 * 346.5
    I = hexes[c].get('infrastructure', 0)
    if not I:
        return 0
    I0 = 2268 / 2
    try:
        race = cities[c]['race'] if c in cities else cities[hegemonies[hexes[c]['hinterland']]['capital']]['race']
    except:
        print(f"Hinterland error: {c}")
        return 0
        # raise KeyError
    d = sociology[c]['desirability'][race]
    return round(L * d / (1 + exp(-k * (I - I0))))


def relationship(i, j):

    try:
        return hegemonies[cities.get(i, dict()).get('hegemony', hexes[i].get('hinterland'))]['conflict'].get(
            cities.get(j, dict()).get('hegemony', hexes[j].get('hinterland')), 0)
    except KeyError:
        # print(i, j)
        return 2


def is_coastal(h, ocean):

    if h in ocean:
        return False
    else:
        return any(n in ocean for n in all_neighbors.get(h, {}))


def nearCoast(h, ocean):

    if h not in ocean:
        return False
    else:
        return any(n not in ocean for n in neighbors(h))


def rawAngleN(h, g, base=1):

    if g not in neighbors(h):
        return None
    if 'S' not in h and 'S' in g:
        return None
    if 'S' in h and 'S' not in g:
        return None

    return rawAngle(h, g, base=base)


def hexAngle(h, g):
    if g not in neighbors(h):
        return None

    n = neighbors(h)

    if h == '0N-0':
        pass
    if h == '0S-0':
        pass

    g = parseID(g)
    h = parseID(h)

    if h[1] == 311 and g[1] == -310:
        g = (g[0], 312)
        n = [i if parseID(i)[1] != -310 else '312N-' + i.split('-')[1] for i in n]
    elif h[1] == -310 and g[1] == 311:
        g = (g[0], -311)
        n = [i if parseID(i)[1] != 311 else '311S-' + i.split('-')[1] for i in n]

    if h[1] == g[1]:
        if sorted([h[0], g[0]]) == [-1, 0]:
            return 0 if h[0] < g[0] else 180
        if math.copysign(1, h[0]) != math.copysign(1, g[0]):
            return 0
        if h[0] < 0 < g[0]:
            return 0
        elif g[0] < 0 < h[0]:
            return 180
        elif h[0] < g[0]:
            return 0
        else:
            return 180
    elif h[1] > g[1]:
        n = [i for i in n if parseID(i)[1] < h[1]]
        if g[0] == parseID(min(n, key=lambda x: parseID(x)[0]))[0]:
            return 120
        else:
            return 60
    elif h[1] < g[1]:
        n = [i for i in n if parseID(i)[1] > h[1]]
        if g[0] == parseID(min(n, key=lambda x: parseID(x)[0]))[0]:
            return 240
        else:
            return 300


def getTarget(source, angle):

    angle = roundBase(angle, base=60) % 360
    target = {hexAngle(source, n): n for n in neighbors(source)}.get(angle, None)
    return target


def getRawTarget(source, angle):

    angle = roundBase(angle, base=60) % 360
    target = {rawAngle(source, n) % 360: n for n in neighbors(source)}.get(angle, None)
    return target


def reconstruct_id(h):
    x, y = h
    g = f"{abs(y)}{'S' if y < 0 else 'N' if y > 207 else ''}-{abs(x)}{'W' if x < 0 else 'E' if x > 0 else ''}"
    return g


# class SetEncoder(json.JSONEncoder):
#
#     def default(self, obj):
#         if isinstance(obj, set):
#             return list(obj)
#         return json.JSONEncoder.default(self, obj)
#
# def as_python_object(dct):
#     if '_python_object' in dct:
#         return pickle.loads(str(dct['_python_object']))
#     return dct


def check_target(h, theta, hexes=hexes):
    t = latlong((
        round(hexdict[h][0][0] + 50 * math.sqrt(3) * math.cos(math.radians(theta)), 2),
        round(hexdict[h][0][1] + 50 * math.sqrt(3) * math.sin(math.radians(theta)), 2)
    ))

    return t


def rawAngle(i, j, base=60):

    return roundBase(math.degrees(math.atan2(
        hexMap[j][1] - hexMap[i][1],
        hexMap[j][0] - hexMap[i][0],
    )), base=base)


def neighbors(h):

    _neighbors = set()
    for i in range(6):
        n = hexNeighbor(*hexMap[h], i * 60)
        # print(i*60, n)
        for m in [n, (n[0], round(n[1] - 0.00001, 5)), (n[0], round(n[1] + 0.00001, 5))]:
            # print(m)
            if tuple(m) in hexMapPt:
                # print(m, hexMapPt[m])
                _neighbors.add(hexMapPt[tuple(m)])
                break
        if '310S-' in h:
            n  = hexReflect(*n)
            # print(n, n in hexMapPt)
            for m in [n, (n[0], round(n[1] - 0.00001, 5)), (n[0], round(n[1] + 0.00001, 5))]:
                if tuple(m) in hexMapPt:
                    # print(m, hexMapPt[m])
                    if '311-' in hexMapPt[tuple(m)]:
                        _neighbors.add(hexMapPt[tuple(m)])
                        break
    # if '310S' in h:
    #     for i in list(_neighbors):
    #         if '309S' not in i:
    #             continue
    #         else:
    #             _neighbors.add('311-' + i.split('-')[1])
    if '311-' in h:
        for i in list(_neighbors):
            if '310N-' not in i:
                continue
            else:
                _neighbors.add('310S-' + i.split('-')[1])
                # n = hexReflect(*hexMap[i])
            # print(n)
            # for m in [n, (n[0], round(n[1] - 0.00001, 5)), (n[0], round(n[1] + 0.00001, 5))]:
            #     if tuple(m) in hexMapPt:
            #         # print(hexMapPt[m])
            #         _neighbors.add(hexMapPt[tuple(m)])
            #         break
    return list(_neighbors)


def latitude(h):
    """
    Adds latitude to the hex object. Why don't I just roll this into the load?
    :return:
    """

    # for h in hexes:
    p = parseID(h)
    return math.copysign(round(abs(p[1]) * -90 / 311 + 90, 2), p[1])


def parseID(id):
    # the negative is not the right way to do this; it doesn't work

    x = id.split('-')[1]
    y = id.split('-')[0]
    if 'E' in x:
        x = int(x[:-1])
    elif 'W' in x:
        x = -int(x[:-1])
    else:
        x = int(x)
    if 'N' in y:
        y = int(y[:-1])
    elif 'S' in y:
        y = -int(y[:-1])
    else:
        y = int(y)
    return x, y


def distToCoast(h, ocean):
    if h in ocean:
        return 0
    current = set(neighbors(h))
    visited = set()
    d = 1
    while len(current):
        for c in list(current):
            if c in visited:
                continue
            if c in ocean:
                return d
            else:
                current |= set(neighbors(c))
                current.discard(c)
                visited.add(c)
        # print(len(current), len(visited), d)
        d += 1


def save(soup, extra_file=1, hexes=hexes):
    name = soup.find('svg')['sodipodi:docname']
    f = basefile + '\\' + name

    with open(basefile + r'\style.css', 'r') as s:
        style = s.read()

    soup.find('style').string = style

    with open(f[:-4] + '.svg', 'wb') as f:
        try:
            soup.find('body').replace_with_children()
        except AttributeError:
            pass
        try:
            soup.find('html').replace_with_children()
        except AttributeError:
            pass
        f.write(soup.prettify('utf-8'))
        # f.write(str(soup))
    if name == 'World Map v7':
        with open(worldmapfile, 'r') as f:
            lines = f.read()
        lines = re.sub('clippath', 'clipPath', lines)
        with open(worldmapfile, 'w') as f:
            f.write(lines)
    if name == 'Terrain':
        with open(basefile + r'\Terrain.svg', 'r') as f:
            lines = f.read()
        lines = re.sub('clippath', 'clipPath', lines)
        with open(basefile + r'\Terrain.svg', 'w') as f:
            f.write(lines)
        # f.write(str(soup))
    # with open(hexpickle, 'wb') as f:
    #     pickle.dump(hexes, f)
    # open(hex_file, 'w').close()
    # with open(hex_file, 'w') as f:
    #     f.write('lat,long,altitude,koppen,holdridge,terrain,topography,city\n')
    # with open(hex_file, 'a') as f:
    #     for h in hexes:
    #         f.write('{:},{:},{:},{:},{:},{:},{:}\n'.format(
    #             hexes[h]['id'].split('-')[0],
    #             hexes[h]['id'].split('-')[1],
    #             terrain[h]['altitude'],
    #             climate[h]['koppen'],
    #             climate[h]['holdridge'],
    #             terrain[h]['terrain'],
    #             terrain[h]['topography'],
    #             sociology[h].get('city', False))
    #         )


def timer(state, prec='s'):
    """
    """

    global start, stop

    if state != 0 and state != 1:
        # log('Timer is broken')
        return

    if not state:
        start = datetime.now()
    else:
        stop = datetime.now()
        # print(str((stop - start).seconds) + " s")
        # log(str((stop - start).seconds) + " s")
        if prec == 's':
            return (stop - start).seconds
        elif prec == 'ms':
            return (stop - start).microseconds


class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = None

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = self._original_stdout


def clean_path(path):
    path = re.sub('L ', ' ', path)
    path = re.sub(' +', ' ', path)
    i = 0
    while re.search('[VH] (-?\d+\.?\d*) (-?\d+\.?\d*) ', path):
        path = re.sub('V (-?\d+\.?\d*) (-?\d+\.?\d*) ', 'V \g<2> ', path)
        path = re.sub('H (-?\d+\.?\d*) (-?\d+\.?\d*) ', 'H \g<2> ', path)
    while 'V' in path or 'H' in path:
        # path = re.sub('(-?\d+\.?\d*),(-?\d+\.?\d*) V (-?\d+\.?\d*) H (-?\d+\.?\d*)',
        #               '\g<1>,\g<2> \g<1>,\g<3> \g<4>,\g<3>', path)
        # path = re.sub('(-?\d+\.?\d*),(-?\d+\.?\d*) H (-?\d+\.?\d*) V (-?\d+\.?\d*)',
        #               '\g<1>,\g<2> \g<3>,\g<2> \g<3>,\g<4>', path)
        path = re.sub('(-?\d+\.?\d*),(-?\d+\.?\d*) H (-?\d+\.?\d*)', '\g<1>,\g<2> \g<3>,\g<2>', path)
        path = re.sub('(-?\d+\.?\d*),(-?\d+\.?\d*) V (-?\d+\.?\d*)', '\g<1>,\g<2> \g<1>,\g<3>', path)
        i += 1
        if i > 1000:
            print(path)
            break
    path = re.sub(' +', ' ', path)
    if path[-1] == ' ':
        path = path[:-1]

    return path


def parse_coordinate_list(path):
    """
    converts absolute d-string paths into coordinate lists
    :param path:
    :return:
    """

    # id = path['id']
    if type(path) != str:
        path = path['d']
    path = clean_path(path).split(' ')
    parsed = [eval(pt) for pt in path if pt not in ['', 'M', 'Z']]
    return parsed
    # path = re.sub('([A-Za-z])(\d)', '\g<1> \g<2>', path)
    # path = re.sub('(\d)([A-Za-z])', '\g<1> \g<2>', path)
    # parsed = []
    # i = 0
    # while i < len(path):
    #     if path[i] in ['M', 'Z', 'm', 'z', 'L', 'l', 'C', 'c']:
    #         i += 1
    #     elif path[i] == 'H':
    #         # print(path[i + 1])
    #         try:
    #             parsed.append((
    #                 round(eval(path[i + 1]), 2),
    #                 round(eval(path[i - 1])[1], 2)
    #             ))
    #         except TypeError:
    #             # print(id)
    #             return
    #         i += 2
    #     elif path[i] == 'V':
    #         # print(path[i + 1])
    #         try:
    #             parsed.append((
    #                 round(eval(path[i - 1])[0], 2),
    #                 round(eval(path[i + 1]), 2)
    #             ))
    #         except TypeError:
    #             # print(id)
    #             return
    #         i += 2
    #     else:
    #         # print(path[i])
    #         try:
    #             parsed.append((
    #                 round(eval(path[i])[0], 2),
    #                 round(eval(path[i])[1], 2)
    #             ))
    #         except TypeError:
    #             # print(id)
    #             return
    #         i += 1
    # return parsed


def path_length(path):
    try:
        path = parse_coordinate_list(path)
    except AttributeError:
        pass
    except TypeError:
        pass
    if not path:
        return 0
    t = 0
    for i, c in enumerate(path):
        if i == 0:
            pass
        else:
            try:
                t += math.sqrt((c[0] - path[i - 1][0]) ** 2 + (c[1] - path[i - 1][1]) ** 2)
            except TypeError:
                print(path)
                return t
    return t


def latlong(p, id=True, terr=True, sector=False):
    """

    :param p:
    :param id: not sure what this is for
    :param terr: not sure why this would ever be false
    :param sector: return the map sector (1-12)
    :return: ID, which is the map coordinate of the center of the hex
    """

    # LONGITUDE
    x = round((p[0] - north_pole[0]) / 86.6, 1)
    if x % 1 == 0.5:
        x -= 0.5
        x += 1
    else:
        pass

    x = int(x)

    xdir = ''

    if x > 0:
        xdir = 'E'
    elif x < 0:
        xdir = 'W'
    else:
        pass

    # LATITUDE
    y = round((north_pole[1] - view_box[1] + p[1] + 50) / 75)

    ydir = ''
    if y < 207:
        ydir = 'N'
    elif y > 207:
        ydir = 'S'
    else:
        pass

    s = ''

    # assign regions and perform coordinate transformations
    if terr:
        if y < 0:
            if y / 2 - (1 - y % 2) / 2 > x:
                # a = x
                # x = abs(a)
                # x0 =
                b = abs(x) - y - math.floor(abs(y) / 2)
                a = 3 * b - math.floor(b / 2) - (b - abs(y))
                x, y = abs(a), abs(b)
                s = 1
                # print('{:}, {:}: OOB, 1'.format(y, x))
            elif - y / 2 - (1 - y % 2) / 2 < x:
                x0 = math.ceil(abs(y) / 2 - (1 - y % 2) / 2)
                b = abs(y) + (x - x0)
                a = math.ceil(2 * b - b / 2) + x0 - (x - b)
                x, y = int(abs(a)), int(abs(b))
                s = 3
                # print('{:}, {:}: OOB, 3'.format(y, x))
            else:
                b = abs(y)
                a = 3 * b + (b % 2) - abs(x) - (x <= 0) * (b % 2)
                x, y = abs(a), abs(b)
                if b % 2 and a == 3 * b and xdir == '':
                    xdir = 'W'
                s = 2
                # print('{:}, {:}: OOB, 2'.format(y, x))
        elif y <= 207:
            if y / 2 - (1 - y % 2) / 2 < x:
                x0 = math.ceil(y / 2 - (1 - y % 2) / 2)
                b = y + (x - x0)
                a = x + math.floor((x - x0) / 2) + (b % 2) * (x - x0) % 2
                x, y = int(abs(a)), int(abs(b))
                s = 6
                # print('{:}, {:}: OOB, 6'.format(y, x))
            elif -2 * x - (1 - y % 2) > y:
                x0 = math.ceil(y / 2 - y % 2 / 2)
                b = y - (x + x0)
                y = b
                a = x + math.floor((x + x0) / 2) + (b % 2) * (x - x0) % 2
                a -= (b % 2)
                x, y = int(abs(a)), int(abs(b))
                s = 4
                # print('{:}, {:}: OOB, 4'.format(y, x))
            else:
                b = y
                a = x - (y % 2) * (x <= 0)
                if x <= 0 and y % 2:
                    xdir = 'W'
                x, y = abs(a), abs(b)
                s = 5
                # print('{:}, {:}: OOB, 5'.format(y, x))
        elif 207 < y <= 414:
            if y > 2 * x + 414 - (y % 2):
                b = abs(x) + (414 - y) - math.floor((414 - y) / 2)
                a = math.ceil(b / 2) + abs(x) - abs(math.floor((y - 414 - (y % 2)) / 2)) + (y % 2)
                x, y = abs(a), abs(b)
                s = 7
                # print('{:}, {:}: OOB, 7'.format(y, x))
            elif y > -2 * x + 415 - (1 - y % 2):
                b = abs(x) + (414 - y) - math.floor((414 - y) / 2) - (y % 2)
                a = math.ceil(b / 2) + abs(x) - abs(math.floor((y - 414 - (y % 2)) / 2))
                x, y = abs(a), abs(b)
                s = 9
                # print('{:}, {:}: OOB, 9'.format(y, x))
            else:
                y = 414 - y
                x = abs(x) + (x <= 0) * (y % 2)
                if (y % 2) and xdir == '':
                    xdir = 'W'
                s = 8
                # print('{:}, {:}: OOB, 8'.format(y, x))
        elif y > 414:
            if y < -2 * x + 414 + (y % 2):
                b = y - 414 - x - math.ceil((y - 414) / 2 - (y % 2) / 2)
                a = 2 * b + math.ceil(b / 2) + x + math.ceil((y - 414) / 2 - (y % 2) / 2)
                x, y = abs(a), abs(b)
                s = 10
                # print('{:}, {:}: OOB, 10'.format(y, x))
            elif y > 2 * x + 414 - (y % 2):
                b = y - 414
                a = 3 * b + (b % 2) - abs(x) - (x <= 0) * (b % 2)
                if b % 2 and a == 3 * b and xdir == '':
                    xdir = 'W'
                x, y = abs(a), abs(b)
                s = 11
                # print('{:}, {:}: OOB, 11'.format(y, x))
            else:
                b = y - 414 + x - math.ceil((y - 414) / 2 - (1 - y % 2) / 2)
                a = math.ceil(2 * b - b / 2) + math.ceil((y - 414) / 2 - (1 - y % 2) / 2) - (x - b)
                x, y = abs(a), abs(b)
                s = 12
                # print('{:}, {:}: OOB, 12'.format(y, x))
        if y == 207:
            ydir = ''
    if sector:
        return s
    if id:
        return '{:}{:}-{:}{:}'.format(y, ydir, x, xdir)


class hexNotValid(Exception):

    def __init__(self, h, message='Hex ID is not valid'):
        self.h = h
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'{self.h}: {self.message}'


def getSector(h):

    if h not in hexMap:
        raise hexNotValid(h)
    if 'S' not in h:
        origin = '0N-0'
        alpha = rawAngle(origin, h, base=1) % 360
        for i, s in {range(120, 180): 1, range(60, 120): 2, range(0, 60): 3, range(180, 240): 4, range(240, 300): 5, range(300, 360): 6}.items():
            if alpha in i:
                return s
    else:
        origin = '0S-0'
        alpha = rawAngle(origin, h, base=1) % 360
        for i, s in {range(120, 180): 7, range(60, 120): 8, range(0, 60): 9, range(180, 240): 10, range(240, 300): 11, range(300, 360): 12}.items():
            if alpha in i:
                return s


def fix_hex(h):
    x, y = parseID(h)
    if abs(y) < 207:
        return h
    y = 207 - (abs(y) - 207)
    h = f"{abs(y)}{'S' if 'N' in h else 'N' if y != 207 else ''}-{abs(x)}{'W' if x < 0 else 'E' if x > 0 else ''}"
    return h


def hexDistance(h1, h2):

    """
    :param h1:
    :param h2:
    :return: the manhattan distance between two hexes
    """

    if 'S' in h1 and 'S' not in h2:
        h1, h2 = h2, h1
    p1, p2 = hexMap[h1], hexMap[h2]
    if 'S' not in h1 and 'S' in h2:
        p2 = hexTransform(h2)
    deltaX = abs(p2[0] - p1[0])
    deltaY = abs(p2[1] - p1[1])
    hexDeltaY = deltaY / latitudeY(1)[1]
    hexDeltaX = deltaX / 2 / hexr
    delta = round(abs(hexDeltaY) + abs(hexDeltaX))
    delta = round(abs(min(ssd.euclidean(p1, p2), ssd.euclidean(p1, hexMap[h2])) / latitudeY(1)[1]))

    return delta


def hexTransform(h2):

    return {7: sectorTransform(h2, '311-156W', -120),
            9: sectorTransform(h2, '311-156E', 120),
            8: hexMap[h2],
            10: sectorTransform(h2, sectorTransform('310S-775W', '310S-465W', -180), -240),
            11: sectorTransform(sectorTransform(h2, '0S-0', 180), '0N-0', 180),
            12: sectorTransform(h2, sectorTransform('310S-775E', '310S-465E', 180), 240)}[getSector(h2)]


def neighborsWithin(h, d=0):

    i = 0
    visited = set([h])
    current = set(neighbors(h))
    while i < d:
        for c in list(current):
            for n in neighbors(c):
                if c in visited:
                    continue
                else:
                    current.add(n)
            current.discard(c)
            visited.add(c)
        i += 1
    visited.discard(h)

    return visited


def sectorTransform(h, P, theta):

    if type(h) == str or type(h) == np.str_:
        hx, hy = hexMap[h]
    else:
        hx, hy = h
    if type(P) == str or type(P) == np.str_:
        Px, Py = hexMap[P]
    else:
        Px, Py = P

    g = np.array([hx,
                  hy,
                  1,
                  ])

    g = np.dot(np.array([
        [1, 0, -Px],
        [0, 1, -Py],
        [0, 0, 1],
    ]), g)

    g = np.dot(np.array([
        [math.cos(math.radians(theta)), -math.sin(math.radians(theta)), 0],
        [math.sin(math.radians(theta)), math.cos(math.radians(theta)), 0],
        [0, 0, 1],
    ]), g)

    g = np.dot(np.array([
        [1, 0, Px],
        [0, 1, Py],
        [0, 0, 1],
    ]), g)

    return [round(i, 5) for i in g[0:2]]


def distance(h1, h2):
    """

    :param h1: point 1
    :param h2: point 2
    :return: the distance between the two points
    """

    if type(h1) == str:
        h1 = (hexdict[h1][0][0], hexdict[h1][0][1] + 50)
    if type(h2) == str:
        h2 = (hexdict[h2][0][0], hexdict[h2][0][1] + 50)

    return math.sqrt(
        (h1[0] - h2[0]) ** 2 +
        (h1[1] - h2[1]) ** 2
    )


def screen(col, bw):
    """bw is from 0 to 255"""

    r, g, b = int(col[:2], 16), int(col[2:4], 16), int(col[4:], 16)
    r, g, b = r / 255, g / 255, b / 255

    bw /= 255
    bw = clip(bw, 0, 1)

    r = 1 - (1 - r) * (1 - bw)
    g = 1 - (1 - g) * (1 - bw)
    b = 1 - (1 - b) * (1 - bw)

    r, g, b = r * 255, g * 255, b * 255

    col = '{:02x}{:02x}{:02x}'.format(int(r), int(g), int(b))

    return col


def colorTemp(t, k=(-17, 103 - 17), col0='67a9cf', col1='aaaaaa', col2='ef8a62', mid=0.5):
    r0, g0, b0 = int(col0[:2], 16), int(col0[2:4], 16), int(col0[4:], 16)
    r0, g0, b0 = r0 / 255, g0 / 255, b0 / 255

    r1, g1, b1 = int(col1[:2], 16), int(col1[2:4], 16), int(col1[4:], 16)
    r1, g1, b1 = r1 / 255, g1 / 255, b1 / 255

    r2, g2, b2 = int(col2[:2], 16), int(col2[2:4], 16), int(col2[4:], 16)
    r2, g2, b2 = r2 / 255, g2 / 255, b2 / 255

    k = clip((t - k[0]) / (k[1] - k[0]), 0, 1)

    if k < mid:
        k = k / mid
        r = (1 - k) * r0 + k * r1
        g = (1 - k) * g0 + k * g1
        b = (1 - k) * b0 + k * b1
    else:
        k = (k - mid) / mid
        r = (1 - k) * r1 + k * r2
        g = (1 - k) * g1 + k * g2
        b = (1 - k) * b1 + k * b2

    col = '{:02x}{:02x}{:02x}'.format(int(255 * r), int(255 * g), int(255 * b))

    return col


def project(a, b):
    return (
        np.dot(a, b) / np.dot(b, b) * b[0],
        np.dot(a, b) / np.dot(b, b) * b[1],
    )


def make_path(x1, y1, x2, y2):
    return mplPath.Path([[x1, y1], [x1, y2], [x2, y2], [x2, y1]])


def perp(a):
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b


def seg_intersect(a1, a2, b1, b2):
    da = a2 - a1
    db = b2 - b1
    dp = a1 - b1
    dap = perp(da)
    denom = np.dot(dap, db)
    num = np.dot(dap, dp)
    x3 = ((num / denom.astype(float)) * db + b1)[0]
    y3 = ((num / denom.astype(float)) * db + b1)[1]
    p1 = make_path(a1[0], a1[1], a2[0], a2[1])
    p2 = make_path(b1[0], b1[1], b2[0], b2[1])
    if p1.contains_point([x3, y3]) and p2.contains_point([x3, y3]):
        return x3, y3
    else:
        return False


def point_in_path(path, pt):
    try:
        return mplPath.Path(np.array(path)).contains_point(pt)
    except ValueError:
        print(path, pt)


def angle_btwn(a, b):
    return math.degrees(math.acos(
        round(np.dot(a, b) / math.hypot(*a) / math.hypot(*b), 5)
    ))


def floor_base(x, prec=2, base=.05):
    return math.floor(base * math.floor(float(x) / base))


def ceil_base(x, prec=2, base=.05):
    return math.ceil(base * math.ceil(float(x) / base))


def roundBase(x, prec=2, base=.05):
    return round(base * round(float(x) / base), prec)


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def clip(x, a=None, b=None):
    """

    :param x: input value
    :param a: minimum
    :param b: maximum
    :return: clipped value
    """

    if a is None and b is None:
        return x
    if a is None and b is not None:
        return min(x, b)
    if a is not None and b is None:
        return max(x, a)
    return min(max(x, a), b)


def roll(dice):
    return int(xdice.roll(dice))


def rescale(x, a, b, c, d):
    """
    :param x: input value 
    :param a: input min
    :param b: input max
    :param c: output min
    :param d: output max
    :return: output value
    """

    y = (x - a) / (b - a) * (d - c) + c

    return y


def births(P, K, r):
    if K == 0:
        K = 1

    return math.ceil(r * P * (1 - P / K))


def randcolor():
    return ''.join(
        ['{:0x}'.format(random.randint(128, 204)), '{:0x}'.format(random.randint(128, 204)),
         '{:0x}'.format(random.randint(128, 204))]
    )


def hexColor(c):

    c = colors.hex2color(f"#{c}")
    return round(c[0], 3), round(c[1], 3), round(c[2], 3)


def get_property(p, prop):
    return dict(x.split(':') for x in p.get('style').split(';'))[prop]


def poissonElimination(samples, N, verbose=False):

    M = len(samples)
    N = 30
    A = 2 * math.sqrt(3) * (hexPx / 2) ** 2 * M
    rmax = math.sqrt(A / (2 * math.sqrt(3) * N))
    rmin = rmax * (1 - (N / M) ** 1.5) * 0.65
    rmax /= latitudeY(1)[1]
    rmin /= latitudeY(1)[1]
    wij = lambda i, j: (1 - clip(2 * rmin, hexDistance(i, j), 2 * rmax) / (2 * rmax)) ** 8
    wi = lambda i: sum(wij(i, j) for j in samples.intersection(neighborsWithin(i, 2 * rmax)))
    while len(samples) > N:
        sj = max(samples, key=lambda i: wi(i))


def poissonSample(samples, r=10, k=30, verbose=False):

    """
    Fast Poisson Disk Sampling in Arbitrary Dimensions, Bridson (ACM SIGGRAPH 2007 sketches, article 22)
    :param samples:
    :param r:
    :param verbose:
    :return:
    """
    # active_list = set(samples)
    samples = set(samples)
    i = random.choice(list(samples))
    background_grid = {i}
    active_list = set()
    active_list.add(i)
    while active_list:
        i = random.choice(list(active_list))
        found = False
        kSet = np.random.choice(list((neighborsWithin(i, 2 * r) - neighborsWithin(i, r)) - background_grid), size=k)
        for j in kSet:
            if str(j) in background_grid:
                active_list.discard(str(j))
                continue
            if all(hexDistance(b, str(j)) > r for b in background_grid):
                active_list.add(str(j))
                background_grid.add(str(j))
                found = True
        if not found:
            active_list.discard(i)
        if verbose: print(len(active_list), len(background_grid), len(active_list) + len(background_grid))

        for b in background_grid:
            if hexDistance(b, i) < r:
                active_list.discard(i)
                break
        else:
            active_list.discard(i)
            background_grid.add(i)
        if verbose: print(len(active_list), len(background_grid), len(active_list) + len(background_grid))
    return background_grid


def poissonSamplev2(samples, r=10, verbose=False):

    samples = set(samples)
    poisson = set()
    active_list = set(samples)
    background_grid = set()
    while active_list:
        k = np.random.choice(list(active_list))
        for n in neighborsWithin(k, r):
            active_list.discard(n)
        background_grid.add(k)
        active_list.discard(k)
        if verbose: print(len(active_list), len(background_grid))

    return background_grid


def oceanGet(seaLevel=None):
    if not seaLevel:
        seaLevel = seaLevelGet()
    # simply get all below or at sea level
    ocean = {h for h in elevation if elevation[h]['altitude'] <= seaLevel}
    return ocean


def seaLevelGet(s=0.29, verbose=False):

    p = lambda level: sum([elevation[h]['altitude'] >= level for h in elevation]) / len(elevation)
    sea_level = min(elevation[h]['altitude'] for h in elevation)
    step = 1000
    while step:
        pS = p(sea_level)
        while pS > s:
            sea_level += step
            if verbose: print(sea_level, pS)
            pS = p(sea_level)
        sea_level -= step
        step = int(step / 2)
    return int(round(sea_level))


def box_blur(data, g=3, r=1):
    i = 0
    for _ in range(g):
        data_copy = {c: float(data[c]) for c in data if data[c] is not None}
        for c in data_copy:
            i += 1
            if data[c] is None:
                continue
            data[c] = round(
                statistics.mean([data_copy[n] for n in neighborsWithin(c, r) if n in data_copy] + [data_copy[c]]), 2)
            print(f'{i / len(data_copy) / g:.4%}')


def newMap(year):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [hexMap[i] for i in ['311-156E', '310S-465E', '310S-775E', '310S-775W', '310S-465W', '311-156W',
                             '311-467W', '311-778W', '311-778E', '311-467E']]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#bad9ff'))

    # yh = {h: hexdict[h][0][0] for h in terrain.keys() if hexdict[h][0][1] >= hexdict['207-518W'][0][1]}
    # xh = {h: hexdict[h][0][1] + 50 for h in terrain.keys() if hexdict[h][0][1] >= hexdict['207-518W'][0][1]}
    # y = [yh[h] for h in yh.keys() if h in terrain]
    # x = [xh[h] for h in xh.keys() if h in terrain]
    # ax.scatter(x=x, y=y, marker='h', c='w', edgecolors=None, s=1 / 4, zorder=100)

    ax.axis('equal')
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    ax.annotate(f"{year: 5}", (0.0, 0.0), xycoords='axes fraction', size=12, fontfamily='monospace')
    fig.tight_layout()
    ax.invert_xaxis()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    return fig, ax


def avg(x, weights=None):
    """
    :param x: a list
    :return: the average of the list
    """

    s = 0
    n = 0
    if not weights:
        weights = [1 for i in x]

    for i, w in zip(x, weights):
        if i is None:
            s = s
            n = n
        else:
            s += float(i) * w
            n += w

    if n == 0:
        return 0
    else:
        return s / n


def hegemony_get(c):

    if c not in sociology:
        return None
    elif 'hinterland' in sociology[c]:
        return sociology[c]['hinterland']
    elif c in cities:
        return cities[c]['hegemony']
    return None


def flood_fill(start):

    current = set([start])
    visited = set()

    while current:
        for c in list(current):
            if c in visited:
                current.remove(c)
                continue
            current |= set([i for i in all_neighbors[c] if i in terrain])
            visited.add(c)
            current.remove(c)
        # print(len(visited), len(current))

    return visited