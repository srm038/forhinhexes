from defs import *

load_data(all_neighbors, all_neighbors_file)
load_data(pressure, fr"{basefile}\pressure.json")
load_data(elevation, fr"{basefile}\elevation.json")


def pressure_define(season='Jul', blur=True, save=True):

    pressure[season] = {h: 0 for h in hexdict}
    itcz = (-15, 5+1) if season == 'Jul' else (-5, 15+1)
    for h in pressure[season]:
        if itcz[0] <= latitude(h) <= itcz[1]:
            pressure[season][h] = -1

    sea_level = seaLevelGet()
    ocean = oceanGet(seaLevel=sea_level)

    cells = set()
    for h in pressure[season]: # overseas HP, east side of the coast
        if (30 <= latitude(h) <= 40) if season == 'Jul' else (-40 <= latitude(h) <= -30):
            if nearCoast(h, ocean):
                for n in all_neighbors[h]:
                    if elevation[n]['altitude'] >= sea_level and hexAngle(h, n) in [0, 60, 300]:
                        pressure[season][h] = 1
                        cells.add(h)
    d = 0
    current = set(cells)
    visited = set()
    while current and d <= 30:
        for h in list(current):
            if h in visited:
                continue
            for n in all_neighbors[h]:
                if n in visited or elevation[n]['altitude'] >= sea_level:
                    continue
                if (30 <= latitude(n) <= 40) if season == 'Jul' else (-40 <= latitude(n) <= -30):
                    if hexAngle(h, n) in [180, 120, 240]:
                        current.add(n)
            current.discard(h)
            visited.add(h)
        print(len(visited), len(current), d)
        d += 1
    for h in visited:
        pressure[season][h] = 1

    for h in pressure[season]:  # overseas HP, continuous
        if h in ocean:
            if (-40 <= latitude(h) <= -30) if season == 'Jul' else (30 <= latitude(h) <= 40):
                pressure[season][h] = 1

    for h in pressure[season]: # inland HP
        if (latitude(h) <= -23) if season == 'Jul' else (23 <= latitude(h)):
            if h not in ocean:
                if (-40 <= latitude(h) <= -30) if season == 'Jul' else (30 <= latitude(h) <= 40):
                    pressure[season][h] = 1
                elif distToCoast(h, ocean) >= 10:
                    pressure[season][h] = 1

    for h in pressure[season]: # overseas LP
        if h in ocean:
            if (60 <= latitude(h) <= 70) if season == 'Jul' else (50 <= latitude(h) <= 60):
                pressure[season][h] = -1

    for h in pressure[season]: # overseas LP
        if h in ocean:
            if (-60 <= latitude(h) <= -50) if season == 'Jul' else (-70 <= latitude(h) <= -60):
                pressure[season][h] = -1

    for h in pressure[season]: # inland LP
        if (latitude(h) >= 0) if season == 'Jul' else (0 >= latitude(h)):
            if h not in ocean:
                if (-60 <= latitude(h) <= -50) if season == 'Jul' else (-70 <= latitude(h) <= -60):
                    pressure[season][h] = -1
                elif distToCoast(h, ocean) >= 10:
                    pressure[season][h] = -1

    if blur: box_blur(pressure[season], g=3, r=10)

    a = min(pressure[season].values())
    b = max(pressure[season].values())
    for h in pressure[season]:
        pressure[season][h] = round(rescale(pressure[season][h], a, b, -1, 1), 2)

    if save: save_data(pressure, fr"{basefile}\pressure.json")

    # pressure_map(season=season)


def pressure_map(season='Jul'):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#ffffff'))

    s = 1 / 2
    t = sorted([h for h in pressure[season] if hexdict[h][0][1] >= hexdict['207-518W'][0][1]], key=lambda h: pressure[season].get(h, 0))
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    c = np.array([colors.hex2color(f"#{colorTemp(pressure[season][h], k=(-1, 1), col1='ffffff')}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Pressure\{season}.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()
