from defs import *

load_data(elevation, fr"{basefile}\elevation.json")
# load_data(tectonics, fr"{basefile}\tectonics.json")
load_data(climate, fr"{basefile}\climate.json")
load_data(terrain, fr"{basefile}\terrain.json")
load_data(topography, fr"{basefile}\topography.json")
load_data(all_neighbors, all_neighbors_file)


def topography_define():

    for h in (h for h in climate if elevation[h]['altitude']):
        topo = []
        plains = 0
        hills = 0
        highlands = 0
        mountains = 0
        valleys = 0
        depressions = 0
        rolling = 1
        a = elevation[h]['altitude']
        # if type(a) == str:
        # print(hexes[h]['id'])
        # continue
        for n in (n for n in all_neighbors[h] if n in climate and elevation[n]['altitude']):
            b = elevation[n]['altitude']
            # if type(b) == str:
            # print(n.id)
            # continue
            if abs(a - b) <= 200:
                plains += 1
            if 1200 >= abs(a - b) >= 200:
                hills += 1
            if a - b >= 800:
                highlands += 1
            if abs(a - b) >= 1200:
                mountains += 1
            if a - b <= 200:
                valleys += 1
            if b > a:
                depressions += 1
        if plains >= 3:
            topo.append('plains')
            rolling = 0
        if hills >= 3:
            topo.append('hills')
            rolling = 0
        if highlands >= 3:
            topo.append('highland')
            rolling = 0
        if mountains >= 3:
            topo.append('mountain')
            rolling = 0
        if 5 >= valleys >= 4:
            topo.append('valley')
            rolling = 0
        if depressions == len(all_neighbors[h]):
            topo.append('depression')
            rolling = 0
        if rolling:
            topo.append('rolling')
        topography[h] = ' '.join(topo)
    save_data(topography, fr"{basefile}\topography.json")


def topography_map():

    string = ''
    with open(basefile + r'\Topography.svg', 'r') as f:
        file = bs(f, 'lxml')
    for h in topography:
        if all(i not in topography[h] for i in ['hills', 'mountain', 'highland']):
            continue
        if 'hills' in topography[h] and 'mountain' in topography[h]:
            ref = 'hillsmountain'
        elif 'highland' in topography[h] and 'mountain' in topography[h]:
            ref = 'highlandmountain'
        elif 'highland' in topography[h]:
            ref = 'highland'
        # elif 'hills' in terrain[h]['topography']:
        #     ref ='hills'
        elif 'mountain' in topography[h]:
            ref = 'mountain'
        elif 'hills' in topography[h]:
            ref = 'hills'
        else:
            continue
        string += '<use x="{:}" y="{:}" href="#{:}"/>'.format(
            hexdict[h][0][0],
            hexdict[h][0][1] + 50,
            # 20,
            ref
        )
    else:
        with open(basefile + r'\Topography.svg', 'r') as f:
            file = bs(f, 'lxml')
        file.find('g', id='Topography').contents = bs(string, 'lxml').find('body').contents
        save(file, extra_file=basefile + r'\Topography.svg')


def timberline(h):
    """

    :param lat: the latitude of the hex
    :return: the altitude below which there can be trees
    """

    x = latitude(h)

    return int(14500 * math.exp(-(x - 20) ** 2 / 2 / 37 ** 2))


def snowline(h):
    """

    :param lat: the latitude of the hex
    :return: the altitude above which there can be snow
    """

    x = latitude(h)

    return int(15000 * math.exp(-(x - 24) ** 2 / 2 / 60 ** 2))


def terrain_define(sea_level=None, ocean=None):

    # if not sea_level:
    #     sea_level = sea_level_get()
    # if not ocean:
    #     ocean = ocean_get(sea_level=sea_level, full=True)

    def terrain_type(h, ocean):

        if climate[h]['koppen'] == 'EF':
            return 'i'
        try:
            if 2 < tectonics[h]['raw'].get('D', (0, 100))[1] < 5:
                if elevation[h]['dist_to_coast'] < 10:
                    if 'mountain' in topography[h]:
                        return 'v'  # ifrandom.randint(1, 100) < 50 else ' '
        except AttributeError:
            pass
        if elevation[h]['altitude'] > timberline(h):
            if elevation[h]['altitude'] > snowline(h):
                return 'i'
            else:
                return 'k'
        if climate[h]['aridity'] > 1 and climate[h]['annual rain'] > 700:
            if elevation[h]['drainage'] > 250 and 'plains' == topography[h]:
                if len([n for n in all_neighbors[h] if 'plains' == topography[n]]) > 0:
                    if ('forest' in climate[h]['holdridge']) ^ ('woodland' in climate[h]['holdridge']):
                        return 'w'
                    else:
                        return 'm'
            if is_coastal(h, ocean) and elevation[h]['altitude'] < 50 and 'plains' == topography[h]:
                return 'm'
            if 'depression' in topography[h] and not is_coastal(h, ocean):
                return 'f'
            if 'plains' in topography[h] and ('highland' in topography[h] or 'valley' in topography[h]):
                if climate[h]['annual rain'] > 1100:
                    if not is_coastal(h, ocean):
                        return 'b'
                    elif 'b' in ''.join([terrain.get(n, '') for n in all_neighbors[h]]):
                        return 'b'
            if not len(elevation[h]['drain_in']) and elevation[h]['drainage'] > 4 and 'plain' == topography[h]:
                return 'b'
        else:
            if elevation[h]['drainage'] > 250 and 'plains' == topography[h]:
                if len([n for n in all_neighbors[h] if 'plains' == topography[h]]) > 0:
                    if ('forest' in climate[h]['holdridge']) ^ ('woodland' in climate[h]['holdridge']):
                        return 'w'
                    else:
                        return 'm'
        for t in ['w', 'm', 'b', 'f']:
            if 'plains' == topography[h] and len([n for n in all_neighbors[h] if t in terrain.get(n, '')]) > 2:
                return t  # ifrandom.randint(1, 100) < 10 else ' '
        if 'Aw' in climate[h]['koppen']:
            return 'p'
        if 'rain forest' in climate[h]['holdridge'] or 'Af' in climate[h]['koppen']:
            if hexes.get(h, dict()).get('infrastructure', 0) >= 2268/2:
                return 'g'
            return 'r'
        if 'D' in climate[h]['koppen']:
            if hexes.get(h, dict()).get('infrastructure', 0) >= 2268/2:
                return 's'
            return 'c'
        if 'BS' in climate[h]['koppen']:
            return 'g'
        if 'B' in climate[h]['koppen']:
            return 'd'
        if 'steppe' in climate[h]['holdridge']:
            return 's'
        if climate[h]['koppen'] == 'ET':
            return 'b'
        if 'thorn' in climate[h]['holdridge']:
            return 'n'
        if 'scrub' in climate[h]['holdridge']:
            return 'n'
        if 'forest' in climate[h]['holdridge']:
            if hexes.get(h, dict()).get('infrastructure', 0) >= 2268/2:
                return 'g'
            return 'j'
        if 'C' in climate[h]['koppen']:
            return 'j'


    terrain.clear()
    terrain_check = 'x'
    while terrain_check != ''.join([terrain.get(h, '') for h in terrain]):
        terrain_check = ''.join([terrain.get(h, '') for h in terrain])
        for h in climate:
            terrain[h] = terrain_type(h, ocean)
        print(len(terrain_check))
    save_data(terrain, fr"{basefile}\terrain.json")



def terrain_svg(update_map=False, r=3, verbose=False):

    string = ''
    for h in terrain:
        string += '<path d="M {:} Z" id="{:}" style="color:#{:}" class="hex"/>'.format(
                ' '.join([','.join([str(i) for i in p]) for p in hexdict[h]]),
                h, screen(terr2color[terrain[h]], elevation[h]['altitude'] // 100)
            )
    with open(basefile + r'\Terrain.svg', 'r') as f:
        file = bs(f, 'html.parser')
    file.find('g', id='Terrain').contents = bs(string, 'lxml').find('body').contents
    save(file, extra_file=basefile + r'\Terrain.svg')


def terrain_png(update_map=False, r=3, verbose=False):

    fig, ax = plt.subplots()
    plt.ioff()

    b = [[8883.271, 7002.587], [26809.989, 7002.5899], [35773.36, 22527.59], [26766.7, 38127.59], [35686.76, 53577.59],
         [26766.7, 69027.59], [8926.571, 69027.59], [6.5069, 53577.59], [8926.57, 38127.59], [-80.09012, 22527.59]]
    xlim = min([i[1] for i in b]) - 50, max([i[1] for i in b]) + 50
    ylim = min([i[0] for i in b]) - 50, max([i[0] for i in b]) + 50
    b.extend([b[0]])
    ax.fill([i[1] for i in b], [i[0] for i in b], c=colors.hex2color('#bad9ff'))

    s = 1 / 2
    t = sorted([h for h in terrain if hexdict[h][0][1] >= hexdict['207-518W'][0][1]],
               key=lambda h: terrain[h])
    yh = {h: hexdict[h][0][0] for h in t}
    xh = {h: hexdict[h][0][1] + 50 for h in t}
    y = np.array([yh[h] for h in yh.keys()])
    x = np.array([xh[h] for h in xh.keys()])
    c = np.array([colors.hex2color(
        f"#{screen(terr2color[terrain[h]], elevation[h]['altitude'] // 100)}") for h in t])
    ax.scatter(x=x, y=y, marker='*', c=c, s=s, edgecolors=c, zorder=100, linewidths=s / 2)

    ax.axis('equal')
    plt.gca().invert_xaxis()
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    plt.axis('off')
    fig.tight_layout()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig(fr"{basefile}\Terrain\Terrain.png", dpi=300, bbox_inches='tight', pad_inches=0)
    ax.clear()
    plt.cla()
    plt.clf()
    plt.close()
