from word_gen import *
import random
from math import exp, sqrt, pi, log10, floor, log2
from roman import *
import matplotlib.pyplot as plt
import networkx as nx
import forceatlas2 as fa2
import cProfile as cp

names = []
personsfile = r'Family Members.txt'
treefile = r'Family Tree.txt'
open(personsfile, 'w').close()
open(treefile, 'w').close()

global possible_names
possible_names = {makeword() for i in range(500)}
possible_names = list(possible_names)


class Person:
    def __init__(self, family = None, father = None, mother = None, adopted = None, birth = None, death = None,
                 alive = True, married = False, age = 0, sex = None, head = False):

        # TODO instead of calling this function every time, just make a whole bunch at the beginning: saves processing
        self.praenomen = random.choice(possible_names)
        self.family = family
        if not sex:
            self.sex = random.choice(['M', 'F'])
        else:
            self.sex = sex
        if self.sex == 'F':
            self.praenomen += 'ia'
        self.father = father
        self.mother = mother
        self.birth = birth
        self.death = death
        self.alive = alive
        self.married = married
        self.age = age
        self.head = head
        self.progeny = []
        if not self.father:
            self.ancestry = []
        elif not self.father.ancestry:
            self.ancestry = [self.father.name]
        else:
            self.ancestry = self.father.ancestry + [self.father.name]
        if family and father:
            self.name = self.family + ' da\'' + self.father.praenomen + ' ' + self.praenomen
        elif family and not father:
            self.name = self.family + ' ' + self.praenomen
        elif not family and father:
            self.name = ' da\'' + self.father.praenomen + ' ' + self.praenomen
        else:
            self.name = self.praenomen
        if adopted:
            self.name = self.praenomen + ' di ' + self.family
        i = 1
        check_name = self.name
        while check_name in names:
            check_name = self.name + ' ' + toRoman(i + 1)
            i += 1
        else:
            self.name = check_name
        names.append(self.name)
        self.log = []
        if self.age == 0:
            if father and mother:
                self.addlog(str(self.birth) + ', ' + self.name + ', born' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + self.father.name + ' and ' + self.mother.name)
                self.record(str(self.birth) + ', ' + self.name + ', born' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + self.father.name + ' and ' + self.mother.name)
            elif father:
                self.addlog(str(self.birth) + ', ' + self.name + ', born' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + self.father.name + ' and ' + 'unknown')
                self.record(str(self.birth) + ', ' + self.name + ', born' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + self.father.name + ' and ' + 'unknown')
            elif mother:
                self.addlog(str(self.birth) + ', ' + self.name + ', born' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + 'unknown' + ' and ' + self.mother.name)
                self.record(str(self.birth) + ', ' + self.name + ', born' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + 'unknown' + ' and ' + self.mother.name)
            else:
                self.addlog(str(self.birth) + ', ' + self.name + ', born' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + 'unknown' + ' and ' + 'unknown')
                self.record(str(self.birth) + ', ' + self.name + ', born' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + 'unknown' + ' and ' + 'unknown')
        else:
            if father and mother:
                self.addlog(str(self.birth + self.age) + ', ' + self.name + ', approved' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + self.father.name + ' and ' + self.mother.name)
                self.record(str(self.birth + self.age) + ', ' + self.name + ', approved' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + self.father.name + ' and ' + self.mother.name)
            elif father:
                self.addlog(str(self.birth + self.age) + ', ' + self.name + ', approved' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + self.father.name + ' and ' + 'unknown')
                self.record(str(self.birth + self.age) + ', ' + self.name + ', approved' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + self.father.name + ' and ' + 'unknown')
            elif mother:
                self.addlog(str(self.birth + self.age) + ', ' + self.name + ', approved' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + 'unknown' + ' and ' + self.mother.name)
                self.record(str(self.birth + self.age) + ', ' + self.name + ', approved' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + 'unknown' + ' and ' + self.mother.name)
            else:
                self.addlog(str(self.birth + self.age) + ', ' + self.name + ', approved' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + 'unknown' + ' and ' + 'unknown')
                self.record(str(self.birth + self.age) + ', ' + self.name + ', approved' + ', ' + str(
                    'son' if self.sex == 'M' else 'daughter') + ' of ' + 'unknown' + ' and ' + 'unknown')
                # self.record(str(self.birth) + ', ' + self.name + ' born')

    def die(self, year):

        # TODO optimize
        global living_pop
        self.death = year
        self.alive = False
        # living_pop.remove(self)
        # except: return
        self.addlog('died in ' + str(year) + ', age ' + str(self.age) + ', survived by ' + ', '.join(
            [p.name for p in self.progeny if p.alive]))
        if len(self.progeny):
            self.record(str(year) + ', ' + self.name + ' dies, age ' + str(self.age) + ', survived by ' + ', '.join(
                [p.name for p in self.progeny if p.alive]))
        else:
            self.record(str(year) + ', ' + self.name + ' dies, age ' + str(self.age))
        if self.head:
            if self.progeny:
                male_heirs = [p for p in self.progeny if p.sex == 'M' and p.alive]
            else:
                male_heirs = None
            if male_heirs:
                male_heirs[0].head = True
                male_heirs[0].addlog('became family head in ' + str(year) + ', age ' + str(male_heirs[0].age))
                male_heirs[0].record(str(year) + ', ' + male_heirs[0].name + ' becomes family head')
            else:
                self.record(str(year) + ', ' + 'no direct male heir could be found for the ' + self.family + ' family')
                heir = [p for p in living() if p.family == self.family and p.sex == 'M' and p.alive]
                if heir:
                    heir = heir[0]
                else:
                    self.record(str(year) + ', the ' + self.family + ' line will die out soon, supported by ' + ', '.join(goodterms[self.family]))
                    if self.family not in first_families:
                        return
                    # heir = [p for p in people if
                    #         p.family in goodterms[self.family] and
                    #         p.sex == 'M' and p.alive and not p.head]
                    # print(self.family)
                    # try:
                    #     heir = heir[-1]
                    #     heir.family = self.family
                    #     heir.name += ' re\'' + self.family
                    # except IndexError:
                    ### TODO Find a bastard
                    a =random.randint(16, 40)
                    people.append(Person(family =  self.family, father = self, birth = year - a, age = a, sex = 'M'))
                    heir = people[-1]
                    heir.name = 'ba\'' + heir.name
                heir.head = True
                heir.addlog('became family head in ' + str(year) + ', age ' + str(heir.age))
                heir.record(str(year) + ', ' + heir.name + ' becomes family head')
        if len([p for p in living() if p.family == self.family and p.alive]) == 0:
            self.record(str(year) + ', the ' + self.family + ' family has died out')
            # self.save()

    def accident(self, year):
        """
        The implication that something might go wrong
        :return:
        """

        global living_pop
        a = log10(family_size(self.family) + 1) / 450

        if random.random() < (a if self.sex == 'M' else (a / 2)):
            self.record(str(year) + ', ' + self.name + ' dies unexpectedly')
            self.die(year)

    @staticmethod
    def record(i):

        # return
        with open(personsfile, 'a') as f:
            f.write(i + '\n')

    def obit(self):

        pass

    def addlog(self, i):

        self.log.append(i)
        # with open(personsfile, 'a') as f:
        #     f.write(i + '\n')

    def growup(self, year):

        global living_pop
        self.age += 1

        if self.family in first_families:
            fatality = (exp(-0.003 * exp(((self.age - 10) - 25) / 7)))
        else:
            fatality = (exp(-0.003 * exp((self.age - 25) / 10)))

        if random.random() > fatality:
            self.die(year)

    def getmarried(self, year):

        global living_pop
        if self.married or self.sex == 'F' or not self.alive or self.age < 16:
            return
        # TODO if you are the firstborn son, you WILL marry the first available person

        try:
            if [p for p in self.father.progeny if p.sex == 'M'][0].name == self.name:
                matrimony = 1
            else:
                matrimony = (norm(self.age, 30, 10) * 0.9 / norm(30, 30, 10))
        except:
            matrimony = (norm(self.age, 30, 10) * 0.9 / norm(30, 30, 10))

        if random.random() < matrimony:
            try:
                for p in reversed(people):
                    if not p.alive: continue
                    elif p.married: continue
                    elif p.sex == self.sex: continue
                    elif kissingcousins(self, p): continue
                    elif not friendly(self.family, p.family) if family_size(self.family) < 200 else self.family != p.family: continue
                    elif p.age < 16: continue
                    else:
                        spouse = p
                        self.married = spouse
                        spouse.married = self
                        self.addlog('marries, age ' + str(self.age))
                        self.record(str(year) + ', ' + self.name + ' marries ' + spouse.name + ', age ' + str(self.age))
                        return
                # for p in reversed(people):
                #     if not p.alive:
                #         continue
                #     elif p.married:
                #         continue
                #     elif p.sex == self.sex:
                #         continue
                #     elif kissingcousins(self, p):
                #         continue
                #     elif friendly(self.family, p.family):
                #         continue
                #     elif p.age < 16:
                #         continue
                #     else:
                #         spouse = p
                #         self.married = spouse
                #         spouse.married = self
                #         self.addlog('marries, age ' + str(self.age))
                #         self.record(
                #             str(year) + ', ' + self.name + ' marries ' + spouse.name + ', age ' + str(self.age))
                #         return
                # potentials = [p for p in living_pop if not p.married and
                #               p.sex != self.sex and
                #               not kissingcousins(self, p) and
                #               self.family == p.family and
                #               p.age >= 16 and
                #               p.alive]
                # if len(potentials) == 0:
                #     potentials = [p for p in living_pop if not p.married and
                #                   p.sex != self.sex and
                #                   not kissingcousins(self, p) and
                #                   friendly(self.family, p.family) and
                #                   p.age >= 16 and
                #                   p.alive]
                # spouse = potentials[-1]
            except IndexError:
                return

            # self.record(str(year) + ', ' + self.name + ' marries ' + spouse.name + ', age ' + str(self.age))

    def havechildren(self, year):

        if not self.married or not self.married.alive or self.sex == 'F' or not self.alive:
            return
        if len(self.progeny) > 0:
            if year - self.progeny[-1].birth < 5:
                return
        # if self.family in first_families:
        #     fertility = (norm(self.married.age, 25, 8) * 0.4 / norm(25, 25, 8) * (self.married.age > 16))
        # else:
        fertility = (norm(self.married.age, 25, 7) * 0.5 / norm(25, 25, 7) * (self.married.age > 16))
        roll = random.random()
        if roll < (18 / 1000):
            n = 2
        elif roll < (1 / 1000):
            n = 3
        elif roll < (1 / 1e6):
            n = 4
        else:
            n = 1
        if random.random() < fertility:
            for i in range(0, n):
                people.append(Person(family = self.family, father = self, mother = self.married, birth = year))
                self.progeny.append(people[-1])


def family_size(f):

    return len([p for p in people if p.family == f and p.alive])


def living():

    return {p for p in people if p.alive}


def norm(x, m, s):
    return 1 / (sqrt(2 * pi) * s) * exp(-((x - m) / s) ** 2)


def kissingcousins(p1, p2):
    # return False

    if p1.family != p2.family:
        return False
    else:
        n = floor(log10(family_size(p1.family)))

    try:
        return bool(set(p1.ancestry[-n:]) & set(p2.ancestry[-n:]))
    except:
        return True


goodterms = { # which noble houses are allied
    'Vanadj': ['Darivadj', 'Irn', 'Mabdin'],
    'Darivadj': ['Vanadj', 'Tjavan', 'Irn'],
    'Njita': ['Mabdin', 'Mijin', 'Napsern'],
    'Mijin': ['Njita', 'Napsern', 'Mabdin'],
    'Napsern': ['Tjavan', 'Mijin', 'Njita'],
    'Irn': ['Vanadj', 'Darivadj', 'Tjavan'],
    'Tjavan': ['Darivadj', 'Irn', 'Napsern'],
    'Mabdin': ['Mijin', 'Njita', 'Vanadj']
}


def bond(f1, f2):
    b = 0
    for p in people:
        try:
            if p.father.family == f1 and p.mother.family == f2:
                b += 1
            if p.mother.family == f1 and p.father.family == f2:
                b += 1
        except:
            pass
    return b


def network():

    G = nx.Graph()
    for f in families:
        for g in goodterms[f]:
            G.add_edge(f, g, weight = bond(f, g))
    nx.draw_circular(
        G,
        # fa2.forceatlas2_networkx_layout(G, niter = 1000),
        weight = 'weight',
        width = [log2(bond(u, v) + 1) for u, v in G.edges()],
        arrows = False,
        node_color = '#6dffbd',
        edge_color = '#bfd7ff',
        font_family = 'JasmineUPC',
        font_size = 18,
        node_size = [log2(family_size(u) + 1) * 50 for u in G.nodes()],
        with_labels = True,
        linewidths = 0,
    )


def friendly(f1, f2):
    if f1 == f2:
        return True

    try:
        return f2 in goodterms[f1]
    except KeyError:
        return False


def current():
    for f in families:
        print('{:}: {:}'.format(
            f,
            len([p for p in people if p.alive and p.family == f])
        ))

    if len(set(first_families) & set(families)) / len(first_families) < 1:
        print('\nFirst Family Survival: ' + str(len(set(first_families) & set(families)) / len(first_families)))


def most_kids():
    max = 0
    for p in people:
        if len(p.progeny) > max:
            max = len(p.progeny)
            print(p.name, max)


import random
global people
people = []


def maketree(years):
    global people, first_families, families, living_pop, possible_names
    # people = []
    y0, yf = 1, years + 1
    first_families = ['Vanadj', 'Njita', 'Darivadj', 'Tjavan', 'Mijin', 'Mabdin', 'Napsern', 'Irn']
    families = first_families.copy()
    all_families = first_families.copy()
    for f in families:
        people.append(Person(family = f, birth = y0 - 20, age = 20, sex = 'M', head = True))
        people.append(Person(family = f, birth = y0 - 20, age = 20, sex = 'F'))
        people[-2].married, people[-1].married = people[-1], people[-2]
        people.append(Person(family = f, birth = y0, sex = 'M', father = people[-2], mother = people[-1]))
        people[-3].progeny.append(people[-1])
    for y in range(y0, yf):
        # living_pop = [p for p in people if p.alive]
        j = 0
        for p in reversed(people):
            if p.alive:
                j += 1
                funcs = [p.getmarried, p.havechildren, p.growup, p.accident]
                random.shuffle(funcs)
                for func in funcs:
                    func(y)
        for f in families:
            if len([p for p in people if p.alive and p.family == f]) == 0:
                families.remove(f)
                goodterms.pop(f)
                for g in goodterms:
                    try:
                        goodterms[g].remove(f)
                    except:
                        pass
        if y % 100 == 0:
            for i in range(0,random.randint(0, 3)):
                new = random.choice(possible_names)
                while new in families:
                    new = random.choice(possible_names)
                voters = random.sample(set(families), len(families) // 3)
                families.append(new)
                all_families.append(new)
                for v in voters:
                    goodterms[v].append(new)
                goodterms.update({new: voters})
                people.append(Person(family = new, birth = y - 30, age = 30, sex = 'M', head = True))
                people[-1].record(str(y) + ', ' + new + ' family added to the records, approved by ' + ', '.join(voters))
        print(str(y) + ': ' + str(j) + ' / ' + str(len(people)))
        if len(people) > 11000:
            break
        if len(living()) > 2500:
            break

    for f in all_families:
        plt.plot(list(range(y0, yf)), [len([p for p in people if (
            (not p.alive and p.birth <= y < p.death) or (p.alive and p.birth <= y)) and p.family == f]) for y in
                                       range(y0, yf)],
                 drawstyle = 'steps')

    current()


def pickle_save():

    i = 0
    r = 1000
    import sys, pickle, dill
    sys.setrecursionlimit(r)
    # p = pickle.Pickler(open('family09122017.p', 'wb'))
    while i == 0:
        try:
            print(sys.getrecursionlimit())
            pickle.dump(people, open('family09122017.p', 'wb'))
            i = 1
        except RuntimeError:
            r += 1000
            sys.setrecursionlimit(r)
    # p.clear_memo()

def pickle_load():

    import pickle
    global people
    people = pickle.load(open(r'family09122017.p', 'rb'))

# network()

def find(name):

    global people
    for p in people:
        if p.name == name:
            return p


cousin_chart = [
    ['ca', 'c', 'gc', 'ggc', 'gggc', 'ggggc', 'gggggc', 'ggggggc'],
    ['c', 's', 'n', 'gn', 'ggn', 'gggn', 'ggggn', 'gggggn'],
    ['gc', 'u', '1c', '1c1r', '1c2r', '1c3r', '1c4r', '1c5r'],
    ['ggc', 'gu', '1c1r', '2c', '2c1r', '2c2r', '2c3r', '2c4r'],
    ['gggc', 'ggu', '1c2r', '2c1r', '3c', '3c1r', '3c2r', '3c3r'],
    ['ggggc', 'gggu', '1c3r', '2c2r', '3c1r', '4c', '4c1r', '4c2r'],
    ['gggggc', 'ggggu', '1c4r', '2c3r', '3c2r', '4c1r', '5c', '5c1r'],
    ['ggggggc', 'gggggu', '1c5r', '2c4r', '3c3r', '4c2r', '5c1r', '6c']
]


def related(p1, p2):

    if p1 == p2:
        return'self'
    def common(p1, p2):
        for a in reversed(p1.ancestry + [p1.name]):
            for b in reversed(p2.ancestry + [p2.name]):
                if a == b:
                    return a
    common_ancestor = common(p1, p2)
    if not common_ancestor:
        print('No common paternal ancestor')
        return
    # print(common_ancestor)
    def dist(p1, ca = common_ancestor):
        if p1.name == ca:
            return 0
        else:
            return list(reversed(p1.ancestry + [p1.name])).index(ca)
    d1 = dist(p1) - 1
    d2 = dist(p2) - 1
    if d1 == d2 == 0:
        return p1.name + ' and ' + p2.name + ' are siblings'
    # TODO add parsing for direct descent, order, nephews/etc
    c = min(d1, d2)
    r = abs(d1 - d2)
    older = p1.name if d1 > d2 else p2.name
    younger = p1.name if d1 < d2 else p2.name
    # print(older)
    # print('c: ' + str(c))
    # print('r: ' + str(r))
    if c == 0:
        if r == 2:
            print(older + ' is the ' + 'grand' + 'uncle of ' + younger)
        if r > 2:
            print(older + ' is the ' + 'great-' * (r - 1) + 'grand' + 'uncle of ' + younger)
        else:
            print(older + ' is the uncle of ' + younger)
    elif c == -1:
        if r == 2:
            print(older + ' is the ' + 'grand' + 'father of ' + younger)
        if r > 2:
            print(older + ' is the ' + 'great-' * (r - 1) + 'grand' + 'father of ' + younger)
        else:
            print(older + ' is the uncle of ' + younger)
    else:
        print(older + ' is the ' + str(c) + ' cousin, ' + str(r) + ' removed of ' + younger)
    # print(younger)
    # print('{0} cousins, {1} times removed'.format(str(min(d1, d2)), str(abs(d1 - d2))))

import copy

def draw_tree():

    # G = nx.Graph()
    # for p in people:
    #     for c in p.progeny:
    #         G.add_edge(p.name, c.name)
    #         G.add_edge(p.married.name, c.name)
    # nx.draw(G, fa2.forceatlas2_networkx_layout(G, niter = 100))
    # TODO https://www.saltycrane.com/blog/2008/08/python-recursion-example-navigate-tree-data/
    global people
    sortedtree = []
    peoplebybirth = sorted(people, key = lambda k: k.birth)
    s = ' ' * 0
    print(len(peoplebybirth))
    # peoplebybirthcopy = sorted(peoplebybirth, key = lambda k: k.birth)
    for p, person in enumerate(peoplebybirth):
        if not person.ancestry:
            # sortedtree.append('{:}{:} ({:}-{:})\n'.format(s, person.name, person.birth, person.death))
            sortedtree.append(person)
            # peoplebybirth.remove(person)
            # print(person.name, len(sortedtree), len(peoplebybirth))
    # print(len(peoplebybirth))
    while len(peoplebybirth) > len(sortedtree):
         for p in peoplebybirth:
             if p not in sortedtree:
                 try:
                     sortedtree.insert(sortedtree.index(p.father) + 1, p)
                 except ValueError:
                     pass
                     # print(p.father.name)
        # sortedtreecopy = copy.copy(sortedtree)
        # for p, person in enumerate(sortedtreecopy):
        #     if person.progeny:
        #         # print(person.progeny)
        #         for c, child in enumerate(person.progeny):
        #             if child.name in [x.name for x in peoplebybirth]:
        #                 peoplebybirth.remove(child)
        #                 sortedtree.insert(sortedtree.index(person) + 1 + c, child)
        # print(len(peoplebybirth), len(sortedtree))
    open(treefile, 'w').close()
    with open(treefile, 'a', encoding = 'utf-8') as f:
        for p in sortedtree:
            s = ' ' * (len(p.ancestry))
            # s = '│' * (len(p.ancestry) - 1) + '└' * bool(p.ancestry)
            f.write('{:}{:} ({:}-{:}){:}{:}{:}\n'.format(
                s,
                p.name,
                p.birth,
                p.death,
                ', HEAD' if p.head else '',
                ', m. ' if p.married else '',
                p.married.name if p.married else ''
            ))
    # while g < 2:
    #     s = ' ' * g
    #     for p in sorted(people, key = lambda k: k.birth):
    #         if len(p.ancestry) == g:
    #             sortedtree.append('{:}{:} ({:}-{:})\n'.format(s, p.name, p.birth, p.death))
    #     else:
    #         g += 1
    # with open(treefile, 'a') as f:
    #     for g in range(0, 2):
    #         s = ' ' * g
    #         for p in sorted(people, key = lambda k: k.birth):
    #             if len(p.ancestry) == g:
    #                 f.write('{:}{:}, {:}-{:}\n'.format(s, p.name, p.birth, p.death))
