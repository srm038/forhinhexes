def add_drainage_labels():
    """
    Deletes all existing drainage labels.
    Adds text to the label SVG for drainage values
    :return:
    """

    timer(0)  # 232 s
    print('Fixing drainage labels...')
    with open(basefile + r'\Labels.svg', 'r') as f:
        file = bs(f, 'lxml')
    # clear the existing hex label
    file.find('g', id='Drainage').clear()
    string = ''
    for h in (x for x in hexes if hexes[x].altitude and hexes[x].drainage > 2):
        try:
            string += '<text id="{:}-drainage" x="{:.02f}" y="{:.02f}">{:}</text>\n'.format(
                h, hexes[h].x, hexes[h].y + 245, hexes[h].drainage
            )
            # if hexes[h].drainage > 2:
            # t = labelmap.new_tag('text', id=h + ' drainage', x=hexes[h].x, y=hexes[h].y + 45)
            # t.string = str(hexes[h].drainage)
            # labelmap.find(id='Drainage').append(t)
        except AttributeError:
            pass
    else:
        # print('Saving...')
        file.find('g', id='Drainage').contents = bs(string, 'lxml').find('body').contents
        save(file)
    print('{:} s'.format(timer(1)))


def adjust_elevation(split = 1, merge = 1):
    """
    Grabs elevation data from the SVG and adds it into the hex object. Unfortunately not persistent (yet)
    :return:
    """

    timer(0)
    global hexes

    with open(elevationfile, 'r') as f:
        elevationmap = bs(f, 'lxml')

    if split: split_elevation()
    scaling = 0
    # TODO scale everything to 255ish?
    print('Adjusting elevation...')
    for h in elevationmap.find(id='Elevation').find_all('path'):
        id = latlong(eval(h.get('d').split(' ')[1]), terr=True)
        color = get_property(h, 'fill')
        height = int(color[-2:], 16) * 100
        try:
            a = sum([random.randint(0, 4) for i in range(16)]) * sum([random.randint(0, 4) for i in range(16)]) * 4 / (8192 / 100) - 5
            hexes[id].altitude = height + int(a)
        except KeyError:
            pass
    else:
        save(elevationmap)
    print('Lowering coasts...', end = '', flush = True)
    # coastal hexes must be lower than at least one non-coastal neighbor
    for h in (g for g in hexes if hexes[g].coastal):
        a = int(hexes[h].altitude)
        for n in (i for i in hexes[h].neighbors if not hexes[i].coastal):
            if hexes[n].altitude < a:
                a = max(1, hexes[n].altitude - random.randrange(1, 5))
        hexes[h].altitude = a
    # rescale_altitude()
    if split and merge:
        merge_elevation()
    save(elevationmap)
    print('{:} s'.format(timer(1)))

