import csv
import math
import random

# city_climate_file = r'C:\Users\Shelby\Google Drive\D&D\Behind the Veil\Maps\climate.csv'
city_climate_file = r'D:\Drive\D&D\Behind the Veil\Maps\climate.csv'


def statistics.mean(L):
    return sum(L) / len(L)


climates = {}
with open(city_climate_file, 'r') as f:
    reader = csv.reader(f, delimiter = ',')
    next(reader, None)
    for row in reader:
        try:
            climates[str(row[0])] = {
                'lat': round(float(row[1]) * 23 / 69 / 2 / math.sqrt(2), 2),
                'long': round(float(row[2]) * 23 / 69, 2),
                'elevation': int(row[3]),
                'low': dict(zip(list(range(1, 13)), [int(x) for x in row[4:16]])),
                'high': dict(zip(list(range(1, 13)), [int(x) for x in row[16:28]])),
                'precipitation': dict(zip(list(range(1, 13)), [int(x) for x in row[28:40]])),
                'connecting': row[40].split(';')
            }
        except:
            pass

for c in climates:
    climates[c]['mean'] = {}
    climates[c]['minimum'] = {}
    climates[c]['maximum'] = {}
    for i in list(range(1, 13)):
        climates[c]['mean'][i] = int(statistics.mean([climates[c]['low'][i], climates[c]['high'][i]]))
        climates[c]['minimum'][i] = int(climates[c]['low'][i] + climates[c]['low'][i] - climates[c]['mean'][i])
        climates[c]['maximum'][i] = int(climates[c]['high'][i] + climates[c]['high'][i] - climates[c]['mean'][i])

pressure_pairs = {
    'HH': {'type': 'updraft', 'mod': 0.5, 'conditions': 'clear'},
    'HM': {'type': 'unstable thermal', 'mod': 0.5, 'conditions': 'rain clouds'},
    'HL': {'type': 'inversion wind shear', 'mod': 1.0, 'conditions': 'thunderhead'},
    'MH': {'type': 'stable thermal', 'mod': 0.4, 'conditions': 'light clouds'},
    'MM': {'type': 'calm', 'mod': 0.0, 'conditions': 'clear'},
    'ML': {'type': 'adiabatic cool', 'mod': 0.3, 'conditions': 'clear'},
    'LH': {'type': 'downburst', 'mod': 1.2, 'conditions': 'wind storm'},
    'LM': {'type': 'downdraft', 'mod': 0.1, 'conditions': 'condensation'},
    'LL': {'type': 'subsidence', 'mod': 0.5, 'conditions': 'overcast'},
}


def storm(temp, precipitation, conditions):
    """

    :param temp:
    :param precipitation:
    :param conditions:
    :return:
    """

    roll = random.randint(1, 100)
    clouds = cloud(precipitation, conditions)

    if conditions == 'thunderhead':
        if roll < 2 * precipitation:
            if temp <= 31:
                return 'blizzard'
            elif 31 < temp <= 34:
                return 'sleet'
            elif 34 < temp <= 69:
                return 'quiet downpour'
            else:
                return 'thunderstorm'
    elif conditions == 'rain clouds':
        if clouds == 'altostratus':
            if roll < precipitation:
                if temp <= 33:
                    return 'crystals'
                else:
                    return 'spattering'
            else:
                return 'fair'
        elif clouds == 'stratus':
            if roll < precipitation:
                if temp <= 33:
                    return 'flurry'
                else:
                    return 'pelting'
            else:
                if temp <= 33:
                    return 'tiny flakes'
                else:
                    return 'spattering'
        elif clouds == 'cumulus':
            if roll < 2 * precipitation:
                if temp <= 33:
                    return 'snowfall'
                else:
                    return 'shower'
            else:
                if temp <= 33:
                    return 'soft snow'
                else:
                    return 'drizzle'
    elif conditions == 'wind storm':
        if 0 < precipitation < 3.75:
            return 'sandstorm'
        elif 3.75 < precipitation < 7.5:
            return 'dust storm'
        elif 7.5 < precipitation < 15:
            return 'blowing grit'
        else:
            return 'fair'
    elif conditions == 'condensation':
        if roll < precipitation:
            if temp < -35:
                return 'frozen fog'
            elif -35 < temp < 27:
                return 'hoar frost'
            elif 27 < temp < 33:
                return 'rime'
            else:
                return 'fog'
        elif roll < 2 * precipitation:
            if temp <= 33:
                return 'frost'
            else:
                return 'mist'
        elif roll < 3 * precipitation:
            if temp <= 33:
                return 'freezing'
            else:
                return 'thin mist'
        else:
            if temp <= 33:
                return 'cold surface'
            else:
                return 'dew'
    else:
        return 'fair'

    return 'fair'


def cloud(precipitation, conditions):
    """

    :param precipitation:
    :param conditions:
    :return:
    """

    if conditions == 'clear' or conditions == 'condensation':
        return 'clear'

    if 0 < precipitation < 15:
        return {
            'overcast': 'haze',
            'wind storm': 'cirrus',
            'rain clouds': 'altostratus',
            'light clouds': 'altocumulus',
            'thunderhead': 'cumulus'
        }[conditions]
    elif 15 < precipitation < 30:
        return {
            'overcast': 'cirrus',
            'wind storm': 'altostratus',
            'rain clouds': 'stratus',
            'light clouds': 'altocumulus',
            'thunderhead': 'nimbus'
        }[conditions]
    elif 30 < precipitation < 60:
        return {
            'overcast': 'altostratus',
            'wind storm': 'altocumulus',
            'rain clouds': 'stratus',
            'light clouds': 'cumulus',
            'thunderhead': 'nimbus'
        }[conditions]
    else:
        return {
            'overcast': 'stratus',
            'wind storm': 'altocumulus',
            'rain clouds': 'cumulus',
            'light clouds': 'altostratus',
            'thunderhead': 'nimbus'
        }[conditions]


# from itertools import chain

directions = {
    'E': list(range(-181, -157)) + list(range(157, 181)),
    'NE': range(-157, -112),
    'N': range(-112, -67),
    'NW': range(-67, -22),
    'W': range(-22, 22),
    'SW': range(22, 67),
    'S': range(67, 112),
    'SE': range(112, 157),
}

temp_scale = {
    'polar': range(-300, -40),
    'arctic': range(-40, -30),
    'bitterly cold': range(-30, -20),
    'very cold': range(-20, -10),
    'cold': range(-10, 0),
    'wintry': range(0, 10),
    'icy': range(10, 20),
    'frosty': range(20, 30),
    'chilly': range(30, 40),
    'brisk': range(40, 50),
    'cool': range(50, 60),
    'pleasant': range(60, 70),
    'warm': range(70, 80),
    'balmy': range(80, 90),
    'sweaty': range(90, 100),
    'sweltering': range(100, 110),
    'feverish': range(110, 120),
    'baking': range(120, 130),
    'scorching': range(130, 200)
}

beaufort = {
    0: {'range': range(1), 'desc': 'calm'},
    1: {'range': range(1, 4), 'desc': 'light air'},
    2: {'range': range(4, 8), 'desc': 'light breeze'},
    3: {'range': range(8, 13), 'desc': 'gentle breeze'},
    4: {'range': range(13, 19), 'desc': 'moderate breeze'},
    5: {'range': range(19, 25), 'desc': 'fresh breeze'},
    6: {'range': range(25, 32), 'desc': 'strong breeze'},
    7: {'range': range(32, 39), 'desc': 'near gale'},
    8: {'range': range(39, 47), 'desc': 'gale'},
    9: {'range': range(47, 55), 'desc': 'strong gale'},
    10: {'range': range(55, 65), 'desc': 'storm'},
    11: {'range': range(65, 73), 'desc': 'violent storm'},
    12: {'range': range(73, 1000), 'desc': 'hurricane'}
}


def rh(tmin, t, precipitation):
    for i in [tmin, t]:
        i = (i - 32) * 5 / 9
    # uses initial minimum temperature approximation
    td = tmin + precipitation / 25

    return round(max(0, min(100, 100 - 25 / 9 * (t - td))))


def wind_direction(loc1, loc2, dir = 1):
    """

    :param loc1:
    :param loc2:
    :return:
    """

    if dir:
        angle = math.degrees(math.atan2(
            climates[loc2]['lat'] - climates[loc1]['lat'],
            climates[loc1]['long'] - climates[loc2]['long']
        ))
    else:
        angle = math.degrees(math.atan2(
            climates[loc1]['lat'] - climates[loc2]['lat'],
            climates[loc2]['long'] - climates[loc1]['long']
        ))

    angle = int(angle)
    # print(angle)

    for k in directions:
        if angle in directions[k]:
            direction = str(k)
            break
    else:
        direction = ''

    return direction


def random_temp(a, b):
    """

    :param a:
    :param b:
    :return:
    """

    return statistics.mean([
       random.randint(a, b),
       random.randint(a, b)
    ])


spring = [2, 3, 4]
summer = [5, 6, 7]
fall = [8, 9, 10]
winter = [11, 12, 1]


def day(loc, month = 8, time = 'day'):
    """

    :param loc:
    :param month:
    :param time:
    :return:
    """

    ht = climates[loc]['elevation']

    if time == 'day':
        if ht == -1 and month in summer + winter:
            if month in winter:
                temp = random_temp(
                    climates[loc]['high'][month],
                    climates[loc]['maximum'][month]
                )
            elif month in summer:
                temp = random_temp(
                    climates[loc]['low'][month],
                    climates[loc]['mean'][month]
                )
        else:
            temp = random_temp(
                climates[loc]['low'][month],
                climates[loc]['maximum'][month]
            )
    elif time == 'night':
        if ht == -1 and month in summer + winter:
            if month in winter:
                temp = random_temp(
                    climates[loc]['mean'][month],
                    climates[loc]['high'][month]
                )
            elif month in summer:
                temp = random_temp(
                    climates[loc]['minimum'][month],
                    climates[loc]['low'][month]
                )
        else:
            temp = random_temp(
                climates[loc]['minimum'][month],
                climates[loc]['high'][month]
            )

    drift = int(temp - climates[loc]['mean'][month])
    return {
        'temp': temp,
        'drift': drift
    }


def conditions(loc1, month = 8, time = 'day'):
    """

    :param loc1:
    :param month:
    :param time:
    :return:
    """

    # connections are determined by Delauney triangulation where no side is longer than 85px
    loc2 = None
    while loc2 not in climates:
        loc2 = random.choice(climates[loc1]['connecting'])

    day1 = day(loc1, month, time)
    day2 = day(loc2, month, time)
    drift1 = day1['drift']
    drift2 = day2['drift']
    temp1 = day1['temp']
    temp2 = day2['temp']
    pressure1 = 'H' if drift1 > 0 else ('L' if drift1 < 0 else 'M')
    pressure2 = 'H' if drift2 > 0 else ('L' if drift2 < 0 else 'M')
    pair = pressure1 + pressure2 if climates[loc1]['elevation'] > climates[loc2]['elevation'] else pressure2 + pressure1
    speed = abs(temp2 - temp1) * pressure_pairs[pair]['mod']
    precipitation1 = climates[loc1]['precipitation'][month]

    if pair in ['HH', 'HM', 'ML', 'LH', 'HL', 'MM']:
        movement = climates[loc1]['elevation'] > climates[loc2]['elevation']
    elif pair in ['LL', 'MH', 'LM']:
        movement = climates[loc1]['elevation'] < climates[loc2]['elevation']

    dir = wind_direction(
        loc1,
        loc2,
        movement
    )

    for k, v in beaufort.items():
        if round(speed) in v['range']:
            scale = k
            desc = v['desc']
            break
    else:
        scale = 0
        desc = beaufort[0]['desc']

    humidity = round(rh(climates[loc1]['minimum'][month], temp1, precipitation1) / 100, 2)

    storm1 = storm(temp1, precipitation1, pressure_pairs[pair]['conditions'])

    # wind chill uses the 2007 McMillan Coefficient
    if temp1 >= 80:
        eff_temp = round(-42.4 + 2 * temp1 + 10.1 * humidity - 0.2 * temp1 * humidity - 0.007 * temp1 ** 2 -
                         0.05 * humidity ** 2 + 0.001 * temp1 ** 2 * humidity + 0.0009 * temp1 * humidity ** 2 -
                         0.00002 * temp1 ** 2 * humidity ** 2, 1)
        eff_temp = max(temp1, eff_temp)
        if storm1 not in ['sand storm', 'dust storm', 'blowing grit', 'fair']:
            eff_temp = round(eff_temp - speed / 2, 1)
    elif temp1 <= 50 and speed > 3:
        eff_temp = round(temp1 - speed, 1)
    elif speed > 3 or storm1 not in ['sand storm', 'dust storm', 'blowing grit', 'fair']:
        eff_temp = round(temp1 - speed / 2, 1)
    else:
        eff_temp = temp1

    for k, v in temp_scale.items():
        if round(eff_temp) in v:
            temp_desc = k
            break
    else:
        temp_desc = 0

    return {
        'speed': round(speed, 1),
        'pair': pair,
        'conditions': pressure_pairs[pair]['conditions'],
        'beaufort': scale,
        'description': desc,
        'feels': temp_desc,
        'direction': dir if pair not in ['LH', 'HL'] else 'unstable',
        'clouds': cloud(precipitation1, pressure_pairs[pair]['conditions']),
        'temperature': temp1,
        'storm': storm1,
        'effective temperature': eff_temp,
        'humidity': humidity
    }


def display(condition):
    """

    :param condition:
    :return:
    """

    # day =

    print(','.join([
        str(condition['temperature']),
        str(condition['effective temperature']),
        condition['feels'],
        condition['description'],
        condition['conditions'],
        condition['storm'],
        str(condition['speed']),
        condition['direction']
    ]))


def year(loc, time):
    """

    :param loc:
    :param time:
    :return:
    """

    L = zip(
        [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        list(range(1, 13))
    )

    for i, j in L:
        for k in range(1, i + 1):
            c = conditions(loc, month = j, time = time)
            try:
                display(c)
            except TypeError:
                print(c)
                break

from scipy.special import jv

def sun(lat, long, m, d, alt, a = 1.496e11, c_earth = 3.998e7, e = 1/60, epsilon = math.radians(23.4393), len_yr = 365, len_day = 24, m_sun = 1.989e30, P = math.radians(102.9372), time = None, display = False):

    _G = 6.67408e-11 # m3/kg/s2
    r_earth = c_earth / (2 * pi)
    m_earth = 5.51e3 * 4 / 3 * r_earth ** 3
    mu = _G * (m_sun + m_earth)
    if display: print('mu', mu)
    n = sum([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][:m - 1]) + d
    if display: print('n', n)
    M = math.sqrt(mu / a ** 3) * n / len_yr * (len_yr * len_day * 60 * 60)
    if display: print('M', M)

    def b(q):
        _b = 0
        for n in range(1, 20):
            _b += math.factorial(2 * n + q - 1) / (math.factorial(n) * math.factorial(n + q)) * (e / 2) ** (2 * n)
        _b *= q
        _b += 1
        _b *= (e / 2) ** q

        return _b
    # if display: print(b(1))

    nu = M
    nu += 2 * sum(
        (1 / s * math.sin(s * M) * (jv(s, s * e) + sum(
            (b(p) * (jv(s - p, s * e) + jv(s + p, s * e)) for p in range(1, 20))
        )) for s in range(1, 20))
    )
    if display: print('nu', nu)

    C = nu - M
    if display: print('C', C)

    lambda_sun = (M + C + P + pi) % (2 * pi)
    lambda_sun_0 = (P + pi) % (2 * pi)
    if display: print('lambda', lambda_sun, lambda_sun_0)

    delta = amath.sin(math.sin(lambda_sun) * math.sin(epsilon))
    if display: print('delta', delta)

    def h(alt):

        return - math.radians(1.15) * math.sqrt(alt) / 60
    if display: print('h', h(alt))

    omega = math.acos(
        (math.sin(math.radians(-0.83) + h(alt)) - math.sin(math.radians(lat)) * math.sin(delta)) /
        (math.cos(math.radians(lat)) * math.cos(delta))
    )
    if display: print('omega', omega)

    dtey = -2 * e * math.sin(M) + math.tan(epsilon / 2) ** 2 * math.sin(2 * M + 2 * lambda_sun_0)
    if display: print('dtey', dtey)

    tz = - math.radians(long) / math.radians(360 / len_day)
    if display: print('tz', tz)

    J_transit = 0.5 + -math.radians(long) / math.radians(360) + dtey + tz / 24
    if display: print('Jtransit', J_transit)

    J_set = J_transit + omega / math.radians(360)
    J_rise = J_transit - omega / math.radians(360)

    def hms(x):

        return (
            math.floor(x * 24),
            math.floor((x * 24) % math.floor(x * 24) * 60),
            math.floor((x * 24) % math.floor(x * 24) * 60 %
                  math.floor((x * 24) % math.floor(x * 24) * 60) * 60)
        )

    J_transit_time = hms(J_transit)
    J_sunset_time = hms(J_set)
    J_sunrise_time = hms(J_rise)

    if time == 'set':
        return 'Sunset:  {:}:{:}:{:}'.format(J_sunset_time[0], J_sunset_time[1], J_sunset_time[2])
    elif time == 'rise':
        return 'Sunrise: {:}:{:}:{:}'.format(J_sunrise_time[0], J_sunrise_time[1], J_sunrise_time[2])
    elif time == 'noon':
        return 'Noon:    {:}:{:}:{:}'.format(J_transit_time[0], J_transit_time[1], J_transit_time[2])
    else:
        return 'Sunrise: {:}:{:}:{:}\nNoon:    {:}:{:}:{:}\nSunset:  {:}:{:}:{:}'.format(
            J_sunrise_time[0], J_sunrise_time[1], J_sunrise_time[2], J_transit_time[0], J_transit_time[1],
            J_transit_time[2], J_sunset_time[0], J_sunset_time[1], J_sunset_time[2]
        )
