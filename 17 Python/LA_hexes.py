import math
from defs import *

hexes = {
    '1': {
        'c': (-4 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'hills',
        'altitude': 260,
        'neighbors': [2, 10, 11],
        'infrastructure': 3,
        'terrain': 'c'
    },
    '2': {
        'c': (-3 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'hills',
        'altitude': 280,
        'neighbors': [1, 3, 11, 12],
        'terrain': 'c',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'2313': '12', '1918': '12'},
        'seeds': {
            'terrain': 'c106fc34fc22fc14fc18f2c21fc34fc7fc10f2c14fc1f1c1fc11fc3fc14fc4fc2fc7fcf1c2fc4fc7f1c1f2cfcfc1f1',
            'wild': '0x140000000000000040400000000000080000000000000000',
            'rivers': '01070208030804090509061007100810090910091109120913091410141114121512161316121712181319132014211322142313x041705170617071708170916101711161217121813181418151815171617171718181918',
            'roads': '',
            'lakes': '',
            'drain_in': {'0107': 2, '0417': 2},
            'drain_out': {'2313': 2, '1918': 2},
        }
    },
    '3': {
        'c': (-2 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'hills',
        'altitude': 460,
        'neighbors': [2, 4, 12, 13],
        'infrastructure': 3,
        'terrain': 'c',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'2115': '13', '1319': '4'},
        'seeds': {
            'terrain': 'c399',
            'wild': '0x1000008000300000000000000000000000020000000000000000000000000',
            'rivers': '0912101310141114111511161217121813181319x14151514161417141814191420152115',
            'roads': '',
            'lakes': '',
            'drain_in': {},
            'drain_out': {'1319': 1, '2115': 1},
        }
    },
    '4': {
        'c': (-1 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'hills',
        'altitude': 250,
        'neighbors': [3, 5, 13, 14],
        'terrain': 'c',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'1719': '5', '2214': '14', '1300': '3'},
        'seeds': {
            'terrain': 'c399',
            'wild': '0x6000020000000000000000000000000',
            'rivers': '03040405050506060706080709070908090910101110121113111312131314141415151516161617161816191719x1300140114021502150315041605170517061807180819081909191020112012211222132214',
            'roads': '',
            'lakes': '131313141414141514161514151515161616',
            'drain_in': {'0304': 1, '1300': 1},
            'drain_out': {'1719': 2, '2214': 1},
        }
    },
    '5': {
        'c': (0 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'hills',
        'altitude': 250,
        'neighbors': [4, 6, 14, 15],
        'terrain': 'c',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'1700': '4', '2411': '15'},
        'seeds': {
            'terrain': 'c399',
            'wild': '0x200000000000010000000008800000500000',
            'rivers': '1700170116021603160417041805190520062106220723072308230923102411',
            'roads': '',
            'lakes': '220923082309231023112409241024112412',
            'drain_in': {'1700': 2},
            'drain_out': {'2411': 2},
        }
    },
    '6': {
        'c': (1 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'hills',
        'altitude': 50,
        'neighbors': [5, 7, 15, 16],
        'terrain': 'c',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'2115': '16'},
        'seeds': {
            'terrain': 'c399',
            'wild': '0x18000000000000008000000000000000000000000000000000000000000000000000000000000',
            'rivers': '0416051506150714081409141014111412151315141515141614171418141914191520162115',
            'roads': '',
            'lakes': '',
            'drain_in': {'0416': 3},
            'drain_out': {'2115': 3},
        }
    },
    '7': {
        'c': (2 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [6, 8, 16, 17],
        'terrain': 'c',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0819': '8', '2003': '16'},
        'seeds': {
            'terrain': '',
            'wild': '0x80000000008000010000000000008600040000000000000000000000',
            'rivers': '08190918091709161016101511141215121613151314141414131512161216111510141114121312121212111310141015091609160817071807190619051805180419032003',
            'roads': '',
            'lakes': '',
            'drain_in': {'0819': 2},
            'drain_out': {'2003': 2},
        }
    },
    '8': {
        'c': (3 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [7, 9, 17, 18],
        'terrain': 'm',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0800': '7', '2103': '17', '1119': '9', '2017': '18'},
        'seeds': {
            'terrain': 'm6cm2cm3cm17cm6jmcm7cm2jm6cm16cmcm13jm1cm9cm6cm18cm18cm17jmcm2cm8j1mjmcm17jc2m3cm11c1mc1m5jm5jm4cm7jm6cm15jm7cm4j4m1cm5jm3jm15jmjm1j2c1mjm5jm1jcm1jmjmjm5j2m1j1mjmjm',
            'wild': '0x80000000008000010000000000008600040000000000000000000000',
            'rivers': '0501060107000800x02140313041305120612071108110911101111101210130914091508160917081707170617051805190420042103x11191219131914191519161917191819181819172017',
            'roads': '',
            'lakes': '',
            'drain_in': {'1119': 1, '0501': 1, '0214': 1},
            'drain_out': {'0800': 1, '2103': 1, '2017': 1},
        }
    },
    '9': {
        'c': (4 * 100 * math.sqrt(3) / 2, -2 * 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [8, 18, 19],
        'terrain': 'm',
        'drainage': 5,
        'constraints': [],
        'road_io': {},
        'river_io': {'2205': '18', '1100': '8', '2215': '19'},
        'seeds': {
            'terrain': 'm32jm11jm4jm10jm1jm54jm39jmjm17jm2j1m10j2m2jm10jmj1m19j3m29jm2j1m19jm3jm12j2m8jm7j1m2jmjmjm3jmjm8jm4jmjm1jm2jm2j1m1jm1jmjm',
            'wild': '0x200008000000000000000000000000000000',
            'rivers': '0501060107000801090110011100x1804190420052105210622062205x031504150514061407130813091210121111121113101411151115121513141413141214111310140914081507150716071708180918101911181219131814191518161917181819191820182017211621152215',
            'roads': '',
            'lakes': '',
            'drain_in': {'0501': 1, '0315': 15},
            'drain_out': {'1100': 1, '2205': 1, '2215': 15},
        }
    },
    '11': {
        'c': (-3.5 * 100 * math.sqrt(3) / 2, -1 * 75),
        'topography': 'hills',
        'altitude': 250,
        'neighbors': [1, 2, 10, 12, 21, 22],
        'terrain': 'f'
    },
    '12': {
        'c': (-2.5 * 100 * math.sqrt(3) / 2, -1 * 75),
        'topography': 'hills',
        'altitude': 460,
        'neighbors': [11, 13, 2, 3, 21, 22],
        'terrain': 'c',
        'drainage': 5,
        'constraints': ['2012'],
        'road_io': {'1700': '11', '0719': '13', '2312': '22', '2215': '22'},
        'river_io': {'0404': '2', '0009': '2', '1300': '11', '2412': '22'},
        'seeds': {
            'terrain': 'c32fc10fc16fcfc1f1c33fc1fc38fc14fc4fc12fc1fc15fc1f1c14fcf3c13f3c15fcfcfcfc3fc11fc15f1c16f3cf1c3f1c6f2c13f2c5fc4fcfc14fc4f',
            'wild': '0x10000500480090000',
            'rivers': '040405030603070208030903100311031203130213011300x00090010011002110310041105100610071008100909100911091210131014101510161017101811191020112111221123112412',
            'roads': '2312221221122012x2215221421132112x071908190918101811171217131614161515161517141814191319122012x1700170117021703170417051706170718081809181019092010211021112112',
            'lakes': '',
            'drain_in': {'0404': 2, '0009': 2},
            'drain_out': {'1300': 2, '2412': 2},
        }
    },
    '13': {
        'c': (-1.5 * 100 * math.sqrt(3) / 2, -75),
        'topography': 'hills',
        'altitude': 460,
        'neighbors': [12, 14, 3, 4, 22, 23],
        'terrain': 'c',
        'drainage': 5,
        'constraints': ['0507'],
        'road_io': {'0700': '12'},
        'river_io': {'0206': '3', '1900': '22', '1219': '14'},
        'seeds': {
            'terrain': 'c399',
            'wild': '0x20001800020000000000000000000000000000000000000000000000000000000000000000000000000000000000',
            'rivers': '0206030604070408050806090610071007110712081309131014101510161017101811181219x09001000110012001300140115001600170018001900',
            'roads': '070007010702070306040605060605060507',
            'lakes': '0609061006120710071107120614071408100812081308150911091209130914091509161013101410151016101711131114111511161213',
            'drain_in': {'0206': 1},
            'drain_out': {'1900': 1, '1219': 2},
        }
    },
    '14': {
        'c': (- 0.5 * 100 * math.sqrt(3) / 2, -75),
        'topography': 'hills',
        'altitude': 250,
        'neighbors': ['13', '15', '23', '24', '4', '5'],
        'infrastructure': 5,
        'terrain': 'c',
        'drainage': 2,
        'constraints': [],
        'road_io': {'2002': '23'},
        'river_io': {'0819': '15', '1200': '13', '0304': '4'},
        'seeds': {
            'terrain': 'c400',
            'wild': '0x8000000000000000000000000000000000100010000000000140000000000000000000000000000000000',
            'rivers': '03040405050505060507060806090709081008110812091209130914091510161017101809180819x12001201130113021303130413051306130714081409141015101511151215131414141514161417131712181118101809180819',
            'roads': '2002190218031703160415041405130412051105100609050806',
            'lakes': '08190918091910171018101911161117111812171218',
            'drain_in': {'1200': 2, '0304': 1},
            'drain_out': {'0819': 2},
        }
    },
    '15': {
        'c': (0.5 * 100 * math.sqrt(3) / 2, -75),
        'topography': 'hills',
        'altitude': 180,
        'neighbors': ['5', '6', '24', '25', '14', '16'],
        'infrastructure': 9,
        'terrain': 'c',
        'drainage': 3,
        'constraints': ['0806'],
        'road_io': {'1918': '25'},
        'river_io': {'2018': '25', '0800': '14', '0501': '5'},
        'seeds': {
            'terrain': 'c400',
            'wild': '0x400006000000000000000000000000000000000000000000000000000000000000000000000000',
            'rivers': '08000801080208030804090409050906100711071208120912101211121212131313131414151515161517151816191620172018',
            'roads': '',
            'lakes': '0501x0502x0602x0603x0604x0700x0702x0703x0704x0800x0801x0802x0803x0804x0805x0900x0901x0902x0903x0904x0905x0906x0907x1000x1002x1005x1006x1007x1008x1105x1205x1206x1107x1108x1109x1208x1209x1210x1308x1309',
            'drain_in': {'0800': 2, '0501': 2},
            'drain_out': {'2018': 3},
        }
    },
    '16': {
        'c': (1.5 * 100 * math.sqrt(3) / 2, -75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [6, 7, 15, 17, 25, 26],
        'infrastructure': 9,
        'drainage': 3,
        'terrain': 'c',
        'constraints': ['0614'],
        'road_io': {'2104': '25'},
        'river_io': {'0112': '7', '2103': '25', '0206': '6'},
        'seeds': {
            'terrain': 'c60mc38mc18mc15mc20mc18mc20mc15mc1mc16m2c13mc1m2c14m1cm1c15mcmc11mc6mc12m3c1mc4mc8m1c2mc1mc1mc1m1c12m1c2mc1m1c2',
            'wild': '0x4000c8000480004000080000c00000000000000000000000000000000000000000000000000000000000000000',
            'rivers': '011202120311041205110612071208120811081007090809090910091108110711061105120513041404150315021501160117011802190220032103x02060305040605060607070608070906100611061105120513041404150315021501160117011802190220032103',
            'roads': '2104200519051806170616071507140813081209110910100910091109120813071306140513',
            'lakes': '',
            'drain_in': {'0206': 3, '0112': 2},
            'drain_out': {'2103': 3},
        }
    },
    '17': {
        'c': (2.5 * 100 * math.sqrt(3) / 2, -75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [7, 8, 16, 18, 26, 27],
        'drainage': 1,
        'terrain': 'm',
        'constraints': [],
        'road_io': {'2409': '26', '1119': '18'},
        'river_io': {'0213': '8', '2408': '26'},
        'seeds': {
            'terrain': 'c5m4c1m1cm8cmcmjc1m7cm4c1m13jmc1mcmc1mcm3jm4cm1cmcm8jm1jmc5m12jcmcm6cm7jm3c1m17cm13jc3m6jm2jm5cmcm8jm2j1m1c1m8jm2j2m1cm14jm3cm11jmj1mcm5jm5j1mj1c1m11jm1j2mc1m8j4mj1cm6j1mj1m1j1cm6j1mjm5j2m7jm4jmjm1',
            'wild': '0x400000000018000100000000000000000000000000000100000000000000000000001000',
            'rivers': '0213031204130512061307120813091209111011111012111311141215111611171018101909200920082107220723072408',
            'roads': '240923092210211020111911181217111612151214131312121211121012x11191118111711161115111411131112',
            'lakes': '',
            'drain_in': {'0213': 1},
            'drain_out': {'2408': 1},
        }
    },
    '18': {
        'c': (3.5 * 100 * math.sqrt(3) / 2, -75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [8, 9, 17, 19, 27, 28],
        'terrain': 'j',
        'drainage': 1,
        'constraints': [],
        'road_io': {'1100': '17', '0719': '19'},
        'river_io': {'0107': '8', '2003': '27', '0315': '9', '2509': '27'},
        'seeds': {
            'terrain': 'jm4j1m3j1mjmjm1jm1jmj4mj4m1j5mjm2j3mj3m1j1m1j3m1j2m1jmj1m1j2mj11m1j3mj1mj1m1j3mjmj2m2j1m1jmj2mj6mjm2jmj12m1j1mj15mjmj1mj10m6j3mj11mj2mj16mj15mj1mj4mj7mj16mj1mj2mj5mj7mjmj3m1j5mj1m3j10m3j7mj4m1j4mj7m1j4m2j1',
            'wild': '0x40000000000000000000000000000c0000000000000040000000000000000000000000000000000000000000000000000',
            'rivers': '0107020803080409050806080708080809071007110611051205130414041504160517041805190419032003x03150314041405130613071208120912101311121111121113111412151116111710181019102011211122112210230924102509',
            'roads': '1100110111021003100410050905x0719071807170716071507140713081309120911091009090908090709060905',
            'lakes': '',
            'drain_in': {'0107': 1, '0315': 1},
            'drain_out': {'2003': 1, '2509': 1},
        }
    },
    '19': {
        'c': (4.5 * 100 * math.sqrt(3) / 2, -75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [9, 18, 28, 29],
        'terrain': 'm',
        'drainage': 1,
        'constraints': ['0602'],
        'road_io': {'0700': '18'},
        'river_io': {'0305': '9', '2510': '29'},
        'seeds': {
            'terrain': 'mjm2jm6jm12jm8j1m3j1m6jm10jm17jm2jm5jm2j1m2jmjm16jm23jm15jm17jmjmjm14jm2jm18jm13jmj1m17jm133',
            'wild': '0xe00000000000000000000000000000000000000000000000000000000000000000000000000000000000',
            'rivers': '030504050504060407040804090410051006100710081009110912101310141015091508160817071706180619052005210522062207210721082009190918101811181219122013211322132312231124112510',
            'roads': '',
            'lakes': '',
            'drain_in': {'0305': 15},
            'drain_out': {'2510': 15},
        }
    },
    '21': {
        'c': (-3 * 100 * math.sqrt(3) / 2, 0 * 75),
        'topography': 'depression',
        'altitude': 250,
        'neighbors': [11, 12, 20, 22, 31, 32],
        'terrain': 'f'
    },
    '22': {
        'c': (-2 * 100 * math.sqrt(3) / 2, 0),
        'topography': 'hills',
        'altitude': 250,
        'neighbors': [21, 23, 32, 33, 12, 13],
        'infrastructure': 17,
        'terrain': 'c',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {},
        'seeds': {
            'terrain': '',
            'wild': '',
            'rivers': '',
            'roads': '',
            'lakes': '',
            'drain_in': {},
            'drain_out': {},
        }
    },
    '23': {
        'c': (- 100 * math.sqrt(3) / 2, 0),
        'topography': 'hills',
        'altitude': 380,
        'neighbors': ['14', '24', '34', '13', '33', '22'],
        'infrastructure': 9,
        'terrain': 'c',
        'drainage': 1,
        'constraints': ['0505'],
        'road_io': {'0700': '22', '0819': '24', '0111': '14'},
        'river_io': {'2103': '33', '2215': '34'},
        'seeds': {
            'terrain': 'c241nc10nc8nc10nc5ncn1c9n1cn1c1n1c7n1c1ncnc3nc2nc10nc2n1c5nc3n2cnc3n1c3n1c2n1c1n1c3n4cn4c',
            'wild': '0xc000080000000000000000000000000000000000000000000000000000000000000000000000000000000000',
            'rivers': '070508060906100711061207130614061505160517041804190320032103x07080709081009101011111112121312141215121612171218131913201421142215',
            'roads': '0700070107020703060406050505x0819081808170816081508140813081208110710070906090608060706060605',
            'lakes': '',
            'drain_out': {'2103': 1, '2215': 1}
        }
    },
    '24': {
        'c': (0, 0),
        'topography': 'hills',
        'altitude': 280,
        'neighbors': ['14', '15', '23', '25', '34', '35'],
        'infrastructure': 8,
        'terrain': 'c',
        'drainage': 1,
        'drain_out': {'35': 1},
        'road_io': {'0800': '23', '0819': '25', '2313': '35'},
        'river_io': {'2412': '35'},
        'constraints': ['0703'],
        'seeds': {
            'terrain': 'c203nc35nc1nc22nc11n1c19n1cnc4nc5ncnc3nc10n4cnc2nc1ncncnc2ncnc1nc9n2cn8c2n3cn1cn',
            'wild': '0x100006000008000000000000000000000000000000000000000000000000000000000000000000000000',
            'roads': '08000801080207020703x08190818081708160815081408130812081108100809080808070806080508040703x2313221421142014191418141714161515141414131312131112101309120812',
            'rivers': '160617061806190620072008210821092210221123112412',
            'lakes': '',
            'egress': {'35': '2313'}
        }
    },
    '25': {
        'c': (100 * math.sqrt(3) / 2, 0),
        'topography': 'plains',
        'altitude': 60,
        'neighbors': ['15', '16', '24', '26', '35', '36'],
        'terrain': 'c',
        'drainage': 4,
        'constraints': ['0812'],
        'road_io': {'0800': '24', '0819': '26', '0214': '16', '0009': '15'},
        'river_io': {'0108': '15', '0213': '16', '2313': '36'},
        'seeds': {
            'terrain': 'c79mc15mc3mc14mc2mc13mcmc1mc18mc16m1c23n1c9mc7nc10mcm1ncnc11mc12mcnc3mcmc1nc1n1c1mcnc5m1c1ncnc1nc5m3cm1nc5mnc2nm1c1m2cnc1nc8mcmc1ncncnc3mcmcnc2nc2mcnmc5nmnmcm2',
            'wild': '0x2000180008800000000fc0001800020000140000000000000000000000000000000000000000000000000000000',
            'rivers': '01080209030903100411051106110711081109111012111212131312141315121511151016101710171118121811191120112111221222132313x0213031203110411051106110711081109111012111212131312141315121511151016101710171118121811191120112111221222132313',
            'roads': '0819081808170816081508140813x0214031404140514061507150816x08000801080208030804080508060807080808090810081108120813x0009001000110111011202130214',
            'lakes': '',
            'egress': '2313',
            'drain_in': {'0108': 3, '0213': 3},
            'drain_out': {'2313': 4},
        }
    },
    '26': {
        'c': (2 * 100 * math.sqrt(3) / 2, 0),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': ['16', '17', '25', '27', '36', '37'],
        'infrastructure': 7,
        'terrain': 'm',
        'drainage': 1,
        'constraints': ['1116'],
        'road_io': {'0800': '24', '0518': '17'},
        'river_io': {'0517': '17', '2205': '36'},
        'seeds': {
            'terrain': 'cm2cmcm3cm5cm2c1m4jcmcmcmcm7cm1cm4cm6jm1cm2cm1cm11c3m8jm3c1mcm10jm3c1m7cm2j1mj2c1m1c1m1cm7j2mcmcm12jm1c2mc2m9jmjm6cm5jmjmj1m1cm1cm7jm6cm11jmj2c2m13jm1c1m14jmjm8jm1jm1jm2jm3cm10jm1jc2m10j2m10j1m1jm2jm1jm1jm2jm4j3m',
            'wild': '0x1800018000180000000000000000000000000000000000000000000000000',
            'roads': '0800080108020803080408050806080708080809081008110911091209131014101511151116x0518061807170817091610171116',
            'rivers': '051706170516041605150615071508160915091408140813081208110910101011101111121213111411141015091508160816091708180819072007200621052205',
            'lakes': '',
            'egress': '2313',
            'drain_in': {'0517': 1},
            'drain_out': {'2205': 1},
        }
    },
    '27': {
        'c': (3 * 100 * math.sqrt(3) / 2, 0),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [17, 18, 26, 28, 37, 38],
        'infrastructure': 3,
        'terrain': 'j',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0112': '18', '0619': '18', '2409': '37', '2018': '38'},
        'seeds': {
            'terrain': 'j2mj1mj3mjmj2m2j8mjmj3m1j7mj1m1j10m3j7mj3mj2m1j17m1j1mj7mj2m1jm1j17mj2mj12m1j3mjmj8mjm2jmj10mj4mj16m4j5mj3mj3mj2mj1mj10m2j15mj14mj1mj3m1jmjm1j4mj6mj12mj19mj11mj26',
            'wild': '0x8000080000200000000000000400000000000000000004000080000020000000000004000000200100',
            'rivers': '011202120312041305120511061106100710081009091010110912101310141114101509161017091810181119112012211121102109221022112311241124102409x061907190819091910191119111811171217131614171417161716181518151916191719181919182018',
            'roads': '',
            'lakes': '',
            'drain_in': {'0112': 1, '0619': 1},
            'drain_out': {'2409': 1, '2018': 1},
        }
    },
    '28': {
        'c': (4 * 100 * math.sqrt(3) / 2, 0),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [18, 19, 27, 29, 38, 39],
        'terrain': 'm',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'1219': '29', '1519': '29'},
        'seeds': {
            'terrain': 'm9jm5j1m1jm8j1m4jm5j1m17jm1j1m12jm4jmjm10j3m1jm12jmjm16jmj1mjm35jmj1mjm12j1m17jm1jm14jm19jm2jm13j1m18jm18jmj1m14jm55',
            'wild': '0x200000000100001000000000000000600000000000000000000000000000',
            'rivers': '121911181018091710171116121713171418141815181519',
            'roads': '',
            'lakes': '',
            'drain_in': {'1219': 15},
            'drain_out': {'1519': 15},
        }
    },
    '29': {
        'c': (5 * 100 * math.sqrt(3) / 2, 0),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [19, 28, 39],
        'terrain': 'm',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0601': '19', '1200': '28', '1500': '28', '2103': '39', '2206': '39', '2509': '39'},
        'seeds': {
            'terrain': 'm399',
            'wild': '0x9000000000000000000000000000000000020000000000000000000000000000000000000000000',
            'rivers': '060107000801080209020903100411041204130313021301130012011200x150015011502150315041505160617051805190420042103x2206220722082108210921102211231024102509',
            'roads': '',
            'lakes': '',
            'drain_in': {'0601': 15, '1500': 15, '2206': 15},
            'drain_out': {'1200': 15, '2103': 15, '2509': 15},
        }
    },
    '32': {
        'c': (-2.5 * 100 * math.sqrt(3) / 2, 1 * 75),
        'topography': 'depression',
        'altitude': 160,
        'neighbors': [21, 22, 31, 33, 41, 42],
        'terrain': 'f'
    },
    '33': {
        'c': (-1.5 * 100 * math.sqrt(3) / 2, 75),
        'topography': 'hills',
        'altitude': 290,
        'neighbors': [32, 34, 22, 23, 42, 43],
        'infrastructure': 7,
        'terrain': 'n'
    },
    '34': {
        'c': (- 0.5 * 100 * math.sqrt(3) / 2, 75),
        'topography': 'hills',
        'altitude': 250,
        'neighbors': [33, 35, 23, 24, 43, 44],
        'infrastructure': 5,
        'terrain': 'n'
    },
    '35': {
        'c': (0.5 * 100 * math.sqrt(3) / 2, 75),
        'topography': 'plains',
        'altitude': 250,
        'neighbors': ['34', '36', '24', '25', '44', '45'],
        'infrastructure': 6,
        'terrain': 'n',
        'drainage': 1,
        'constraints': [],
        'road_io': {'2018': '45', '0404': '24'},
        'river_io': {'0403': '24', '2017': '45'},
        'seeds': {
            'terrain': 'nc1ncn2c2nc1ncn2cn5cn2c1ncncn4cnc1n2cn1c1n4cn1mc1n12cn3cn1cn10mn2c1n15mn1mn5cncn5mn2cn13cn3mn1c1ncn9mn2cn15mnmn18mn36mn36mn20mn18mn58',
            'wild': '0x10000000000000000000000000080000000000000000000100001000000000000000',
            'rivers': '040305030504060506060706070708080809090910101011111112121312131314141415151516161716181719172017',
            'roads': '0x20000040000000000000000000000000000000000000000000000000000000100001000000000000000',
            'lakes': '',
            'drain_in': {'0403': 1},
            'drain_out': {'2017': 1},
        }
    },
    '36': {
        'c': (1.5 * 100 * math.sqrt(3) / 2, 75),
        'topography': 'plains',
        'altitude': 30,
        'neighbors': [25, 26, 35, 37, 44, 45],
        'terrain': 'm',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0404': '25', '0314': '26', '2306': '45', '2017': '46'},
        'seeds': {
            'terrain': 'm3c1m1c1m5cm2cm2c1m3cm2cmc1m2cjm2cm5cm8cncm13jmjnmnmc1m13n1m2cm9jm3nm2cm10jmjmn1mnm13jn1m2nm7jm1jm21nm5nm8jm1nm3nm7jmj1m1n1m2nm12jn3m6jm5jm1nm6nm9n2mn1mnm11nm8n1mjm3nmnmnm4nm7nm3n1m2nm1jnmnm7n1m2nm1n2mn1m',
            'wild': '0x2000000200020000000000000010002800000280800008100000000020004000000000000000c00082000000020000',
            'rivers': '040405040605070407030702060206010700080009001000110011011102120313021402150215031504160417031803190320042104220522062306x0314031304140514051305120412051106110710081009091010110912101310141014091509151014111511161116121613161416151715181619162017',
            'roads': '',
            'lakes': '',
            'drain_in': {'0404': 4, '0314': 1},
            'drain_out': {'2306': 4, '2017': 1},
        }
    },
    '37': {
        'c': (2.5 * 100 * math.sqrt(3) / 2, 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [26, 27, 36, 38, 45, 46],
        'terrain': 'j',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0518': '27', '2214': '47'},
        'seeds': {
            'terrain': 'm4j7mj2mj9mj1m1j11mj3mj6m1j1mjmjmj1mj8mj3m1jm1j10m5j1mj6mj4mjmj1mj11m3jnmjmj10mj1nmnj19nj10mj2mj3mj6m1j4mjnj17mjn1j2nj3mj6nj2mj9mj2mn1jmj9mjmj2mjnj12m2jm1j1nj8m1j1mjm1n3mjmjmjmjmjm3nj8mj3mj2mjmj2mj1m1j1m1j1m',
            'wild': '0x4000092000000000000028000100000000000000000000000000000200000001000800000000',
            'rivers': '051805170617071708180917091609151016101710181118121813181418141714161415151416151715181619151914201421132214',
            'roads': '',
            'lakes': '',
            'drain_in': {'0518': 1},
            'drain_out': {'2214': 1},
        }
    },
    '38': {
        'c': (3.5 * 100 * math.sqrt(3) / 2, 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [27, 28, 37, 39, 46, 47],
        'terrain': 'm',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0108': '27', '2205': '47'},
        'seeds': {
            'terrain': 'm1jm1jmjm9jmjm9j1m5jm5j2m2jm9j1m3jm12jm7jm9jmjm18jm16jm6jm12j2m17jm17j1m15jm1jm18jm17jm16j2m113',
            'wild': '0x400008000000001000000000000000000000000000000000000000000000000000000000',
            'rivers': '01080109020902080207030604060506060706080508040904100510061007090708070707060606060505040503060307030803090309040905080608070808090810081107110611051104120413031403140415041405130512061207120812091210131014101509150815071506150516051704180519052006210622062205',
            'roads': '',
            'lakes': '',
            'drain_in': {'0108': 1},
            'drain_out': {'2205': 1},
        }
    },
    '39': {
        'c': (4.5 * 100 * math.sqrt(3) / 2, 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [28, 29, 38, 48],
        'terrain': 'm',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0213': '29', '2509': '48', '0315': '29', '0619': '29'},
        'seeds': {
            'terrain': 'm399',
            'wild': '0x2800000000000000000000000000000000000000000',
            'rivers': '0213031303140315x06190718081809170916091510151114121513141415151416141713171216121511141113101410150915081507150616061605160417031803190220032103210421052006200720081908180817081609161017101711181219122013211222122311241124102509',
            'roads': '',
            'lakes': '',
            'drain_in': {'0213': 15, '0619': 15},
            'drain_out': {'2509': 15, '0315': 15},
        }
    },
    '42': {
        'c': (-1 * 100 * math.sqrt(3) / 2, 2 * 75),
        'topography': 'depression',
        'altitude': 130,
        'neighbors': [41, 43, 32, 33, 50, 51],
        'terrain': 'f'
    },
    '43': {
        'c': (-2 * 100 * math.sqrt(3) / 2, 2 * 75),
        'topography': 'hills',
        'altitude': 130,
        'neighbors': [42, 44, 33, 34, 51, 52],
        'terrain': 'n'
    },
    '44': {
        'c': (0 * 100 * math.sqrt(3) / 2, 2 * 75),
        'topography': 'hills',
        'altitude': 150,
        'neighbors': [43, 45, 34, 35, 52, 53],
        'infrastructure': 4,
        'terrain': 'n'
    },
    '45': {
        'c': (1 * 100 * math.sqrt(3) / 2, 2 * 75),
        'topography': 'hills',
        'altitude': 100,
        'neighbors': [35, 36, 44, 46, 53, 54],
        'terrain': 'n',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {},
        'seeds': {
            'terrain': '',
            'wild': '',
            'rivers': '',
            'roads': '',
            'lakes': '',
            'drain_in': {},
            'drain_out': {},
        }
    },
    '46': {
        'c': (2 * 100 * math.sqrt(3) / 2, 2 * 75),
        'topography': 'plains',
        'altitude': 30,
        'neighbors': [36, 37, 45, 47, 54, 55],
        'terrain': 'm',
        'drainage': 1,
        'constraints': [],
        'road_io': {},
        'river_io': {'0107': '36', '0900': '45', '2412': '55'},
        'seeds': {
            'terrain': 'mjm4j1m11jm4j2m5j2m4jm14jm1nm2nmnm9jmjn1m15jm20nm1nm2nm8jm2nmnm16nm3n1m9jmnm18nm1nmnm13n1m17n1m17n1m3nm12n1m17n2mn1m13n2m3nm2nm8n1mnm11n1m1nmnm7nm8n1mnmn1m1nm4',
            'wild': '0x400000000600000800000000020000000000000002000000000000000000008000000020000000',
            'rivers': '01070208030704070406040505040605070508060906100611051206130613071408140915091610171017111812191220132112221323122412x09001001110112021302140315031604170418051806170617071708180919091910201120122112221323122412',
            'roads': '',
            'lakes': '',
            'drain_in': {'0107': 1, '0900': 4},
            'drain_out': {'2412': 4},
        }
    },
    '47': {
        'c': (3 * 100 * math.sqrt(3) / 2, 2 * 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [37, 38, 46, 48, 55, 56],
        'terrain': 'm'
    },
    '48': {
        'c': (4 * 100 * math.sqrt(3) / 2, 2 * 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [38, 39, 47, 57],
        'terrain': 'm'
    },
    '52': {
        'c': (-0.5 * 100 * math.sqrt(3) / 2, 3 * 75),
        'topography': 'hills',
        'altitude': 190,
        'neighbors': [42, 43, 51, 53, 60, 61],
        'terrain': 'n'
    },
    '53': {
        'c': (0.5 * 100 * math.sqrt(3) / 2, 3 * 75),
        'topography': 'hills',
        'altitude': 40,
        'neighbors': [52, 54, 43, 44, 61, 62],
        'terrain': 'n'
    },
    '54': {
        'c': (1.5 * 100 * math.sqrt(3) / 2, 3 * 75),
        'topography': 'hills',
        'altitude': 250,
        'neighbors': [44, 45, 53, 55, 62, 63],
        'terrain': 'n'
    },
    '55': {
        'c': (2.5 * 100 * math.sqrt(3) / 2, 3 * 75),
        'topography': 'plains',
        'altitude': 1,
        'neighbors': [45, 46, 54, 56, 63, 64],
        'terrain': 'm'
    },
    '62': {
        'c': (1 * 100 * math.sqrt(3) / 2, 4 * 75),
        'topography': 'depression',
        'altitude': 250,
        'neighbors': [61, 63, 54, 55, 69, 70],
        'terrain': 'n'
    },
}

for h in hexes:
    hexes[h].update({'neighbors': [str(i) for i in hexes[h]['neighbors']]})

cities = {
    'Ruston': {'hex': '24', 'p0': 21859, 'region': 'Hills'},
    'Monroe': {'hex': '25', 'p0': 48815, 'region': 'Delta'},
    'Rayville': {'hex': '26', 'p0': 3695, 'region': 'Delta'},
    'Arcadia': {'hex': '23', 'p0': 2919, 'region': 'Hills'},
    'Farmerville': {'hex': '15', 'p0': 3860, 'region': 'Hills'},
    'Bastrop': {'hex': '16', 'p0': 3860, 'region': 'Delta'},
    'Lake Providence': {'hex': '19', 'p0': 3860, 'region': 'Delta'},
    'Minden': {'hex': '12', 'p0': 13082, 'region': 'Hills'},
    'Homer': {'hex': '13', 'p0': 3237, 'region': 'Hills'},
    'Coushatta': {'hex': '42', 'p0': 1964, 'region': 'Hills'},
    'Winnfield': {'hex': '44', 'p0': 4840, 'region': 'Hills'},
    'Columbia': {'hex': '45', 'p0': 390, 'region': 'Hills'},
    'Harrisonburg': {'hex': '54', 'p0': 348, 'region': 'Hills'},
}

regions = {
    'Hills': {str(i) for i in {2, 3, 4, 5, 6, 12, 13, 14, 15, 21, 22, 23, 24, 32, 33, 34, 35, 42, 43, 44, 45, 51, 52, 53, 61}},
    'Delta': {str(i) for i in {7, 8, 9, 16, 17, 18, 19, 25, 26, 27, 28, 29, 36, 37, 38, 39, 46}},
}

# infrastructure rules

urbanization = 0.4

for city in cities:
    cities[city]['pop'] = 350000 * math.sin(cities[city]['p0'] / 1207930) / urbanization
    cities[city]['pop'] = {'rural': int(cities[city]['pop'] * (1 - urbanization)), 'urban': int(cities[city]['pop'] * urbanization)}

# for h in hexes:
#     hexes[h]['infrastructure'] = list()
#     for city in cities:
#         if cities[city]['hex'] != h:
#             continue
#         else:
#             hexes[h]['infrastructure'].append(int(cities[city]['pop']['urban'] / 346))

# infra = {cities[c]['hex']: {c: int(cities[c]['pop']['urban'] / 346)} for c in cities}
infra = dict()
for c in cities:
    infra.setdefault(cities[c]['hex'], dict())
    infra[cities[c]['hex']].update({c: int(cities[c]['pop']['urban'] / 346)})

for t in cities:
    current = set(n for n in hexes[cities[t]['hex']]['neighbors'] if n in hexes and n in regions[cities[t]['region']])
    visited = set([cities[t]['hex']])
    i = True
    while i:
        i = False
        for c in list(current):
            if c in visited:
                continue
            infra.setdefault(c, dict())
            try:
                source = max((
                    n for n in hexes[c]['neighbors'] if
                    n in hexes and
                    n in infra and
                    t in infra[n] and
                    n in regions[cities[t]['region']]
                ), key=lambda x: infra.get(x, dict()).get(t, 0))
            except ValueError:
                continue
            if not source:
                current.remove(c)
                visited.add(c)
            r = clip(2 + clip((hexes[c]['altitude'] - hexes[source]['altitude']) / 400, 0, None), 2, None)
            infra[c].update({t: math.floor(infra[source][t] / r)})
            if infra[c][t] > 0:
                i = True
            current |= set(
                n for n in hexes[c]['neighbors'] if n in hexes and n in regions[cities[t]['region']])
            # print(current)
            current.remove(c)
            visited.add(c)

for h in infra:
    hexes[h]['infrastructure'] = sum(infra[h].values())